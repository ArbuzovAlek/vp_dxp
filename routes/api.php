<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('list','ListController@get_list');
Route::get('get_diagnosis_for_patient','SigmaController@get_diagnosis_for_current_patient');
Route::get('update_diagnosis_list', 'ListController@update_diagnosis_list');