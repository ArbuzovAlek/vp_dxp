<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\AuthChecker;
use App\Http\Middleware\isSelectedDiagnosisOption;
use App\Http\Middleware\isSelectedPatientCenter;
use App\Http\Middleware\LimitCriteria;

Route::get('/', function(){
        return view('home.index');
    })->name("home");
    Route::get('get_patients', 'SelectPatientController@get_patients');
    Route::view('/error_patients', 'layouts.error_patient');
    Route::view('/error_criteria', 'layouts.error_criteria');
    Route::view('/center/error_patients', 'center.error_patient');
    Route::post('/select_patient/{id}','SelectPatientController@select_patient');
    Route::get('/select_patient/{id}', 'SelectPatientController@select_patient');

    Route::group(['middleware' =>['isSelectPat', LimitCriteria::class]], function(){
        Route::view('list', 'list.index');

        Route::get('diagnosis/{diagnosis}/specifiers/{specify}/result', 'DiagnosisResultController@index');
        Route::post('diagnosis/{diagnosis}/specifiers/{specify}/result', 'DiagnosisResultController@store');

        Route::get('diagnosis/{diagnosis}/criteria/result', 'DiagnosisResultController@index');
        Route::post('diagnosis/{diagnosis}/criteria/result', 'DiagnosisResultController@store');

        Route::get('diagnosis/{diagnosis}/criteria/is_criteria_have_been_met', 'CriteriaController@is_criteria_have_been_met');

        Route::resources([
            'diagnosis' => 'DiagnosisController',
            'diagnosis.criteria' => 'CriteriaController',
            'diagnosisSteps' => 'DiagnosisStepsController',
            'sigma' => 'SigmaController',
//            'history' => 'HistoryController',
            'diagnosis.specifiers' => 'SpecifiersController',
            'users' => 'UsersController',
            // 'patient_diagnosis' => 'PatientDiagnosisController',
        ]);

        Route::get('/checker', 'CheckerController@index');
        Route::get('/checker/search', 'CheckerController@searchSymptom');
        Route::get('/checker/results', 'CheckerController@results');
        Route::post('/checker/add_history', 'CheckerController@addToHistory');
    });

    Route::group(['middleware' => AuthChecker::class], function(){
        Route::get('/checker/searchDiagnosis', 'CheckerController@searchDiagnosis');
        Route::get('/checker/searchDiagnosisWithWeight', 'CheckerController@searchDiagnosisWithWight');
        Route::get('/checker/symptom', 'CheckerController@symptom');
        Route::post('/checker/symptom', 'CheckerController@attachToDiagnosis');
        Route::get('/checker/diagnosis', 'CheckerController@diagnosis');
        Route::post('/checker/diagnosis', 'CheckerController@updateWeight');
    });
    Route::group(['middleware' =>['isSelectPatCenter']], function(){
        Route::get('center/results', 'SigmaController@results_page');
        Route::get('center/history', 'SigmaController@history_page');
        Route::get('history/formals-paginate', 'SigmaController@render_formals');
        Route::get('history/history-paginate', 'SigmaController@render_history');
        Route::get('/center/diagnosis/{reload_table?}/{selected_diagnos?}/{free_text_id_step?}','SigmaController@center_diagnosis')->name('center_diagnosis');
        Route::get('/center/set_ordering','SigmaController@set_ordering');
        Route::post('/center/del_diagnose','SigmaController@del_diagnose');
        Route::post('/center/diagnosis/add','SigmaController@add_diagnose_free_text');
        Route::post('/center/diagnosis/edit','SigmaController@edit_diagnose_free_text');
        Route::view('/center','center/start/start');
    });

    Route::group(['middleware' => [isSelectedDiagnosisOption::class, isSelectedPatientCenter::class]], function() {
        Route::get('/center/analytics', 'AnalyticsController@index');
        Route::get('/center/analytics/summary', 'AnalyticsController@summary');
        Route::get('/center/analytics/recommendations', 'AnalyticsController@recommendations');
        Route::get('/center/analytics/diagnosis_history', 'AnalyticsController@diagnosis_history');
    });

    Route::get('/center/settings', 'SettingsController@index');
    Route::get('/center/settings/export_demographic_diagnoses', 'SettingsController@export_demographic_diagnoses');
    Route::get('/center/settings/export_severity_scale', 'SettingsController@export_severity_scale');
    Route::post('/center/settings/update_settings', 'SettingsController@update_settings');
    Route::get('/center/settings/restore_defaults', 'SettingsController@restore_defaults');
    Route::get('/center/settings/print', 'SettingsController@print');
    Route::get('/center/groups_manager', 'SettingsController@groups_manager');
    Route::get('/center/records_cabinet', 'SettingsController@records_cabinet');

    Route::post('select_diagnosis_result/{patient_id}/{diagnosis_id}', 'DiagnosisResultController@select_result');
    Route::get('diagnosis_view', 'DiagnosisController@change_view');
	Route::post('diagnosisSteps/store', 'DiagnosisStepsController@store');
	Route::post('specifiers/store', 'SpecifiersController@store');
	Route::get('specifiers/least_one_chosen', 'SpecifiersController@least_one_chosen');
	Route::get('patient_diagnosis/create_patient_session', 'PatientDiagnosisController@create_patient_session');
    Route::get('to_analytics', 'AnalyticsController@to_analytics');
