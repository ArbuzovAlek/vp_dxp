<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteToDiagnosisStepAndDiagnosisVariables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diagnosis_steps', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('diagnosis_options', function (Blueprint $table) {
            $table->softDeletes();
        });

        Schema::table('patient_diagnosis_variables', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diagnosis_steps', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('diagnosis_options', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('patient_diagnosis_variables', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
