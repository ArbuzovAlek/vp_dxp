<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\PatientVp;
use App\UserVp;

class RearrangePatientIdsByUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach(UserVp::cursor() as $user) {
            $patients = PatientVp::where('user_id', $user->id)->orderBy('id', 'asc');
            $patients->each(function ($item, $key) {
                $item->user_own_id = $key + 1;
                $item->save();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
