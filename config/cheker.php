<?php

return [
    'username' => env('CHECKER_USERNAME'),
    'password' => env('CHECKER_PASSWORD'),
];