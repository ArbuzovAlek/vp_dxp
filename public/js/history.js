let queryData = {};

(function ($) {
    var preAndNextSelector = '.art-article-controls-pagination-page-prev, .art-article-controls-pagination-page-next';
    $(document).ready(function() {
        history_table = $('#history_table');
        queryData.diagnosis_id = history_table.data('diagnosis_id');
        queryData.patient_diagnosis_id = history_table.data('patient_diagnosis_id');
    });

    $(document).on('click', preAndNextSelector, function () {
        var pagination = $(this).closest('.art-article-controls-pagination');
        var wrapper = $(pagination.data('wrapper'));
        var url = wrapper.data('url');
        var rowNumber = wrapper.find('.art-article-controls-pagination-pages-go-to-input').val();
        queryData.page = $(this).data('page');

        $.get(url, queryData).then((response) => {
            wrapper.html(response);
            hasSelectedDiagnosis = false;
            updateActionButtonsStatuses();
        });
    });

    $(document).on('submit', '.art-article-controls-pagination-pages-go-to', function (e) {
        e.preventDefault();

        var pagination = $(this).closest('.art-article-controls-pagination');
        var wrapper = $(pagination.data('wrapper'));
        var url = wrapper.data('url');
        queryData.rowNumber = $(this).find('.art-article-controls-pagination-pages-go-to-input').val();

        $.get(url, queryData).then((response) => {
            wrapper.html(response);
            hasSelectedDiagnosis = false;
            updateActionButtonsStatuses();
        });
    });

    $(document).on('click', '.col-sort', function () {
        var table = $(this).closest('table');
        var wrapper = $(table.data('wrapper'));
        var url = wrapper.data('url');

        queryData['order-by'] = $(this).data('col');
        queryData['order-direction'] = $(this).hasClass('col-sort-desc') ? 'asc' : 'desc';

        $.get(url, queryData).then((response) => {
            wrapper.html(response);
            hasSelectedDiagnosis = false;
            updateActionButtonsStatuses();
        });
    });

    $(document).on('click', '.formals-wrapper tr:not(:first-child)', function (e) {
        $(this).siblings().removeClass('selected');

        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            hasSelectedDiagnosis = false;
        } else {
            $(this).addClass('selected');
            hasSelectedDiagnosis = true;
        }

        updateActionButtonsStatuses();
    });

    $(document).on('click', '#results_table tr:not(.selected,:first-child)', function() {
        const isCreated = Boolean($(this).data('is_created'));

        /*if (isCreated) {
            $('.art-article-controls-actions-button-analytics').addClass('disabled');
        } else {*/
            const patient_id = $(this).data('patient_id');
            const diagnosis_id = $(this).data('diagnosis_id');
            const option_id = $(this).data('option_id');

        selectDiagnosisForAnalytic(patient_id, diagnosis_id, option_id);
        // }
    });

    $(document).on('click', '.results-diagnosis-edit', function () {
        var row = getSelectedRow();
        var diagnosis_id = row.data('diagnosis_id');
        var formalData = row.data('formal') || {};
        var text = formalData[0] ? formalData[0].text : formalData.text || '';

        editDiagnoseSelectorModal(formalData.diagnosis_id, {
            code: formalData[0] ? formalData[0].code : formalData.code,
            text: text.replace(/\s+/g, ' '),
            diagnosis_id: formalData.diagnose_id,
            assigned: formalData.assigned,
            option: formalData.option,
        });
    });

    $(document).on('click', '.results-diagnosis-add', function () {
        addDiagnoseSelector();
    });

    $(document).on('click', '.results-diagnosis-delete', function () {
        var diagnosis_id = getSelectedRow().data('diagnosis_id');
        delConfirmationModal(diagnosis_id);
    });

    function getSelectedRow() {
        return $('.formals-wrapper tr.selected')
    }

    window.refreshTable = function () {
        var wrapper = $('.formals-wrapper');
        wrapper.load(wrapper.data('url'));
    }

    window.saveEdit = function () {
        var selectedRow = getSelectedRow();
        var diagnosis_id = selectedRow.data('diagnosis_id');
        var free_text_id_step = selectedRow.data('free_text_id_step');

        saveEditDiagnosis(diagnosis_id, free_text_id_step, refreshTable);
    };

    window.confirmDelete = function () {
        var selectedRow = getSelectedRow();
        var diagnosis_id = selectedRow.data('diagnosis_id');
        var free_text_id_step = selectedRow.data('free_text_id_step');

        deleteDiagnose(diagnosis_id, free_text_id_step, refreshTable);
    };

    window.programEdit = function () {
        var diagnosis_id = getSelectedRow().data('diagnosis_id');
        editWithProgram(diagnosis_id);
    };

    $(document).on('click', '.results-help', function () {
        popup = $("#results-help-modal").find(".info-note-popup");
        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open");
    });

    $(document).on('click', '.history-help', function () {
        popup = $("#history-help-modal").find(".info-note-popup");
        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open");
    });

    $(function () {
        var activeRow = $('#results_table tr.selected');

        if (activeRow.length > 0) {
            hasSelectedDiagnosis = activeRow.length > 0;
            selectDiagnosisForAnalytic(
                activeRow.data('patient_id'),
                activeRow.data('diagnosis_id'),
                activeRow.data('option_id')
            );
        }

        updateActionButtonsStatuses();
    });

})(jQuery);
