$(function () {

    (function () {

        const input = $('#search-symptom-input');
        const list = $('#checked-symptom');
        const selectedItems = {};

        // $(input).autocomplete({
        //     serviceUrl: '/checker/search',
        //     onSelect: (suggestion) => {
        //         selectedItems[suggestion.data.name] = suggestion.data;
        //         input.val('');
        //         updateViewList();
        //     }
        // });

        function updateViewList() {
            list.empty();

            Object.values(selectedItems).forEach(item => {
                list.append(`
                    <li>
                        ${item}
                        <span class="checked-symptom-del" data-name="${item}" style="cursor: pointer">(x)</span> 
                    </li>
                `);
            });
        }

        list.on('click', '.checked-symptom-del',  function () {
            delete selectedItems[$(this).data('name')];
            updateViewList();
        });

        $('#add_symptom').click(function() {
            const value = $('#search-symptom-input').val();

            if (!value.trim()) {
                return;
            }

            selectedItems[value] = value;
            $('#search-symptom-input').val('');
            updateViewList();
        });

        $('#checker-finish').on('click', () => {

            // const symptom_ids = Object.values(selectedItems).reduce((prev, item) => {
            //     if (prev.indexOf(item.symptom.id) === -1) {
            //         prev.push(item.symptom.id);
            //     }
            //     return prev;
            // }, []);

            // if (!symptom_ids.length) {
            //     showMessage('Symptom is required', 'Please choose symptom');
            //     return;
            // }

            //document.location = '/checker/results?' + $.param({ selectedItems });
            //$('#checked_symptoms').submit();


            if(!Object.keys(selectedItems).length) {
                showMessage('Symptom is required', 'Please add at least one symptom.');
                return;
            }

            var symptoms = Object.values(selectedItems).join('_');

            $.ajax({
                type: "POST",
                url: '/checker/add_history',
                data: {
                    symptoms: symptoms
                },
                error: function (data) {
                    console.log('Error:', data);
                },
                complete: function () {
                    document.location = '/checker/results?' + $.param({ symptoms: symptoms });
                },
            });

        });
    })();

    (function () {
        const input = $('#new-symptom-diagnosis');
        const inputSymptomName = $('#new-symptom-name');
        const list = $('#checker-new-diagnosis');
        const selected = {};

        $(input).autocomplete({
            serviceUrl: '/checker/searchDiagnosis',
            onSelect: (suggestion) => {
                selected[suggestion.data.id] = suggestion.data;
                input.val('');
                updateView();
            }
        });

        $('#checker-add-new-diagnosis').on('click', () => {

            const name = inputSymptomName.val();
            const diagnosisIds = Object.keys(selected);

            if (!diagnosisIds.length) {
                showMessage('Diagnosis is required', 'Please provide diagnosis');
                return;
            }

            if (!name.trim()) {
                showMessage('Symptom name is required', 'Please provide symptom name');
                return;
            }

            $.ajax({
                type: "POST",
                url: '/checker/symptom',
                data: {
                    name: name,
                    diagnosis_ids: diagnosisIds
                },
                success: function (data, textStatus, request) {
                    showMessage('Thank you!', 'Symptom is added');
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });

        function updateView() {
            list.empty();

            Object.values(selected).forEach(symptom => {
                list.append(`
                    <li>
                        ${symptom.name} 
                        <span class="checked-new-diagnosis-del" data-id="${symptom.id}" style="cursor: pointer">(x)</span> 
                    </li>
                `);
            });
        }

        list.on('click', '.checked-new-diagnosis-del',  function () {
            delete selected[$(this).data('id')];
            updateView();
        });
    })();

    (function () {
        const input = $('#edit-diagnosis');
        const weightData = [];
        let selectedDiagnosisData = null;

        $(input).autocomplete({
            serviceUrl: '/checker/searchDiagnosisWithWeight',
            onSelect: (suggestion) => {
                selectedDiagnosisData = suggestion.data;
                onSelected();
            }
        });

        function onSelected() {
            const list = $('#checker-diagnosis-weight-list');

            list.empty();

            Object.values(selectedDiagnosisData.weight).forEach(weight => {
                list.append(`
                    <div class="checker-diagnosis-weight-list-item">
                        ${weight.symptom.name}
                        <div 
                            class="checker-diagnosis-weight-list-item-slider" 
                            data-value="${weight.weight}" 
                            data-symptom-id="${weight.symptom_id}" 
                            data-diagnosis-id="${weight.diagnosis_id}"
                        ></div>
                    </div>
                `);
            });

            $(".checker-diagnosis-weight-list-item-slider").each(function() {
                $(this).ionRangeSlider({
                    min: 0,
                    max: 100,
                    step: 1,
                    from: $(this).data('value'),
                    onStart: (data) => {
                        const input = data.input;
                        const weight = {
                            diagnosis_id: input.data('diagnosis-id'),
                            symptom_id: input.data('symptom-id'),
                            weight: input.data('value')
                        };

                        weightData.push(weight);
                    },
                    onFinish: (data) => {
                        const input = data.input;
                        let weight = weightData.find(weight => (
                            weight.diagnosis_id === input.data('diagnosis-id') && weight.symptom_id === input.data('symptom-id')
                        ));

                        weight.weight = data.from;
                    }
                })
            });

            $(document).on('click', '#checker-update-diagnosis', function () {
                const total = weightData.reduce((total, weight) => total + weight.weight, 0);

                if (total !== 100) {
                    showMessage('Total weight must be 100', 'Total weight must be 100 and now is ' + total);
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: '/checker/diagnosis',
                    data: {
                        diagnosis_id: selectedDiagnosisData.id,
                        weight: weightData
                    },
                    success: function (data, textStatus, request) {
                        showMessage('Thank you!', 'Diagnosis is updated');
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            })
        }
    })();

    function showMessage(title, text) {
        popup = $(".error-modal-content").find(".modal-content");

        popup.find('.modal-header .modal-header-text').text(title);
        popup.find('.modal-body p').text(text);

        $("#myModal").html(popup.clone());
        $("#myModal").show();
    }
});