let patients_table = {};
let records_table = {};

const ASC = 'ASC';
const DESC = 'DESC';

const sortClasses = {
    ASC: 'sort-up',
    DESC: 'sort-down'
}

const defaultState = {
    p_page : 1,
    r_page : 1,
    p_sorting_field : 'edited_at',
    p_sorting_order : DESC,
    r_sorting_field : 'edited_at',
    r_sorting_order : DESC
}

const tableRowsAmount = 5;

$(document).ready(function () {

    $('.sortable').click(function () {
        let table = $(this).closest('table').data('table');
        let prefix = $(this).closest('table').data('prefix');
        let state = history.state;
        if ($(this).data('field') === state[`${prefix}_sorting_field`]) {
            state[`${prefix}_sorting_order`] = $(this).hasClass('sort-up') ? DESC : ASC;
        } else {
            state[`${prefix}_sorting_order`] = DESC;
            state[`${prefix}_sorting_field`] = $(this).data('field');
        }
        $(`#${table}_table .sortable`).removeClass('sort-up').removeClass('sort-down');
        updateState(state);
        reload(table);
    });


    $('#record_add_button').click(function() {
        let uri = $('#select_record_type input[type=radio]:checked').val();
        if(uri) {
            document.location = uri;
        }
    });


    $('[data-target=select_record_type]').click(function() {
        $('#select_record_type input').each(function() {
            $(this).prop('checked', false);
        });
        if($('#records_table tbody tr[data-type=DiagnosisHistory]').length) {
            $('#select_record_type input#dxp').prop('disabled', 'disabled').next('label').addClass('grey');
        } else {
            $('#select_record_type input#dxp').removeAttr('disabled').next('label').removeClass('grey');
        }
    });


    $(document).on('click', '.action_button img', function() {
        $(this).parent().children('ul').show();
        $('#action-menu-freezer').show();
    });


    $(document).on('click', '#action-menu-freezer, .action_button li', function() {
        $('.action_button ul').hide();
        $('#action-menu-freezer').hide();
    });


    $(document).on('click', '.delete-patient', function () {
        if (confirm('Are you sure that you want to delete this patient? Operation is irreversible')) {
            let patient_id = $(this).closest('tr').data('id');

            $.ajax({
                type: 'DELETE',
                url: 'records/api/patients?id=' + patient_id,
                success: function () {
                    loadPatients();
                }
            });
        }
    });


    $(document).on('click', '.delete-record', function () {
        if (confirm('Are you sure that you want to delete this record? Operation is irreversible')) {
            let record_id = $(this).closest('tr').data('id');
            let type = $(this).closest('tr').data('type');
            $.ajax({
                type: 'DELETE',
                url: `records/api/records?id=${record_id}&type=${type}`,
                success: function () {
                    loadRecords();
                }
            });
        }
    });


    $(document).on('click', '#patients_table tbody tr:not(.empty)', function (e) {
        /* preventing clicking on action button and inner elements */
        if($('.action_button').has(e.target).length){
            return;
        }
        let patient_id = $(this).data('id');
        let current = this;
        url = $(this).hasClass('active') ? 'records/unset_patient' : 'records/select_patient/'+patient_id;
        $.post(url, function (result) {
            loadPatients();
            loadRecords();
        });
    });


    $(document).on('click', '#records_table tbody tr', function(e) {
        if(!$('.action_button').has(e.target).length){
            $(this).toggleClass('active').siblings().removeClass('active');
        }
    });


    $(document).on('click', '.init-patient-popup', function() {
        let patient_id = $(this).data('patient_id');
        let patient_popup = $('#patient_popup');
        patient_popup.find('.preloader').show();
        patient_popup.popup('show');
        patient_popup.find('.form-wrapper').hide();
        $.post('records/api/patients/patient_form'+(patient_id ? `/${patient_id}` : ''), function(result) {
            if(result) {
                patient_popup.find('.main-header').html(patient_id ? 'Edit patient' : 'Add patient')
                patient_popup.find('.form-wrapper').html(result).show();
            } else {
                patient_popup.popup('hide');
                loadPatients();
            }
            patient_popup.find('.preloader').hide();
        });
        patient_popup.find('form-wrapper').empty();
        patient_popup.find('preloader').show();
    });

    
    $(document).on('submit', '#patient_popup form', function() {
        let patient_id = $(this).data('patient_id');
        let popup = $('#patient_popup');
        $.post('records/api/patients/patient_form'+(patient_id ? `/${patient_id}` : ''), $(this).serialize(), function(result) {
            if(result === 'ok') {
                loadPatients();
                popup.popup('hide');
            } else {
                popup.find('.form-wrapper').html(result);
            }
        });
        return false;
    });


    $(document).on('click', '.datepicker', function() {
        if(!$(this).datepicker('widget').is(':visible')) {
            $(this).datepicker('show');
        }
    });


    $(document).on('click', '.ui-datepicker-close', function() {
        let month = $('.ui-datepicker-month').val();
        let year = $('.ui-datepicker-year').val();
        $('.datepicker').datepicker('setDate', new Date(year, month, 1)).datepicker('hide');
        $('#datepicker-freezer').hide();
    });


    $('#datepicker-freezer').click(function() {
        $('.datepicker').datepicker('hide');
        $(this).hide();
    });


    $('.navigation .page-arrow').click(function () {
        let table = $(this).closest('.navigation').data('table');
        let prefix = $(this).closest('.navigation').data('prefix');
        let state = history.state;
        state[`${prefix}_page`] = parseInt(state[`${prefix}_page`]) + parseInt($(this).data('inc'));
        updateState(state);
        reload(table);
    });


    patients_table = $('#patients_table');
    records_table = $('#records_table');

    let historyState = defaultState;
    let searchParams = new URL(document.location).searchParams;
    searchParams.forEach(function(value, key) {
        historyState[key] = value;
    });

    updateState(historyState);

    window.onpopstate = function (e) {
        updateNavigation(e.state);

        loadPatients();
        loadRecords();
    }

    loadPatients();
    loadRecords();
});

function loadPatients() {
    let patientsTbody = patients_table.find('tbody');
    $('.patients-panel .preloader').show();
    patientsTbody.hide();

    $.getJSON('records/api/patients', {
        page_num: history.state.p_page,
        sorting: {
            field: history.state.p_sorting_field,
            order: history.state.p_sorting_order
        },
    }, function (result) {
        $(patientsTbody).empty();
        if (result.total_rows) {
            patientsTbody.append(result.rows);
        } else {
            patientsTbody.append('<tr><td colspan="100%">No patients in database. Select <a href="records/patient">Add</a> to create new patient.</td></tr>');
        }
        let rowsAmountToAppend = tableRowsAmount - patientsTbody.children('tr').length;
        for(let i = 0; i < rowsAmountToAppend; i++) {
            patientsTbody.append('<tr class="empty"><td colspan="100%">&nbsp</td></tr>')
        }
        let navigation = $('.patients-panel .navigation');
        navigation.find('.from-row').html(result.limit_from + 1);
        navigation.find('.to-row').html(result.limit_from + result.num_rows);
        navigation.find('.total-rows').html(result.total_rows);

        let newState = history.state;
        newState.p_page = result.current_page;
        history.replaceState(newState, '', '?'+$.param(newState));

        patientsTbody.show();
        $('.patients-panel .preloader').hide();
    });
}

function loadRecords() {
    $('.selected-record-panel .preloader').show();
    let recordsTbody = records_table.find('tbody');
    recordsTbody.hide();

    $.getJSON('records/api/records', {
        page_num: history.state.r_page,
        sorting: {
            field: history.state.r_sorting_field,
            order: history.state.r_sorting_order
        },
    }, function(result) {
        recordsTbody.empty();
        let navigation = $('.selected-record-panel .navigation');
        if(result.total_rows) {
            recordsTbody.append(result.rows);
            $('#records_table').slideDown();
            $('[data-target=select_record_type]').removeClass('active').css('pointer-events', 'auto');
        } else if(result.is_selected_patient) {
            $('#records_table').slideDown();
            $('[data-target=select_record_type]').removeClass('active').css('pointer-events', 'auto');
            recordsTbody.append('<tr><td colspan="100%">No records in database. Click <a href="javascript:void(0)" data-target="select_record_type" data-action="popup">Add</a> to create a new record for selected patient.</td></tr>');
        } else {
            $('#records_table').slideUp();
            $('[data-target=select_record_type]').addClass('active').css('pointer-events', 'none');
        }

        if(result.total_rows > 5) {
            navigation.show();
        } else {
            navigation.hide();
        }
        
        navigation.find('.from-row').html(result.limit_from + 1);
        navigation.find('.to-row').html(result.limit_from + result.num_rows);
        navigation.find('.total-rows').html(result.total_rows);

        let newState = history.state;
        newState.r_page = result.current_page;
        history.replaceState(newState, '', '?'+$.param(newState));

        recordsTbody.show();
        $('.selected-record-panel .preloader').hide();
    });

    $('#records_table tbody').empty();
}

function reload(table) {
    if (table === 'patients') {
        $.post('records/unset_patient', function() {
            loadPatients();
            loadRecords();
        });
    } else {
        loadRecords();
    }
}

function updateState(state) {
    history.pushState(state, '', '?'+$.param(state));
    updateNavigation(state);
}

function updateNavigation(state) {
    if(history.state) {
        $('table .sortable').removeClass('sort-down').removeClass('sort-up');
        $(`#patients_table .sortable[data-field=${state.p_sorting_field}]`).addClass(sortClasses[state.p_sorting_order]);
        $(`#records_table .sortable[data-field=${state.r_sorting_field}]`).addClass(sortClasses[state.r_sorting_order]);
    }
}