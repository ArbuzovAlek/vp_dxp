$(document).ready(function() {

    var chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)'
    };

    /* criteria chart init */
    if (document.querySelector('#criteria_pie_canvas')) {
        var criteriaChart = new Chart(document.querySelector('#criteria_pie_canvas'), {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        +$('#criteria_table [data-label=No]').html(),
                        +$('#criteria_table [data-label=Yes]').html(),
                        +$('#criteria_table [data-label=Uncertain]').html(),
                    ],
                    backgroundColor: [
                        chartColors.red,
                        chartColors.green,
                        chartColors.orange,
                    ],
                    label: 'Dataset'
                }],
                labels: [
                    'No',
                    'Yes',
                    'Uncertain',
                ]
            },
            options: {
                legend: false,
                responsive: true,
                maintainAspectRatio: true,
                plugins: {
                    labels: [
                        {
                            render: 'label',
                            position: 'border',
                            fontColor: '#fff',
                            textMargin: 8,
                        },
                        {
                            render: 'percentage',
                            fontColor: '#fff'
                        }
                    ]
                }
            }
        });


    }


    /* subcriteria chart init */
    if (document.querySelector('#subcriteria_pie_canvas')) {
        new Chart(document.querySelector('#subcriteria_pie_canvas'), {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        +$('#subcriteria_table [data-label=No]').html(),
                        +$('#subcriteria_table [data-label=Yes]').html(),
                    ],
                    backgroundColor: [
                        chartColors.red,
                        chartColors.green,
                    ],
                    label: 'Dataset'
                }],
                labels: [
                    'No',
                    'Yes',
                ]
            },
            options: {
                legend: false,
                responsive: true,
                aspectRatio: 1.5,
                maintainAspectRatio: true,
                plugins: {
                    labels: [
                        {
                            render: 'label',
                            position: 'border',
                            fontColor: '#fff',
                            textMargin: 8
                        },
                        {
                            render: 'percentage',
                            fontColor: '#fff'
                        }
                    ]
                }
            }
        });

    }

    /* specifiers chart init */
    if (document.querySelector('#specifiers_pie_canvas')) {
        new Chart(document.querySelector('#specifiers_pie_canvas'), {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        +$('#specifiers_table [data-label="with_entry"]').html(),
                        +$('#specifiers_table [data-label="without_entry"]').html(),
                    ],
                    backgroundColor: [
                        chartColors.red,
                        chartColors.green,
                    ],
                    label: 'Dataset'
                }],
                labels: [
                    'With entry',
                    'Without entry',
                ]
            },
            options: {
                legend: false,
                responsive: true,
                aspectRatio: 1.5,
                maintainAspectRatio: true,
                plugins: {
                    labels: [
                        {
                            render: 'label',
                            position: 'border',
                            fontColor: '#fff',
                            textMargin: 8
                        },
                        {
                            render: 'percentage',
                            fontColor: '#fff'
                        }
                    ]
                }
            }
        });

    }

});