$(document).ready(function() {
    const popup = $('#additional_info_popup');

    $('.additional-info-action:not(.inactive)').click(function() {
        popup.popup('show');
        popup.find('.content').html($(`#additional_info_container #${$(this).data('field')}`).html());
    });

    $('.recommendation-edit-diagnosis').click(function () {
        const data = $(this).data() || {};
        const formalData = data.formal || {};

        editDiagnoseSelectorModal(data.diagnosis_id, {
            code: formalData[0] ? formalData[0].code : '',
            text: formalData[0] ? formalData[0].text.replace(/\s+/g, ' ') : '',
            diagnosis_id: formalData.diagnosis_id,
            assigned: formalData.assigned,
            option: formalData.option,
        });
    });

    window.programEdit = function () {
        editWithProgram($('.recommendation-edit-diagnosis').data('diagnosis_id'));
    };

    window.saveEdit = function () {
        var diagnosis_id = $('.recommendation-edit-diagnosis').data('diagnosis_id');
        saveEditDiagnosis(diagnosis_id, null, function () {
            document.location.reload();
        });
    };

    $('.recommendation-add-diagnosis').click(function () {
        addDiagnoseSelector();
    });

});