(function($){
// var dialogModule = modulejs.require("dialog");

let receiving_container_selector = '.container';
let is_routine_popup = false;
let routine_dismiss_flag = false;
let sub, sub_sub;
let url = '';
let opened_routine_popup = [];
let urls = [];

// $(document).ready(function(){
window.history.replaceState({prevUrl: 'first'}, document.title, document.location.href);

$(window).bind('popstate',function(event){
  event.preventDefault();
  var state = event.originalEvent ? event.originalEvent.state : event.state;
  if(state && window.location){
    if(history.state.is_prev_routine) {
        $.get(document.location, (page) => {
            $(receiving_container_selector).html(page);
        });
    } else {
        document.location.reload();
    }
  }
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function() {
    /*if(popup_url = $('#popup_url').val()) {
        routinePopupInit(popup_url);
    }*/
    sub = +$('#sub_required').val();
    sub_sub = +$('#sub_sub_required').val();
});


display_disorders = function(){

    $(document).on('click',".diagnosis-name a, .history a",function(e){
        if ($(this).hasClass('routine-link')) {
            return;
        }

        e.preventDefault();
        url = $(this).attr('href');
        $.ajax({
            type: "GET",
            url: url,
            success: function (data, textStatus, request) {
                successFunc(data,request);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });             
    })

    $(document).on('click', ".criterion-answers a", function(){
        input = $(this).next("input")
        if($(this).hasClass('chosen')){
            $(this).removeClass('chosen')
            $(input).prop('checked',false)
        }
        else{
            row = $( this ).closest( ".criterion-answers" );
            id = $(input).attr('name').replace('criterion-','');
            $(row).find("a").removeClass("chosen");
            $(this).addClass("chosen");
            $(input).prop("checked", true);
            $("#criterion-panel-"+id).removeClass("marked");
        }
    })
    $(document).on('click', '.criterion-answers label.art-radiobutton', function(){
        input = $(this).next("input")
        if($(this).hasClass('art-checked')){
            $(this).removeClass('art-checked')
            $(input).prop('checked',false)
        }
        else{
            row = $( this ).closest( ".criterion-answers" );
            $(row).find("label.art-radiobutton").removeClass("art-checked");
            $(this).addClass("art-checked");
            $(input).prop("checked", true);
        }


    });
    var unanswered = [];
    $(document).on('click','#continue-btn', function(){
        criteria_continue();
    })
    $(document).on('click', '#other-enter-btn',function(){
        free_text = $("#myModal .other-text-area").val();
        if(free_text ==''){
            popup = $(".popuptext");
            popup.addClass("show"); setTimeout(function () {
                $(popup).removeClass('show');
            }, 30000);
            return
        }
        save_criteria(free_text, function (data, textStatus, request) {
            successFunc(data,request);
        });
        closeModalPopup();

    });

    $(document).on('click','#specify-continue-btn', specify_continue);
  
    $(document).on('click', 'span.close,a.close', function(){
        closeModalPopup();
    })
    $(document).on('click touchend', function(event) {
        if (event.target == $("#myModal")[0] && $("#myModal").css('display') == 'block' && !$("#myModal").hasClass('permanent')) {
            closeModalPopup();
        }
        else{
            if(event.target != $(receiving_container_selector+" .options")[0] && event.target != $(receiving_container_selector+' img.dropbtn')[0] && $(receiving_container_selector+" .menu-buttons .dropdown .dropdown-content").css('display') == 'block' && !$(event.target).parent().hasClass('dropdown-link')){
                $(receiving_container_selector + " .menu-buttons .dropdown .dropdown-content").css('display', 'none');
            }
        } 
    });
    $(document).on('click', ".specifiers-answers a", function(e){
        e.preventDefault();
        id = $(this).attr('id');
        row = $( this ).closest(".specifiers-answers" );
        input = $(this).next("input");
        var variables = JSON.parse($(receiving_container_selector+" #variables").val());
        var variable = $(receiving_container_selector+" #variable").val();
        var behavior =$(receiving_container_selector+" #specify-behavior").val();
        var other_variables = $(this).data('variables');
        if(behavior == 'ncd_types')
            ncd_url(this);
        else{    
            if($(input).attr('type') == 'radio'){
                if($(this).hasClass("chosen")){
                    $(this).removeClass('chosen')
                    $(input).prop("checked", false);
                    // $("#specify-values").val('');
                    if(variable != ""){
                        variables[variable] = '';
                        $(receiving_container_selector+" #variables").val((JSON.stringify(variables)));
                    }
        
                }
                else{
                    $(row).find("a").removeClass("chosen");
                    $(this).addClass("chosen");
                    $(input).prop("checked", true);
                    // $("#specify-values").val('{"item_id": "'+id+'"}');             
                    if(variable != ""){
                        if(behavior=="free_text_if_yes" && (variable=="associated_medical" || variable == "associated_another"))
                            variables[variable]= {"var_data": $(this).attr("var_data")};
                        else
                            variables[variable]=$(this).attr("var_data");
                        $(receiving_container_selector+" #variables").val(JSON.stringify(variables));
                    }           
                }
            }
            else if($(input).attr('type') == 'checkbox'){
                var_data = $(this).attr('var_data');
                // values = $('#specify-values').val();
                // $values = variables[variable];
                // jsonobj = $.parseJSON(values);
                // if(!jsonobj['item_ids'])
                //     jsonobj['item_ids'] = []
                if(!variables[variable] || (variables[variable] && typeof(variables[variable]) !=="object"))
                    variables[variable] = [];
                if($(this).hasClass("chosen")){
                    $(this).removeClass('chosen')
                    $(input).attr('checked',false)
                    variables[variable].splice($.inArray(var_data, variables[variable]),1);
                }
                else{
                    $(this).addClass("chosen");
                    $(input).prop("checked", true);
                    variables[variable].push(var_data);
                }
                variables[variable] = variables[variable].filter((value, index, self) => self.indexOf(value) === index);
                $(receiving_container_selector+" #variables").val(JSON.stringify(variables))
            }
        }

        if(other_variables && typeof(other_variables) == "object") {
            for(x in other_variables) {
                variables[x] = other_variables[x];
            }
            variables[variable] = variables[variable].filter((value, index, self) => self.indexOf(value) === index);
            $(receiving_container_selector+" #variables").val(JSON.stringify(variables))
        }
    })
    $(document).on('click', '.info.with_content', function(){
        info = $(receiving_container_selector + " #info-popup").find('.info-note-popup');
        info_content = $(info).find(".content");
        if($(info_content).text().match("^popup-"))
            loadPage($.trim($(info_content).text()));
        else
            openModalPopup(info);

    });

    $(document).on('click', '.note.with_content', function(){
        note = $(receiving_container_selector + " #note-popup").find('.info-note-popup');
        note_content = $(note).find(".content");
        if($(note_content).text().match("^popup-"))
            loadPage($.trim($(note_content).text()));
        else
            openModalPopup(note);
    });

    $(document).on('click','.options.with_content', function(){
        $(this).closest('.dropdown').find('.dropdown-content').show();
    });

    $(document).on('click','.dropdown-content a.criteria.active-menu',function(e){
        e.preventDefault();
        input = $(this).find('input[name="present_type"]');
        if(!$(input).is(':checked')) {
            $('input[name="present_type"]').prop('checked',false);
            $(input).prop('checked',true);
        }
        present_favorite = $(this).attr('id');
        diagnosis_id = $(receiving_container_selector+" #diagnosis_id").val();
        user_id = $("#user_id").val();
        $.ajax({
            type: "PUT",
            url: '/users/' + user_id,
            data: {'id': user_id, 'present_favorite': present_favorite, 'diagnosis_id': diagnosis_id},
            success: function (data, textStatus, request) {
                window.location.reload();
                // successFunc(data,request)
           },
            error: function (data) {
                console.log('Error:', data);
                window.location.reload();
            }
        });         
    })

    $(document).on('click','#choose_patient_btn',function(e){
        e.preventDefault();
        patient_id = $("#patient_input").val();
        if(patient_id !=''){
            $.ajax({
                type: "GET",
                url: '/patient_diagnosis/create_patient_session',
                // dataType: 'json',
                data: {'patient_id': patient_id},
                success: function (data, textStatus, request) {
                    window.location.reload();
                },
                error: function (data) {
                }
            });
        }         
    })

    $(document).on('click','.x-close', function(){
        closeModalPopup();
    })

    $(document).on('click', ".popup-art-main a", function(e){
        e.preventDefault();
        if($(this).hasClass('popup-chosen')){
            $(this).removeClass('popup-chosen')
        }
        else{
            popup = $( this ).closest( ".popup-art-main" );
            $(popup).find("a").removeClass("popup-chosen");
            $(this).addClass("popup-chosen");
            a_text = $(this).text();
            a = $(".specifiers-answers a:contains('"+a_text+"')");
            // if(!$(a).hasClass('chosen')){
                $(a).trigger('click');
                closeModalPopup();
            // }


        }
    })

    $(document).on('click', '.popuptext .close-popup', function(){
        $(".popuptext").removeClass('show');
    })

    $(document).on('change','input[name="present_type"]', function(){
        // $('input[name="present_type"]').prop('checked',false);
        // $(this).prop('checked',true);
        // present_favorite = $(this).val();
        // diagnosis_id =$("#diagnosis_id").val();
        // $.ajax({
        //     type: "PUT",
        //     url: '/users/1',
        //     data: {'id': 1, 'present_favorite': present_favorite, 'diagnosis_id': diagnosis_id},
        //     success: function (data, textStatus, request) {
        //         window.location.reload();
        //         // successFunc(data,request)
        //    },
        //     error: function (data) {
        //         console.log('Error:', data);
        //     }
        // });          
    })

    function criteria_continue() {
        var all_answered = true;
        $(".criterion-answers input[type=radio]").each(function(item){
            name = $(this).attr("name");
            id = name.replace('criterion-','');
            level = $("#"+id).data('level');
            if(((level == 0) || (level == 1 && sub) || (level == 2 && sub_sub)) && $("input:radio[name="+name+"]:checked").length == 0) {
                all_answered = false;
                unanswered.push(id);
            }
        });
        if(all_answered){
            other = $("#other-popup");
            if(other.length) {
                if($("input:radio:checked").val() == "Yes"){
                    other = $(other).find(".other-popup-content");
                    $(".popuptext").removeClass('show');
                    openModalPopup(other);
                    lostFocus();
                    return;
                }
            }

            save_criteria(undefined, function (data, textStatus, request) {
                if ($(receiving_container_selector + " #next-criterion").val() || is_routine_popup) {
                    successFunc(data, request);
                } else {
                    const url = (document.location.pathname + '/is_criteria_have_been_met')
                        .replace('//', '/')
                        .replace('/result', '')
                    $.ajax({
                        type: "GET",
                        url: url,
                        success: function (result) {
                            if (result.is_criteria_have_been_met === false) {
                                openModalPopup($("#criteria_have_not_been_met").find(".info-note-popup"));
                            } else {
                                successFunc(data, request);
                            }
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            });
        }
        else{
            validation = $("#validation-error").find(".modal-content");
            $(validation).find("p").text("Please fill all criteria.");
            openModalPopup(validation);
        }
    }

    function successFunc(data,request){
        url = request.getResponseHeader("urlPath");
        const isReturn = new URL(url).searchParams.get('return');
        const isResult = url.indexOf('/result') > -1;
        urls.push(url);

        if(isReturn) {
            $('#routineModal').hide().empty();
            $('body').removeClass('modal-open');
            receiving_container_selector = '.container';
            // specify_continue(false);
        }
        if(url.indexOf("/result") > 0) {
            $("#myModal").addClass("permanent").html(data);
            $("body").addClass("modal-open"); 
            $("#myModal").show();
        } else {
            $(receiving_container_selector).html(data);
            if (receiving_container_selector === '#routineModal') {
                const closeRoutineModalButton = $('<div class="close-btn close-routine-modal"></div>');
                const cancelRoutineModalButton = $('<a class="art-button cancel-routine-modal">Cancel</a>');
                const backRoutineModalButton = $('<a class="art-button routine-back-button">Back</a>');


                closeRoutineModalButton.add(cancelRoutineModalButton).on('click', function () {
                    receiving_container_selector = '.container';
                    $('#routineModal').hide().empty();
                    $('body').removeClass('modal-open');
                    /*var return_url = '/' + new URL(url).searchParams.get('return_to').replace('//', '/');
                    $.ajax({
                        type: "GET",
                        url: return_url,
                        success: function (data, status, request) {
                            $('#routineModal').hide().empty();
                            successFunc(data, request);
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });*/
                });

                $(receiving_container_selector)
                    .find('.art-layout-wrapper')
                    .prepend(closeRoutineModalButton)

                $(receiving_container_selector)
                    .find('#specify-continue-btn')
                    .after(cancelRoutineModalButton)

                $(receiving_container_selector)
                    .find('#specify-continue-btn')
                    .after(backRoutineModalButton)
            }
        }
        if(url.indexOf('return_from_catatonia') >= 0 || url.indexOf('return_from_ncd') >= 0){
            arr = url.split('?');
            url = arr[0];
        }

        if (!is_routine_popup && !isResult) {
            document.title = request.getResponseHeader("title");
            window.history.pushState({prevUrl:window.location.href, is_prev_routine: is_routine_popup}, document.title, url);
        }

        if(isReturn) {
            is_routine_popup = false;
            routine_dismiss_flag = true;
        }
    }

    errorFunc = function(data)
    {
     $(receiving_container_selector).html('<h1>500 Internal Server Error</h1>');   
        // window.history.pushState({prevUrl:window.loction.href},document.title,url)
    }


    loadPage = function(page_name){
        page = page_name + '.html'
        $("#myModal").load('/' + page, function(){
            $("#myModal").show();
            $("body").addClass("modal-open");
            load_popup_selections();
        });
    }

    load_popup_selections = function(){
        a = $(".specifiers-answers a.chosen")[0];
        if(a){
            a_text = $(a).text();
            console.log( a_text);
            popup_a = $("#myModal a:contains('"+a_text+"')")[0];
            $(popup_a).addClass("chosen");
        }
       // $(popup_a).trigger('click');
    }

    save_criteria = function(free_text, successCallback){
        free_text = (typeof free_text === 'undefined') ? '' : free_text;
        var answers = {};
        $(".criterion-answers").each(function(){
            id = $(this).attr('id');
            radio = $("#" + id +" input:radio:checked")
            answers[id] = radio.val();
        });
        patient_diagnosis_id = $(receiving_container_selector+" #patient_diagnosis_id").val();
        next_criterion = $(receiving_container_selector+" #next-criterion").val();
        criterion_page = $(receiving_container_selector+" #criterion-page").val();
        diagnosis_id = $(receiving_container_selector+" #diagnosis_id").val();
        return_to = $(receiving_container_selector+" #return_to").val();
        return_type = $(receiving_container_selector+" #return_type").val();
        nested_return_to = $(receiving_container_selector+" #nested_return_to").val();

        if (!successCallback) {
            successCallback = $.noop;
        }

        $.ajax({
            type: "POST",
            url: '/diagnosisSteps',
            data: {'diagnosis_id': diagnosis_id, 'patient_id': patient_diagnosis_id, 'answers': answers, 'free_text': free_text, 'next_criterion': next_criterion, 'criterion_page': criterion_page, 'return_to': return_to, 'return_type': return_type, 'nested_return_to': nested_return_to},
            //dataType:'json',
            success: successCallback,
            error: function (data) {
                console.log('Error:', data);
                // errorFunc(data);
            }
        }); 
    }

    save_specifiers = function(data){
        $.ajax({
            type: "POST",
            url: '/specifiers/store',
            data: data,
            success: function (data, textStatus, request) {
                successFunc(data,request);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });    
    }
    lostFocus = function(){
        $("textarea.other-input").blur();
    } 

    least_one_chosen = function(specify_id, cb){
        var result;
        $.ajax({
            type: "GET",
            url: '/specifiers/least_one_chosen',
            data: {'specify_id': specify_id},
            success: function (data) {
                result = data.one_chosen;
                cb(data.one_chosen)
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

    ncd_url = function(btn_link){
        url = $(btn_link).attr('href');
        $.ajax({
            type: "GET",
            url: url,
            success: function (data, textStatus, request) {
                successFunc(data,request);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });   
    }

    remove_empty = function(obj,variable){
        new_obj=obj;
        if(obj[variable] && typeof(obj[variable])=="object"){
            $.each( obj[variable], function( key, value ) {
                if(key.match("^free-text") && blank(value['code']) && blank(value['name']))
                    delete new_obj[variable][key];
            });
        }
        return JSON.stringify(new_obj);
    }
    
    blank = function(variable){
        if(variable == "" || typeof(variable) =='undefined')
            return true;
        return false;
    }

    openModalPopup = function(popup){
        if($("#routineModal").is(":visible")) {
            $("#routineModal").addClass("in_background").hide();
        }
        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open"); 
    }

    closeModalPopup = function(){
        $("#myModal").html('');
        $("#myModal").hide();
        $("#myModal").removeClass('modal-centred');
        $("body").removeClass("modal-open");
        if(unanswered.length>0) {
            for(x in unanswered) {
                $("#criterion-panel-"+unanswered[x]).addClass("marked");
            }
            unanswered = [];
        }
        if(!$("#routineModal").is(":visible") && $("#routineModal").hasClass("in_background")) {
            $("#routineModal").removeClass("in_background").show();
        }
    }

    /**
     * result step
     */
    ;(function () {

        var diagnosis_id = null;
        var diagnosis_code = null;
        var modalContent = '';

        $(document).on('click', '#note-popup-2-continue-btn, #note-popup-3-continue-btn', function () {
            sendData();
        });

        $(document).on('click', '.result-cancel-btn', function () {
            closeModalPopup();
            /*
            var url = document.location.href.replace('/result', '');
            $.ajax({
                type: 'GET',
                url: url,
                success: function (data, textStatus, request) {
                    successFunc(data,request);
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });*/

        });

        $(document).on('click', '#result-continue-btn', function () {
            var form = $('#result-step-form');
            var formData = new FormData(form[0]);
            var criteria = form.data('criteria');
            var assignToPatient = formData.get('assign_to') === 'Yes';

            diagnosis_id = $('#result-step-form').data('diagnosis_id');
            diagnosis_code = $('#result-step-form').data('diagnosis_code');
            modalContent = $('#myModal').clone().html();

            /*if (criteria && formData.get('assign_to') === null) {
                openModalPopup($('#note-result-popup').find('.modal-content'));
                return;
            }*/

            if (criteria && assignToPatient) {
                sendData();
                return;
            }

            if (criteria && !assignToPatient) {
                sendData();
                // openModalPopup($('#note-result-popup-2').find('.info-note-popup'));
                return;
            }

            if (!criteria) {
                openModalPopup($('#note-result-popup-3').find('.info-note-popup'));
            }
        });

        $(document).on('click', '.show-result-modal', function () {
            openModalPopup($(modalContent));
        });

        function sendData() {
            var form = $('#result-step-form');
            var formData = new FormData(form[0]);
            var option = formData.get('option');
            var assignToPatient = null;

            if (formData.get('assign_to')) {
                assignToPatient = Number(formData.get('assign_to') === 'Yes');
            }
            $.ajax({
                type: 'POST',
                url: document.location.href + '/result'.replace('//', '/'),
                data: {
                    options: option,
                    assign_to_patient: assignToPatient
                },
                success: function (data, textStatus, request) {
                    if(assignToPatient) {
                        window.location.href = "/center/diagnosis/?highlight=" + diagnosis_id;
                    } else {
                        window.location.href = "/center/results/?highlight=" + diagnosis_id;
                        // successFunc(data,request);
                        // closeModalPopup();
                    }
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }


    })();

    routinePopupInit = function(diagnosis_url) {
        $('#routineModal').empty().show();
        $('body').addClass('modal-open');
        receiving_container_selector = '#routineModal';
        $.ajax({
            type: "GET",
            url: diagnosis_url,
            success: function (data, textStatus, request) {
                successFunc(data, request);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    }

    $(document).on("click",".routine-link",function(e){
        routine_dismiss_flag = false;
        history.pushState(history.state, document.title, window.location.href);
        routinePopupInit($(this).attr('href'));
        is_routine_popup = true;
        e.preventDefault();
    });

    $(document).on('click', '.routine-back-button', function() {
        urls.pop();
        routinePopupInit(urls.pop());
    });


    function specify_continue(check_on_routine = true) {

        // arr = (window.location.origin + window.location.pathname).split('/');
        specify_id = $(receiving_container_selector+" #specify_id").val();
        // values = $("#specify-values").val();
        next_specify = $(receiving_container_selector+" #next-specify").val();
        diagnosis_id = $(receiving_container_selector+" #diagnosis_id").val();
        behavior = $(receiving_container_selector+" #behavior").val();
        specify_behavior = $(receiving_container_selector+" #specify-behavior").val();
        is_require = $(receiving_container_selector+" #is_require").val();
        return_to = $(receiving_container_selector+" #return_to").val();
        variables = $(receiving_container_selector+" #variables").val();
        variable = $(receiving_container_selector+" #variable").val();
        var_obj = $.parseJSON(variables);

        if (opened_routine_popup.indexOf(specify_id) === -1) {
            opened_routine_popup.push(specify_id);
        }

        if(var_obj[variable] != 'without') {
            if(variable == "with_disorder_of" && var_obj["associated"] && var_obj[variable].length==0 || var_obj[variable] == 'with') {
                delete var_obj["associated"];
                variables = JSON.stringify(var_obj);
            }
        }
        if(specify_behavior == 'free_text_if_yes')
            variables = remove_empty(var_obj,variable);
        data = {
            'diagnosis_id': diagnosis_id,
            'specify_id': specify_id,
            'next_specify': next_specify,
            'behavior': behavior,
            'return_to': return_to,
            'variables': variables,
            'opened_routine_popup': opened_routine_popup
        };

        if(specify_behavior == 'least_one_of_group'){
            s_data = var_obj[variable];
            if(!s_data || s_data.length == 0){
                least_one_chosen(specify_id,function(myCallback){
                    if(!myCallback){
                        validation = $("#validation-error").find(".modal-content");
                        $(validation).find("p").text("You must choose at least one option from the specifiers group of impairements in reading, writing, or mathematics.");
                        openModalPopup(validation);
                    }
                    else{
                        save_specifiers(data)
                    }
                });
                return;
            }
        }
        else{
            if(is_require == "1" && !return_to){
                if(!var_obj || !var_obj[variable]){
                    validation = $("#validation-error").find(".modal-content");
                    $(validation).find("p").text("This is a required specifier. To continue, please provide an input.");
                    openModalPopup(validation);
                    return;

                }   
            }
        }
        save_specifiers(data);
        routine_dismiss_flag = false;
    }

    $(function () {

        /*if ($('[data-open-result-modal-in-specifier="1"]').length) {
            specify_continue();
        }

        if ($('[data-open-result-modal-in-criteria="1"]').length) {
            criteria_continue();
        }*/

    });

    $(function () {
        $('.criteria-have-not-been-met-continue')
    })
}
display_disorders();

})(jQuery);