queryData = window.queryData || {};

let patients_table = {};
let records_table = {};

const ASC = 'ASC';
const DESC = 'DESC';

const sortClasses = {
    ASC: 'sort-up',
    DESC: 'sort-down'
}

let sorting = {
    patients: {
        field: 'id',
        order: DESC
    },
    records: {
        field: 'id',
        order: DESC
    }
}

$(document).ready(function() {

    $('.sortable').click(function () {
        let table = $(this).closest('table').data('table');
        if ($(this).data('field') === sorting[table].field) {
            sorting[table].order = $(this).hasClass('sort-up') ? DESC : ASC;
        } else {
            sorting[table].order = DESC;
            sorting[table].field = $(this).data('field');
        }
        $(`#${table}_table .sortable`).removeClass('sort-up').removeClass('sort-down');
        $(this).addClass(sortClasses[sorting[table].order]);
        if (table === 'patients') {
            loadPatients();
        } else {
            loadRecords();
        }
    });

    $(document).on('click', '#patients_table th', function () {
        var wrapper = $(this).closest('.patients-panel');
        var url     = wrapper.data('url');

        $.get(url, {
            'order-by': $(this).data('col'),
            'order-direction': $(this).hasClass('col-sort-desc') ? 'asc' : 'desc'
        }).then((response) => {
            wrapper.replaceWith(response);
        });
    });


    $(document).on('click', '#patients_table tr:not(:first)', function () {
        let patient_id = $(this).data('id');
        let current = this;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.post('/select_patient/'+patient_id, function (result) {
            if (result !== 'error') {
                if (location.pathname.indexOf('/error_patients') > -1) {
                    document.location = location.pathname.replace('/error_patients', '') || '/';
                } else if (typeof result === 'object' && result.return_to && result.return_to.indexOf('/center') > -1) {
                    document.location = result.return_to;
                } else if (location.pathname.indexOf('/center') > -1) {
                    document.location.reload();
                } else {
                    document.location = '/';
                }

                $('#patients_table tr').removeClass('active');
                $("#select-patient-image").attr("src","/css/images/object2026325657.png");
                $('#myModal #header-selected_patient').text('Selected patient: '+patient_id);
                $('span #patient-id').text(patient_id);
                $(current).addClass('active');
            } else {
                reload('patients');
            }
        });
    });
});

function loadPatients() {
    var patientsPanelWrapper = $('.patients-panel-wrapper');
    patientsPanelWrapper.hide();

    $.get('/get_patients', {}, function (result) {
        patientsPanelWrapper.html(result);
        patientsPanelWrapper.show();
        patientsPanelWrapper.find('.pagination-block .dropdown-toggle').dropdown();
    });
}

function loadRecords(patient_id) {
    //$('#records_table tbody').empty();
}

function reload(table) {
    loadPatients();
}

function PopUpShow(){
    popup = $("#select-patient-popup").find(".modal-content");
    $("#myModal").html(popup.clone());
    $("#myModal").addClass('modal-centred');
    $("#myModal").css('display', 'flex');
    $("body").addClass("modal-open");

    $(`#patients_table .sortable[data-field=${sorting.patients.field}]`).addClass(sortClasses[sorting.patients.order]);
    $(`#records_table .sortable[data-field=${sorting.records.field}]`).addClass(sortClasses[sorting.records.order]);


    patients_table = $('#patients_table');
    records_table = $('#records_table');

    loadPatients();
}
