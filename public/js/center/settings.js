$(document).ready(function() {
    $('.toggle-button').click(function() {
        const currentButton = $(this);
        const key = $(this).data('key');
        const field = $(this).data('field');

        const trueVal = $(this).data('checked_val') ? $(this).data('checked_val') : 1;
        const falseVal = $(this).data('unchecked_val') ? $(this).data('unchecked_val') : 0;

        const newValue = $(this).hasClass('disabled') ? trueVal : falseVal;

        if ($(this).hasClass('always-filled')) {
            const prevText = $(this).next('.setting-item-actions-text');
            const nextText = $(this).prev('.setting-item-actions-text');
            const color = '#808080';

            prevText.css('color', '');

            nextText.css('color', '');
            if (newValue === trueVal) {
                nextText.css('color', color);
            } else {
                prevText.css('color', color);
            }
        }

        updateSetting(field, key, newValue, function() {
            currentButton.toggleClass('disabled');
        });
    });

    $('.click-onload').click();
});

async function updateSetting(field, key, value, callbackSuccess = () => {}, callbackError = () => {}) {
    $.post('/center/settings/update_settings', { field: field, key: key, value: value }, await function(result) {
        if(result == 'complete') {
            callbackSuccess();
        } else {
            callbackError();
        }
    });
}