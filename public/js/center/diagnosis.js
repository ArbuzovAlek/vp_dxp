let hasSelectedDiagnosis = false;

function selectSelectedDiagnosis () {
    selected_diagnose = $('#table-diagnosis').data('selected_diagnos');
    if (selected_diagnose && selected_diagnose!=-1){
        $('#table-diagnosis tr#'+selected_diagnose).addClass('active');
    }
    else if (selected_diagnose = $('#table-diagnosis').data('free_text_id_step_selected')) {
        $('*[data-free_text_id_step="'+selected_diagnose+'"]').addClass('active');
    }
}

selectSelectedDiagnosis();

$(document).on('click','#table-diagnosis tr', function () {
    let current = this;
    $(current).siblings().removeClass('active');

    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        hasSelectedDiagnosis = false;
    } else {
        $(this).addClass('active');
        hasSelectedDiagnosis = true;
    }

    updateActionButtonsStatuses();
});

$(document).on('click', 'span.use-program', function(){
    window.location.href = "/center";
})

$(document).on('click', 'span.free-text', function(){
    addFreeTextDiagnose();
})

$(document).on('click', 'span.free-text-edit', function(){
    editFreeTextDiagnose();
})

function programEdit() {
    let diagnos_id = $('#table-diagnosis tr.active').data('diagnos_id');
    editWithProgram(diagnos_id);
}


function refreshTable(diagnos_id,free_text_id_step=0) {
    const page = $('.diagnosis-table-wrapper').data('page');
    const per_page = $('.diagnosis-table-wrapper').data('per-page');
    const url = '/center/diagnosis/reload_table/' + diagnos_id + '/' + free_text_id_step + '?page=' + page + '&per_page=' + per_page
    $('div.table-container').load(url, function () {
        $('div.table-container').fadeIn();
        selectSelectedDiagnosis();
    });
}

function diagnosUp(direction) {
    let diagnos_id = $('#table-diagnosis tr.active').data('diagnos_id');
    let free_text_id_step = $('#table-diagnosis tr.active').data('free_text_id_step');
    if (diagnos_id){
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.get('/center/set_ordering/',{direction:direction, diagnosis_id:diagnos_id, free_text_id_step:free_text_id_step}, function(result) {
            if (result !== 'error' ) {
                refreshTable(diagnos_id,free_text_id_step);
            } else {

            }
        });
    }
    else{
        popup = $("#select-patient-error").find(".modal-content");
        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open");
    }

}

function confirmDelete() {
    var diagnos_id = $('#table-diagnosis tr.active').data('diagnos_id');
    var free_text_id_step = $('#table-diagnosis tr.active').data('free_text_id_step');
    deleteDiagnose(diagnos_id, free_text_id_step, refreshTable);
}

function delConfirmation(){
    let diagnos_id = $('#table-diagnosis tr.active').data('diagnos_id');
    delConfirmationModal(diagnos_id);
}

function delConfirmationModal(diagnos_id) {
    if (diagnos_id){
        popup = $("#del-confirmation").find(".modal-content");
        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open");
    }
    else{
        popup = $("#select-patient-error").find(".modal-content");
        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open");
    }
}

function deleteDiagnose(diagnos_id, free_text_id_step, refreshFn){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        cache: false,
        url:"/center/del_diagnose",
        type: 'POST',
        dataType: 'json',
        data: {
            diagnose_id:diagnos_id,
            free_text_id_step:free_text_id_step
        },
        success: function (data) {
            if (typeof refreshFn === "function") {
                refreshFn();
            }
            closeModalPopup();
        }
    });
}

function getFormDataAsArray($form) {
    return $form.serializeArray().reduce(function (obj, item) {

        if (obj[item.name] !== undefined) {
            if (!Array.isArray(obj[item.name])) {
                obj[item.name] = [obj[item.name]];
            }
            obj[item.name].push(item.value);
        } else {
            obj[item.name] = item.value;
        }

        return obj;
    }, {});
}

function saveAdd() {
    $.ajax({
        url:'/center/diagnosis/add',
        type:'post',
        data: getFormDataAsArray($('#myModal #addForm')),
        success:function(){
            refreshTable();
            closeModalPopup();
        }
    });
}

function saveEdit() {
    var diagnos_id = $('#table-diagnosis tr.active').data('diagnos_id');
    var free_text_id_step = $('#table-diagnosis tr.active').data('free_text_id_step');

    saveEditDiagnosis(diagnos_id, free_text_id_step, refreshTable);
}

function saveEditDiagnosis(diagnos_id, free_text_id_step, refreshFn ) {
    $.ajax({
        url:'/center/diagnosis/edit',
        type:'post',
        data:{
            main:getFormDataAsArray($('#myModal #editForm')),
            diagnose_id:diagnos_id,
            free_text_id_step:free_text_id_step
        }, 
        success:function(){
            if (typeof refreshFn === "function") {
                refreshFn();
            }
            closeModalPopup();
        }
    });
}
function editDiagnoseSelector(){
    var data = $('#table-diagnosis tr.active').data() || {};
    var formalData = data.formal || {};

    editDiagnoseSelectorModal(data.diagnos_id, {
        code: formalData[0] ? formalData[0].code : '',
        text: formalData[0] ? formalData[0].text.replace(/\s+/g, ' ') : '',
        diagnosis_id: formalData.diagnose_id,
        assigned: formalData.assigned,
        option: formalData.option,
    });
}
function editDiagnoseSelectorModal(diagnos_id, formalData){
    if (diagnos_id){
        const editBlock = $('#edit-diagnose-free-text');
        popup = $("#edit-diagnose-selector").find(".modal-content");

        editBlock.find('input[name="options"]:checked').prop('checked', false);
        editBlock.find('input[name="assign"]:checked:not(:disabled)').prop('checked', false);
        editBlock.find('input[name="ICD"]').val(formalData.code);
        editBlock.find('[name="diagnosis"]').val(formalData.text);
        
        if (Array.isArray(formalData.option)) {
            formalData.option.forEach(function (value) {
                editBlock.find('input[name="options"][value="' + value  + '"]').prop('checked', true);
            })
        } else {
            editBlock.find('input[name="options"][value="' + formalData.option  + '"]').prop('checked', true);
        }

        editBlock.find('input[name="assign"][value="' + formalData.assigned  + '"]:not(:disabled)').prop('checked', true);

        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open");
    }
    else{
        popup = $("#select-patient-error").find(".modal-content");
        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open");
    }
}
function addDiagnoseSelector(){
    popup = $("#add-diagnose-selector").find(".modal-content");
    $("#myModal").html(popup.clone());
    $("#myModal").show();
    $("body").addClass("modal-open");
}

function addFreeTextDiagnose(){
    closeModalPopup();
    popup = $("#add-diagnose-free-text").find(".modal-content");
    $("#myModal").html(popup.clone());
    $("#myModal").show();
    $("body").addClass("modal-open");
}
function editFreeTextDiagnose(){
    closeModalPopup();
    popup = $("#edit-diagnose-free-text").find(".modal-content");
    $("#myModal").html(popup.clone());
    $("#myModal").show();
    $("body").addClass("modal-open");
}

function editWithProgram(diagnos_id){
    if (diagnos_id!=-1){
        window.location.href="/diagnosis/"+diagnos_id+"/criteria";
    }
    else{
        closeModalPopup();
        popup = $("#free-text-error").find(".modal-content");
        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open");
    }
}
function closeModalPopup(){
        $("#myModal").html('');
        $("#myModal").hide();
        $("#myModal").removeClass('modal-centred');
        $("body").removeClass("modal-open");
    }

function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}


(function () {
    var timeout = null;

    function setCodeAndTextEqualHeight() {
        var table = $('.art-article-table-aligned-blocks');

        table.find('tr .result-table-item-code').each(function (index) {
            var text = $(table.find('tr .result-table-item-text')[index]);
            var height = Math.max($(this).height(), text.height());

            $(this).css('min-height', height);
            text.css('min-height', height);

        });
    }

    $(window).on('resize', function () {
        clearTimeout(timeout);

        timeout = window.setTimeout(setCodeAndTextEqualHeight, 100);
    });

    setCodeAndTextEqualHeight();

})();

$(document).on('click', '#table-diagnosis tr', function() {
    const isCreated = Boolean($(this).data('is_created'));

    /*if (isCreated) {
        $('.art-article-controls-actions-button-analytics').addClass('disabled');
    } else {*/
        const patient_id = $(this).data('patient_id');
        const diagnosis_id = $(this).data('diagnos_id');
        const option_id = $(this).data('option_id');

        selectDiagnosisForAnalytic(patient_id, diagnosis_id, option_id);
    // }
});

$(document).on('click', '.diagnosis-help', function(){
    popup = $("#diagnosis-help-modal").find(".info-note-popup");
    $("#myModal").html(popup.clone());
    $("#myModal").show();
    $("body").addClass("modal-open");
});

$(document).on('click', '.diagnosis-diagnosis-add', function(){
    addDiagnoseSelector();
});

$(document).on('click', '.diagnosis-diagnosis-edit', function(){
    editDiagnoseSelector();
});
$(document).on('click', '.diagnosis-diagnosis-delete', function(){
    delConfirmation();
});

$(document).on('click', '.art-article-controls-actions-button-analytics', function () {
    if ($(this).hasClass('disabled')) {
        popup = $("#select-patient-error").find(".modal-content");
        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open");
    } else {
        document.location = '/center/analytics';
    }

});

function updateActionButtonsStatuses() {
    if (hasSelectedDiagnosis) {
        $('.art-article-controls-actions-button-edit').removeClass('disabled');
        $('.art-article-controls-actions-button-delete').removeClass('disabled');
        $('.art-article-controls-actions-button-analytics').removeClass('disabled');
    } else {
        $('.art-article-controls-actions-button-edit').addClass('disabled');
        $('.art-article-controls-actions-button-delete').addClass('disabled');
        $('.art-article-controls-actions-button-analytics').addClass('disabled');
    }
}

$(function () {
    var activeRow = $('#table-diagnosis tr.active');

    if (activeRow.length > 0) {
        hasSelectedDiagnosis = activeRow.length > 0;
        selectDiagnosisForAnalytic(
            activeRow.data('patient_id'),
            activeRow.data('diagnos_id'),
            activeRow.data('option_id')
        );
    }

    updateActionButtonsStatuses();
});

function selectDiagnosisForAnalytic(patient_id, diagnosis_id, option_id) {
    let url = `/select_diagnosis_result/${patient_id}/${diagnosis_id}`;

    if (option_id) {
        url += `?option_id=${option_id}`
    }

    $.post(url);
}