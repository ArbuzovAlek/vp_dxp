$(document).ready(function() {
    $(document).on('click', '[data-action=popup]', function() {
        $('#'+$(this).data('target')).addClass('visible');
        $("html").addClass("static");
    });

    $(document).on('click', '[data-action=popup_close]', function() {
        $(this).closest('div.popup').removeClass('visible');
        $("html").removeClass("static");
    });

    $(document).on('click', '.popup.visible.closable', function(e) {
        let target = $(e.target);
        let popup_content = $('.popup-content');
        if(!target.hasClass('popup-content') && popup_content.has(e.target).length === 0) {
            $(this).removeClass('visible');
            $("html").removeClass("static");
        }
    });

    $.fn.extend({
        'popup': function(action) {
            switch(action) {
                case 'show':
                    $(this).addClass('visible');
                    $('html').addClass('static');
                    break;
                case 'hide':
                    $(this).removeClass('visible');
                    $('html').removeClass('static');
                    break;
            }
        }
    })

    $(document).on('click', '.analytics-help', function () {
        popup = $("#analytics-help-modal").find(".info-note-popup");
        $("#myModal").html(popup.clone());
        $("#myModal").show();
        $("body").addClass("modal-open");
    });
});