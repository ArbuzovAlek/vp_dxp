queryData = window.queryData || {};
$(function () {

    $(document).on('click', '.pagination-wrapper .dropdown-item', function (e) {
        var wrapper = $($(this).closest('.pagination-block').data('wrapper')).first();
        wrapper.find('.page-size').text($(this).text());

        reloadTable(wrapper, 1);

        e.preventDefault();
    });

    $(document).on('click', '.pagination-block .page-link-prev, .pagination-block .page-link-next', function (e) {
        var wrapper = $($(this).closest('.pagination-block').data('wrapper')).first();

        reloadTable(wrapper, $(this).data('page'));

        e.preventDefault();
    });

    $(document).on('click', '.pagination-block .page-link-number', function (e) {
        var wrapper = $($(this).closest('.pagination-block').data('wrapper')).first();

        reloadTable(wrapper, $(this).text())

        e.preventDefault();
    });

    function reloadTable(wrapper, page, data = {}) {
        var url = wrapper.data('url');
        var orderBy = wrapper.data('order-by');
        var orderDirection = wrapper.data('order-direction');

        queryData.per_page = wrapper.find('.page-size').text().trim();

        if (page) {
            queryData.page = page;
        }

        if (orderBy) {
            queryData['order-by'] = orderBy;
        }

        if (orderDirection) {
            queryData['order-direction'] = orderDirection;
        }

        $.get(url, Object.assign({}, queryData, data)).then((response) => {
            wrapper.replaceWith(response);
            wrapper.find('.pagination-block .dropdown-toggle').dropdown();
        });
    }

});