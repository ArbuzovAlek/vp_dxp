<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NCDCode extends Model
{
    protected $table = 'ncd_codes';

    public function diagnosis() {
        return $this->belongsTo('App\Diagnosis');
    }
}
