<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PatientVp extends Model
{
    protected $connection = 'mysql_virtual_psychology';
    protected $table = 'patients';
    protected $guarded = ['password'];
    protected $dates = ['birth_date'];

    public $timestamps = false;

    public const GENDER_MALE = 'M';
    public const GENDER_FEMALE = 'F';
    public const GENDER_NOT_SPECIFIED = 'X';

    public const EDUCATION_ELEMENTARY = 1;
    public const EDUCATION_HIGH_SCHOOL = 2;
    public const EDUCATION_UNDERGRADUATE = 3;
    public const EDUCATION_GRADUATE = 4;

    public const SOCIOECONOMIC_LOWER = 1;
    public const SOCIOECONOMIC_MIDDLE = 2;
    public const SOCIOECONOMIC_UPPER = 3;

    public const MARITAL_STATUS_SINGLE_NEVER_MARRIED = 1;
    public const MARITAL_STATUS_MARRIED = 2;
    public const MARITAL_STATUS_LIVING_WITH_PARTNER = 3;
    public const MARITAL_STATUS_SEPARATED_DIVORCED = 4;
    public const MARITAL_STATUS_REMARRIED = 5;
    public const MARITAL_STATUS_WIDOWED = 6;

    public const SETTING_INPATIENT_OUTPATIENT = 1;
    public const SETTING_PSYCHIATRIC_EVALUATION = 2;
    public const SETTING_PSYCHOTHERAPY = 3;
    public const SETTING_EDUCATIONAL = 4;
    public const SETTING_RESEARCH = 5;
    public const SETTING_VOCATIONAL = 6;

    public const GROUP_TESTING = 1;
    public const GROUP_PRE = 2;
    public const GROUP_POST = 3;
    public const GROUP_CLIENTS = 4;

    public function getGenderLabel() {
        return array_search($this->gender, self::getGenderList()) ?: 'Not specified';
    }

    public function getEducationLabel() {
        return array_search($this->education, self::getEducationList()) ?: 'Not specified';
    }

    public function getSocioeconomicLabel() {
        return array_search($this->socioeconomic, self::getSocioeconomicList()) ?: 'Not specified';
    }

    public function getMaritalStatusLabel() {
        return array_search($this->marital_status, self::getMaritalStatusList()) ?: 'Not specified';
    }

    public function getSettingLabel() {
        return array_search($this->setting, self::getSettingList()) ?: 'Not specified';
    }

    public function getGroupLabel() {
        return array_search($this->group, self::getGroupList()) ?: 'Unassigned';
    }

    public function age() {
        return (new \DateTime())->diff($this->birth_date)->y;
    }

    public function month_age() {
        $m = (new \DateTime())->diff($this->birth_date)->m;

        if ($m < 10) {
            $m = '0' . $m;
        }

        return $m;
    }

    public static function getGenderList() {
        return [
            'Male' => self::GENDER_MALE,
            'Female' => self::GENDER_FEMALE,
            'Not specified' => self::GENDER_NOT_SPECIFIED
        ];
    }

    public static function getSocioeconomicList() {
        return [
            'Lower' => self::SOCIOECONOMIC_LOWER,
            'Middle' => self::SOCIOECONOMIC_MIDDLE,
            'Upper' => self::SOCIOECONOMIC_UPPER
        ];
    }

    public static function getMaritalStatusList() {
        return [
            'Single/Never married' => self::MARITAL_STATUS_SINGLE_NEVER_MARRIED,
            'Married' => self::MARITAL_STATUS_MARRIED,
            'Living with partner' => self::MARITAL_STATUS_LIVING_WITH_PARTNER,
            'Separated/Divorced' => self::MARITAL_STATUS_SEPARATED_DIVORCED,
            'Remarried' => self::MARITAL_STATUS_REMARRIED,
            'Widowed' => self::MARITAL_STATUS_WIDOWED
        ];
    }

    public static function getSettingList() {
        return [
            'Inpatient/Outpatient' => self::SETTING_INPATIENT_OUTPATIENT,
            'Psychiatric evaluation' => self::SETTING_PSYCHIATRIC_EVALUATION,
            'Psychotherapy' => self::SETTING_PSYCHOTHERAPY,
            'Educational' => self::SETTING_EDUCATIONAL,
            'Research' => self::SETTING_RESEARCH,
            'Vocational' => self::SETTING_VOCATIONAL
        ];
    }

    public static function getGroupList() {
        return [
            'Testing' => self::GROUP_TESTING,
            'Pre' => self::GROUP_PRE,
            'Post' => self::GROUP_POST,
            'Clients' => self::GROUP_CLIENTS
        ];
    }

    public static function getEducationList() {
        return [
            'Elementary' => self::EDUCATION_ELEMENTARY,
            'High school' => self::EDUCATION_HIGH_SCHOOL,
            'Undergraduate' => self::EDUCATION_UNDERGRADUATE,
            'Graduate' => self::EDUCATION_GRADUATE
        ];
    }

    public function getCriteriaCountAttribute() 
    {
        return !empty($this->attributes['criteria_count']) ? unserialize($this->attributes['criteria_count']) : [];
    }

    public function getCriteriaCount()
    {
        return \App\DiagnosisSteps::where('patient_diagnosis_id','=',$this->id)->distinct('criterion_id')->count('criterion_id');
    }

    public function checkCount() {
        $counts = $this->allowed_criteria_count;
        $criteria_counts = \App\DiagnosisSteps::where('patient_diagnosis_id','=',$this->id)->distinct('criterion_id')->count('criterion_id');
        if($criteria_counts > $counts)
            return false;
        return true;
    }

    public function get_demographic_data_for_export() {
        $age = $this->age() . '.' . $this->month_age();

        if ($age > 99.12) {
            $age = 99.99;
        }

        return [
            $this->user_own_id,
            $this->gender,
            $age,
            $this->education ?: 'N/A',
            $this->marital_status ?: 'N/A',
            $this->socioeconomic ?: 'N/A',
            $this->setting ?: 'N/A'
        ];
    }

    public function get_clinician_rated_data_for_export() {
        $total_clinician_rated = [];

        $clinician_rated = AdditionalInfo::where([
            ['patient_diagnosis_id', '=', $this->id],
            ['type', '=', AdditionalInfo::TYPE_CLINICIAN_SEVERITY_SCALE]
        ])->orderBy('created_at', 'ASC')->get();

        if(empty($clinician_rated)) return false;

        foreach($clinician_rated as $assession) {
            $severity = $assession->value;
            $total_clinician_rated[] = [
                $assession->created_at->format('Y/m/d'),
                $severity['hallucinations'] ?? 'N\A',
                $severity['delusions'] ?? 'N\A',
                $severity['disorganized_speech'] ?? 'N\A',
                $severity['abnormal_psychomotor_behavior'] ?? 'N\A',
                $severity['negative_symptoms'] ?? 'N\A',
                $severity['impaired_cognition'] ?? 'N\A',
                $severity['depression'] ?? 'N\A',
                $severity['mania'] ?? 'N\A'
            ];
        }

        return $total_clinician_rated;
    }

    public function get_diagnoses_list_for_export() {
        $defined_diagnoses = DiagnosisOption::where([['patient_diagnosis_id', $this->id], ['diagnosis_id', '>', 0]])
                                ->leftJoin('diagnosis', 'diagnosis.id', '=', 'diagnosis_options.diagnosis_id')
                                ->select('name', 'ICD10 as icd', 'updated_at');

        $free_text_diagnoses = DiagnosisSteps::where([['patient_diagnosis_id', $this->id], ['diagnosis_id', -1]])
                                ->select('free_text->text as name', 'free_text->code as icd', 'updated_at');

        $merged_diagnoses = $defined_diagnoses->union($free_text_diagnoses)->orderBy('updated_at', 'DESC')->get('name','icd')->toArray();

        return $merged_diagnoses;
    }
}
