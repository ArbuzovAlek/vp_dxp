<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiagnosisSymptomWeight extends Model
{
    protected $table = 'diagnosis_symptoms';

    protected $fillable = [
        'diagnosis_id',
        'symptom_id',
        'weight'
    ];

    protected $primaryKey = null;

    public $incrementing = false;

    public $timestamps = false;

    public function symptom()
    {
        return $this->hasOne(Symptom::class, 'id', 'symptom_id');
    }
}
