<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiagnosisSteps extends Model
{
    use SoftDeletes;
   
    protected $table = 'diagnosis_steps';
}
