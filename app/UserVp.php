<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cookie;

class UserVp extends Model
{
    protected $connection = 'mysql_virtual_psychology';
    protected $table = 'users';
    protected $guarded = ['password'];
    protected $casts = [
        'settings' => 'array'
    ];
    public $timestamps = false;

    public static function getOwnUser()
    {   
        $raw_secret = Cookie::get('bingo_auth_VPModelsUser');
        $secret = explode('-', $raw_secret);
        $user = self::where('secret',$secret[0])->first();
        return $user;
    }

    public function getPresentFavoriteAttribute()
    {
        return empty($this->attributes['present_favorite']) ? 'all' : $this->attributes['present_favorite'];
    }

    public function restoreDefaultSettings() {
        $this->present_favorite = 'all';
        $this->settings = [
            'sub' => 1,
            'sub_sub' => 0,
            'diagnose_view' => 'matrix',
        ];
        $this->save();
    }
}
