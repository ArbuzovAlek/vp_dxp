<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Symptom extends Model
{
    public function synonyms()
    {
        return $this->hasMany(SymptomSynonym::class);
    }

    public function weight()
    {
        return $this->hasMany(DiagnosisSymptomWeight::class);
    }

    public function diagnosis()
    {
        return $this->belongsToMany(Diagnosis::class, 'diagnosis_symptoms', 'symptom_id', 'diagnosis_id');
    }
}
