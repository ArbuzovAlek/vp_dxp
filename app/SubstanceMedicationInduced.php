<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubstanceMedicationInduced extends Model
{
    protected $table = 'substance_medication_induced';

    public function diagnosis() {
        return $this->belongsTo('App\Diagnosis');
    }
}
