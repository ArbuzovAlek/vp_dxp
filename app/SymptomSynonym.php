<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SymptomSynonym extends Model
{
    protected $fillable = [
        'name',
        'symptom_id'
    ];

    public function symptom()
    {
        return $this->belongsTo(Symptom::class, 'symptom_id', 'id');
    }
}
