<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SymptomSearchHistory extends Model
{
    protected $table = 'symptoms_search_history';

    protected $fillable = [
        'user_id',
        'term'
    ];
}
