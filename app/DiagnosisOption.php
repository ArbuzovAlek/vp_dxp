<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DiagnosisOption extends Model
{
    use SoftDeletes;

    const OPTION_PRINCIPAL = 'principal';
    const OPTION_PROVISIONAL = 'provisional';
    const OPTION_REASON_FOR_REFERRAL = 'reason for referral';

    protected $fillable = [
        'patient_diagnosis_id',
        'diagnosis_id',
        'options'
    ];

    protected $casts = [
        'options' => 'array'
    ];

    public static function get_option_variants()
    {
        return [
            static::OPTION_PRINCIPAL,
            static::OPTION_PROVISIONAL,
            static::OPTION_REASON_FOR_REFERRAL,
        ];
    }
}
