<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Protocol extends Model
{
    protected $connection = 'mysql_virtual_psychology_rap';
    protected $table = 'protocols';
    protected $guarded = ['password'];
    protected $casts = [
        'LastEdit' => 'datetime',
        'origDate' => 'datetime'
    ];
}
