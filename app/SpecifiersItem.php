<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecifiersItem extends Model
{
   
    protected $table = 'specifiers_items';

    public $timestamps = false;

    public function specify()
    {
        return $this->belongsTo('App\Specify');
    }

    public function diagnosis() {
        return Diagnosis::where('id', $this->specify->diagnosis_id)->first();
    }

}
