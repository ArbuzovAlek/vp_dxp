<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PatientDiagnosisVariables extends Model
{
    use SoftDeletes;

    protected $table = 'patient_diagnosis_variables';

}