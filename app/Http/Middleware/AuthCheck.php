<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;
use Log;
use Crypt;
use App\UserVp;

class AuthCheck
{
  /**
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if (!empty($raw_secret = Cookie::get('bingo_auth_VPModelsUser'))){
        $secret = explode('-', $raw_secret);
        $user = UserVp::where('secret',$secret[0])->first();
        if (!empty($user)) {
            $request->selected_patient = $user->selected_patient;
            $patient = \App\PatientVp::where('id', $user->selected_patient)->first();
            $request->selected_patient_user_id = $patient ? $patient->user_own_id : null;
            $request->selected_diagnosis_option = $user->selected_diagnosis_option;
            $request->own_user = $user;
            view()->share('ownUser', $user);
            return $next($request);
        }
        else return redirect("http://35.224.165.111:8080/");
    }
    else return redirect("http://35.224.165.111:8080/");


  }

}
