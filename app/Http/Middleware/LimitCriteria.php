<?php

namespace App\Http\Middleware;

use App\PatientVp;
use Closure;
use Illuminate\Support\Facades\Session;

class LimitCriteria
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $patient_diagnosis_id = $request->selected_patient;
        $patient = PatientVp::find($patient_diagnosis_id);

        if(!$patient || !$patient->checkCount()) {
            Session::put('page_before_error_criteria', $request->getRequestUri());
            return redirect('/error_criteria');
        }

        return $next($request);
    }
}
