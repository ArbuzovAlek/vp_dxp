<?php

namespace App\Http\Middleware;

use Closure;

class isSelectedDiagnosisOption
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->selected_diagnosis_option) return $next($request);
        else return redirect('/center/diagnosis?error_diagnosis_option=1');
    }
}
