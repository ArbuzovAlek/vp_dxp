<?php

namespace App\Http\Middleware;

use Closure;

class AuthChecker
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $username = config('cheker.username');
        $password = config('cheker.password');

        if ($request->getUser() !== $username && $request->getPassword() !== $password) {
            $headers = array('WWW-Authenticate' => 'Basic');

            return response('Unauthorized', 401, $headers);
        }

        return $next($request);
    }
}
