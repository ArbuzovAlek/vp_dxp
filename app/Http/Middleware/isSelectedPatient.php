<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;
use Log;
use Crypt;
use App\UserVp;

class isSelectedPatient
{
  /**
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if ($request->selected_patient != null) return $next($request);
    else return redirect('/error_patients');
  }

}
