<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\History;
use Cookie;
use Crypt;
use Illuminate\Http\Request;

class DiagnosisController extends Controller
{
    protected $disorder_full_list;
    public function __construct()
    {
        $this->disorder_full_list = Diagnosis::cached_all();
    }


    public function index(Request $request)
    {
        $request->title = 'Classification cluster';
        $diagnosis_view = $request->own_user->settings['diagnose_view'] ?? null;

        if ($diagnosis_view === null) {
            $diagnosis_view = Cookie::get('diagnose-view');
            $this->updateView($request->own_user, $diagnosis_view);

            Cookie::queue(
                Cookie::forget('diagnose-view')
            );
        }

        $template = $diagnosis_view === 'list' ? 'diagnosis.index-list' : 'diagnosis.index';

        return view($template, [
            'diagnosis' => $this->disorder_full_list,
            'layout'    => !$request->ajax()
        ]);

    }

    public function show(Request $request, $id)
    {
        $params = explode(".", $id);
        $diagnosis = Diagnosis::find(end($params));
        $d = $this->disorder_full_list;
        $params_length = count($params);
        for($i=0; $i < $params_length; $i++){
            $d = $d[$params[$i]];
            if($params_length>$i+1)
                $d=$d['children'];
        }
        if(empty($d['children'])){
            return redirect()->action('CriteriaController@index', ['id' => $id]);
        }
        $request->title = $d['name'];
        $history = History::create(['patient_diagnosis_id' => $request->selected_patient, 'title' => $d['name'],'url' => $request->fullUrl()]);
        return view('diagnosis.show',['diagnosis_obj' => $d, 'layout' => !$request->ajax(), 'level' =>$id, 'breadcrumbs' => $params,'diagnosis' => $diagnosis]);
    }

    public function change_view(Request $request){
        $this->updateView($request->own_user, $request->view);

        return redirect('/diagnosis');
    }

    private function updateView($user, $view)
    {
        $settings                  = $user->settings;
        $settings['diagnose_view'] = $view;
        $user->settings            = $settings;
        $user->save();
    }

//     public function get_disorders($diagnosis_result)
//     {
//         $refs = array();
//         $list = array();

// //         $sql = "SELECT item_id, parent_id, name FROM items ORDER BY name";

// // /** @var $pdo \PDO */
// // $result = $pdo->query($sql);

//         foreach ($diagnosis_result as $row)
//         {
//             $ref = & $refs[$row->id];
//             $ref['parent'] = $row->parent;
//             $ref['name']      = $row->name;

//             if ($row->parent == 0)
//             {
//                 $list[$row->id] = & $ref;
//             }
//             else
//             {
//                 $refs[$row->parent]['children'][$row->id] = & $ref;
//             }
//         }
//         return $list;
//     }

}
