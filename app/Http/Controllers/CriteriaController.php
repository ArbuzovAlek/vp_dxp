<?php

namespace App\Http\Controllers;

use App\Criteria;
use App\Diagnosis;
use App\History;
use App\SpecifiersItem;
use App\Specify;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Response;


class CriteriaController extends Controller
{

    public function index(Request $request, $id)
    {
        $user                 = User::current_user();
        $user_favorite        = ($user->present_favorite ?? 'one');//'all';//
        $params               = explode(".", $id);
        $diagnosis_id         = end($params);
        $diagnosis            = Diagnosis::find($diagnosis_id);
        if(count($params) == 1) {
            $params = explode(".",\App\Diagnosis::get_dignosis_path($diagnosis));
        }
        $patient_diagnosis_id = $request->selected_patient;

        if(!$request->ajax() && $request->input('return_to', false)) {
            return redirect()->to($request->input('return_to'))->with(['popup_url' => $request->getRequestUri()]);
        }

        if (in_array($diagnosis->behavior, [
            "major_ncd",
            "major_type",
            "mild_ncd",
            "mild_type",
            "major_type;substance_use",
            "mild_type;substance_use"
        ])) {
            $status_condition = '';
        } elseif (empty($request->return_to)) {
            $status_condition = 'and (ds.status is null or ds.status = "")';
        } else {
            $status_condition = 'and ds.status="related"';
        }
        $criteria_list = DB::select('select c.*,ds.result,ds.free_text from ' . DB::getTablePrefix() . 'criteria c left join ' . DB::getTablePrefix() . 'diagnosis_steps ds on ds.patient_diagnosis_id=' . $patient_diagnosis_id . ' and c.id=ds.criterion_id ' . $status_condition . ' where c.diagnosis_id=' . $diagnosis_id);

        if ($request->return_from_catatonia) {
            $opened_routine_popup = $request->get('opened_routine_popup', []);
            $regex                = '/<a[^>]+href=(?:[\'"])\/diagnosis\/(?<diagnosis_ids>.+?)\/specifiers\/(?<specifier_id>\d.?).+?(?:[\'"])[^>]*>/i';

            foreach ($criteria_list as $criteria_key => $item) {
                if (preg_match_all($regex, $item->name, $result)) {
                    foreach ($result['specifier_id'] as $key => $specifier_id) {
                        if (!in_array($specifier_id, $opened_routine_popup)) {
                            continue;
                        }

                        $criteria_diagnosis_ids = explode('.', $result['diagnosis_ids'][$key]);
                        $criteria_diagnosis_id = end($criteria_diagnosis_ids);
                        $p_variables          = DB::select('select * from ' . DB::getTablePrefix() . 'patient_diagnosis_variables where patient_diagnosis_id=' . $patient_diagnosis_id . ' and diagnosis_id=' . $criteria_diagnosis_id);
                        $variables            = json_decode($p_variables[0]->selections ?? "{}");

                        $specifier          = Specify::find($specifier_id);
                        $specifier_variable = $specifier->variable;
                        $criteria_vars      = $variables->$specifier_variable ?? [];
                        $items              = SpecifiersItem::where([
                            'specify_id' => $specifier_id,
                            'parent'     => -1
                        ])
                            ->select(['var_data'])
                            ->pluck('var_data')
                            ->toArray();

                        if (!empty($criteria_vars) && count(array_intersect($criteria_vars, $items)) === count($items)) {
                            $criteria_list[$criteria_key]->result = 'Yes';
                        }
                    }
                }
            }
        }

        // $criteria_list = DB::table('criteria')
        //     ->leftJoin('diagnosis_steps', function($join) use($patient_diagnosis_id,$ds_status){
        //             $join->on('diagnosis_steps.patient_diagnosis_id', '=', DB::raw("'".$patient_diagnosis_id."'"));
        //             $join->on('criteria.id', '=', 'diagnosis_steps.criterion_id');
        //             $join->on('diagnosis_steps.status', '=', $ds_status); })
        //     ->select('criteria.*', 'diagnosis_steps.result', 'diagnosis_steps.free_text')
        //     ->where('criteria.diagnosis_id', '=', $diagnosis_id)
        //     ->get();
        $nested_criteria = $this->get_nested_criteria($criteria_list, $id, $patient_diagnosis_id, $diagnosis);
        $next_criterion  = '';
        $criterion_page  = 0;
        if ($user_favorite == "one") {
            if (!empty($request->criterion_id) && !empty($nested_criteria[$request->criterion_id])) {
                $key = $request->criterion_id;
                if (isset($request->criterion_page)) {
                    $criterion_page = $request->criterion_page + 1;
                }
            } else {
                $key = key($nested_criteria);
            }
            $one_criterion   = array($key => $nested_criteria[$key]);
            $parent_keys     = array_keys($nested_criteria);
            $next_criterion  = current(array_slice($parent_keys, array_search($key, $parent_keys) + 1));
            $nested_criteria = $one_criterion;
        }
        $request->title = $diagnosis->name;
        $history        = History::create([
            'patient_diagnosis_id' => $request->selected_patient,
            'title'                => $diagnosis->name . ' - Criteria',
            'url'                  => $request->fullUrl()
        ]);
        return view('criteria.index', [
            'criteria'          => $criteria_list,
            'layout'            => !$request->ajax(),
            "diagnosis"         => $diagnosis,
            'breadcrumbs'       => $params,
            'nested_criteria'   => $nested_criteria,
            'present'           => $user_favorite,
            'next_criterion'    => $next_criterion,
            'criterion_page'    => $criterion_page,
            'breadcrumbs_str'   => $id,
            'return_to'         => $request->return_to ? $request->return_to : "",
            'return_type'       => $request->return_type ? $request->return_type : '',
            'nested'            => $request->nested ? $request->nested : '',
            'open_result_modal' => Session::get('open_result_modal'),
            'patient_id'        => $patient_diagnosis_id,
        ]);
    }

    public function is_criteria_have_been_met(Request $request, $id)
    {
        $patient_id = $request->selected_patient;
        $params     = explode(".", $id);
        /** @var Diagnosis $diagnosis */
        $diagnosis  = Diagnosis::find(end($params));
        /** @var Criteria $criteria */
        $criteria   = Criteria::findByDiagnosisAndPatient($patient_id, $diagnosis->id);

        return response()->json([
            'is_criteria_have_been_met' => $diagnosis->is_criteria_have_been_met($criteria)
        ]);
    }

    private function get_nested_criteria($criteria_list, $breadcrumbs, $patient_diagnosis_id, $diagnosis)
    {
        $refs = array();
        $list = array();
        foreach ($criteria_list as $row) {
            $ref             = &$refs[$row->id];
            $ref['parent']   = $row->parent;
            $ref['name']     = $row->name;
            $ref['behavior'] = $row->behavior;
            if ($row->behavior == 'see_m_ncd' && empty($row->result)) {
                $ref['result'] = $diagnosis->m_ncd_result($patient_diagnosis_id);
            } else {
                $ref['result'] = $row->result;
            }
            $ref['free_text'] = $row->free_text;

            if ($row->parent == 0) {
                $list[$row->id] = &$ref;
            } else {
                $refs[$row->parent]['children'][$row->id] = &$ref;
            }
        }
        return $list;
    }

}