<?php

namespace App\Http\Controllers;

use App\Criteria;
use App\Diagnosis;
use App\DiagnosisOption;
use App\History;
use App\PatientVp;
use App\Protocol;
use App\Specify;
use App\PatientDiagnosisVariables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AnalyticsController extends Controller
{
    /** @var Diagnosis diagnosis */
    public $diagnosis;
    public $diagnosis_option;
    public $criteria;
    public $full_criteria_met;
    public $is_free_text = false;
    public $free_text_was_created = false;
    public $patient = false;

    public function __construct(Request $request) {
        /** @var DiagnosisOption diagnosis_option */
        $this->diagnosis_option = DiagnosisOption::find($request->selected_diagnosis_option);

        if ($this->diagnosis_option) {
            $this->is_free_text = $this->diagnosis_option->diagnosis_id === -1;
            $this->free_text_was_created = $this->diagnosis_option->options['is_created'] ?? false;
            $this->patient = PatientVp::where('id', $this->diagnosis_option->patient_diagnosis_id)->first();
            $diagnosis_id = $this->diagnosis_option->options['original_diagnosis_id'] ?? $this->diagnosis_option->diagnosis_id;

            if($diagnosis_id) {
                /** @var Diagnosis diagnosis */
                $this->diagnosis = Diagnosis::where('id', $diagnosis_id)->first();

                if ($this->diagnosis) {
                    /** @var Criteria criteria */
                    $this->criteria = Criteria::findByDiagnosisAndPatient($this->diagnosis_option->patient_diagnosis_id, $this->diagnosis->id);
                    $this->full_criteria_met = $this->diagnosis->is_criteria_have_been_met($this->criteria);
                }
            }
        }

        view()->share('patient', $this->patient);
        view()->share('full_criteria_met', $this->full_criteria_met);
        view()->share('diagnosis', $this->diagnosis);
        view()->share('diagnosis_option', $this->diagnosis_option);
        view()->share('layout', !$request->ajax());
        view()->share('is_free_text', $this->is_free_text);
        view()->share('free_text_was_created', $this->free_text_was_created);
    }

    public function index(Request $request) {
        return view('center.analytics.index');
    }

    public function summary(Request $request) {

        if (!$this->diagnosis) {
            return redirect('/center/analytics');
        }

        $patient = PatientVp::find($this->diagnosis_option->patient_diagnosis_id);
        $last_rap = Protocol::where('clientID', $patient->id)->orderBy('origDate', 'DESC')->first();

        $specifiers_amount = Specify::where('diagnosis_id', $this->diagnosis->id)->count();
        $variables = PatientDiagnosisVariables::where([
            'diagnosis_id' => $this->diagnosis->id,
            'patient_diagnosis_id' => $patient->id
        ])->first();
        $selections = !empty($variables->selections) ? json_decode($variables->selections, JSON_OBJECT_AS_ARRAY) : [];

        $view_data['specifiers_amount'] = ['input' => count($selections), 'total' => $specifiers_amount];
        $view_data['criterion_amount'] = $this->criteria_amount($this->criteria, ['Yes' => 0, 'No' => 0, 'Uncertain' => 0]);
        $view_data['patient'] = $patient;
        $view_data['last_rap_date'] = !empty($last_rap->origDate) ? $last_rap->origDate->format('Y.m.d') : false;

        $criterion_ids = array_reduce($this->criteria, function($carry, $item) {
            $carry[] = $item->id;
            return $carry;
        });

        $subcriteria = Criteria::findByDiagnosisAndPatient($this->diagnosis_option->patient_diagnosis_id, $this->diagnosis->id, $criterion_ids);
        $view_data['subcriterion_amount'] = $this->criteria_amount($subcriteria, ['Yes' => 0, 'No' => 0]);

        return view('center.analytics.summary', $view_data);
    }

    public function recommendations(Request $request) {
        if (!$this->diagnosis) {
            return redirect('/center/analytics');
        }

        $view_data['cluster_diagnoses'] = Diagnosis::where([
            ['id', '!=', $this->diagnosis->id],
            ['parent', '=', $this->diagnosis->parent]
        ])->get();

        $view_data['diagnosis_info'] = $this->diagnosis->text_parent ? $this->diagnosis->parent_text_diagnosis : $this->diagnosis;
        $view_data['additional_info_list'] = Diagnosis::get_additional_info_list();

        // TODO refactoring formal method to some service class
        /** @var SigmaController $sigma_controller */
        $sigma_controller = app(SigmaController::class);
        $formal           = $sigma_controller->get_formal($this->diagnosis, $request->selected_patient, []);

        $formal['diagnosis_id'] = $this->diagnosis->id;
        $formal['assigned']     = $this->diagnosis_option->options['assign_to_patient'];
        $formal['option']       = $this->diagnosis_option->options['option'];

        $view_data['formal'] = $formal;

        if($this->full_criteria_met && $this->diagnosis_option->options['assign_to_patient']) {
            return view('center.analytics.recommendations_true', $view_data);
        } else {
            $view_data['criterion_amount'] = $this->criteria_amount($this->criteria, ['Yes' => 0, 'No' => 0, 'Uncertain' => 0]);
            return view('center.analytics.recommendations_false', $view_data);
        }
    }

    public function diagnosis_history(Request $request) {
        if (!$this->diagnosis) {
            return redirect('/center/analytics');
        }

        $view_data['history'] = History::getHistory(1, null, 'updated_at', 'desc', $this->diagnosis, $this->diagnosis_option->patient_diagnosis_id);
        $view_data['diagnosis_id'] = $this->diagnosis->id;
        $view_data['patient_diagnosis_id'] = $this->diagnosis_option->patient_diagnosis_id;
        return view('center.analytics.diagnosis_history', $view_data);
    }

    public function to_analytics(Request $request) {
        $user = $request->ownUser;
        $last_diagnosis_option = DiagnosisOption::where('user_id', $user->id)->orderBy('id', 'desc')->first();
        if($last_diagnosis_option) {
            $user->selected_diagnosis_option = $last_diagnosis_option->id;
            $user->save();
        }
        
        return redirect()->to('center/analytics');
    }

    private function criteria_amount($criteria, $results) {
        $result = array_merge($results, ['total' => count($criteria)]);
        foreach($criteria as $criterion) {
            if(isset($result[$criterion->result]))
                $result[$criterion->result]++;
        }
        return $result;
    }
}