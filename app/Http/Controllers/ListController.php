<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Specify;
use App\SpecifiersItem;
use App\NCDCode;
use App\MoodCode;
use App\SubstanceMedicationInduced;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\CriteriaController;
use Illuminate\Support\Facades\DB;

class ListController extends Controller
{
    public function get_list(Request $request)
    {   $as_specifier_id=array('135','42','283');
        $all_diagnosis = Diagnosis::orderBy('name')->get(['id', 'name', 'ICD10']);
        $list = array();
        foreach ($all_diagnosis as $diagnos){
          if (!empty($diagnos->ICD10))
            array_push($list,$diagnos);
          elseif (empty(Diagnosis::where('parent',$diagnos->id)->first())){
            $specifiers = Specify::where('diagnosis_id',$diagnos->id)->get();
            if ($specifiers->isEmpty() && in_array($diagnos->id, $as_specifier_id)){
              $diagnos->ICD10 = 'as specifier';
              array_push($list,$diagnos);
            }
            else{
              foreach ($specifiers as $specify) {
                $specify_items = SpecifiersItem::where('specify_id',$specify->id)->get();
                foreach ($specify_items as $specify_item) {
                    $diagnos_for_list = new Diagnosis;
                    $diagnos_for_list->name = $diagnos->name.', '.$specify_item->icd_text;
                    if (empty($specify_item->icd) && in_array($diagnos->id, $as_specifier_id)){
                        $diagnos_for_list->ICD10 = 'as specifier';
                        $diagnos_for_list->id = $diagnos->id;
                        $diagnos_for_list->specify = $specify ->id;
                        $diagnos_for_list->specify_item = $specify_item ->id;
                        array_push($list,$diagnos_for_list);
                    }
                    elseif (!empty($specify_item->icd)) 
                    {
                    $diagnos_for_list->ICD10 = $specify_item->icd;
                    $diagnos_for_list->id = $diagnos->id;
                    $diagnos_for_list->specify = $specify ->id;
                    $diagnos_for_list->specify_item = $specify_item ->id;
                    array_push($list,$diagnos_for_list);
                    }
                }
              }
            }
          }
        }
        return $list;
    }
    

    public function update_diagnosis_list(Request $request) {
        $as_specifier_id=array('135','42','283');
        $list = [];

        $diagnoses = Diagnosis::where($this->getNotNullOrNotEmptyFunction('ICD10'))->orWhereIn('id', $as_specifier_id)->orderBy('name')->get();
        $specifiers_items = SpecifiersItem::where($this->getNotNullOrNotEmptyFunction('icd_2'))->orWhere($this->getNotNullOrNotEmptyFunction('icd'))->get();

        $mood_codes = MoodCode::where(function($query) {
            $query->where($this->getNotNullOrNotEmptyFunction('mild'));
            $query->orWhere($this->getNotNullOrNotEmptyFunction('moderate'));
            $query->orWhere($this->getNotNullOrNotEmptyFunction('severe'));
            $query->orWhere($this->getNotNullOrNotEmptyFunction('with_psychotic'));
            $query->orWhere($this->getNotNullOrNotEmptyFunction('in_partial_remission'));
            $query->orWhere($this->getNotNullOrNotEmptyFunction('in_full_remission'));
            $query->orWhere($this->getNotNullOrNotEmptyFunction('unspecified'));
        })->get();

        $ncd_codes = NCDCode::where($this->getNotNullOrNotEmptyFunction('icd_code'))->get();

        $med_induced = SubstanceMedicationInduced::where(function($query) {
            $query->orWhere($this->getNotNullOrNotEmptyFunction('mild'));
            $query->orWhere($this->getNotNullOrNotEmptyFunction('moderate_or_severe'));
            $query->orWhere($this->getNotNullOrNotEmptyFunction('without'));
        })->get();

        foreach($diagnoses as $diagnosis) {
            $item = new \stdClass;
            $item->id = $diagnosis->id;
            $item->name = $diagnosis->name;
            $item->ICD10 = $diagnosis->ICD10 ?: 'specifier';
            $list[] = $item;
        }

        foreach($specifiers_items as $specifier) {
            $diagnosis = $specifier->diagnosis();
            if(!$diagnosis) continue;
            if($specifier->icd) {
                $item = new \stdClass;
                $item->id = $diagnosis->id;
                $item->name = $diagnosis->name.', '.$specifier->icd_text;
                $item->ICD10 = $specifier->icd;
                $list[] = $item;
            }
            if($specifier->icd_2) {
                $item = new \stdClass;
                $item->id = $diagnosis->id;
                $item->name = $diagnosis->name.', '.$specifier->icd_text;
                $item->ICD10 = $specifier->icd_2;
                $list[] = $item;
            }
        }

        foreach($mood_codes as $code) {
            if($code->mild) {
                $item = new \stdClass;
                $item->id = $code->diagnosis->id;
                $item->name = $code->diagnosis->name.', mild'.', '.$code->episode;
                $item->ICD10 = $code->mild;
                $list[] = $item;
            }
            if($code->moderate) {
                $item = new \stdClass;
                $item->id = $code->diagnosis->id;
                $item->name = $code->diagnosis->name.', moderate'.', '.$code->episode;
                $item->ICD10 = $code->moderate;
                $list[] = $item;
            }
            if($code->severe) {
                $item = new \stdClass;
                $item->id = $code->diagnosis->id;
                $item->name = $code->diagnosis->name.', severe'.', '.$code->episode;
                $item->ICD10 = $code->severe;
                $list[] = $item;
            }
            if($code->with_psychotic) {
                $item = new \stdClass;
                $item->id = $code->diagnosis->id;
                $item->name = $code->diagnosis->name.', with psychotic'.', '.$code->episode;
                $item->ICD10 = $code->with_psychotic;
                $list[] = $item;
            }
            if($code->in_partial_remission) {
                $item = new \stdClass; 
                $item->id = $code->diagnosis->id;
                $item->name = $code->diagnosis->name.', in partial remission'.', '.$code->episode;
                $item->ICD10 = $code->in_partial_remission;
                $list[] = $item;
            }
            if($code->in_full_remission) {
                $item = new \stdClass;
                $item->id = $code->diagnosis->id;
                $item->name = $code->diagnosis->name.', in full remission'.', '.$code->episode;
                $item->ICD10 = $code->in_full_remission;
                $list[] = $item;
            }
            if($code->unspecified) {
                $item = new \stdClass;
                $item->id = $code->diagnosis->id;
                $item->name = $code->diagnosis->name.', unspecified'.', '.$code->episode;
                $item->ICD10 = $code->unspecified;
                $list[] = $item;
            }
        }

        foreach($ncd_codes as $code) {
            $item = new \stdClass;
            $item->id = $code->diagnosis->id;
            $item->name = $code->diagnosis->name.($code->behavioral ? ', '.$code->behavioral : '').($code->probable_possible ? ', '.$code->probable_possible : '');
            $item->ICD10 = $code->icd_code;
            $list[] = $item;
        }

        foreach($med_induced as $code) {
            if($code->mild) {
                $item = new \stdClass;
                $item->id = $code->diagnosis->id;
                $item->name = $code->diagnosis->name.', with mild '.$code->substance.' use';
                $item->ICD10 = $code->mild;
                $list[] = $item;
            }
            if($code->moderate_or_severe) {
                $item = new \stdClass;
                $item->id = $code->diagnosis->id;
                $item->name = $code->diagnosis->name.', with moderate or severe '.$code->substance.' use';
                $item->ICD10 = $code->moderate_or_severe;
                $list[] = $item;
            }
            if($code->without) {
                $item = new \stdClass;
                $item->id = $code->diagnosis->id;
                $item->name = $code->diagnosis->name.', without '.$code->substance.' use';
                $item->ICD10 = $code->without;
                $list[] = $item;
            }
        }


        file_put_contents('json/diagnosis_list.json', json_encode($list));
        return response('OK ', 200);
    } 

    private function getNotNullOrNotEmptyFunction($column_name) {
        return function($query) use($column_name) {
            $query->whereNotNull($column_name);
            $query->where($column_name, '!=', '');
        };
    }

}
