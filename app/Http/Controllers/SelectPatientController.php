<?php

namespace App\Http\Controllers;

use App\PatientVp;
use App\UserVp;
use Guzzle\Http\Client;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Session;

class SelectPatientController extends Controller
{
    public function get_patients(Request $request)
    {
        $user            = UserVp::getOwnUser();
        $page            = $request->get('page', 1);
        $per_page        = $request->get('per_page', 5);
        $order_by        = $request->get('order-by', 'id');
        $order_direction = $request->get('order-direction', 'asc');
        $query           = PatientVp::where('user_id', $user->id);

        if ($order_by === 'criteria') {
            $patients = $query->get();

            if (strtolower($per_page) === 'all') {
                $per_page = $patients->count();
            }

            $offset = ($page * $per_page) - $per_page;

            $patients = $patients->sortBy(function (PatientVp $patient) {
                return $patient->getCriteriaCount();
            }, SORT_NUMERIC, $order_direction === 'desc');

            $patients = new LengthAwarePaginator(
                $patients->slice($offset, $per_page),
                $patients->count(),
                $per_page,
                $page,
                ['path' => '', 'query' => []]
            );
        } else {
            if (strtolower($per_page) === 'all') {
                $per_page = $query->count();
            }

            $query    = $query->orderBy($order_by, $order_direction);
            $patients = $query->paginate($per_page);
        }

        return view('layouts.patients', [
            'patients' => $patients
        ]);
    }

    public function select_patient(Request $request, $id)
    {
        $user    = UserVp::getOwnUser();
        $patient = PatientVp::where('id', $id)->first();

        if ($patient->user_id == $user->id) {
            $user->selected_patient = $patient->id;
            $user->save();
            $page_before_error_criteria = Session::get('page_before_error_criteria');

            if ($request->ajax() && $page_before_error_criteria) {
                Session::forget('page_before_error_criteria');
                return response()->json([
                    'return_to' => $page_before_error_criteria
                ]);
            }

            if ($return_to = $request->get('return_to', false)) {
                return redirect($return_to);
            }
        }
    }
}
