<?php

namespace App\Http\Controllers;

use App\PatientVp;
use Barryvdh\DomPDF\PDF;
use Cookie;
use Illuminate\Http\Request;

class SettingsController extends Controller {

    public const FORMAT_CSV = 'csv';
    
    public const FILENAME_DEMOGRAPHIC_AND_DIAGNOSES = 'demographic_and_diagnoses_';
    public const FILENAME_SEVERITY_SCALE = 'severity_scale_';

    public static function get_export_headers($filename, $format) {
        return [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => "text/$format",
            'Content-Disposition' => "attachment; filename=$filename.$format",
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];
    }
    
    public function index(Request $request) {
        $current_patient = PatientVp::find($request->own_user->selected_patient);
        $severity_scale_available = ($current_patient && $current_patient->get_clinician_rated_data_for_export()) ? true : false;
        return view('center.settings.index', ['layout' => !$request->ajax(), 'severity_scale_available' => $severity_scale_available]);
    }

    public function export_demographic_diagnoses(Request $request) {
        $patient = PatientVp::find($request->own_user->selected_patient);

        $demographic_data = $patient->get_demographic_data_for_export();
        $diagnoses_data = $patient->get_diagnoses_list_for_export();

        //merge arrays to csv format
        $output = implode(',', $demographic_data)."\n".array_reduce($diagnoses_data, function($carry, $item) use ($diagnoses_data) {
            $carry .= '['.$item['name'].'] (['.$item['icd'].'])';
            //add comma only if element is not last
            if($item !== end($diagnoses_data)) $carry .= "\n";
            return $carry;
        });

        $headers = self::get_export_headers(self::FILENAME_DEMOGRAPHIC_AND_DIAGNOSES.$patient->id, self::FORMAT_CSV);
        return response()->make($output, 200, $headers);
    }

    public function export_severity_scale(Request $request) {
        $patient = PatientVp::find($request->input('patient_id', $request->own_user->selected_patient));

        $clinician_rated = $patient->get_clinician_rated_data_for_export();

        $output = '';

        if($clinician_rated) {
            foreach($clinician_rated as $assession) {
                $output .= implode(',', $assession)."\n";
            }
        } else {
            $output = 'no data';
        }

        $headers = self::get_export_headers(self::FILENAME_SEVERITY_SCALE.$patient->id, self::FORMAT_CSV);
        return response()->make($output, 200, $headers);
    }

    public function update_settings(Request $request) {
        $field = $request->input('field');
        $key = $request->input('key', false);
        $value = $request->input('value');
        $user = $request->own_user;

        if($key) {
            $dbField = $user->$field;
            $dbField[$key] = $value;
            $user->$field = $dbField;
        } else
            $user->$field = $value;

        $user->save();
        return response('complete', 200);
    }

    public function restore_defaults(Request $request) {
        $request->own_user->restoreDefaultSettings();

        return redirect('/center/settings')->with(['show_settings_modal' => true]);
    }

    public function print(Request $request)
    {
        /** @var PatientVp $current_patient */
        $current_patient           = PatientVp::find($request->own_user->selected_patient);
        $diagnoses_list_for_export = $current_patient->get_diagnoses_list_for_export();
        $data                      = [
            'patient'   => $current_patient,
            'diagnosis' => $diagnoses_list_for_export
        ];

        if ($request->has('preview-as-html')) {
            return view('center.settings.print', $data);
        }

        /** @var PDF $pdf */
        $pdf = \App::make('dompdf.wrapper');
        $pdf->setOptions([
            'enable_php' => true,
        ]);
        $pdf->getDomPDF()->setPaper([0, 0, 612.50, 794.50], 'portrait');

        $pdf->loadView('center.settings.print', $data);
        return $pdf->stream('print.pdf');
    }

    public function groups_manager() {
        return view('center.settings.groups_manager');
    }

    public function records_cabinet() {
        return view('center.settings.records_cabinet');
    }

}