<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\DiagnosisSteps;
use App\Specify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DiagnosisStepsController extends Controller
{
    public function store(Request $request)
    {
        // if(empty($_POST['patient_diagnosis_id'])){
        //     $patient_diagnosis = new PatientDiagnosis;
        //     $patient_diagnosis->patient_id = $_POST['patient_id'];
        //     $patient_diagnosis->diagnosis_id = $_POST['diagnosis_id'];
        //     $patient_diagnosis->save();
        // }
        // else{
        $patient_diagnosis_id = $request->selected_patient;
        $patient_diagnosis    = $request->selected_patient;
        $params               = explode(".", $request->diagnosis_id);
        $real_id              = end($params);
        $d                    = Diagnosis::find($real_id);
        $status               = ($request->return_to ? 'related' : null);

        if (in_array($d->behavior, ["major_ncd", "major_type", "mild_ncd", "mild_type"])) {
            $status = null;
        }
        if($request->answers) {
            foreach ($request->answers as $key => $value) {
                $diagnosis_steps = DiagnosisSteps::where([
                    'patient_diagnosis_id' => $patient_diagnosis_id,
                    'criterion_id'         => $key,
                    'status'               => $status
                ])->first();
    
                if (empty($diagnosis_steps)) {
                    $diagnosis_steps                       = new DiagnosisSteps;
                    $diagnosis_steps->patient_diagnosis_id = $patient_diagnosis_id;
                    $diagnosis_steps->criterion_id         = $key;
                    $diagnosis_steps->diagnosis_id         = $real_id;
                }
    
                if ($diagnosis_steps->result != $value || $diagnosis_steps->free_text != $request->free_text) {
                    $diagnosis_steps->result    = $value;
                    $diagnosis_steps->free_text = $request->free_text;
    
                    if (
                        !empty($request->return_to) &&
                        !(!empty($request->return_type) && in_array($request->return_type, ["ncd_specify", "ncd_type"]))
                    ) {
                        $diagnosis_steps->status = "related";
                    }
    
                    $diagnosis_steps->save();
                }
            }
        }

        if(!empty($request->next_criterion)){
            return redirect()->action('CriteriaController@index', [
                'id'             => $request->diagnosis_id,
                'criterion_id'   => $request->next_criterion,
                'criterion_page' => $request->criterion_page,
                'return_to'      => $request->return_to ? $request->return_to : ""
            ]);
        }

        $specify = Specify::where('diagnosis_id', $real_id)->orderBy('order_num', 'asc')->first();
        if($specify && !empty($specify->conditions)) {
            $answer = current($request->answers);
            $conditions = json_decode($specify->conditions, true);
            if(isset($conditions[$answer])) {
                if(empty($conditions[$answer])) {
                    $specify = null;
                } else {
                    $specify = Specify::where(['diagnosis_id' => $real_id,'variable' => $conditions[$answer]])->first();
                }
            }
        }
        if (!empty($request->return_to)) {
            if (!empty($request->return_type) && $request->return_type == 'substance') {
                $is_real_diagnostic = $d->get_diagnostic_indication($patient_diagnosis_id);
                $result             = ($is_real_diagnostic ? 'With' : 'Without');

                if (empty($specify) || !$is_real_diagnostic) {
                    return redirect($request->return_to . "?return_from_substance=true&result=" . $result . "&substance_name=" . strtolower($d->name));
                } else {
                    return redirect()->action('SpecifiersController@show', [
                        'diagnosis_id'   => $request->diagnosis_id,
                        'id'             => $specify->id,
                        'return_to'      => $request->return_to,
                        'result'         => $result,
                        'substance_name' => strtolower($d->name),
                        'behavior'       => 'use_substance'
                    ]);
                }
            }
            // elseif($request->return_type== 'ncd_specify'){
            // elseif(strpos($d->behavior,"mild_type") !== false || strpos($d->behavior,"major_type") !==false){
            //     $is_real_diagnostic = $d->get_diagnostic_indication($patient_diagnosis_id);
            //     if(empty($specify)){
            //         $q_char = (parse_url($request->return_to, PHP_URL_QUERY) ? '&' : '?');
            //         return redirect($request->return_to.$q_char."return_from_ncd=true&type=".$d->slice_name()."&result=".$is_real_diagnostic);
            //     }
            //     else{
            //         return redirect()->action('SpecifiersController@show', ['diagnosis_id' => $request->diagnosis_id, 'id' => $specify->id, 'return_to' => $request->return_to, 'result'=>$is_real_diagnostic]);
            //     }
            // }
            // elseif($request->return_type='ncd_type'){
            else if ($d->behavior == "major_ncd" || $d->behavior == "mild_ncd") {
                $is_real_diagnostic = $d->get_diagnostic_indication($patient_diagnosis_id);
                return redirect($request->return_to);
            }
        }

        if(!empty($specify)){
            return redirect()->action('SpecifiersController@show', [
                'diagnosis_id' => $request->diagnosis_id,
                'id'           => $specify->id
            ]);
        }

        if (strpos($d->behavior, "mild_type") !== false || strpos($d->behavior, "major_type") !== false) {
            $specifiers = DB::select('
                SELECT s.id FROM ' . DB::getTablePrefix() . 'specifiers s 
                LEFT JOIN ' . DB::getTablePrefix() . 'behaviors b 
                    ON s.behavior_id=b.id 
                WHERE
                    s.diagnosis_id=' . $d->parent . ' AND 
                    (b.name is null OR b.name <> "ncd_types") 
                ORDER BY order_num');

            if (!empty($specifiers)) {
                $specify_id  = $specifiers[0]->id;
                $breadcrumbs = $params;

                array_pop($breadcrumbs);

                $parent_crumbs = implode('.', $breadcrumbs);

                return redirect()->action('SpecifiersController@show', [
                    'diagnosis_id' => $parent_crumbs,
                    'id'           => $specify_id
                ]);
            }

        }

        return redirect("diagnosis/{$request->get('diagnosis_id')}/criteria/result")->with(['result_in_modal' => true]);

    }

}