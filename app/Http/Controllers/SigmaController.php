<?php

namespace App\Http\Controllers;

use App\Criteria;
use App\Diagnosis;
use App\DiagnosisOption;
use App\DiagnosisSteps;
use App\History;
use App\PatientDiagnosisVariables;
use App\PatientVp;
use App\SpecifiersItems;
use App\Specify;
use App\UserVp;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class SigmaController extends Controller
{
    const FORMALS_PER_PAGE = 5;

    const EXCLUDE_IDS_FROM_DIAGNOSIS_CENTER = [
        135
    ];

    const DIAGNOSIS_IDS_WITH_EXCLUDED_FREE_TEXT_CODE = [
        22
    ];

    public function index(Request $request)
    {
        $patient_diagnosis_id = $request->selected_patient;

        $formals = $this->get_formals($patient_diagnosis_id);
        $history  = History::where('patient_diagnosis_id',$patient_diagnosis_id)->orderBy('created_at', 'desc')->get();
        $request->title = 'Sigma';
        History::create(['patient_diagnosis_id' => $request->selected_patient, 'title' => 'Sigma','url' => $request->fullUrl()]);
        return view('sigma.show',['layout' => !$request->ajax(),'history' => $history, 'formals' =>$formals]);
    }

    private function get_formals($patient_diagnosis_id, $get_all = false)
    {
        $patient = PatientVp::find($patient_diagnosis_id);
        $capitalize_words = $this->capitalize_words();
        $res = DB::select('
            SELECT c.diagnosis_id,c.id criterion,ds.result FROM '.DB::getTablePrefix().'criteria c 
                LEFT JOIN '.DB::getTablePrefix().'diagnosis_steps ds 
                    ON ds.patient_diagnosis_id ='.$patient_diagnosis_id.' AND ds.criterion_id = c.id AND ds.deleted_at IS NULL
                WHERE 
                    c.diagnosis_id in(
                        SELECT DISTINCT diagnosis_id FROM '.DB::getTablePrefix().'diagnosis_steps 
                        WHERE 
                            patient_diagnosis_id='.$patient_diagnosis_id.' 
                            AND 
                            (status <> "related" OR status IS NULL) 
                            AND
                            deleted_at IS NULL
                    ) 
                    AND 
                    c.behavior NOT IN( "crit_as_title","prob_pos_crit")
                ORDER BY ds.updated_at DESC');
        $diagnosis_arr=array();
        $formals =array();
        foreach ($res as $key => $value) {
            if(empty($diagnosis_arr[$value->diagnosis_id]))
                $diagnosis_arr[$value->diagnosis_id] = array('full' => true);
            if(empty($value->result))
                $diagnosis_arr[$value->diagnosis_id]['full'] = false;
        }
        foreach ($diagnosis_arr as $key => $value) {
            if($value['full']){
                $diagnosis_options = DiagnosisOption::where('diagnosis_id', $key)
                    ->where('patient_diagnosis_id', $patient_diagnosis_id)
                    ->first();

                if ($get_all === false && isset($diagnosis_options->options['assign_to_patient']) && $diagnosis_options->options['assign_to_patient'] === false) {
                    continue;
                }

                $diagnosis = Diagnosis::find($key);

                if(strpos($diagnosis->behavior, 'major_ncd') === false && strpos($diagnosis->behavior, 'mild_ncd') === false) {
                    $formals[$key] = $this->get_formal($diagnosis, $patient_diagnosis_id, $capitalize_words);
                    $formals[$key] = array_map(function ($formal) use($diagnosis_options, $patient_diagnosis_id, $diagnosis, $formals, $key, $patient) {
                        $assigned       = null;
                        $option_options = null;
                        $free_text_is_created = false;

                        if ($diagnosis_options) {
                            if (isset($diagnosis_options->options['assign_to_patient'])) {
                                $assigned = $diagnosis_options->options['assign_to_patient'];
                            }

                            if (isset($diagnosis_options->options['option'])) {
                                $option_options = $diagnosis_options->options['option'];
                            }

                            if (isset($diagnosis_options->options['is_created'])) {
                                $free_text_is_created = $diagnosis_options->options['is_created'];
                            }
                        }


                        return array_merge($formal, [
                            'diagnosis_id'         => $diagnosis->id,
                            'assigned'             => $assigned,
                            'patient_id'           => $patient_diagnosis_id,
                            'updated_at'           => $formals[$key][0]['updated_at'],
                            'option'               => $option_options,
                            'option_id'            => $diagnosis_options ? $diagnosis_options->id : '',
                            'options_text'         => $option_options ? '(' . implode(', ', (array)$option_options) . ')' : '',
                            'free_text_is_created' => $free_text_is_created,
                            'multi_icd_text'       => $formals[$key]['text'] ?? '',
                            'patient_user_own_id'  => $patient->user_own_id,
                        ]);
                    }, $formals[$key]['multi_icd'] ?? $formals[$key]);
                }
            }
        }
        return $formals;
    }

    public function add_diagnose_free_text(Request $request){
        $patient_diagnosis_id       = $request->selected_patient;
        $step                       = new DiagnosisSteps;
        $step->patient_diagnosis_id = $patient_diagnosis_id;
        $free_text                  = ['text' => $request->diagnosis, 'code' => $request->ICD];
        $step->free_text            = json_encode($free_text);
        $step->diagnosis_id         = -1;
        $step->save();

        $option                       = new DiagnosisOption;
        $option->patient_diagnosis_id = $patient_diagnosis_id;
        $option->diagnosis_id         = -1;

        $option->options = [
            'option'                => $request->options,
            'assign_to_patient'     => $request->assign,
            'free_text_id'          => $step->id,
            'is_created'            => true,
            'original_diagnosis_id' => null,
        ];

        $max_ordering_item = DiagnosisOption::where('patient_diagnosis_id', $request->selected_patient)
            ->max('ordering');

        if (!empty($max_ordering_item)) {
            $current_ordering = $max_ordering_item + 1;
        } else {
            $current_ordering = 1;
        }

        $option->ordering = $current_ordering;
        $option->save();

        return $step->id;
    }

    public function edit_diagnose_free_text(Request $request) {
        $patient_diagnosis_id = $request->selected_patient;
        if ($request->diagnose_id != -1) {
            $formal = $this->get_formal(Diagnosis::find($request->diagnose_id), $patient_diagnosis_id, $this->capitalize_words());
            $option   = DiagnosisOption::where('patient_diagnosis_id', $request->selected_patient)
                ->where('diagnosis_id', $request->diagnose_id)
                ->first();
            $ordering = $option ? $option->ordering : 0;

            if (!empty($formal)) {
                $formal_text  = strtolower(preg_replace('/\s+/', '', $formal[0]['text']));
                $request_text = strtolower(preg_replace('/\s+/', '', $request->main['diagnosis']));

                if ($formal_text === $request_text && $formal[0]['code'] === $request->main['ICD']) {
                    $option->options = array_merge($option->options, [
                        'option'            => $request->main['options'] ?? null,
                        'assign_to_patient' => isset($request->main['assign']) ? $request->main['assign'] === 'true' : null,
                    ]);

                    $option->save();

                    $criteria_list = Criteria::findByDiagnosisAndPatient($request->selected_patient, $request->diagnose_id);
                    if (!empty($criteria_list)) {
                        DiagnosisSteps::find($criteria_list[0]->step_id)->touch();
                    }

                    return $request;
                }
            }

            $variable = PatientDiagnosisVariables::where('patient_diagnosis_id', $request->selected_patient)
                ->where('diagnosis_id', $request->diagnose_id)
                ->first();

            if ($variable) {
                $variable->delete();
            }

            /** @var DiagnosisSteps[] $original_steps */
            $original_steps = DiagnosisSteps::where('patient_diagnosis_id', $request->selected_patient)
                ->where('diagnosis_id', $request->diagnose_id)
                ->get();

            /** @var DiagnosisOption[] $original_options */
            $original_options = DiagnosisOption::where('patient_diagnosis_id', $request->selected_patient)
                ->where('diagnosis_id', $request->diagnose_id)
                ->get();

            $step                       = new DiagnosisSteps;
            $step->patient_diagnosis_id = $patient_diagnosis_id;
            $free_text                  = ['text' => $request->main['diagnosis'], 'code' => $request->main['ICD']];
            $step->free_text            = json_encode($free_text);
            $step->diagnosis_id         = -1;
            $step->save();
    
            $option                       = new DiagnosisOption;
            $option->patient_diagnosis_id = $patient_diagnosis_id;
            $option->diagnosis_id         = -1;
            $options                      = [
                'option'                => $request->main['options'] ?? null,
                'assign_to_patient'     => isset($request->main['assign']) ? ($request->main['assign'] === 'true' ? true : false) : null,
                'free_text_id'          => $step->id,
                'original_diagnosis_id' => $request->diagnose_id,
                'is_created'            => false,
                'original_steps_ids'    => $original_steps->pluck('id')->toArray(),
                'original_options_ids'  => $original_options->pluck('id')->toArray(),
            ];
            $option->options              = $options;
            $option->ordering             = $ordering;
            $option->save();

            foreach ($original_steps as $original_step) {
                $original_step->delete();
            }

            foreach ($original_options as $original_step) {
                $original_step->delete();
            }
        } else {
            $step            = DiagnosisSteps::where('id', $request->free_text_id_step)->first();
            $free_text       = ['text' => $request->main['diagnosis'], 'code' => $request->main['ICD']];
            $step->free_text = json_encode($free_text);
            $step->save();

            /** @var DiagnosisOption[] $options */
            $options = DiagnosisOption::where('patient_diagnosis_id', $request->selected_patient)
                ->where('diagnosis_id', -1)
                ->get();

            foreach ($options as $option) {
                if ($option->options['free_text_id'] == $request->free_text_id_step) {
                    break;
                }
            }

            $option->options = array_merge($option->options, [

                'option'            => $request->main['options'] ?? null,
                'assign_to_patient' => isset($request->main['assign']) ? ($request->main['assign'] === 'true' ? true : false) : null,
                'free_text_id'      => $step->id
            ]);
            $option->save();
        }
        return $request;
    }

    public function center_diagnosis(Request $request, $reload_table = false,$selected_diagnos = 0, $free_text_id_step = 0){
        $patient_diagnosis_id = $request->selected_patient;
        $formals              = $this->get_formals_center($patient_diagnosis_id);
        usort($formals, function($a,$b){
            return ($b['order']-$a['order']);
        });

        $page     = $request->get('page', 1);
        $per_page = $request->get('per_page', self::FORMALS_PER_PAGE);
        $formals  = $this->paginate_formals($formals, $page, $per_page);

        if ($reload_table === 'reload_table') {
            return response()->view('center.diagnosis.table', [
                'formals'           => $formals,
                'selected_diagnos'  => $selected_diagnos,
                'free_text_id_step' => $free_text_id_step,
                'patient_id'        => $patient_diagnosis_id
            ]);
        }

        return view('center.diagnosis.diagnosis', ['formals' => $formals, 'patient_id' => $patient_diagnosis_id]);
    }

    public function del_diagnose(Request $request, $responsable = true){
        if ($request->diagnose_id != -1){
            $variable = PatientDiagnosisVariables::where('patient_diagnosis_id', $request->selected_patient)->where('diagnosis_id',$request->diagnose_id)->first();
            if ($variable) $variable->forceDelete();
            DB::table('diagnosis_steps')->where('patient_diagnosis_id',$request->selected_patient)->where('diagnosis_id',$request->diagnose_id)->delete();
            DB::table('diagnosis_options')->where('patient_diagnosis_id',$request->selected_patient)->where('diagnosis_id',$request->diagnose_id)->delete();
        }
        else {
            DB::table('diagnosis_steps')->where('patient_diagnosis_id',$request->selected_patient)->where('diagnosis_id',-1)->where('id',$request->free_text_id_step)->delete();
            $options = DiagnosisOption::where('patient_diagnosis_id',$request->selected_patient)->where('diagnosis_id',-1)->get();
            foreach ($options as $option){
                if ($option->options['free_text_id'] == $request->free_text_id_step) $option->forceDelete();
            }
        }
        if ($responsable)
        return response()->json(['success' => 'success'], 200);
    }

    public function set_ordering (Request $request){
        $direction = $request->direction;
        $patient_diagnosis_id = $request->selected_patient;
        $diagnos_id = $request->diagnosis_id;
        $patient_options =DiagnosisOption::where('patient_diagnosis_id',$patient_diagnosis_id)->where('options->assign_to_patient',true)->orderBy('ordering','asc')->get();
        if ($diagnos_id != -1){
            foreach($patient_options as $key => $option){
                if ($option->diagnosis_id == $diagnos_id){
                    $position_key = $key;
                    break;
                }
            }
        }
        else {
            foreach($patient_options as $key => $option){
                if (isset($option->options['free_text_id']) && ($option->options['free_text_id'] == $request->free_text_id_step)){
                    $position_key = $key;
                    break;
                }
            }
        }

        if ($direction == 'down' && $position_key != 0){
            $patient_options[$position_key]->ordering = $patient_options[$position_key]->ordering - 1;
            $patient_options[$position_key]->save();
            if (isset($patient_options[$position_key-1])){
            $patient_options[$position_key-1]->ordering = $patient_options[$position_key]->ordering + 1;
            $patient_options[$position_key-1]->save();
            }
        }
        elseif (($direction == 'up') && ($position_key != (count($patient_options)-1))) {
            $patient_options[$position_key]->ordering = $patient_options[$position_key]->ordering + 1;
            $patient_options[$position_key]->save();
            if (isset($patient_options[$position_key+1])){
            $patient_options[$position_key+1]->ordering = $patient_options[$position_key]->ordering - 1;
            $patient_options[$position_key+1]->save();
            }
        }
        return 'sucess';

    }

    public function get_order ($patient_diagnosis_id,$diagnosis) {
            $variable = DiagnosisOption::where('patient_diagnosis_id',$patient_diagnosis_id)->where('diagnosis_id',$diagnosis->id)->first();
            if (!empty($variable)) $order=$variable->ordering;
            else $order=null;
            return $order;
    }

    public function get_formals_center($patient_diagnosis_id){

        $capitalize_words = $this->capitalize_words();
        $res = DB::select('
            SELECT c.diagnosis_id,c.id criterion,ds.result 
            FROM '.DB::getTablePrefix().'criteria c 
                LEFT JOIN '.DB::getTablePrefix().'diagnosis_steps ds 
                    ON ds.patient_diagnosis_id ='.$patient_diagnosis_id.' AND ds.criterion_id = c.id AND ds.deleted_at IS NULL
            WHERE c.diagnosis_id IN(
                SELECT DISTINCT diagnosis_id FROM '.DB::getTablePrefix().'diagnosis_steps 
                WHERE 
                    patient_diagnosis_id='.$patient_diagnosis_id.' 
                    AND
                    (status <> "related" OR status IS NULL)
                    AND
                    deleted_at IS NULL
                ) 
                AND 
                c.behavior NOT IN("crit_as_title","prob_pos_crit") 
                ORDER BY ds.updated_at DESC
        ');
        $diagnosis_arr=array();
        $formals =array();
        foreach ($res as $key => $value) {
            if(empty($diagnosis_arr[$value->diagnosis_id]))
                $diagnosis_arr[$value->diagnosis_id] = array('full' => true);
            if(empty($value->result))
                $diagnosis_arr[$value->diagnosis_id]['full'] = false;
        }
        foreach ($diagnosis_arr as $key => $value) {
            if($value['full']){
                $diagnosis_options = DiagnosisOption::where('diagnosis_id', $key)
                    ->where('patient_diagnosis_id', $patient_diagnosis_id)
                    ->first();

                if (empty($diagnosis_options->options['assign_to_patient'])) {
                    continue;
                }

                $diagnosis = Diagnosis::find($key);

                if(strpos($diagnosis->behavior, 'major_ncd') === false && strpos($diagnosis->behavior, 'mild_ncd') === false){
                    $formals[$key]                = $this->get_formal($diagnosis, $patient_diagnosis_id, $capitalize_words);
                    $formals[$key]['diagnose_id'] = $key;

                    if($diagnosis_options) {
                        $formals[$key]['assigned']             = $diagnosis_options->options['assign_to_patient'];
                        $formals[$key]['option']               = $diagnosis_options->options['option'];
                        $formals[$key]['option_id']            = $diagnosis_options->id;
                        $formals[$key]['free_text_is_created'] = $diagnosis_options->options['is_created'] ?? false;

                        if (isset($formals[$key][0])) {
                            $formals[$key][0]['options_text'] = $diagnosis_options->options['option'] ? '(' . implode(', ', (array) $diagnosis_options->options['option']) . ')' : '';
                        }
                    }

                    if (empty($formals[$key]['order'])) {
                        $formals[$key]['order'] = $this->get_order($patient_diagnosis_id, $diagnosis);
                    }
                }

            }
        }
        $patient_user_own_id = PatientVp::find($patient_diagnosis_id)->user_own_id;
        $free_text_formals = $this->get_formal_with_free_text($patient_diagnosis_id, $patient_user_own_id);
        $free_text_formals = array_filter($free_text_formals, function ($formal) {
            return $formal['assigned'] === true;
        });

        $formals = array_merge($formals, $free_text_formals);

        $formals = array_filter($formals, function ($item) {
            $needle = $item['diagnose_id'] ?? 0;
            return !in_array($needle, static::EXCLUDE_IDS_FROM_DIAGNOSIS_CENTER, true) && (!empty($item[0]['code']) || !empty($item['multi_icd'][0]['code']));
        });

        return $formals;
    }

    public function get_formal_with_free_text($patient_diagnosis_id, $patient_user_own_id = false){
        $options = DiagnosisOption::where('patient_diagnosis_id',$patient_diagnosis_id)->where('diagnosis_id',-1)->get();
        $res=array();
        foreach ($options as $option){
            if (isset($option->options['free_text_id'])) array_push($res, ['id'=>$option->options['free_text_id'], 'order'=>$option->ordering]);
        }
        $steps=array();
        foreach ($res as $option){
            $step=DiagnosisSteps::where('id',$option['id'])->first();
            $step->order = $option['order'];
            array_push($steps,$step);
        }
        $formals = array();   
        foreach ($steps as $key => $step ){
            $free_text            = json_decode($step->free_text);
            $assigned             = null;
            $option_options       = null;
            $free_text_is_created = false;

            $option = $options->first(function ($option) use($step) {
                return $option->options['free_text_id'] === $step->id;
            });

            if ($option) {
                if (isset($option->options['assign_to_patient'])) {
                    $assigned = $option->options['assign_to_patient'];
                }

                if (isset($option->options['option'])) {
                    $option_options = $option->options['option'];
                }

                if (isset($option->options['is_created'])) {
                    $free_text_is_created = $option->options['is_created'];
                }
            }


            $code = $free_text->code;
            $text = $free_text->text;

            $formals[] = [
                [
                    'code'         => $code,
                    'text'         => $text,
                    'options_text' => $option_options ? '(' . implode(', ', (array)$option_options) . ')' : '',
                ]
            ];

            $formals[$key]['diagnose_id']          = -1;
            $formals[$key]['free_text_id_step']    = $step->id;
            $formals[$key]['order']                = $step->order;
            $formals[$key]['updated_at']           = (string)$step->updated_at;
            $formals[$key]['patient_id']           = $step->patient_diagnosis_id;
            $formals[$key]['diagnosis_id']         = $step->diagnosis_id;
            $formals[$key]['assigned']             = $assigned;
            $formals[$key]['option']               = $option_options;
            $formals[$key]['option_id']            = $option ? $option->id : '';
            $formals[$key]['free_text_is_created'] = $free_text_is_created;
            $formals[$key]['patient_user_own_id']  = $patient_user_own_id;
        }

        return $formals;
    }
    public function get_formal($diagnosis,$patient_diagnosis_id,$cwords,$order_flag = false)
    {
        $code = $diagnosis->ICD10;
        $formal = array();
        $text = '';
        $associated = '';
        $and = '';
        $multiple_icd = array();
        $multiple_icd_codes = false;
        $variables = array();

        $criteria = Criteria::findByDiagnosisAndPatient($patient_diagnosis_id, $diagnosis->id);
        // return formal if not all criteria = yes
        if (!$diagnosis->is_criteria_have_been_met($criteria)) {
            $formal[0] = [
                'code' => '',
                'text' => 'Full criteria for ' . $diagnosis->name . ' have not been met',
                'updated_at' => $criteria[0]->updated_at
            ];
            return $formal;
        }
        // return formal if disorder is 'Other'
        if (count($criteria) == 1 && $criteria[0]->behavior == 'free_text') {
            $text = $criteria[0]->free_text;
            // $formal[0] = array('code' => $code, 'text' => $diagnosis->name.', '.$text);
            // return $formal;
        }

        $specifiers = $diagnosis->specifiers;
        if(!empty($specifiers) && count($specifiers)>0)
        {
            $variables = $this->get_patient_diagnosis_variables($diagnosis,$patient_diagnosis_id);
            $prev_specifier_last_item = null;
            /** @var Specify[] $specifiers */
            $specifiers = Specify::where('diagnosis_id', $diagnosis->id)
                 ->leftJoin('behaviors', 'specifiers.behavior_id', '=', 'behaviors.id')
                 ->select(
                     'specifiers.*',
                     'behaviors.name as behavior_name'
                 )
                 ->get();
            foreach ($specifiers as $key => $specifier) {
                // $var = $specifier->variable;

                if ($specifier->parent) {
                    $specifier = $this->get_parent($specifier);
                }

                $var        = $specifier->variable;
                $selections = $variables->$var ?? null;

                $is_require               = $specifier->is_require;
                $require_severity         = $specifier->require_severity($variables);
                $require_alcohol_type     = $specifier->require_alcohol_type($variables);
                $require_delirium         = $specifier->require_delirium($variables, $diagnosis);
                $require_bipolar          = $specifier->require_bipolar($variables, $diagnosis);
                $require_bipolar2         = $specifier->require_bipolar2($variables, $diagnosis);
                $require_major_depressive = $specifier->require_major_depressive($variables, $diagnosis);
                $is_select_from_group     = $specifier->is_select_from_group($specifiers, $variables);
                if (
                    (
                        $is_require &&
                        empty($selections) &&
                        $require_severity &&
                        $require_alcohol_type &&
                        $require_delirium &&
                        $require_bipolar &&
                        $require_bipolar2 &&
                        $require_major_depressive
                    ) ||
                    (
                        $specifier->behavior_name == 'least_one_of_group' && !$is_select_from_group
                    )
                ) {
                    $formal[0] = array('code' => '','text' => 'Full criteria for '.$diagnosis->name.' have been met.', 'updated_at' => $criteria[0]->updated_at);
                    return $formal;
                } else {
                    // $result = json_decode($specifier->result);
                    $var_items = NULL;
                    if (!empty($selections)) {
                        if (gettype($selections) == 'string') {
                            $var_items = addslashes($selections);
                        } elseif (gettype($selections) == 'array') {
                            $var_items = join("','", $selections);
                        }
                    // elseif (!empty($result->clinitian_rated)) {
                    //     $ids_arr=array();
                    //     foreach ($result->clinitian_rated as $k => $v) {
                    //         array_push($ids_arr, $v);
                    //         $ids .= join("','",$ids_arr);
                    //     }
                    // }

                    }
                    // $ids = ($result->item_id ?? (!empty($result->item_ids) ? join("','",$result->item_ids) : NULL));
                    $specifiers_items = DB::select("select * from ".DB::getTablePrefix()."specifiers_items where specify_id=".$specifier->id." and var_data in('$var_items') order by specify_id");
                    $count_items = count($specifiers_items);
                    if($count_items && $specifier->groups)
                        $multiple_icd[$specifier->id]= array();

                    switch ($specifier->behavior_name) {
                        case 'free_text_if_yes':
                        case 'with_disorder_of':
                            if($diagnosis->behavior == 'another_medical'){
                                // $medicals = $this->get_another_medical_formal($selections);
                                // array_push($formal,$medicals);
                            }
                            elseif($diagnosis->behavior == 'associated_input' && !in_array($diagnosis->id,[13,22,251])){
                                list($associated_formal,$associated_text) = $this->get_free_text_associated($specifier,$variables);
                                if($associated_text)
                                    $text .= ' '.$associated_text;
                                // if(!empty($selections)) {
                                //     foreach ($selections as $k1 => $v1) {
                                //         if(strstr($k1, 'free-text-')){
                                //             $associated .= $and.($v1->name ?? null);
                                //             $and = ' and with ';
                                //         }
                                //     }
                                // }
                            }
                            else {
                                if(!empty($selections)) {
                                    if(is_string($selections)) {
                                        $associated .= $selections;
                                    } else {
                                        foreach ($selections as $k1 => $v1) {
                                            if(strstr($k1, 'free-text-')) {
                                                // $associated .= ($v1->name ? '<span class="inline-apa-code">'.$v1->code.'</span> '.$v1->name.";\t" : null);
                                                switch($diagnosis->id) {

                                                }

                                                if (in_array($diagnosis->id, static::DIAGNOSIS_IDS_WITH_EXCLUDED_FREE_TEXT_CODE, true)) {
                                                    $associated .= ($v1->name ? $and . $v1->name : null);

                                                    $and = ' and with ';
                                                } else {
                                                    $associated .= ($v1->name ? $and.(!empty($v1->code) ? $v1->code . ' ' : null).$v1->name : null);
                                                    $and = '; and with ';
                                                }



                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        case 'standart_input':
                        case 'behavioral_input':
                            $text .= (empty($selections) ? null : "\t").$selections;
                            break;
                        case "medication_name":
                            preg_match('/(^.*)(\-induced.*)$/i', $diagnosis->name, $induce_match);
                            if(!empty($selections)) {
                                if(count($induce_match)==3) {
                                    $diagnosis->name = str_replace($induce_match[1], $selections, $diagnosis->name);
                                }
                            } else {
                                if(count($induce_match)==3) {
                                    $diagnosis->name = '[name of the medication]' . str_replace($induce_match[1], $selections, $diagnosis->name);
                                }
                            }
                            break;
                        case 'psycho_stressor':
                            if(!empty($variables->with_psy_stressor) && $variables->with_psy_stressor =='1')
                                $text .= ', '.$variables->psycho_stressor;
                            break;
                        case 'show_catatonia_criteria':
                        case 'catatonia':
                            if($selections=="Yes")
                                $catatonia = true;
                                // $catatonia = array('code' => 'F06.1', 'text' => 'catatonia associated with '.strtolower($diagnosis->name));
                            break;
                            case 'bi_with_catatonia':
                            $specify = Specify::find($specifier->id);
                                 if($specify->catatonia_result($patient_diagnosis_id, $diagnosis->id) == 'Yes')
                                    $catatonia = true;
                                break;
                        case 'with_use_severity':
                            if(!$specifier->require_severity($patient_diagnosis_id,$variables)){
                                break;
                            }
                        case 'substance_name':
                        case 'substance_use_disorder':
                        case 'with_use_severity':
                        case 'delirium':
                        case 'small_substance_name':
                        case 'prescribed_medication':
                            // Temporary implementation for use_disorder specifiers
                            foreach ($specifiers_items as $k => $v) {
                                if(!empty($v->icd_text)){
                                    if(!empty($v->icd))
                                        $code = (isset($variables->{'use_disorder'}) && $variables->{'use_disorder'} == "with" && !empty($v->icd_2) ? $v->icd_2 : $v->icd);

                                    $exclude_specifiers_items = [
                                        103, 113, 111, // in Cannabis

                                    ];

                                    if (!in_array($v->id, $exclude_specifiers_items)) {
                                        $text .= ', '.$v->icd_text;
                                    }

                                }
                            }
                            break;
                        default:
                            // if($count_items > 1){
                            $prev_specifier_icd = null;
                            $first_code = null;
                            foreach ($specifiers_items as $k => $v) {
                                if ($specifier->groups) { //specific learning disorder
                                    $multiple_icd[$specifier->id]['code'] = $v->icd;

                                    if (!empty($multiple_icd[$specifier->id]['text'])) {
                                        $multiple_icd[$specifier->id]['text'] .= ' and ';
                                    } else {
                                        $multiple_icd[$specifier->id]['text'] = '';
                                    }

                                    $multiple_icd[$specifier->id]['text'] .= $v->icd_text;
                                    $multiple_icd_codes                   = true;
                                } elseif (!empty($v->icd_text)) {
                                    if (!empty($v->icd)) {
                                        if (
                                            (
                                                isset($variables->{'remission'}) && !empty($variables->{'remission'}) ||
                                                (
                                                    isset($variables->{'disturbance'}) && $variables->{'disturbance'} == "1"
                                                )
                                            ) &&
                                            !empty($v->icd_2)
                                        ) {
                                            $code = $v->icd_2;
                                        } else {
                                            $code = $v->icd;
                                        }

                                        if ($prev_specifier_last_item && !strpos($v->icd, ".")) {
                                            $code = $prev_specifier_last_item->icd . "." . $code;
                                        }

                                        if ($first_code === null) {
                                            $first_code = $code;
                                        }
                                    }
                                    if ($prev_specifier_icd === null) {
                                        if ($v->id !== 234) {
                                            $text .= (!empty($text) ? ', ' : null) . ($specifier->multi_select ? ($k > 0 ? "\tand\t" : null) : null) . $v->icd_text;
                                        }
                                    } else {
                                        $text .= (!empty($text) ? ', ' : null) . ($specifier->multi_select ? ($k > 0 ? "\tand\t" : null) . '<span class="inline-apa-code">' . $code . "</span>\t" : null) . $v->icd_text;
                                    }

                                }
                                $prev_specifier_icd = $v->icd;
                            }

                            if ($first_code !== null) {
                                $code = $first_code;
                            }

                            if(!empty($multiple_icd[$specifier->id])) {
                                $multiple_icd[$specifier->id]['s_text'] = strtolower($specifier->text);
                            }
                        break;
                    }           
                    $prev_specifier_last_item = end($specifiers_items);
                }

            }
        }
        if($multiple_icd_codes){
            $formal= array();
            $formal['text'] = $text;
            foreach ($multiple_icd as $k => $v) {
                $m_text =  strtr(strtolower($diagnosis->name.($v['s_text'] ? ', '.$v['text'] : null)),$cwords);
                $arr = array('code' => $v['code'],'text' => $m_text);
                if(empty($formal['multi_icd']))
                    $formal['multi_icd'] = array();
                array_push($formal['multi_icd'], $arr);
            }
        }
        else
        {
            switch($diagnosis->id) {
                case 22:
                    $associated  = (empty($associated) ? '' : '&nbsp;associated with '.$associated);
                    break;
                default:
                    $associated  = (empty($associated) ? '' : '; associated with '.$associated);
            }
            if($diagnosis->behavior != 'adult_abuse') {
                $associated = trim($associated);
                switch($diagnosis->id) {
                    case 132 : // Anxiety -> Specific Phobia
                    case 58 :
                    case 256 :
                    case 269 :
                        $text       = strtr(
                            ((!empty($diagnosis->name) && !empty($associated) ? "" : null) . (
                                substr($associated, -1, 1) == ";" ? (empty($text) ? $associated : substr($associated, 0,
                                    strlen($associated) - 1)) : $associated) . (!empty($text) ? $text : null)), $cwords
                            );
                        break;
                    default : 
                        $text       = strtr(
                            (strtolower($diagnosis->name) . (!empty($diagnosis->name) && !empty($associated) ? "" : null) . (
                                substr($associated, -1, 1) == "," ? (empty($text) ? $associated : substr($associated, 0,
                                    strlen($associated) - 1)) : $associated) . (!empty($text) ? (substr($text,
                                    0, 1) != "," ? ",\t" . $text : "\t" . $text) : null)), $cwords
                        );
                }

                $formal = [
                    0 => [
                        'code' => $code,
                        'text' => $text
                    ]
                ];
            }
            else {
                $formal = array(0 => array('code' => $code,'text' => strtr(strtolower(str_replace(', ', '',$text)),$cwords)));
            }
            // array_push($formal,array('code' => $code,'text' => strtolower($diagnosis->name.$associated.$text)));
            if (strpos($diagnosis->behavior, 'substance_use') !== false)
                $formal[0] = $this->get_substance_formal($diagnosis, $patient_diagnosis_id, $variables, $text, $cwords);
            elseif($diagnosis->behavior == 'delirium')
                $formal[0] = $this->get_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            elseif (strpos($diagnosis->behavior, 'mild_type') !==false) 
                $formal[0] = $this->get_mild_ncd_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            elseif (strpos($diagnosis->behavior, 'major_type') !==false) 
                $formal[0] = $this->get_major_ncd_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            // elseif(strpos($diagnosis->behavior,'bipolar') !== false)
            elseif($diagnosis->behavior == 'bipolar')
                $formal[0] = $this->get_bipolar_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            elseif($diagnosis->behavior == 'major_depressive')
                $formal[0] = $this->get_major_depressive_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            if (strpos($diagnosis->behavior, 'another_medical') !==false)
                $formal = $this->get_another_medical_formal($formal,$diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
            if(!empty($associated_formal))
                $formal = array_merge($associated_formal,$formal);
            // elseif (strpos($diagnosis->behavior, 'associated_input') !== false)
            //     $formal = $this->get_associated_formal($formal,$diagnosis,$patient_diagnosis_id,$variables,$text);
            if(!empty($catatonia)){
                // $formal[0]['text'] .= $this->get_catatonia_formal($diagnosis);
                //$formal[1] = $catatonia;
                $catatonia_formal = $this->get_catatonia_formal($diagnosis,$cwords);
                // array_push($formal, $catatonia);
                array_push($formal, $catatonia_formal);

            }  
        }
        $formal[0]['updated_at'] = $criteria[0]->updated_at;

        return  $formal;
    }


    private function is_select_from_group($specifier,$specifiers,$selections)
    {
        $s_var = $specifier->variable;
        foreach($specifiers as $k=>$v){
            if(!empty($v->groups) && $v->groups == $specifier->groups && !empty($selections)){
                $s_var = $v->variable;
                if(!empty($selections->$s_var))
                    return true;
            }
        }
        return false;
    }

    private function get_bipolar_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text,$cwords)
    {
        $code ='';
        $column='mild';
        $episode = str_replace(['_current','_recent'], '', $variables->episode_type);
        $variables->psychotic = isset($variables->psychotic) ? $variables->psychotic : ''; 
        $variables->severity = isset($variables->severity) ? $variables->severity : '';
        $variables->remission = isset($variables->remission) ? $variables->remission: '';
        if(in_array($variables->episode_type, ['manic_current','depressive_current']))
            $column = ($variables->psychotic=='yes' ? 'with_psychotic' : $variables->severity);
        elseif($variables->episode_type=='unspecified')
            $column = 'unspecified';
        elseif(in_array($variables->episode_type, ['manic_recent','hypomanic_current','hypomanic_recent','depressed_recent'])){
            $column = $variables->remission;
        }
        $res = DB::select('select * from '.DB::getTablePrefix().'mood_codes where episode="'.$episode.'"');
        if(!empty($res)){
            $res = $res[0];
            $code=$res->$column;
        }
        $formal = array('code'=>$code, 'text'=>strtolower($icd_text));
        return $formal;
    }

    private function get_major_depressive_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text,$cwords)
    {
        $code ='';
        $column='mild';
        $episode = $variables->single_recurrent;
        $variables->psychotic = isset($variables->psychotic) ? $variables->psychotic : ''; 
        $variables->severity = isset($variables->severity) ? $variables->severity : '';
        $variables->remission = isset($variables->remission) ? $variables->remission: '';
        if($variables->current=='yes')
            $column = ($variables->psychotic=='yes' ? 'with_psychotic' : $variables->severity);
        elseif($variables->current=='no')
            $column = $variables->remission;
        $res = DB::select('select * from '.DB::getTablePrefix().'mood_codes where episode="'.$episode.'"');
        if(!empty($res)){
            $res = $res[0];
            $code=$res->$column;
        }
        $formal = array('code'=>$code, 'text'=>strtolower($diagnosis->name . ', '.$icd_text));
        return $formal;
    }

    private function get_substance_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text,$cwords)
    {
        $variables->substance_class = (!empty($variables->substance_class) ? $variables->substance_class : '');
        $variables->use_disorder = (!empty($variables->use_disorder) ? strtolower($variables->use_disorder) : '');
        $variables->severity = (!empty($variables->severity) ? strtolower($variables->severity) : null);
        $code='';
        $name = ($variables->substance_class ? ucfirst($variables->substance_class) : '');
        if(!empty($variables->use_disorder) && $variables->use_disorder == 'with' && $variables->severity)
            $severity = ($variables->severity == 'mild' ? 'mild' : ($variables->severity=='moderate' || $variables->severity=='severe' ? 'moderate_or_severe' : ''));
        else
            $severity = 'without';
        $n_name = (!empty($variables->alcohol_type) ? $variables->alcohol_type : $name);
        $res = DB::select('select * from '.DB::getTablePrefix().'substance_medication_induced where substance="'.$n_name.'" and diagnosis_id='.$diagnosis->id);
        if(!empty($res))
            $code = $res[0]->$severity;
        $substance_name = (!empty($variables->substance_input) ? $variables->substance_input : $name);
        if($diagnosis->behavior == 'delirium')
            $disorder_name = strtolower($substance_name.' intoxication delirium');
        else
            $disorder_name = strtolower(str_replace('Substance/Medication',$substance_name,$diagnosis->name));
        if($variables->use_disorder == "with")
            $text = $variables->severity. ' '.$substance_name.' use disorder with '.$disorder_name;
        else
            $text = $disorder_name;
        $formal = array("code"=>$code,"text"=>$text.'; '.$icd_text);
        return  $formal;
   }

    private function get_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords)
    {
        $formal = array("code"=>"000","text"=>"Delirium in developing");
        switch ($variables->delirium_whether) {                
            case 'substance intoxication delirium':
                $formal = $this->get_substance_formal($diagnosis,$patient_diagnosis_id,$variables,$text,$cwords);
                break;
            case 'substance withdrawal delirium':
                $formal = $this->get_withdrawal_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$text);
                break;
            case 'medication-induced delirium':
                $formal = $this->get_medication_induced_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$text);
                break;
            case 'delirium due to another medical condition':
                $formal = $this->get_delirium_due_another_formal($diagnosis,$patient_diagnosis_id,$variables,$text);
                break;
            case 'delirium due to multiple etiologies':
                $formal = $this->get_delirium_due_multiple_formal($diagnosis,$patient_diagnosis_id,$variables,$text);
                break;
            default:
                break;

        }
        return $formal;

    }

    private function get_withdrawal_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text)
    {
        $variables->substance_class = (!empty($variables->substance_class) ? $variables->substance_class : '');
        $variables->use_disorder = (!empty($variables->use_disorder) ? strtolower($variables->use_disorder) : '');
        $variables->severity = (!empty($variables->severity) ? strtolower($variables->severity) : null);
        $code='';
        $name = ($variables->substance_class ? ucfirst($variables->substance_class) : '');
        $res = DB::select("select si.id,si.icd from ".DB::getTablePrefix()."specifiers_items si inner join ".DB::getTablePrefix()."specifiers s on s.id=si.specify_id left join ".DB::getTablePrefix()."behaviors b on b.id=s.behavior_id where s.diagnosis_id=".$diagnosis->id." and b.name='small_substance_name' and var_data='".$variables->substance_class."'");
        if(!empty($res))
            $code = $res[0]->icd;
        $substance_name = (!empty($variables->substance_input) ? $variables->substance_input : $name);
        $disorder_name = strtolower($substance_name.' withdrawal delirium');
        if($variables->use_disorder == "with")
            $text = $variables->severity. ' '.$substance_name.' use disorder with '.$disorder_name;
        else
            $text = $disorder_name;
        $formal = array("code" => $code, "text" => $text.$icd_text);
        return $formal;
    }

    private function get_medication_induced_delirium_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text)
    {
        $code ='';
        $medication_class = (!empty($variables->medication_class) ? $variables->medication_class : '');
        $medication = (!empty($variables->substance_input) ? $variables->substance_input : $medication_class);
        $res = DB::select("select si.id,si.icd from ".DB::getTablePrefix()."specifiers_items si inner join ".DB::getTablePrefix()."specifiers s on  s.id=si.specify_id left join ".DB::getTablePrefix()."behaviors b on b.id=s.behavior_id where s.diagnosis_id=".$diagnosis->id." and b.name='prescribed_medication' and si.var_data='".$medication_class."'");
        if(!empty($res))
            $code = $res[0]->icd;
        $formal = array("code" => $code, "text" => $medication."-induced delirium".$icd_text);
        return $formal;

    }

    private function get_delirium_due_another_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text)
    {
        $text='';
        $code='';
        $free_text="free-text-1";
        if(!empty($variables->other_medical && !empty($variables->other_medical->$free_text))){
            $code = (!empty($variables->other_medical->$free_text->code) ? $variables->other_medical->$free_text->code : '');
            $name = (!empty($variables->other_medical->$free_text->name) ? $variables->other_medical->$free_text->name : '');
        }
        $res = DB::select('select icd from '.DB::getTablePrefix().'specifiers_items si inner join '.DB::getTablePrefix().'specifiers s on s.id=si.specify_id where s.diagnosis_id='.$diagnosis->id.' and var_data="'.$variables->delirium_whether.'"');
        if(!empty($res))
            $code2=$res[0]->icd;
        if($code && $name)
            $formal = array("code"=>$code,"text"=>$name."; ".$code2." delirium due to ".$name.$icd_text);
        else
            $formal = array("code"=>$code2,"text"=> "delirium due to unspecified medical condition" );
        return $formal;
    }

    private function get_delirium_due_multiple_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text)
    {
        $text='';
        $code='';
        $multiple = (!empty($variables->multiple) ? $variables->multiple.'; ' : '');
        $res = DB::select('select icd from '.DB::getTablePrefix().'specifiers_items si inner join '.DB::getTablePrefix().'specifiers s on s.id=si.specify_id where s.diagnosis_id='.$diagnosis->id.' and var_data="'.$variables->delirium_whether.'"');
        if(!empty($res))
            $code=$res[0]->icd;
        if($multiple){
            $text = $multiple.$code.' delirium due to multiple etiologies'.$icd_text;
            $code = '';
        }
        else{
            $text = 'delirium due to multiple etiologies'.$icd_text;
        }
        $formal = array("code"=>$code,"text"=> $text );
        return $formal;        
    }

    private function get_mild_ncd_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text,$cwords)
    {
        $formal = array("code"=>"","text" =>"FALSE/".$diagnosis->name);
        $code = $diagnosis->ICD10;
        $mild_ncd_diagnosis = Diagnosis::find($diagnosis->parent);
        $var_data = trim(str_replace('Mild', '', $diagnosis->name));
        $res = DB::select('select s.id,s.parent from '.DB::getTablePrefix().'specifiers s left join '.DB::getTablePrefix().'behaviors b on s.behavior_id=b.id where diagnosis_id='.$mild_ncd_diagnosis->id.' and b.name="ncd_types"');
        if(!empty($res)){
            if(!empty($res[0]->parent))
                $specify_id = $res[0]->parent;
            else
                $specify_id = $res[0]->id;
            $res2 = DB::select('select icd_text from '.DB::getTablePrefix().'specifiers_items where specify_id='.$specify_id.' and LOWER(var_data)="'.strtolower($var_data).'"');
            if(!empty($res2)){
                $type_name = $res2[0]->icd_text;
                $text = strtr(strtolower($mild_ncd_diagnosis->name.' '.$type_name.' '.$icd_text),$cwords);
                $formal = array("code" => $code, "text" => $text);
            }
        }
        return $formal;
    }

    private function get_major_ncd_formal($diagnosis,$patient_diagnosis_id,$variables,$icd_text,$cwords)
    {
        $formal = array("code"=>"","text" =>"FALSE/".$diagnosis->name);
        $type_name = '';
        $major_ncd_diagnosis = Diagnosis::find($diagnosis->parent);
        $var_data = trim(str_replace('Major', '', $diagnosis->name));
        $res = DB::select('select * from '.DB::getTablePrefix().'diagnosis_steps where diagnosis_id='.$diagnosis->id.' and result in("Probable","Possible")');
        if(!empty($res))
            $kind = $res[0]->result;
        else
            $kind = "";
        $behavioral = (($kind == 'Possible' || empty($variables->behavioral)) ? '' : $variables->behavioral);
        $major_name = $major_ncd_diagnosis->name;
        $res = DB::select('select s.id,s.parent from '.DB::getTablePrefix().'specifiers s left join '.DB::getTablePrefix().'behaviors b on s.behavior_id=b.id where diagnosis_id='.$major_ncd_diagnosis->id.' and b.name="ncd_types"');
        if(!empty($res)){
            $specify_id = (!empty($res[0]->parent) ? $res[0]->parent : $res[0]->id);
            $res = DB::select('select icd_text from '.DB::getTablePrefix().'specifiers_items where specify_id='.$specify_id.' and LOWER(var_data)="'.strtolower($var_data).'"');
            if(!empty($res))
                $type_name = $res[0]->icd_text;            
        }
        $res = DB::select('select * from '.DB::getTablePrefix().'ncd_codes where diagnosis_id='.$diagnosis->id.' and behavioral="'.$behavioral.'" and probable_possible ="'.strtolower($kind).'"');
        if(!empty($res)){
            if($res[0]->med_code){
                $code = $res[0]->med_code;
                $text = $res[0]->med_name.'; '.$res[0]->icd_code.' '.strtolower($kind).' '.strtolower($major_name).' '.$type_name . ', ' . $icd_text;
            }
            else{
                $code = $res[0]->icd_code;
                $text = strtr(strtolower($kind).' '.strtolower($major_name).' '.$type_name . ', ' . $icd_text,$cwords);
            }
            $formal = array('code' => $code, "text" => $text);
        }
        return $formal;
    }
    private function get_another_medical_formal($formal,$diagnosis,$patient_diagnosis_id,$variables,$text,$cwords)
    {
        $code_holder = '[ICD code]';
        $name_holder = '[Name of the medical condition]';
        $medical_text='';
        $medicals = array();
        $another_medical = (empty($variables->another_medical) ? '' : $variables->another_medical);
        if(empty($another_medical) || empty((array) $another_medical)){
            array_push($medicals, array('code' => $code_holder, 'text' => $name_holder.';'));
            $medical_text = $name_holder;
        }
        else{
            $len = count((array) $another_medical);
            foreach($another_medical as $k=>$v){
                $code = (empty($v->code) ? $code_holder : $v->code);
                $name = (empty($v->name) ? $name_holder : $v->name);
                if(!empty($medical_text))
                    $medical_text.=', ';
                if($len > 1 && !next($another_medical))
                    $medical_text .='and ';
                $medical_text .= $name;
                array_push($medicals,array('code'=>$code,'text'=>$name.';'));
            }
        }
        $formal[0]['text'] = strtolower(str_replace('another medical condition',$medical_text,$formal[0]['text']));
        $formal = array_merge($medicals,$formal);
        return $formal;
    }

    private function get_free_text_associated($specify,$variables)
    {

        $code_holder = '[ICD code]';
        $name_holder = '['.$specify->additional_text.']';
        $associated_text = '';
        $associated = array();
        $associated_var = (empty($variables->associated) ? '' : $variables->associated);
        if(empty($associated_var) || empty((array) $associated_var)){
            // array_push($associated, array('code' => $code_holder, 'text' => $name_holder.';'));
            // $associated_text = $name_holder;
        }
        else{
            $len = count((array) $associated_var);
            foreach($associated_var as $k=>$v){
                $code = (empty($v->code) ? $code_holder : $v->code);
                $name = (empty($v->name) ? $name_holder : $v->name);
                if(!empty($associated_text))
                    $associated_text.=', and with ';
                $associated_text .= $name;
                array_push($associated,array('code'=>$code,'text'=>$name.';'));
            }
        }
        return array($associated,$associated_text);
    }

    private function get_associated_formal($formal,$diagnosis,$patient_diagnosis_id,$variables,$text)
    {
        if(!in_array($diagnosis->id,[13,22,251])){
            $code_holder = '[ICD code]';
            $name_holder = $this->get_associated_place_holder($diagnosis);
            $associated_text = '';
            $associated = array();
            $associated_var = (empty($variables->associated) ? '' : $variables->associated);
            if(empty($associated_var) || empty((array) $associated_var)){
                array_push($associated, array('code' => $code_holder, 'text' => $name_holder.';'));
                $associated_text = $name_holder;
            }
            else{
                $len = count((array) $associated_var);
                foreach($associated_var as $k=>$v){
                    $code = (empty($v->code) ? $code_holder : $v->code);
                    $name = (empty($v->name) ? $name_holder : $v->name);
                    if(!empty($associated_text))
                        // $associated_text = 'associated with ';
                    // else
                        $associated_text.=', and with ';
                    $associated_text .= $name;
                    array_push($associated,array('code'=>$code,'text'=>$name.';'));
                }
            }
            $diagnosis_name = strtolower($diagnosis->name);
            $formal[0]['text'] = strtolower(str_replace($diagnosis_name,$diagnosis_name.' associated with '.$associated_text,$formal[0]['text']));
            $formal = array_merge($associated,$formal);



        }
        return $formal;
    }

    private function get_associated_place_holder($diagnosis)
    {
        $name_holder = '';
        $res = DB::select('select additional_text from '.DB::getTablePrefix().'specifiers where diagnosis_id='.$diagnosis->id.' and variable="associated"');
        if(!empty($res)){
            $name_holder = '['.$res[0]->additional_text.']';
        }
        return $name_holder;
    }

    private function get_catatonia_formal($diagnosis,$cwords)
    {
        $text = '';
        $res=DB::select('select icd  from '.DB::getTablePrefix().'specifiers s inner join '.DB::getTablePrefix().'specifiers_items si on s.id=si.specify_id where behavior_id=5 and var_data="TRUE"');
        if(!empty($res)){
            $code = $res[0]->icd;
            // $text = '; '.$code.' catatonia associated with '.strtolower($diagnosis->name);
            $text = 'catatonia associated with '.strtolower($diagnosis->name);
        }
        // return $text;
        return array('code'=>$code,'text'=>$text);
    }

    private function get_parent($specify){
        $behavior_name = $specify->behavior_name;
        $specify = Specify::find($specify->id);
        $specify->get_parent();
        $specify->behavior_name=$behavior_name;
        return $specify;

    }
    private function get_patient_diagnosis_variables($diagnosis,$patient_diagnosis_id)
    {
        $patient_diagnosis_variables = DB::select('select * from '.DB::getTablePrefix().'patient_diagnosis_variables where patient_diagnosis_id='.$patient_diagnosis_id. ' and diagnosis_id='.$diagnosis->id.' and (status is null or status<>"related")');
        $variables = $patient_diagnosis_variables ? $patient_diagnosis_variables[0]->selections : "{}";
        $variables = json_decode($variables);
        return $variables;
    }
    private function capitalize_words()
    {
        $words = array(
            "alzheimer's" => "Alzheimer's",
            "lewy body" => "Lewy body",
            "hiv" => "HIV",
            "parkinson's" => "Parkinson's",
            "huntington's" => "Huntington's",
            "alzheimer’s" => "Alzheimer's",
            "parkinson’s" => "Parkinson's",
            "huntington’s" => "Huntington's"
            );
        return $words;
    }

    // private function ncd_icd_text($diagnosis,$patient_diagnosis_id)
    // {
    //     $text = '';
    //     $specifiers = $diagnosis->specifiers;
    //     if(!empty($specifiers) && count($specifiers)>0)
    //     {
    //         $variables = $this->get_patient_diagnosis_variables($diagnosis,$patient_diagnosis_id);
    //         $specifiers = DB::select('select s.id,s.diagnosis_id,s.title,s.text,s.groups,s.is_require,s.variable,s.parent,b.name as behavior_name from specifiers s left join behaviors b on s.behavior_id=b.id  where s.diagnosis_id='.$diagnosis_id);
    //         foreach ($specifiers as $key => $specifier) {
    //             $var = $specifier->variable;
    //             if($specifier->parent)
    //                 $specifier = $this->get_parent($specifier);
    //             $var = $specifier->variable;
    //             $selections = $variables->$var ?? null;
    //             if($specifier->is_require && empty($selections)){
    //                 return 'missing';
    //             }
    //             else{
    //                 $var_items = NULL;
    //                 if(!empty($selections)){
    //                     if(gettype($selections) == 'string')
    //                         $var_items = addslashes($selections);
    //                     elseif (gettype($selections) == 'array')
    //                         $var_items = join("','",$selections);
    //                 }
    //                 $specifiers_items = DB::select("select * from specifiers_items where specify_id=".$specifier->id." and var_data in('$var_items') order by specify_id");
    //                 foreach ($specifiers_items as $k => $v) {
    //                     if(!empty($v->icd_text)){
    //                         if(!empty($v->icd))
    //                             $code = $v->icd; 
    //                         $text .= ', '.$v->icd_text;  
    //                     }

    //                 }
    //             }

    //         }
    //     }
    //     return  $text;
    // }

    public function history_page(Request $request)
    {
        $patient_id = $request->selected_patient;
        $page       = $request->get('page', 1);
        $per_page   = $request->get('per_page', History::HISTORY_PER_PAGE);

        $history = History::getHistory($page, $per_page, 'updated_at', 'desc', false, $patient_id);

        return view('history.show', [
            'layout'  => !$request->ajax(),
            'history' => $history,
        ]);
    }

    public function results_page(Request $request)
    {
        $page       = $request->get('page', 1);
        $patient_id = $request->selected_patient;
        $per_page   = $request->get('per_page', self::FORMALS_PER_PAGE);
        $formals    = $this->load_formals($request->own_user, $patient_id);
        $formals    = $this->paginate_formals($formals, $page, $per_page);

        return view('results.show', [
            'layout'  => !$request->ajax(),
            'formals' => $formals
        ]);
    }

    public function render_formals(Request $request)
    {
        $page            = $request->get('page', 1);
        $patient_id      = $request->selected_patient;
        $per_page        = $request->get('per_page', self::FORMALS_PER_PAGE);
        $order_by        = $request->get('order-by', 'updated_at');
        $order_direction = $request->get('order-direction', 'desc');

        $formals = $this->load_formals($request->own_user, $patient_id, $order_by, $order_direction);
        $formals  = $this->paginate_formals($formals, $page, $per_page);

        return view('history._results', [
            'formals' => $formals,
        ]);
    }

    public function render_history(Request $request)
    {
        $page            = $request->get('page', 1);
        $per_page        = $request->get('per_page', History::HISTORY_PER_PAGE);
        $order_by        = $request->get('order-by', 'updated_at');
        $order_direction = $request->get('order-direction', 'desc');
        $diagnosis       = Diagnosis::find($request->get('diagnosis_id', false));
        $patient_id      = $request->selected_patient;

        $history = History::getHistory($page, $per_page, $order_by, $order_direction, $diagnosis, $patient_id);
        return view('history._history', [
            'history' => $history,
            'diagnosis_id' => @$diagnosis->id
        ]);
    }

    private function load_formals(UserVp $user, $patient_id, $order_by = 'updated_at', $order_direction = 'desc')
    {
        $patient_user_own_id = PatientVp::find($patient_id)->user_own_id;
        $formals = $this->get_formals($patient_id, true);
        $formals_with_free_text = $this->get_formal_with_free_text($patient_id, $patient_user_own_id);

        foreach ($formals_with_free_text as $item) {
            $formals[] = [
                array_merge($item, [
                    'code' => $item[0]['code'],
                    'text' => $item[0]['text'],
                    'options_text' => $item[0]['options_text'],
                ])
            ];
        }

        $sortFormals = function ($a, $b) use ($order_by) {
            $a_value = $a[$order_by];
            $b_value = $b[$order_by];

            if (in_array($order_by, ['code', 'text'], true)) {
                $a_value = strtolower($a_value);
                $b_value = strtolower($b_value);
            }

            if ($a_value == $b_value) {
                return 0;
            }
            return ($a_value < $b_value) ? -1 : 1;
        };

        uasort($formals, function ($a, $b) use($order_by, $sortFormals) {
            uasort($a, $sortFormals);
            uasort($b, $sortFormals);

            if ($a[0][$order_by] == $b[0][$order_by]) {
                return 0;
            }
            return $sortFormals($a[0], $b[0]);
        });

        if ($order_direction === 'desc') {
            $formals = array_reverse($formals, true);
        }

        return $formals;
    }

    private function paginate_formals(array $formals, $page = 1, $per_page = self::FORMALS_PER_PAGE)
    {
        if (strtolower($per_page) === 'all') {
            $per_page = count($formals);
        }

        $offset = ($page * $per_page) - $per_page;

        return new LengthAwarePaginator(
            array_slice($formals, $offset, $per_page, true),
            count($formals),
            $per_page,
            $page,
            ['path' => '', 'query' => []]
        );
    }

    private function get_page_by_row_number($row_number, $per_page)
    {
        $result = (int) ceil($row_number / $per_page);

        if (!$result) {
            return 1;
        }

        return $result;
    }
}