<?php

namespace App\Http\Controllers;

use App\Criteria;
use App\Diagnosis;
use App\DiagnosisOption;
use App\DiagnosisSteps;
use App\PatientDiagnosisVariables;
use App\PatientVp;
use App\Specify;
use App\UserVp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;


class DiagnosisResultController extends Controller
{
    public function index(Request $request, $diagnosis_ids, $specify_id = null)
    {
        $in_modal = Session::get('result_in_modal');

        if (!$in_modal) {
            $redirect_url = str_replace('/result', '', $request->getRequestUri());
            return redirect($redirect_url)->with(['open_result_modal' => true]);
        }

        $params     = explode(".", $diagnosis_ids);
        $patient_id = $request->selected_patient;
        $diagnosis  = $this->get_diagnosis($diagnosis_ids);
        $option     = DiagnosisOption::where('patient_diagnosis_id', $patient_id)
            ->where('diagnosis_id', $diagnosis->id)
            ->first();

        // TODO refactoring formal method to some service class
        $formal = app(SigmaController::class)->get_formal($diagnosis, $patient_id, []);
        $code = $formal[0]['code'] ?? null;

        return view('diagnosis.result', [
            'diagnosis'                        => $diagnosis,
            'layout'                           => !$request->ajax(),
            'breadcrumbs'                      => $params,
            'options'                          => $option,
            'patient_id'                       => $patient_id,
            'option_variants'                  => DiagnosisOption::get_option_variants(),
            'criteria_hav_been_met'            => $this->is_criteria_have_been_met($diagnosis, $patient_id),
            'diagnosis_specifiers_are_missing' => $this->is_diagnosis_specifiers_are_missing($diagnosis, $patient_id),
            'formal_code'                      => $code,
        ]);
    }

    public function store(Request $request, $diagnosis_ids)
    {
        $diagnosis         = $this->get_diagnosis($diagnosis_ids);
        $options           = $request->get('options', null);
        $assign_to_patient = $request->get('assign_to_patient', null);

        if (!$diagnosis instanceof Diagnosis) {
            abort(404, 'Diagnosis not found');
        }

        $option = DiagnosisOption::where('diagnosis_id', '=', $diagnosis->id)
            ->where('patient_diagnosis_id', '=', $request->selected_patient)
            ->first();

        if (!$option) {
            $option                       = new DiagnosisOption();
            $option->diagnosis_id         = $diagnosis->id;
            $option->patient_diagnosis_id = $request->selected_patient;
            $option->options              = [];

            $option->save();
        }

        $option->update([
            'options' => array_merge($option->options, [
                'option'            => $options,
                'assign_to_patient' => $assign_to_patient
            ])
        ]);

        if ($assign_to_patient) {
            $max_ordering_item = DiagnosisOption
                ::where('patient_diagnosis_id', '=', $request->selected_patient)
                ->max('ordering');

            if (!empty($max_ordering_item)) {
                $current_ordering = $max_ordering_item + 1;
            } else {
                $current_ordering = 1;
            }

            $option->ordering = $current_ordering;
            $option->save();
        }

        $criteria_list = Criteria::findByDiagnosisAndPatient($request->selected_patient, $diagnosis->id);
        if (!empty($criteria_list)) {
            DiagnosisSteps::find($criteria_list[0]->step_id)->touch();
        }

        if ($this->is_criteria_have_been_met($diagnosis, $request->selected_patient) && $assign_to_patient) {
            return redirect('/center/diagnosis?highlight=' . $diagnosis->id);
        }

        return redirect()->action('SigmaController@history_page', [
            'highlight' => $diagnosis->id
        ]);
    }

    public function select_result(Request $request, int $patient_id, int $diagnosis_id) {
        $user = UserVp::getOwnUser();
        $option_id = (int) $request->get('option_id');

        if ($diagnosis_id === -1 && $option_id) {
            $diagnosis_option = DiagnosisOption::find($option_id);
        } else {
            $diagnosis_option = DiagnosisOption::where([
                'patient_diagnosis_id' => $patient_id,
                'diagnosis_id' => $diagnosis_id
            ])->first();
        }

        if (!$diagnosis_option) {
            return;
        }

        $patient = PatientVp::find($diagnosis_option->patient_diagnosis_id);

        if ($patient->user_id === $user->id) {
            $user->selected_diagnosis_option = $diagnosis_option->id;
            $user->save();
        }
    }

    private function is_criteria_have_been_met(Diagnosis $diagnosis, $patient_id)
    {
        $criteria = Criteria
            ::leftJoin('diagnosis_steps', function ($join) use($patient_id) {
                $join->on('criteria.id', 'diagnosis_steps.criterion_id');
                $join->where('diagnosis_steps.patient_diagnosis_id', $patient_id);
            })
            ->where('criteria.diagnosis_id', $diagnosis->id)
            ->where('criteria.parent', 0)
            ->where('criteria.behavior', '<>', 'crit_as_title')
            ->where(function ($q) {
                $q->whereNull('diagnosis_steps.status')
                    ->orWhere('diagnosis_steps.status', '<>', 'related');
            })
            ->select(
                'criteria.*',
                'diagnosis_steps.result',
                'diagnosis_steps.free_text'
            )
            ->get();


        if (!$diagnosis->is_criteria_have_been_met($criteria)) {
            return false;
        }

        return true;
    }

    private function is_diagnosis_specifiers_are_missing(Diagnosis $diagnosis, $patient_id)
    {
        $specifiers = $diagnosis->specifiers;

        if (count($specifiers) === 0) {
            return null;
        }

        $variables  = $this->get_patient_diagnosis_variables($diagnosis, $patient_id);
        $specifiers = Specify::where('diagnosis_id', $diagnosis->id)
            ->leftJoin('behaviors', 'specifiers.behavior_id', '=', 'behaviors.id')
            ->select(
                'specifiers.*',
                'behaviors.name as behavior_name'
            )
            ->get();

        foreach ($specifiers as $key => $specifier) {
            if ($specifier->parent) {
                $specifier = $this->get_specify_parent($specifier);
            }

            $variable   = $specifier->variable;
            $selections = $variables->$variable ?? null;

            $is_require               = $specifier->is_require;
            $require_severity         = $specifier->require_severity($variables);
            $require_alcohol_type     = $specifier->require_alcohol_type($variables);
            $require_delirium         = $specifier->require_delirium($variables, $diagnosis);
            $require_bipolar          = $specifier->require_bipolar($variables, $diagnosis);
            $require_bipolar2         = $specifier->require_bipolar2($variables, $diagnosis);
            $require_major_depressive = $specifier->require_major_depressive($variables, $diagnosis);
            $is_select_from_group     = $specifier->is_select_from_group((array)$specifiers, $variables);

            if (
                (
                    $is_require &&
                    empty($selections) &&
                    $require_severity &&
                    $require_alcohol_type &&
                    $require_delirium &&
                    $require_bipolar &&
                    $require_bipolar2 &&
                    $require_major_depressive
                ) ||
                (
                    $specifier->behavior_name === 'least_one_of_group' && !$is_select_from_group
                )
            ) {
                return true;
            }
        }

        return false;
    }

    private function get_patient_diagnosis_variables($diagnosis, $patient_id)
    {
        $patient_diagnosis_variables = PatientDiagnosisVariables
            ::where([
                ['patient_diagnosis_id', $patient_id],
                ['diagnosis_id', $diagnosis->id]
            ])
            ->where(function ($q) {
                $q->whereNull('status')
                  ->orWhere('status', '<>', 'related');
            })
            ->first();

        $variables = $patient_diagnosis_variables ? $patient_diagnosis_variables->selections : "{}";
        $variables = json_decode($variables);
        return $variables;
    }

    private function get_specify_parent($specify)
    {
        $behavior_name = $specify->behavior_name;
        /** @var Specify $specify */
        $specify = Specify::find($specify->id);
        $specify->get_parent();
        $specify->behavior_name = $behavior_name;

        return $specify;
    }

    /**
     * @param $diagnosis_ids
     *
     * @return Diagnosis
     */
    private function get_diagnosis($diagnosis_ids)
    {
        $params = explode('.', $diagnosis_ids);
        return Diagnosis::find(end($params));
    }

}
