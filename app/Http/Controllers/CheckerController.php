<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\DiagnosisSymptomWeight;
use App\helpers\Thesaurus\Client;
use App\helpers\Thesaurus\Exceptions\Exception;
use App\Symptom;
use App\SymptomSearchHistory;
use App\SymptomSynonym;
use App\Specify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CheckerController extends Controller
{

    public const COLUMN_WEIGHTS = [
        'diagnosis' => [
            'name' => 2,
            'note' => 0.75,
            'info' => 1.5
        ],
        'criteria' => [
            'name' => 1.5,
            'note' => 0.75,
            'info' => 1
        ],
        'specifiers' => [
            'text' => 1.5,
            'additional_text' => 0.5,
            'note' => 0.5,
            'info' => 0.75
        ],
        'specifiers_items' => [
            'columnA' => 0.5,
            'columnB' => 0.75
        ]
    ];

    public const SYNONYM_COEFFICIENT = 0.2;

    public function index()
    {
        return view('checker.show');
    }

    public function symptom()
    {
        return view('checker.symptom');
    }

    public function diagnosis()
    {
        return view('checker.diagnosis');
    }

    // public function results(Request $request)
    // {
    //     $this->validate($request, [
    //         'symptom_ids'   => 'required|array',
    //         'symptom_ids.*' => 'required|integer',
    //     ]);

    //     $weight = DiagnosisSymptomWeight::groupBy('diagnosis_id')
    //         ->whereIn('symptom_id', $request->get('symptom_ids'))
    //         ->where('weight', '>', 0)
    //         ->select(DB::raw('SUM(' . DB::getTablePrefix() . 'diagnosis_symptoms.weight) as total_weight, diagnosis_id'))
    //         ->get();

    //     $diagnosis_weight = $weight->keyBy('diagnosis_id')->map(function ($item) {
    //         return $item->total_weight;
    //     })->toArray();

    //     $diagnosis = Diagnosis::whereIn('id', array_keys($diagnosis_weight))
    //         ->get()
    //         ->sort(function ($a, $b) use ($diagnosis_weight) {
    //             return $diagnosis_weight[$b->id] - $diagnosis_weight[$a->id];
    //         });

    //     return view('checker.results', [
    //         'diagnosis'        => $diagnosis,
    //         'diagnosis_weight' => $diagnosis_weight
    //     ]);
    // }

    public function results(Request $request) {
        $symptoms = explode('_', $request->input('symptoms', false));
        $symptoms_with_synonyms = [];

        foreach($symptoms as $symptom) {
            $synonyms = [];
            try {
                $synonyms = Client::get($symptom);
            } catch (\Throwable $e) {
                trigger_error('Couldn\'t get synonyms for symptom - '.$symptom.'\n'.
                                'Error - '.$e->getMessage());
            }
            $symptoms_with_synonyms[] = ['symptom' => $symptom, 'synonyms' => $synonyms];
        }

        $diagnosis_info = [];
        $total_entries = [];
        
        //seeking records with at least one symptom in help info
        foreach(self::COLUMN_WEIGHTS as $table => $columns) {
            $query = DB::table($table)->where(function($query) use ($symptoms_with_synonyms, $columns) {
                foreach($symptoms_with_synonyms as $symptom) {
                    foreach($columns as $column => $weight) {
                        $query->orWhere($column, 'like', '%'.$symptom['symptom'].'%');
                        foreach($symptom['synonyms'] as $synonym)
                            $query->orWhere($column, 'like', '%'.$synonym.'%');
                    }
                }
            });
            if($table === 'specifiers_items') {
                $query->leftJoin('specifiers', 'specifiers_items.specify_id', '=', 'specifiers.id')
                    ->select('specifiers_items.*', 'specifiers.diagnosis_id');
            }
            $diagnosis_info[$table] = $query->get();
        }

        
        //counting total amount of synonyms
        foreach($diagnosis_info as $table => $rows) {
            $id_column_name = ($table === 'diagnosis' ? 'id' : 'diagnosis_id');
            foreach($rows as $row) {
                if(empty($total_entries[$row->$id_column_name])) {
                    $total_entries[$row->$id_column_name] = 0;
                }
                foreach(self::COLUMN_WEIGHTS[$table] as $column => $weight) {
                    foreach($symptoms_with_synonyms as $symptom) {
                        $total_entries[$row->$id_column_name] += substr_count(strtolower($row->$column), strtolower($symptom['symptom'])) * $weight;
                        foreach($symptom['synonyms'] as $synonym)
                            $total_entries[$row->$id_column_name] += substr_count(strtolower($row->$column), strtolower($synonym)) * $weight * self::SYNONYM_COEFFICIENT;
                    }
                }
            }
        }

        //loading all appropriate diagnoses and adding total entries to objects
        $diagnoses_result = Diagnosis::whereIn('id', array_keys($total_entries))->get(['name','id']);
        foreach($diagnoses_result as $row) {
            $row->total_entries = $total_entries[$row->id];
        }

        $diagnoses_result = $diagnoses_result->sort(function($a, $b) {
            if ($a->total_entries === $b->total_entries) {
                return 0;
            }
            return ($a->total_entries > $b->total_entries) ? -1 : 1;
        });

        //calculating probability
        if($diagnoses_result->count() && $diagnoses_result->first()->total_entries) {
            $coefficient = (100 / $diagnoses_result->first()->total_entries) / 100;
        
            foreach($diagnoses_result as $diagnosis)
                $diagnosis->probability = round($diagnosis->total_entries * $coefficient, 2);
        }

        return view('checker.results', [
             'diagnoses' => $diagnoses_result,
        ]);
    }

    public function updateWeight(Request $request)
    {
        $this->validate($request, [
            'diagnosis_id'          => 'required|integer',
            'weight'                => 'required|array',
            'weight.*.diagnosis_id' => 'required|integer',
            'weight.*.symptom_id'   => 'required|integer',
            'weight.*.weight'       => 'required|numeric|min:0|max:100',
        ]);

        $weight = $request->get('weight');

        if (collect($weight)->sum('weight') !== 100) {
            abort(404, 'Total weight must be 100');
        }

        /** @var Diagnosis $diagnosis */
        $diagnosis = Diagnosis::findOrFail($request->get('diagnosis_id'));

        DB::transaction(function () use ($diagnosis, $weight) {
            $diagnosis->weight()->delete();
            $diagnosis->weight()->saveMany(array_map(function ($item) {
                return new DiagnosisSymptomWeight($item);
            }, $weight));
        });

        return $diagnosis;
    }

    public function attachToDiagnosis(Request $request, Client $client)
    {
        $this->validate($request, [
            'diagnosis_ids'   => 'required|array',
            'diagnosis_ids.*' => 'required|integer',
            'name'            => 'required|string',
        ]);

        /** @var Diagnosis $diagnosis */
        $diagnosis = Diagnosis::whereIn('id', $request->get('diagnosis_ids'))->get();

        if (empty($diagnosis)) {
            abort(404, 'Diagnosis not found');
        }

        /** @var Symptom $symptom */
        $symptom = Symptom::where('name', $request->get('name'))->first();

        if (!$symptom) {
            $symptom       = new Symptom();
            $symptom->name = $request->get('name');
            $symptom->save();

            try {
                $synonyms = $client->get($request->get('name'));

                if (!empty($synonyms)) {
                    $symptom->synonyms()->saveMany(array_map(function ($synonym) use ($symptom) {
                        return new SymptomSynonym([
                            'name'       => $synonym,
                            'symptom_id' => $symptom->id
                        ]);
                    }, $synonyms));
                }
            } catch (Exception $e) {
                Log::error($e->getMessage());
            }
        }

        foreach ($diagnosis as $item) {
            $item->symptoms()->syncWithoutDetaching($symptom->id);
        }

        return $symptom;
    }

    public function searchSymptom(Request $request)
    {
        $query = $request->get('query', '');

        if (!$query) {
            return [
                'suggestions' => []
            ];
        }

        /** @var Symptom $symptoms */
        $symptoms = Symptom::where('name', 'like', '%' . $query . '%')->get();

        /** @var SymptomSynonym $synonyms */
        $synonyms = SymptomSynonym::with([
            'symptom',
        ])->where('name', 'like', '%' . $query . '%')->get();

        return [
            'suggestions' => $symptoms->merge($synonyms)->map(function ($item) {
                $symptom = $item instanceof Symptom ? $item : $item->symptom;

                return [
                    'value' => $item->name,
                    'data'  => [
                        'name'    => $item->name,
                        'symptom' => $symptom
                    ]
                ];
            })
        ];
    }

    public function searchDiagnosis(Request $request)
    {
        $query = $request->get('query', '');

        if (!$query) {
            return [
                'suggestions' => []
            ];
        }

        /** @var Diagnosis $diagnosis */
        $diagnosis = Diagnosis::where('name', 'like', '%' . $query . '%')->get();

        return [
            'suggestions' => $diagnosis->map(function ($item) {
                return [
                    'value' => $item->name,
                    'data'  => $item
                ];
            })
        ];
    }

    public function searchDiagnosisWithWight(Request $request)
    {
        $query = $request->get('query', '');

        if (!$query) {
            return [
                'suggestions' => []
            ];
        }

        /** @var Diagnosis $diagnosis */
        $diagnosis = Diagnosis::with([
            'weight.symptom',
        ])
            ->has('weight')
            ->where('name', 'like', '%' . $query . '%')
            ->select(['id', 'name'])
            ->get();

        return [
            'suggestions' => $diagnosis->map(function ($item) {
                return [
                    'value' => $item->name,
                    'data'  => $item
                ];
            })
        ];
    }

    public function addToHistory(Request $request)
    {
        $symptoms = explode('_', $request->input('symptoms', false));

        foreach ($symptoms as $symptom) {
            SymptomSearchHistory::create([
                'user_id' => $request->own_user->id,
                'term' => $symptom,
            ]);
        }

        return response()->json([
            'success' => true
        ]);
    }
}
