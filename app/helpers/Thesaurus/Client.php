<?php


namespace App\helpers\Thesaurus;


use App\helpers\Thesaurus\Exceptions\Exception;
use App\helpers\Thesaurus\Exceptions\InvalidKey;
use App\helpers\Thesaurus\Exceptions\NotFound;

class Client
{
    /**
     * @param string $word
     *
     * @return mixed
     *
     * @throws Exception
     */
    public static function get($word)
    {
        $config = config('services.thesaurus');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $config['url'] . '?' . http_build_query([
            'word'     => $word,
            'language' => $config['lang'],
            'key'      => $config['key'],
            'output'   => 'json'
        ]));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        $response = json_decode($data, true);

        if ($info['http_code'] !== 200) {
            throw new Exception($response['error']);
        }

        return self::parseResponse($response['response']);
    }

    /**
     * @param array $response
     *
     * @return array
     */
    private static function parseResponse(array $response)
    {
        $result = [];

        foreach ($response as $item) {
            $synonyms = array_filter(explode('|', $item['list']['synonyms']), function ($item) {
                return strpos($item, '(') === false;
            });

            foreach($synonyms as $synonym)
                $result[] = $synonym;
        }

        $result = array_unique($result);

        return $result;
    }
}