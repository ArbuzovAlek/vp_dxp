<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class History extends Model
{
    const HISTORY_PER_PAGE = 10;

    /** @var string  */
    protected $table = 'diagnosis_history';

    /** @var string[] */
    protected $fillable = ['patient_diagnosis_id','title','url'];

    public static function getHistory($page = 1, $per_page = self::HISTORY_PER_PAGE, $order_by = 'updated_at', $order_direction = 'desc', $diagnosis = false, $patient_diagnosis_id = false){
        $query = History::orderBy($order_by, $order_direction);
        if($diagnosis) {
            $query->where(function ($query) use($diagnosis) {
                $query->orWhere('url', 'LIKE', "%/diagnosis/$diagnosis->parent.$diagnosis->id%")
                    ->orWhere('url', 'LIKE', "%/diagnosis/$diagnosis->id%");
            });
        }
        if($patient_diagnosis_id) {
            $query->where('patient_diagnosis_id', $patient_diagnosis_id);
        }


        if (strtolower($per_page) === 'all') {
            $per_page = $query->count();
        }

        return $query->paginate($per_page, ['*'], 'page', $page);
    }
}
