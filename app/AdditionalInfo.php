<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class AdditionalInfo extends Model
{
    protected $table = 'additional_info';

    protected $fillable = ['patient_diagnosis_id', 'type', 'value'];

    protected $casts = [
        'value' => 'array'
    ];

    public const TYPE_CLINICIAN_SEVERITY_SCALE = 'clinician_severity_scale';

    public static function store_severity_scale($severity_scale, $patient_diagnosis_id) {
        $current_severity_scale = new self([
            'patient_diagnosis_id' => $patient_diagnosis_id,
            'type' => self::TYPE_CLINICIAN_SEVERITY_SCALE,
            'value' => $severity_scale
        ]);
        $current_severity_scale->save();
    }
}
