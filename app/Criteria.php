<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;


class Criteria extends Model
{
   
    protected $table = 'criteria';

    // protected static function boot()
    // {
    //     parent::boot();

    //     static::addGlobalScope('crit_as_title', function (Builder $builder) {
    //         $builder->where('behavior', '<>', 'crit_as_title');
    //     });
    // }

    public function diagnosis()
    {
        return $this->belongsTo('App\Diagnosis');
    }

    public static function get_note($id)
    {
    	$criteria = self::find($id);
    	return $criteria->note;
    }
    public static function get_info($id)
    {
    	$criteria = self::find($id);
    	return $criteria->info;
    }
    public static function show_letter($diagnosis)
    {
        $criteria = DB::select('select * from '.DB::getTablePrefix().'criteria where diagnosis_id='.$diagnosis->id.' and parent=0 or parent is NULL');
        return (count($criteria) > 1 ? true : false);
    }

    public static function findByDiagnosisAndPatient($patient_diagnosis_id, $diagnosis_id, $parent_ids = [0]) {
        $parent_id_string = implode(',',$parent_ids);

        return DB::select('
            SELECT c.*,ds.result,ds.free_text, ds.updated_at, ds.id as step_id FROM '.DB::getTablePrefix().'criteria c 
            LEFT JOIN '.DB::getTablePrefix().'diagnosis_steps ds 
                ON ds.patient_diagnosis_id='.$patient_diagnosis_id. ' AND c.id=ds.criterion_id 
            WHERE c.diagnosis_id='.$diagnosis_id.' AND 
                c.parent IN ('.$parent_id_string.') AND 
                c.behavior <> "crit_as_title" AND 
                (ds.status IS NULL OR ds.status <> "related")'
        );
    }

}
