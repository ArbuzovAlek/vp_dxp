<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoodCode extends Model
{
    protected $table = 'mood_codes';

    public function diagnosis() {
        return $this->belongsTo('App\Diagnosis');
    }
}
