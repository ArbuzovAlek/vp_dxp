<html dir="ltr" class="firefox firefox71 desktop custom-responsive" lang="en-US"><head>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Diagnostic Center</title>
    <meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">
    <link rel="stylesheet" href={{ URL::asset("css/main.css")}} media="all">
    <link rel="stylesheet" href={{ URL::asset("css/myStyle.css")}} media="all">
    <link rel="stylesheet" href={{ URL::asset("css/main.responsive.css")}} media="all">
    <link rel="stylesheet" href={{ URL::asset("css/layout/popup_patients.css")}} media="all">
    <link rel="stylesheet" href={{ URL::asset('css/popup.css')}} media="all">
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">

    <script src={{ URL::asset("js/jquery.js")}}></script>
    <script src={{ URL::asset("js/main.js")}}></script>
    <script type="text/javascript" src={{ URL::asset("js/diagnosis.js")}} ></script>
    <script src={{ URL::asset("js/script.js")}}></script>
    <script src={{ URL::asset("js/script.responsive.js")}}></script>
    <script src={{ URL::asset("js/main.responsive.js")}}></script>
    <script src={{ URL::asset("js/layout/popup_patients.js")}}></script>
    <script src={{ URL::asset('js/popup.js')}}></script>
    <script src={{ URL::asset('js/center/settings.js') }}></script>
    <script src={{ URL::asset('js/pagination.js') }}></script>
    <script src={{ URL::asset("js/list/popper.min.js")}}></script>
    <script src={{ URL::asset("js/list/bootstrap.min.js")}}></script>
</head>

<body>
<div id="art-main" class="art-main-center">
<header class="art-header diagnostic-center-header" style="background-position: 944.78px 43.55px, 11.22px 3.23px, 0px 0px;">
    <div class="art-shapes">
    </div>
    <h1 class="art-headline" style="left: 349px; top: 33.6px;; margin-left: 0px !important;">
    <a href="/center">Diagnostic Center</a>
    </h1>
    <h2 class="art-slogan" style="left: 408px; top: 9.75px;; margin-left: 0px !important;">Virtual Psychology</h2>
    @if (!empty(request()->selected_patient_user_id))
        <a onclick="PopUpShow()" class="select-patient" title="Select patient Id.">
            <div style="text-align: right;">
                <img src="/css/images/object2026325657.png" alt="">
            </div>
            <span>{{request()->selected_patient_user_id}}</span>
        </a>
    @else
        <a onclick="PopUpShow()" class="select-patient" title="Select patient Id.">
            <img id="select-patient-image" src="/css/images/patient-header-03-2.png" width="24px" height="24px" alt="">
        </a>
    @endif
    <div class="authorized">
        {{$ownUser->email}} (<a href="http://35.224.165.111:8080/users/logout.php">logout</a>)
    </div>
</header>
<nav class="art-nav desktop-nav">
    <ul class="art-hmenu">
    <li><a href="/center">Start</a></li>
    <li><a href="/center/results">Results</a></li>
    <li><a href="/center/diagnosis">Diagnosis</a></li>
    <li><a href="/center/analytics">Analytics</a>
        <ul class="art-hmenu-left-to-right" style="">
            <li><a href="/center/analytics/summary">Diagnosis Summary</a></li>
            <li><a href="/center/analytics/recommendations">Recommendations</a></li>
            <li><a href="/center/analytics/diagnosis_history">Diagnosis History</a></li>
        </ul>
    </li>
    <li><a href="/center/history">History</a></li>
    <li><a href="/center/settings">Settings</a></li></ul>

</nav>
<div class="container">
    @yield('content')
</div>
<footer class="art-footer">
  <div class="art-footer-inner">
<div class="art-content-layout-wrapper layout-item-0">
<div class="art-content-layout layout-item-1">
    <div class="art-content-layout-row responsive-layout-row-1">
    <div class="art-layout-cell layout-item-2" style="width: 100%">
        <p>
            <span style="color: rgb(47, 57, 68); font-family: Verdana;">
                <a href="http://35.224.165.111:8080">Home</a>
                &nbsp;|&nbsp;
                <a href="/center/records_cabinet">Records Cabinet</a>
                &nbsp;|&nbsp;
                <a href="/">DxP</a>
                &nbsp;|&nbsp;
                <a href="//35.224.165.111:8080/documentation">Documentation</a>
                &nbsp;|&nbsp;
                <a href="http://35.224.165.111:8080/users">Account</a>
                &nbsp;|&nbsp;
                <a href="http://35.224.165.111:8080/users/logout.php">Logout</a>
            </span>
            <br>
        </p>
        <p><br><span style="font-size: 11px;"><span style="font-family: Verdana; color: #3E4C59;">DxP Version 5.19&nbsp;</span><span style="font-family: Verdana; color: #3E4C59;">© Virtual Psychology</span></span><span style="color: rgb(47, 57, 68); font-family: Verdana;"><br></span>
        </p>
    </div>
    </div>
</div>
</div>
</div>
</footer>
</div>

    <div id="validation-error">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close height-for-close">&times;</span>
                <span>Validation Error</span>
            </div>
            <div class="modal-body">
                <p>You must fill all criteria</p>
            </div>
            <div class="modal-footer">
                <span class ="close">close</span>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal"></div>

    @include('general._select_patient_modal')
    <script>
        
    </script>

<div id="art-resp"><div id="art-resp-m"></div><div id="art-resp-t"></div></div><div id="art-resp-tablet-landscape"></div><div id="art-resp-tablet-portrait"></div><div id="art-resp-phone-landscape"></div><div id="art-resp-phone-portrait"></div></body></html>
