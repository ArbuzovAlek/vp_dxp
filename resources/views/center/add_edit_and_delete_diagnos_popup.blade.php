<div id="add-diagnose-selector" style="display:none">
    <div class="modal-content add-edit-info-diagnosis-modal-content">
        <div class="modal-header info">
            <span class="close height-for-close">&times;</span>
            <span>Add Diagnosis</span>
        </div>
        <div class="modal-body">
            <p>How you want to add diagnosis?</p>
        </div>
        <div class="modal-footer info">
            <span class="use-program" style="margin-left: 20px;">Use program</span>
            <span class="free-text" style="margin-left: 20px;"> Free text</span>
        </div>
    </div>
</div>

<div id="free-text-error" style="display:none">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close height-for-close">&times;</span>
            <span>Error</span>
        </div>
        <div class="modal-body">
            <p>You have selected "free-text" diagnosis. Select another, please.</p>
        </div>
        <div class="modal-footer">
            <span class ="close">close</span>
        </div>
    </div>
</div>

<div id="del-confirmation" style="display: none">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close height-for-close">&times;</span>
            <span>Confirmation</span>
        </div>
        <div class="modal-body">
            <p>If you select to delete this diagnosis all its data will be permanently lost. Are you sure you want to delete?</p>
        </div>
        <div class="modal-footer">
            <span class ="close" style="margin-right: 20px;">Cancel</span>
            <span onclick="confirmDelete()" class ="confirm-delete" style="margin-left: 20px;">Yes</span>
        </div>
    </div>
</div>

<div id="select-patient-error" style="display: none">
    <div class="modal-content">
        <div class="modal-header">
            <span class="close height-for-close">&times;</span>
            <span>Error</span>
        </div>
        <div class="modal-body">
            <p>First select (click/tap) table row line.</p>
        </div>
        <div class="modal-footer">
            <span class ="close">close</span>
        </div>
    </div>
</div>

<div id="edit-diagnose-selector" style="display:none">
    <div class="modal-content add-edit-info-diagnosis-modal-content">
        <div class="modal-header info">
            <span class="close height-for-close">&times;</span>
            <span>Edit Diagnosis</span>
        </div>
        <div class="modal-body">
            <p>How you want to Edit diagnosis?</p>
        </div>
        <div class="modal-footer info">
            <span onclick="programEdit()" class ="use-program-edit" style="margin-left: 20px;">Use program</span>
            <span class ="free-text-edit" style="margin-left: 20px;"> Free text</span>
        </div>
    </div>
</div>

<div id="edit-diagnose-free-text" style="display:none">
    <div class="modal-content add-edit-diagnosis-modal-content">
        <div class="modal-header info">
            <span class="close height-for-close">&times;</span>
            <span>Edit Diagnosis</span>
        </div>
        <div class="modal-body">
            <div class="form-wrapper">
                <form id="editForm" onsubmit="saveEdit(); return false;">
                    <div class="heading-ticket">
                        <div class="inputs-wrapper">
                            <div class="icd">
                                <div>ICD-Code</div>
                                <input name="ICD" required type="text">
                            </div>
                            <div class="diagnosis">
                                <div>Diagnosis</div>
                                <textarea name="diagnosis" required maxlength="220"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="custom-inputs-wrapper">
                        <div class="input-group">
                            <div class="label">Options:</div>
                            <div class="input">
                                <div>
                                    <input name="options" id="principal_rb" type="checkbox" value="principal">
                                    <label for="principal_rb">principal</label>
                                </div>
                                <div>
                                    <input name="options" id="reason-for-referal_rb" type="checkbox" value="referral">
                                    <label for="reason-for-referal_rb">referral</label>
                                </div>
                                <div>
                                    <input name="options" id="provisional_rb" type="checkbox" value="provisional">
                                    <label for="provisional_rb">provisional</label>
                                </div>
                            </div>
                        </div>
                        <div class="input-group">
                            <div class="label">Assign:</div>
                            <div class="input">
                                <div>
                                    <input name="assign" id="principal_rb" type="radio" value="true">
                                    <label for="principal_rb">yes</label>
                                </div>
                                <div>
                                    <input name="assign" id="reason-for-referal_rb" type="radio" value="false">
                                    <label for="reason-for-referal_rb">no</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="buttons-wrapper">
                        <button type="submit">Save</button>
                        <a onclick="closeModalPopup()">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="add-diagnose-free-text" style="display:none">
        <div class="modal-content add-edit-diagnosis-modal-content">
            <div class="modal-header info">
                <span class="close height-for-close">&times;</span>
                <span>Add Diagnosis</span>
            </div>
            <div class="modal-body">
                    <div class="form-wrapper">
                        <form id="addForm" onsubmit="saveAdd(); return false;">
                            <div class="heading-ticket">
                                <div class="inputs-wrapper">
                                    <div class="icd">
                                        <div>ICD-Code</div>
                                        <input name="ICD" required type="text">
                                    </div>
                                    <div class="diagnosis">
                                        <div>Diagnosis</div>
                                        <textarea name="diagnosis" required maxlength="220"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="custom-inputs-wrapper">
                                <div class="input-group">
                                    <div class="label">Options:</div>
                                    <div class="input">
                                        <div>
                                            <input name="options" id="principal_rb" type="checkbox" value="principal">
                                            <label for="principal_rb">principal</label>
                                        </div>
                                        <div>
                                            <input name="options" id="reason-for-referal_rb" type="checkbox" value="referral">
                                            <label for="reason-for-referal_rb">referral</label>
                                        </div>
                                        <div>
                                            <input name="options" id="provisional_rb" type="checkbox" value="provisional">
                                            <label for="provisional_rb">provisional</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="label">Assign:</div>
                                    <div class="input">
                                        <div>
                                            <input name="assign" id="principal_rb" type="radio" value="true">
                                            <label for="principal_rb">yes</label>
                                        </div>
                                        <div>
                                            <input name="assign" id="reason-for-referal_rb" type="radio" value="false">
                                            <label for="reason-for-referal_rb">no</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="buttons-wrapper">
                                <button type="submit">Save</button>
                                <a onclick="closeModalPopup()">Cancel</a>
                            </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>