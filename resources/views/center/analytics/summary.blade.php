@extends(( $layout ? 'center.app' : 'layouts.ajax' ))
@section('content')
<style>.art-content .art-postcontent-0 .layout-item-0 { margin-top: 10px;margin-bottom: 10px;  }
.art-content .art-postcontent-0 .layout-item-1 { border-style:Solid;border-width:1px;border-color:#0FB8AD; color: #0B0D0F; background: rgba(236, 237, 249, 0.5);  border-collapse: separate; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-2 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:1px;border-color:#F7F5FA; color: #0B0D0F; background: ; padding: 10px; vertical-align: top; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-3 { margin: 10px;  }
.art-content .art-postcontent-0 .layout-item-4 { border-style:Solid;border-width:1px;border-color:#11D0C3; color: #0B0D0F; background: #ECEDF9;background: rgba(236, 237, 249, 0.5); border-spacing: 10px 10px; border-collapse: separate; border-radius: 10px;  }
.art-content .art-postcontent-0 .layout-item-5 { color: #0B0D0F; background: ; padding: 10px; vertical-align: top; border-radius: 3px;  }
.art-content .art-postcontent-0 .layout-item-6 { margin-top: 0px;margin-bottom: 0px;  }
.art-content .art-postcontent-0 .layout-item-7 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; border-spacing: 10px 10px; border-collapse: separate; border-radius: 20px;  }
.art-content .art-postcontent-0 .layout-item-8 { border-style:Solid;border-width:1px;border-color:#11D0C3; color: #0B0D0F; background: #ECEDF9;background: rgba(236, 237, 249, 0.5); padding: 10px; vertical-align: top; border-radius: 10px;  }
.art-content .art-postcontent-0 .layout-item-9 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:1px;border-color:#11D0C3; color: #0B0D0F; background: #ECEDF9;background: rgba(236, 237, 249, 0.5); padding: 10px; vertical-align: top; border-radius: 10px;  }
.art-content .art-postcontent-0 .layout-item-10 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:1px;border-color:#11D0C3; color: #0B0D0F; background: #ECEDF9;background: rgba(236, 237, 249, 0.5); padding: 10px; border-radius: 10px;  }
.art-content .art-postcontent-0 .layout-item-11 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:2px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; border-spacing: 10px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-12 { color: #111418; background: ; padding: 3px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.specifiers-wrapper .chartjs-render-monitor { 
    transform: scale(0.8); 
} 
.subcriterion-wrapper .chartjs-render-monitor {
    transform: scale(0.9);
}
</style>
@php
$options = $diagnosis_option->options['option'];
if (!is_array($options))
    $options = [$options]
@endphp
<div class="art-sheet clearfix">
    <div class="art-layout-wrapper">
        <div class="art-content-layout">
            <div class="art-content-layout-row">
                <div class="art-layout-cell art-content">
                    <article class="art-post art-article">
                        <div class="art-postmetadataheader clear-style">
                            <h2 class="art-postheader"><span class="art-postheadericon">Diagnosis Summary</span></h2>
                        </div>

                        <div class="art-postcontent art-postcontent-0 clearfix">
                            @include('center.analytics.diagnosis_block')
                            <div class="art-content-layout-wrapper layout-item-3">
                                <div class="art-content-layout layout-item-4">
                                    <div class="art-content-layout-row">
                                        <div class="art-layout-cell layout-item-5" style="width: 100%">
                                            <p style="text-align: center;"><span
                                                    style="font-weight: bold; font-family: Tahoma; color: rgb(10, 118, 111); font-size: 16px;">Criteria</span>
                                            </p>
                                            <canvas style="text-align: center; height: 300px; width: 100%" id="criteria_pie_canvas"><br></canvas>
                                            <p style="text-align: center;"><span
                                                    style="color: rgb(17, 20, 24); text-align: left; "></span></p>
                                            <table class="art-article" style="margin-right: auto; margin-left: auto; " id="criteria_table">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 77px; text-align: center; ">criterion</td>
                                                        <td style="text-align: center; ">Yes</td>
                                                        <td style="text-align: center; ">No</td>
                                                        <td style="text-align: center; ">Uncertain</td>
                                                    </tr>
                                                    <tr>
                                                        <td data-label="Total" style="text-align: center; ">{{ $criterion_amount['total'] }}</td>
                                                        <td data-label="Yes" style="width: 59px; text-align: center; ">{{ $criterion_amount['Yes'] }}</td>
                                                        <td data-label="No" style="width: 59px; text-align: center; ">{{ $criterion_amount['No'] }}</td>
                                                        <td data-label="Uncertain" style="text-align: center; ">{{ $criterion_amount['Uncertain'] }}</td>
                                                    </tr>
                                                </tbody>
                                            </table><br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="art-content-layout-wrapper layout-item-6">
                                <div class="art-content-layout layout-item-7">
                                    <div class="art-content-layout-row">
                                        <div class="art-layout-cell layout-item-8 subcriterion-wrapper" style="width: 50%">
                                            <p style="text-align: center;"><span
                                                    style="font-weight: bold; font-size: 16px; color: #0A766F;">Sub-Criterion</span>
                                            </p>
                                            @if($subcriterion_amount['total']) 
                                            <canvas style="text-align: center;  height: 300px; width: 100%" id="subcriteria_pie_canvas"><br></canvas>
                                            <p style="text-align: center;"><span
                                                    style="color: rgb(17, 20, 24); text-align: left; "></span></p>
                                            <table class="art-article" style="margin-right: auto; margin-left: auto; " id="subcriteria_table">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 77px; text-align: center; ">criterion</td>
                                                        <td style="text-align: center; ">Yes</td>
                                                        <td style="text-align: center; ">No</td>
                                                    </tr>
                                                    <tr>
                                                        <td data-label="Total" style="text-align: center; ">{{ $subcriterion_amount['total'] }}</td>
                                                        <td data-label="Yes" style="width: 59px; text-align: center; ">{{ $subcriterion_amount['Yes'] }}</td>
                                                        <td data-label="No" style="width: 59px; text-align: center; ">{{ $subcriterion_amount['No'] }}</td>
                                                    </tr>
                                                </tbody>
                                            </table><br>
                                            @else
                                                <p style="text-align: center;"><span
                                                    style="color: rgb(17, 20, 24); text-align: left; ">N/A</span></p>
                                            @endif
                                        </div>
                                        <div class="art-layout-cell specifiers-wrapper layout-item-8" style="width: 50%">
                                            <p style="text-align: center;"><span
                                                    style="font-weight: bold; font-size: 16px; color: #0A766F;">Specifiers</span>
                                            </p>
                                            <canvas style="text-align: center;  height: 300px; width: 100%" id="specifiers_pie_canvas"><br></canvas>
                                            <br>
                                            <table class="art-article" style="margin-right: auto; margin-left: auto; " id="specifiers_table">
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: center; ">Specifiers</td>
                                                        <td style="text-align: center; ">With entry</td>
                                                        <td style="text-align: center; ">Without entry</td>
                                                    </tr>
                                                    <tr>
                                                        <td data-label="specifiers" style="text-align: center; ">{{ $specifiers_amount['total'] }}</td>
                                                        <td data-label="with_entry" style="text-align: center; ">{{ $specifiers_amount['input'] }}</td>
                                                        <td data-label="without_entry" style="width: 59px; text-align: center; ">{{ $specifiers_amount['total'] - $specifiers_amount['input'] }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="art-content-layout-wrapper layout-item-6">
                                <div class="art-content-layout layout-item-7">
                                    <div class="art-content-layout-row">
                                        <div class="art-layout-cell layout-item-9" style="width: 50%">
                                            <p style="text-align: center;"><span
                                                    style="font-weight: bold; font-size: 16px; color: #0A766F;">Diagnosis
                                                    Options</span></p>
                                            <p style="text-align: center;"><input
                                                        type="checkbox" name="radio_name" {{ in_array('principal', $options)  ? 'checked="checked"' : '' }} disabled>principal &nbsp;
                                                &nbsp;<input type="checkbox"
                                                        name="radio_name" disabled {{ in_array('referral', $options) ? 'checked="checked"' : '' }}>reason for referral &nbsp;
                                                &nbsp;<input type="checkbox"
                                                        name="radio_name" disabled {{ in_array('provisional', $options) ? 'checked="checked"' : '' }}>provisional<br></p>
                                        </div>
                                        <div class="art-layout-cell layout-item-10" style="width: 50%">
                                            <p style="text-align: center;"><span
                                                    style="font-weight: bold; font-size: 16px; color: #0A766F;">Assigned
                                                    to Patient</span></p>
                                            @if($full_criteria_met)
                                                @if($diagnosis_option->options['assign_to_patient'])
                                                    <p><span>Diagnosis assigned to patient Id
                                                    {{ $patient->user_own_id }}.</span><br><span>You can unassign diagnosis from patient at the Results table.&nbsp;</span><br></p>
                                                @else
                                                    <p><span style="color: #802A00;">Diagnosis has not been assigned to patient
                                                    Id {{ $patient->user_own_id }}.</span><br><span style="color: #802A00;">You can assign this
                                                    diagnosis to patient Id {{ $patient->user_own_id }}&nbsp;at the Results table in History
                                                    page.&nbsp;</span></p>
                                                @endif
                                            @else
                                                <p><span style="color: #802A00;">Criteria for diagnosis have not been meet,
                                                    and diagnosis cannot be assigned to patient.</span></p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="art-content-layout-wrapper layout-item-6">
                                <div class="art-content-layout layout-item-7">
                                    <div class="art-content-layout-row">
                                        <div class="art-layout-cell layout-item-9" style="width: 50%">
                                            <p style="text-align: center;"><span
                                                    style="font-weight: bold; font-size: 16px; color: #0A766F;">Associated
                                                    Risks</span></p>
                                            <p><span style="font-weight: bold;">Suicide
                                                    Risk</span> - See recommendations</p>
                                            <p><span style="font-weight: bold;">Functional Consequences</span>&nbsp;- See
                                                recommendations</p>
                                            <p><span style="font-weight:bold;"><span style="font-weight: bold;">Other
                                                        Risks</span> -</span> <span style="color: #455463;">NA</span>
                                            </p>
                                        </div>
                                        <div class="art-layout-cell layout-item-10" style="width: 50%">
                                            <p style="text-align: center;"><span
                                                    style="font-weight: bold; font-size: 16px; color: #0A766F;">Measurements</span>
                                            </p>
                                            <p style="text-align: left;"><span style="font-weight: bold;">Rorschach test protocol</span>&nbsp;</p>
                                            <p>
                                                @if ($last_rap_date)
                                                    Available ({{ $last_rap_date }}) 
                                                @else
                                                    NA
                                                @endif
                                            </p>
                                            <p style="text-align: left;"><span style="font-weight: bold;">Clinician-rated psychotic symptoms severity scale</span>&nbsp;</p>
                                            <p>
                                                @if ($patient->get_clinician_rated_data_for_export())
                                                    Available (<a href="/center/settings/export_severity_scale?patient_id={{ $patient->id }}">export</a>)
                                                @else
                                                    NA
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="art-content-layout-wrapper layout-item-6">
                                <div class="art-content-layout layout-item-7">
                                    <div class="art-content-layout-row">
                                        <div class="art-layout-cell layout-item-9" style="width: 100%">
                                            <p style="text-align: center;"><span
                                                    style="color: rgb(10, 118, 111); font-size: 16px; font-weight: bold;">Demographics</span><br>
                                            </p>
                                            <p><b>Age</b> - {{ $patient->age() }}</p>
                                            <p><b>Gender</b> - {{ $patient->getGenderLabel() }}</p>
                                            <p><b>Education</b> - {{ $patient->getEducationLabel() }}</p>
                                            <p><b>Socioeconomic</b> - {{ $patient->getSocioeconomicLabel() }}</p>
                                            <p><b>Marital Status</b> - {{ $patient->getMaritalStatusLabel() }}</p>
                                            <p><b>Setting</b> - {{ $patient->getSettingLabel() }}</p>
                                            <p><b>Group</b> - {{ $patient->getGroupLabel() }}</p>
                                            <p><br></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
<script src={{ URL::asset('js/chartjs/chart.bundle.min.js')}}></script>
<script src={{ URL::asset('js/chartjs/chartjs-plugin-labels.js')}}></script>
<script src={{ URL::asset('js/analytics/summary.js') }}></script>
@endsection