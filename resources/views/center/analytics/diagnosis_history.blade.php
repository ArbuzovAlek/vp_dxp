@extends(( $layout ? 'center.app' : 'center.ajax' ))
@section('content')
<style>.art-content .art-postcontent-0 .layout-item-0 { margin-top: 10px;margin-bottom: 10px;  }
.art-content .art-postcontent-0 .layout-item-1 { border-style:Solid;border-width:1px;border-color:#0FB8AD; color: #0B0D0F; background: #EAF8FB;  border-collapse: separate; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-2 { color: #0B0D0F; background: ; padding: 10px; vertical-align: top; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-3 { margin-top: 40px;margin-right: 10px;margin-bottom: 20px;margin-left: 10px;  }
.art-content .art-postcontent-0 .layout-item-4 { border-style:Solid;border-width:0px;border-color:#9FADBC; color: #111418; background: ;  border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-5 { border-bottom-style:Ridge;border-bottom-width:2px;border-bottom-color:#98AFC8; color: #111418; padding: 0px; vertical-align: top;  }
.art-content .art-postcontent-0 .layout-item-6 { margin-top: 20px;  }
.art-content .art-postcontent-0 .layout-item-7 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:2px;border-color:#8799AB;  border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-8 { padding-top: 0px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
table {width: 100%!important; }
.table-title {display: none; }


</style>
<script src={{ URL::asset("js/history.js")}}></script>
<link rel="stylesheet" href={{ URL::asset("css/history.css")}} media="all">


<div class="art-sheet clearfix">
    <div class="art-layout-wrapper">
        <div class="art-content-layout">
            <div class="art-content-layout-row">
                <div class="art-layout-cell art-content">
                    <article class="art-post art-article">
                        <div class="art-postmetadataheader clear-style">
                            <h2 class="art-postheader"><span class="art-postheadericon">Diagnosis History</span>
                            </h2>
                        </div>
                        <div class="art-postcontent art-postcontent-0 clearfix">
                            @include('center.analytics.diagnosis_block')
                            <div class="art-content-layout-wrapper layout-item-3">
                                <div class="art-content-layout layout-item-4">
                                    <div class="art-content-layout-row responsive-layout-row-1">
                                        <div class="art-layout-cell layout-item-5 responsive-tablet-layout-cell"
                                            style="width: 100%">
                                            <p style="text-align: center; "><span
                                                    style="color: #fff; font-family: Verdana; font-size: 20px; font-variant: small-caps; text-shadow: rgba(97, 32, 0, 0.476563) 0px -1px 1px; ">Diagnosis
                                                    History</span></p>
                                            @include('history._history')
                                        </div>
                                        <div class="cleared responsive-cleared"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection