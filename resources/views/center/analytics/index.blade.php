@extends(( $layout ? 'center.app' : 'layouts.ajax' ))
@section('content')
<script src={{ URL::asset('js/analytics/recommendations.js') }}></script>
<style>
    .art-content .art-postcontent-0 .layout-item-0 {
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .art-content .art-postcontent-0 .layout-item-1 {
        border-style: Solid;
        border-width: 1px;
        border-color: #0FB8AD;
        color: #0B0D0F;
        background: #EAF8FB;
        background: rgba(234, 248, 251, 0.6);
        border-collapse: separate;
        border-radius: 5px;
    }

    .art-content .art-postcontent-0 .layout-item-2 {
        color: #0B0D0F;
        background: ;
        padding: 10px;
        vertical-align: top;
        border-radius: 5px;
    }

    .art-content .art-postcontent-0 .layout-item-3 {
        border-top-style: solid;
        border-right-style: solid;
        border-bottom-style: solid;
        border-left-style: solid;
        border-width: 0px;
        border-color: #B7C2CD;
        color: #111418;
        background: ;
        border-spacing: 50px 15px;
        border-collapse: separate;
    }

    .art-content .art-postcontent-0 .layout-item-4 {
        border-style: Double;
        border-width: 1px;
        border-color: #E1E8EF;
        color: #0B0D0F;
        background: #ECEDF9;
        background: rgba(236, 237, 249, 0.5);
        padding-top: 3px;
        padding-right: 5px;
        padding-bottom: 3px;
        padding-left: 5px;
        vertical-align: top;
        border-radius: 10px;
    }

    .art-content .art-postcontent-0 .layout-item-5 {
        border-style: Double;
        border-width: 1px;
        border-color: #E1E8EF;
        color: #0B0D0F;
        background: #ECEDF9;
        background: rgba(236, 237, 249, 0.5);
        padding: 5px;
        vertical-align: top;
        border-radius: 10px;
    }

    .art-content .art-postcontent-0 .layout-item-6 {
        margin-top: 20px;
    }

    .art-content .art-postcontent-0 .layout-item-7 {
        border-top-style: solid;
        border-right-style: solid;
        border-bottom-style: solid;
        border-left-style: solid;
        border-width: 2px;
        border-color: #8799AB;
    }

    .art-content .art-postcontent-0 .layout-item-8 {
        padding: 15px;
    }

    .ie7 .art-post .art-layout-cell {
        border: none !important;
        padding: 0 !important;
    }

    .ie6 .art-post .art-layout-cell {
        border: none !important;
        padding: 0 !important;
    }
</style>

<div id="analytics-help-modal" style="display:none;">
    <div class="info-note-popup">
        <div class="close-btn x-close"></div>
        <div class="content">
            Analytics provides in-depth information regarding the selected diagnostic record result.
        </div>
        <div class="close-bottom x-close"><p><a class="art-button">Close</a></p></div>
    </div>
</div>


<div class="art-sheet clearfix">
    <div class="art-layout-wrapper">
        <div class="art-content-layout">
            <div class="art-content-layout-row">
                <div class="art-layout-cell art-content">
                    <article class="art-post art-article">
                        <div class="art-postmetadataheader clear-style">
                            <h2 class="art-postheader">
                                <span class="art-postheadericon">Analytics</span>
                                <button type="button" class="help analytics-help">?</button>
                            </h2>
                        </div>

                        @if (!$diagnosis || $free_text_was_created)
                            <div class="art-postcontent art-postcontent-0 clearfix">
                                <div class="art-content-layout-wrapper layout-item-0 analytics-message">
                                    <div class="art-content-layout layout-item-1">
                                        <div class="art-content-layout-row responsive-layout-row-1">
                                            <div class="art-layout-cell layout-item-2 responsive-tablet-layout-cell" style="width: 100%">
                                                @if($free_text_was_created)
                                                    <p>
                                                        <span style="color: rgb(17, 20, 24);">
                                                            The selected diagnosis was created with free-text entries that do not account for in Analytics.
                                                        </span>
                                                    </p>
                                                @else
                                                    <p>
                                                        <span style="color: rgb(17, 20, 24);">
                                                            You have not selected a diagnosis for analytics.
                                                            Go to
                                                            <a href="/center/diagnosis">Diagnosis</a>
                                                            or
                                                            <a href="/center/results">Results</a>
                                                            to select
                                                        </span>
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="cleared responsive-cleared"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="art-postcontent art-postcontent-0 clearfix">
                                @include('center.analytics.diagnosis_block')
                                <div class="art-content-layout layout-item-3">
                                    <div class="art-content-layout-row responsive-layout-row-3">
                                        <div class="art-layout-cell layout-item-4 responsive-tablet-layout-cell"
                                             style="width: 33%">
                                            <p style="text-align: center;"><span style="text-align: left;"><span
                                                            style="font-family: Verdana; color: rgb(0, 128, 119); font-weight: bold; font-size: 16px;">Diagnosis
                                                    Summary</span><br><a href="/center/analytics/summary"><img width="150"
                                                                                                               height="168" alt="" src="/images/analytics/summary.png"
                                                                                                               style="width: 100%; max-width: 150px; height: auto;"></a><br></span>
                                            </p>
                                        </div>
                                        <div class="art-layout-cell layout-item-5 responsive-tablet-layout-cell"
                                             style="width: 34%">
                                            <p style="text-align: center;"><span
                                                        style="color: rgb(10, 118, 111); font-weight: bold; font-size: 16px;">Recommendations</span><br><a
                                                        href="/center/analytics/recommendations"><img width="150" height="168" alt=""
                                                                                                      src="/images/analytics/recomendations.png" class=""
                                                                                                      style="width: 100%; max-width: 150px; height: auto;"></a><br>
                                            </p>
                                        </div>
                                        <div class="art-layout-cell layout-item-4 responsive-tablet-layout-cell"
                                             style="width: 33%">
                                            <p style="text-align: center;"><span
                                                        style="color: rgb(10, 118, 111); font-weight: bold; font-size: 16px;">Diagnosis
                                                History</span><br><a href="/center/analytics/diagnosis_history">
                                                    <img width="150" height="168" alt="" src="/images/analytics/History.png"
                                                         style="width: 100%; max-width: 150px; height: auto;"></a><br>
                                            </p>
                                        </div>
                                        <div class="cleared responsive-cleared"></div>
                                    </div>
                                </div>
                            </div>
                        @endif




                    </article>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection