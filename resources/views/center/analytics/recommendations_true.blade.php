@extends(( $layout ? 'center.app' : 'layouts.ajax' ))
@section('content')
<style>
        .art-content .art-postcontent-0 .layout-item-0 {
                margin-top: 10px;
                margin-bottom: 10px;
        }

        .art-content .art-postcontent-0 .layout-item-1 {
                border-style: Solid;
                border-width: 1px;
                border-color: #0FB8AD;
                color: #0B0D0F;
                background: #EAF8FB;
                border-collapse: separate;
                border-radius: 5px;
        }

        .art-content .art-postcontent-0 .layout-item-2 {
                color: #0B0D0F;
                background: ;
                padding: 10px;
                vertical-align: top;
                border-radius: 5px;
        }

        .art-content .art-postcontent-0 .layout-item-3 {
                margin-top: 1px;
                margin-bottom: 1px;
        }

        .art-content .art-postcontent-0 .layout-item-4 {
                border-top-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-left-style: solid;
                border-width: 0px;
                border-color: #0FB8AD;
                border-spacing: 20px 10px;
                border-collapse: separate;
        }

        .art-content .art-postcontent-0 .layout-item-5 {
                border-top-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-left-style: solid;
                border-width: 1px;
                border-color: #11D0C3;
                color: #0B0D0F;
                background: #ECEDF9;
                background: rgba(236, 237, 249, 0.5);
                padding: 10px;
                vertical-align: top;
                border-radius: 10px;
        }

        .art-content .art-postcontent-0 .layout-item-6 {
                border-top-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-left-style: solid;
                border-width: 1px;
                border-color: #11D0C3;
                color: #0B0D0F;
                background: #ECEDF9;
                background: rgba(236, 237, 249, 0.5);
                padding: 5px;
                vertical-align: top;
                border-radius: 10px;
        }

        .art-content .art-postcontent-0 .layout-item-7 {
                border-top-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-left-style: solid;
                border-width: 0px;
                border-top-color: #9FB4CB;
                border-right-color: #9FB4CB;
                border-bottom-color: #9FB4CB;
                border-left-color: #9FB4CB;
                border-spacing: 15px 10px;
                border-collapse: separate;
        }

        .art-content .art-postcontent-0 .layout-item-8 {
                margin-top: 40px;
        }

        .art-content .art-postcontent-0 .layout-item-9 {
                border-top-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-left-style: solid;
                border-width: 2px;
                border-top-color: #9FB4CB;
                border-right-color: #9FB4CB;
                border-bottom-color: #9FB4CB;
                border-left-color: #9FB4CB;
                border-spacing: 10px 0px;
                border-collapse: separate;
        }

        .art-content .art-postcontent-0 .layout-item-10 {
                border-top-style: solid;
                border-right-style: solid;
                border-bottom-style: solid;
                border-left-style: solid;
                border-top-width: 0px;
                border-right-width: 0px;
                border-bottom-width: 0px;
                border-left-width: 0px;
                border-top-color: #9FB4CB;
                border-right-color: #9FB4CB;
                border-bottom-color: #9FB4CB;
                border-left-color: #9FB4CB;
                color: #111418;
                background: ;
                padding-top: 0px;
                padding-right: 0px;
                padding-bottom: 0px;
                padding-left: 0px;
        }

        .ie7 .art-post .art-layout-cell {
                border: none !important;
                padding: 0 !important;
        }

        .ie6 .art-post .art-layout-cell {
                border: none !important;
                padding: 0 !important;
        }
</style>

<link rel="stylesheet" href={{ URL::asset("css/center/diagnosis.css")}} media="all">
@include('center.add_edit_and_delete_diagnos_popup')

<script src={{ URL::asset("js/center/diagnosis.js")}} ></script>

<div class="art-sheet clearfix">
        <div class="art-layout-wrapper">
                <div class="art-content-layout">
                        <div class="art-content-layout-row">
                                <div class="art-layout-cell art-content">
                                        <article class="art-post art-article">
                                                <div class="art-postmetadataheader clear-style">
                                                        <h2 class="art-postheader"><span
                                                                        class="art-postheadericon">Recommendations</span>
                                                        </h2>
                                                </div>

                                                <div class="art-postcontent art-postcontent-0 clearfix">
                                                        @include('center.analytics.diagnosis_block')
                                                        <div class="art-content-layout-wrapper layout-item-3">
                                                                <div class="art-content-layout layout-item-4">
                                                                        <div class="art-content-layout-row">
                                                                                <div class="art-layout-cell layout-item-5"
                                                                                        style="width: 33%">
                                                                                        <p style="text-align: center;">
                                                                                                <span
                                                                                                        style="font-weight: bold; color: rgb(10, 118, 111);">Differential
                                                                                                        Diagnosis</span>
                                                                                        </p>
                                                                                        <p style="text-align: center;">
                                                                                                &nbsp;<a
                                                                                                        class="art-button additional-info-action {{ $diagnosis_info->differential ? '' : 'inactive' }}" data-field="differential">Learn
                                                                                                        more</a>&nbsp;
                                                                                        </p>

                                                                                </div>
                                                                                <div class="art-layout-cell layout-item-6"
                                                                                     style="width: 33%">
                                                                                        <p style="text-align: center;">
                                                                                                <span
                                                                                                        style="-webkit-border-horizontal-spacing: 15px;"><span
                                                                                                                style="font-weight: bold; color: rgb(10, 118, 111);">Comorbidity</span></span>
                                                                                        </p>
                                                                                        <p style="text-align: center;">
                                                                                                &nbsp;<a data-field="comorbidity"
                                                                                                         class="art-button additional-info-action {{ $diagnosis_info->comorbidity ? '' : 'inactive' }}">Learn
                                                                                                        more</a>&nbsp;<span
                                                                                                        style="-webkit-border-horizontal-spacing: 15px;">&nbsp;</span>
                                                                                        </p>
                                                                                        <p style="text-align: center;">
                                                                                                <span
                                                                                                        style="font-weight: bold; color: #0A766F;"></span>
                                                                                        </p>
                                                                                </div>
                                                                                <div class="art-layout-cell layout-item-5"
                                                                                        style="width: 33%">
                                                                                        <p style="text-align: center;">
                                                                                                <span
                                                                                                        style="font-weight: bold; color: rgb(10, 118, 111);">Associated
                                                                                                        Risks</span><br>
                                                                                        </p>
                                                                                        <ul>
                                                                                                <li><span class="additional-info-action analytics-list-item-link {{ $diagnosis_info->suicide ? '' : 'inactive' }}" data-field="suicide"
                                                                                                                style="line-height: 14px; color: rgb(255, 95, 15) !important;"><span
                                                                                                                        >Suicide
                                                                                                                        Risk</span></span><br>
                                                                                                </li>
                                                                                                <li><span class="additional-info-action analytics-list-item-link {{ $diagnosis_info->functional ? '' : 'inactive' }}" data-field="functional"
                                                                                                                >Functional
                                                                                                                Consequences</span><br>
                                                                                                </li>
                                                                                                <li><span class="additional-info-action analytics-list-item-link {{ $diagnosis_info->other_risks ? '' : 'inactive' }}" data-field="other_risks"
                                                                                                                >Other
                                                                                                                Risks</span><br>
                                                                                                </li>
                                                                                        </ul>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="art-content-layout-wrapper layout-item-3">
                                                                <div class="art-content-layout layout-item-4">
                                                                        <div class="art-content-layout-row">
                                                                                <div class="art-layout-cell layout-item-6"
                                                                                     style="width: 50%">
                                                                                        <p style="text-align: center;">
                                                                                                <span
                                                                                                        style="-webkit-border-horizontal-spacing: 15px;"><span
                                                                                                                style="font-weight: bold; color: rgb(10, 118, 111);">Diagnostic
                                                                                                                Information</span></span>
                                                                                        </p>
                                                                                        <ul>
                                                                                                <li><span class="additional-info-action analytics-list-item-link {{ $diagnosis_info->features ? '' : 'inactive' }}" data-field="features"
                                                                                                          ><span
                                                                                                                        >Associated
                                                                                                                        Features</span>&nbsp;</span><br>
                                                                                                </li>
                                                                                                <li><span class="additional-info-action analytics-list-item-link {{ $diagnosis_info->prevalence ? '' : 'inactive' }}" data-field="prevalence"
                                                                                                          >Prevalence</span><br>
                                                                                                </li>
                                                                                                <li><span class="additional-info-action analytics-list-item-link {{ $diagnosis_info->dev ? '' : 'inactive' }}" data-field="dev"
                                                                                                          >Course</span><br>
                                                                                                </li>
                                                                                                <li><span class="additional-info-action analytics-list-item-link {{ $diagnosis_info->prognostic ? '' : 'inactive' }}" data-field="prognostic"
                                                                                                          >Prognostic Factors</span><br>
                                                                                                </li>
                                                                                                <li><span class="additional-info-action analytics-list-item-link {{ $diagnosis_info->functional ? '' : 'inactive' }}" data-field="functional"
                                                                                                          >Functional Consequences</span><br>
                                                                                                </li>
                                                                                                <li><span class="additional-info-action analytics-list-item-link {{ $diagnosis_info->markers ? '' : 'inactive' }}" data-field="markers"
                                                                                                          >Markers</span><br>
                                                                                                </li>
                                                                                        </ul>
                                                                                </div>
                                                                                <div class="art-layout-cell layout-item-6"
                                                                                     style="width: 50%">
                                                                                        <p style="text-align: center;">
                                                                                                <span
                                                                                                        style="-webkit-border-horizontal-spacing: 15px;"><span
                                                                                                                style="font-weight: bold; color: rgb(10, 118, 111);">Diagnostic
                                                                                                                Cluster</span></span>
                                                                                        </p>
                                                                                        @include('center.analytics.cluster_diagnoses')
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                        <div class="art-content-layout-wrapper layout-item-3">
                                                                <div class="art-content-layout layout-item-4">
                                                                        <div class="art-content-layout-row">

                                                                                <div class="art-layout-cell layout-item-6"
                                                                                        style="width: 33%">
                                                                                        <p style="text-align: center;">
                                                                                                <span
                                                                                                        style="font-weight: bold; color: rgb(10, 118, 111);">Gender</span>
                                                                                        </p>
                                                                                        <p style="text-align: center;">
                                                                                                &nbsp;<a data-field="gender"
                                                                                                        class="art-button additional-info-action {{ $diagnosis_info->gender ? '' : 'inactive' }}">Learn
                                                                                                        more</a>&nbsp;<br>
                                                                                        </p>
                                                                                </div>
                                                                                <div class="art-layout-cell layout-item-6"
                                                                                        style="width: 33%">
                                                                                        <p style="text-align: center;">
                                                                                                <span
                                                                                                        style="font-weight: bold;"><span
                                                                                                                style="text-align: left; color: rgb(10, 118, 111);">Culture</span></span>
                                                                                        </p>
                                                                                        <p style="text-align: center;">
                                                                                                &nbsp;<a data-field="culture"
                                                                                                        class="art-button additional-info-action {{ $diagnosis_info->culture ? '' : 'inactive' }}">Learn
                                                                                                        more</a>&nbsp;
                                                                                        </p>
                                                                                </div>
                                                                                <div class="art-layout-cell layout-item-6"
                                                                                     style="width: 33%">
                                                                                        <p style="text-align: center;">
                                                                                                <span
                                                                                                        style="font-weight: bold; -webkit-border-horizontal-spacing: 15px; color: rgb(10, 118, 111);">Treatment</span><br>
                                                                                        </p>
                                                                                        <p style="text-align: center;">
                                                                                                &nbsp;<a data-field="tx"
                                                                                                         class="art-button additional-info-action {{ $diagnosis_info->tx ? '' : 'inactive' }}">Learn
                                                                                                        more</a>&nbsp;<br>
                                                                                        </p>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                                <div class="art-content-layout layout-item-4">
                                                                        <div class="art-content-layout-row">
                                                                                <div class="art-layout-cell layout-item-6"
                                                                                        style="width: 33%">
                                                                                        <p style="text-align: center;">
                                                                                                <span
                                                                                                        style="-webkit-border-horizontal-spacing: 15px; font-weight: bold; color: rgb(10, 118, 111);">Other
                                                                                                        Conditions&nbsp;That
                                                                                                        May Be a Focus
                                                                                                        of Clinical
                                                                                                        Attention</span><br>
                                                                                        </p>
                                                                                        <p style="text-align: center;">
                                                                                                &nbsp;<a href="/diagnosis/302"
                                                                                                        class="art-button">Open
                                                                                                        cluster</a>&nbsp;<br>
                                                                                        </p>
                                                                                </div>
                                                                                <div class="art-layout-cell layout-item-6"
                                                                                     style="width: 33%">
                                                                                        <p style="text-align: center;">
                                                                                                &nbsp;<a
                                                                                                         data-diagnosis_id="{{ $diagnosis->id  }}"
                                                                                                         data-formal="{{ json_encode($formal) }}"
                                                                                                         class="art-button recommendation-edit-diagnosis"
                                                                                                         accesskey="Review, edit and revised diagnostic process data.">Edit
                                                                                                        diagnosis</a>&nbsp;<span
                                                                                                        style="font-weight: bold;"><br></span>
                                                                                        </p>
                                                                                </div>
                                                                                <div class="art-layout-cell layout-item-6"
                                                                                     style="width: 33%">
                                                                                        <p style="text-align: center;">
                                                                                                &nbsp;<a
                                                                                                         class="art-button recommendation-add-diagnosis"
                                                                                                         title="Start a new diagnostic process for selected patient.">Add
                                                                                                        diagnosis</a>&nbsp;<span
                                                                                                        style="font-weight: bold;"><br></span>
                                                                                        </p>
                                                                                </div>

                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div>
                                        </article>
                                </div>
                        </div>
                </div>
        </div>
</div>

@include('center.analytics.additional_info')

@endsection