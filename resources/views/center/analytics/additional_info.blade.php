<div class="popup closable" id="additional_info_popup">
    <div class="info-note-popup">
        <div class ="close-btn x-close" data-action="popup_close" data-target="additional_info_popup"></div>
        <div class = "content">
            
        </div>
        <div class="close-bottom x-close" data-action="popup_close" data-target="additional_info_popup"><p><a class="art-button">Close</a></p></div>
    </div>
</div>

<div id="additional_info_container">
@foreach($additional_info_list as $additional_info_key)
    <div id="{{ $additional_info_key }}">{!! nl2br(e($diagnosis_info->$additional_info_key)) !!} </div>
@endforeach
</div>