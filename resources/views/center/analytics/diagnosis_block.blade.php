<div class="art-content-layout-wrapper layout-item-0 analytics-message">
    <div class="art-content-layout layout-item-1">
        <div class="art-content-layout-row responsive-layout-row-1">
            <div class="art-layout-cell layout-item-2 responsive-tablet-layout-cell" style="width: 100%">
                <p style="display: flex; justify-content: center;"><span style="-webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px; font-weight: bold;">
                    {{ $diagnosis->name }}
                </span><br></p>
                <p><span style="color: rgb(17, 20, 24);">The diagnostic criteria 
                @if($full_criteria_met)
                have been met
                        {{ $diagnosis_option->options['assign_to_patient']
                            ? 
                            'and diagnosis has been assigned to patient Id '.$patient->user_own_id
                            : 
                            ''
                        }}.
                        </span></p>
                @else
                have not been met
                @endif
            </div>
            <div class="cleared responsive-cleared"></div>
        </div>
    </div>
</div>