<ul>
    @foreach($cluster_diagnoses as $diagnosis)
    <li>
        <a class="analytics-list-item-link" href="/diagnosis/{{ $diagnosis->id  }}/criteria">{{ $diagnosis->name }}</a>
        <br>
    </li>
    @endforeach
</ul>