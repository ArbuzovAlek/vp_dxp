@extends(( $layout ? 'center.app' : 'layouts.ajax' ))
@section('content')

<style>
    .art-content .art-postcontent-0 .layout-item-0 {
        margin-top: 1px;
        margin-bottom: 1px;
    }

    .art-content .art-postcontent-0 .layout-item-1 {
        border-top-style: solid;
        border-right-style: solid;
        border-bottom-style: solid;
        border-left-style: solid;
        border-width: 0px;
        border-top-color: #9FB4CB;
        border-right-color: #9FB4CB;
        border-bottom-color: #9FB4CB;
        border-left-color: #9FB4CB;
        border-spacing: 30px 10px;
        border-collapse: separate;
    }

    .art-content .art-postcontent-0 .layout-item-2 {
        border-top-style: solid;
        border-right-style: solid;
        border-bottom-style: solid;
        border-left-style: solid;
        border-width: 1px;
        border-color: #ECEDF9;
        color: #0B0D0F;
        background: #ECEDF9;
        background: rgba(236, 237, 249, 0.5);
        padding: 3px;
        vertical-align: top;
        border-radius: 10px;
    }

    .ie7 .art-post .art-layout-cell {
        border: none !important;
        padding: 0 !important;
    }

    .ie6 .art-post .art-layout-cell {
        border: none !important;
        padding: 0 !important;
    }
</style>

<div class="art-sheet clearfix settings-sheet">
    <div class="art-layout-wrapper">
        <div class="art-content-layout">
            <div class="art-content-layout-row">
                <div class="art-layout-cell art-content">
                    <article class="art-post art-article">
                        <div class="art-postcontent art-postcontent-0 clearfix">
                            <div class="art-content-layout-wrapper layout-item-0">
                                <div class="art-content-layout layout-item-1">
                                    <div class="art-content-layout-row">
                                        <div class="art-layout-cell layout-item-2" style="width: 25%">
                                            <a class="{{session('show_settings_modal') ? 'click-onload' : ''}}" href="javascript:void(0);" data-action="popup" data-target="dxp_popup"><p style="text-align: center;"><span
                                                    style=" color: #0A766F;">DxP</span></p></a>
                                            <a href="javascript:void(0);" data-action="popup" data-target="dxp_popup"><p style="text-align: center;"><img width="63" height="60" alt=""
                                                    src="/images/settings/v-leg-8.png"></p></a>
                                        </div>
                                        <div class="art-layout-cell layout-item-2" style="width: 25%">
                                            <a href="/center/records_cabinet"><p style="text-align: center;"><span
                                                            style="color: rgb(10, 118, 111);  text-align: center; -webkit-border-horizontal-spacing: 20px; -webkit-border-vertical-spacing: 10px;">Records
                                                    Cabinet</span></p></a>
                                            <a href="/center/records_cabinet"><p style="text-align: center;"><img width="68" height="68" alt=""
                                                                                                                             src="/images/settings/RC.png" class="" style="margin-top: 11px"><br></p></a>
                                        </div>
                                        <div class="art-layout-cell layout-item-2" style="width: 25%">
                                            <a href="/center/groups_manager"><p style="text-align: center;"><span
                                                        style="color: rgb(10, 118, 111);  text-align: center; -webkit-border-horizontal-spacing: 20px; -webkit-border-vertical-spacing: 10px;">Groups
                                                    Manager</span><br></p></a>
                                            <a href="/center/groups_manager"><p style="text-align: center;"><img width="69" height="69" alt=""
                                                                                src="/images/settings/GM.png" class=""><br></p></a>
                                        </div>

                                        <div class="art-layout-cell layout-item-2" style="width: 25%">
                                            <a href="http://35.224.165.111:8080/documentation"><p style="text-align: center;"><span
                                                    style="color: rgb(0, 128, 120); ">Documentation</span>
                                            </p></a>
                                            <a href="http://35.224.165.111:8080/documentation"><p style="text-align: center;"><img width="62" height="62" alt=""
                                                    src="/images/settings/Manual.png" style="margin-top: 15px;" class=""><br></p></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="art-content-layout-wrapper layout-item-0">
                                <div class="art-content-layout layout-item-1">
                                    <div class="art-content-layout-row">
                                        <div class="art-layout-cell layout-item-2" style="width: 25%">
                                            <a href="javascript:void(0);" data-action="popup" data-target="export_popup"><p style="text-align: center;"><span
                                                            style="color: rgb(10, 118, 111); ">Export</span>
                                                </p></a>
                                            <a href="javascript:void(0);" data-action="popup" data-target="export_popup"><p style="text-align: center;"><img width="80" height="80" alt=""
                                                                                                                                                             src="/images/settings/Export.png" class="" style="margin-top: 5px;"><span
                                                            style="color: rgb(10, 118, 111); "><br></span></p></a>
                                        </div>
                                        <div class="art-layout-cell layout-item-2" style="width: 25%">
                                            <p style="text-align: center;"><a href="/center/settings/print" target="_blank"><span
                                                            style=" color: #0A766F;">Print</span></a></p>
                                            <p style="text-align: center;">
                                                <a href="/center/settings/print" target="_blank">
                                                    <img width="80" height="80" alt=""
                                                         src="/images/settings/Print.png"
                                                         class="" style="margin-top: 5px;">
                                                </a>
                                            </p>
                                        </div>

                                        <div class="art-layout-cell layout-item-2" style="width: 25%">
                                            <a href="http://35.224.165.111:8080/users"><p style="text-align: center;"><span
                                                    style="color: rgb(10, 118, 111);  text-align: center; -webkit-border-horizontal-spacing: 20px; -webkit-border-vertical-spacing: 10px;">Account</span><br>
                                            </p></a>
                                            <a href="http://35.224.165.111:8080/users"><p style="text-align: center;"><img width="65" height="65" alt=""
                                                    src="/images/settings/account-2.png" class=""><br></p></a>
                                        </div>
                                        <div class="art-layout-cell layout-item-2" style="width: 25%">
                                            <a href="http://35.224.165.111:8080/users/security.php"><p style="text-align: center;"><span
                                                    style="color: rgb(10, 118, 111); ">Security</span>
                                            </p></a>
                                            <a href="http://35.224.165.111:8080/users/security.php"><p style="text-align: center;"><img width="67" height="67" alt=""
                                                    src="/images/settings/hipaa.png" class="" style="margin-top: 5px;"><br></p></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </article>
                </div>
            </div>
        </div>
    </div>
</div>

@include('center.settings.popups.export')
@include('center.settings.popups.dxp')

@endsection