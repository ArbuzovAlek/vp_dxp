<!DOCTYPE html>
<html dir="ltr" lang="en-US" class="chrome chrome80 desktop custom-responsive">
<head>
    <meta charset="utf-8">
    <title>Diagnostic Report</title>
    <meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width">


    <style>
        @page {
            margin: 1cm 0 1.5cm;
            padding: 0;
        }

        @page :first {
            margin: 0;
        }

        footer {
            position: fixed !important;
            bottom: 0px;
            left: 0px;
            right: 0px;
            background-color: lightblue;
            height: 50px;
        }


        #art-main {
            background: #FFFFFF;
            margin: 0 auto;
            font-size: 14px;
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
            font-weight: normal;
            font-style: normal;
            letter-spacing: 1px;
            line-height: 150%;
            position: relative;
            width: 100%;
            /*min-height: 100%;*/
            /*left: 0;*/
            /*top: 0;*/
            cursor: default;
        }

        table, ul.art-hmenu {
            font-size: 14px;
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
            font-weight: normal;
            font-style: normal;
            letter-spacing: 1px;
            line-height: 150%;
        }

        h1, h2, h3, h4, h5, h6, p, a, ul, ol, li {
            margin: 0;
            padding: 0;
        }

        /* Reset buttons border. It's important for input and button tags.
         * border-collapse should be separate for shadow in IE.
         */
        .art-button {
            border-collapse: separate;
            -webkit-background-origin: border !important;
            -moz-background-origin: border !important;
            background-origin: border-box !important;
            background: #FFFFFF;
            background: rgba(255, 255, 255, 0.75);
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.11);
            -moz-box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.11);
            box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.11);
            border: 1px solid rgba(84, 119, 156, 0.75);
            padding: 0 10px;
            margin: 0 auto;
            height: 30px;
        }

        .art-postcontent,
        ul.art-vmenu a {
            text-align: left;
        }

        .art-postcontent,
        .art-postcontent li,
        .art-postcontent table,
        .art-postcontent a,
        .art-postcontent a:link,
        .art-postcontent a:visited,
        .art-postcontent a.visited,
        .art-postcontent a:hover,
        .art-postcontent a.hovered {
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
            letter-spacing: 1px;
            line-height: 150%;
        }

        .art-postcontent p {
            margin: 12px 0 12px 5px;
        }

        .art-postcontent h1, .art-postcontent h1 a, .art-postcontent h1 a:link, .art-postcontent h1 a:visited, .art-postcontent h1 a:hover,
        .art-postcontent h2, .art-postcontent h2 a, .art-postcontent h2 a:link, .art-postcontent h2 a:visited, .art-postcontent h2 a:hover,
        .art-postcontent h3, .art-postcontent h3 a, .art-postcontent h3 a:link, .art-postcontent h3 a:visited, .art-postcontent h3 a:hover,
        .art-postcontent h4, .art-postcontent h4 a, .art-postcontent h4 a:link, .art-postcontent h4 a:visited, .art-postcontent h4 a:hover,
        .art-postcontent h5, .art-postcontent h5 a, .art-postcontent h5 a:link, .art-postcontent h5 a:visited, .art-postcontent h5 a:hover,
        .art-postcontent h6, .art-postcontent h6 a, .art-postcontent h6 a:link, .art-postcontent h6 a:visited, .art-postcontent h6 a:hover,
        .art-blockheader .t, .art-blockheader .t a, .art-blockheader .t a:link, .art-blockheader .t a:visited, .art-blockheader .t a:hover,
        .art-vmenublockheader .t, .art-vmenublockheader .t a, .art-vmenublockheader .t a:link, .art-vmenublockheader .t a:visited, .art-vmenublockheader .t a:hover,
        .art-headline, .art-headline a, .art-headline a:link, .art-headline a:visited, .art-headline a:hover,
        .art-slogan, .art-slogan a, .art-slogan a:link, .art-slogan a:visited, .art-slogan a:hover,
        .art-postheader, .art-postheader a, .art-postheader a:link, .art-postheader a:visited, .art-postheader a:hover {
            font-size: 32px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
            font-weight: normal;
            font-style: normal;
            line-height: 120%;
        }

        .art-postcontent a, .art-postcontent a:link {
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
            text-decoration: none;
            letter-spacing: 1px;
            color: #852C00;
        }

        .art-postcontent a:visited, .art-postcontent a.visited {
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
            text-decoration: none;
            letter-spacing: 1px;
            color: #597EA6;
        }

        .art-postcontent a:hover, .art-postcontent a.hover {
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
            text-decoration: underline;
            letter-spacing: 1px;
            color: #FF7A38;
        }

        .art-postcontent h1 {
            color: #697F96;
            margin: 10px 0 0;
            font-size: 30px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-blockcontent h1 {
            margin: 10px 0 0;
            font-size: 30px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-postcontent h1 a, .art-postcontent h1 a:link, .art-postcontent h1 a:hover, .art-postcontent h1 a:visited, .art-blockcontent h1 a, .art-blockcontent h1 a:link, .art-blockcontent h1 a:hover, .art-blockcontent h1 a:visited {
            font-size: 30px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-postcontent h2 {
            color: #8A9CAD;
            margin: 10px 0 0;
            font-size: 28px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-blockcontent h2 {
            margin: 10px 0 0;
            font-size: 28px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-postcontent h2 a, .art-postcontent h2 a:link, .art-postcontent h2 a:hover, .art-postcontent h2 a:visited, .art-blockcontent h2 a, .art-blockcontent h2 a:link, .art-blockcontent h2 a:hover, .art-blockcontent h2 a:visited {
            font-size: 28px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-postcontent h3 {
            color: #FF7A38;
            margin: 10px 0 0;
            font-size: 26px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-blockcontent h3 {
            margin: 10px 0 0;
            font-size: 26px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-postcontent h3 a, .art-postcontent h3 a:link, .art-postcontent h3 a:hover, .art-postcontent h3 a:visited, .art-blockcontent h3 a, .art-blockcontent h3 a:link, .art-blockcontent h3 a:hover, .art-blockcontent h3 a:visited {
            font-size: 26px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-postcontent h4 {
            color: #496888;
            margin: 10px 0 0;
            font-size: 18px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-blockcontent h4 {
            margin: 10px 0 0;
            font-size: 18px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-postcontent h4 a, .art-postcontent h4 a:link, .art-postcontent h4 a:hover, .art-postcontent h4 a:visited, .art-blockcontent h4 a, .art-blockcontent h4 a:link, .art-blockcontent h4 a:hover, .art-blockcontent h4 a:visited {
            font-size: 18px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-postcontent h5 {
            color: #496888;
            margin: 10px 0 0;
            font-size: 15px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-blockcontent h5 {
            margin: 10px 0 0;
            font-size: 15px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-postcontent h5 a, .art-postcontent h5 a:link, .art-postcontent h5 a:hover, .art-postcontent h5 a:visited, .art-blockcontent h5 a, .art-blockcontent h5 a:link, .art-blockcontent h5 a:hover, .art-blockcontent h5 a:visited {
            font-size: 15px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-postcontent h6 {
            color: #A2B7CD;
            margin: 10px 0 0;
            font-size: 13px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-blockcontent h6 {
            margin: 10px 0 0;
            font-size: 13px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        .art-postcontent h6 a, .art-postcontent h6 a:link, .art-postcontent h6 a:hover, .art-postcontent h6 a:visited, .art-blockcontent h6 a, .art-blockcontent h6 a:link, .art-blockcontent h6 a:hover, .art-blockcontent h6 a:visited {
            font-size: 13px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
        }

        header, footer, article, nav, #art-hmenu-bg, .art-sheet, .art-hmenu a, .art-vmenu a, .art-slidenavigator > a, .art-checkbox:before, .art-radiobutton:before {
            -webkit-background-origin: border !important;
            -moz-background-origin: border !important;
            background-origin: border-box !important;
        }

        header, footer, article, nav, #art-hmenu-bg, .art-sheet, .art-slidenavigator > a, .art-checkbox:before, .art-radiobutton:before {
            display: block;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        ul {
            list-style-type: none;
        }

        ol {
            list-style-position: inside;
        }

        html, body {
            height: 100%;
        }

        /**
         * 2. Prevent iOS text size adjust after orientation change, without disabling
         *    user zoom.
         * https://github.com/necolas/normalize.css
         */

        html {
            -ms-text-size-adjust: 100%;
            /* 2 */
            -webkit-text-size-adjust: 100%;
            /* 2 */
        }

        body {
            padding: 0;
            margin: 0;
            min-width: 700px;
            color: #111418;
        }

        .art-header:before,
        #art-header-bg:before,
        .art-layout-cell:before,
        .art-layout-wrapper:before,
        .art-footer:before,
        .art-nav:before,
        #art-hmenu-bg:before,
        .art-sheet:before {
            width: 100%;
            content: " ";
            display: table;
            border-collapse: collapse;
            border-spacing: 0;
        }

        .art-header:after,
        #art-header-bg:after,
        .art-layout-cell:after,
        .art-layout-wrapper:after,
        .art-footer:after,
        .art-nav:after,
        #art-hmenu-bg:after,
        .art-sheet:after,
        .cleared, .clearfix:after {
            clear: both;
            font: 0/0 serif;
            display: block;
            content: " ";
        }

        table.position {
            position: relative;
            width: 100%;
            table-layout: fixed;
        }

        .art-shapes {
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            overflow: hidden;
            z-index: 0;
        }

        .art-object1133370623 {
            display: block;
            left: 1%;
            margin-left: 0px;
            position: absolute;
            top: 11px;
            width: 34px;
            height: 34px;
            background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAiCAYAAAA6RwvCAAAACXBIWXMAAA7DAAAOwwHHb6hkAAALSklEQVRYR81YeVRTVx7u4nSfLtPa1qV1xEpVauuCqEPVKlbFoS64IKDsIlTcq7Q9ncpxaI+ddjoux6lWcWcRMQiBEAhLCElIQlgCpBBjKkZNKdXHwYhPeEnu/L6Y2GE69fS/mXvOd8i793fv/d5vfzzwfzsYYw96fuL3w4RBnr8Pedfw1zP3COEpwrOEV/eWlAzCupHjX6bn4YQ/EJ4kPEp4GGu/edAGXIgLHiM8TniC8HsCDsT87wg4GPNPEwYTRmMuXd723rIcZdLcXNmzMo4DwdcIIAVZ7MML3HvRXx0khLcEARyCS14k4M3wxiDyAgHzIPYcYSjhdQIufHBFobYhrKy1e3pm1RG9zTbYcyaI4hyQf8h90f0GhDzCuPAlwliC+zAMjuMxN8QDr+ohM5HwUMgZ5eFIcWP/1vLv2Cblxdvryltqc0zWVfxdWZCGVtym+9VBAlAZfAGmwAVPYH63TPbYAbU5JjhXW5wka9nBce71MQQQeIMQQBicKmvYukCkv5ksbWQbpA3ChxVNbEudjYWUNqtEJmuA5w5o8jcRgQ1hgidERtu0gCPS9DEHJdKQs7prM0QGtrS4QaG0WKfQOvxhJmEOwe+A2rR0VqaqM5pIJEl0zuRCjfBxjYmtqTLfTFO1p1s47o8k9wwBRNx+4rn2vw8SgFmG4HdYnnLtiG9KbszJVbOl51QsRtLAwiWNnfv15s0kA42EC4zNEtPbTjouN64o1LO4Yp1jbaFa2F7Z6govN7PPdN8fsnL8eJL1IYwieCMIZOAG96JwwKBJqB1O9VyGxjQuMLOmeeFZFQvPkzvixWrnyqJGIUXWUtzZ0zuPZObb7PaAGacqS2adrWVRRbXOmHxlf4qkzhFR1sY2yS/UmLvsi0huGgHaA6FXCTgf2oG/IChAamBYexbgVL54jhDVfj3hWMWtVflKZ+x5lSOu1MDiZN+ZSyydoSQzPbVMf8T3SBkRrXGF5VX3x55XOiOkRrZV3l5qvt77LskEEhYS3ia8RRhHQITBtDAXnB6u8KibgHfQBHIE2CIUXzqgaXs3MKu6ZbFIw9acq3YmldSzTTWWH3epLZ/s1ZgTntonYSvzNSz8bDWLyVeziJImtkbWbv1abYosMVlnKi51LrZc71nAC0KgwAR/QRD8eJ4fSWcP5YgA+Y43ET7roXDPWZFDsDiC4Mvx/IiowtrMgBMKR0KR1pUo1ro+Vl5gG8uM2qkZspRpWdUXZmQqemfnyH+ac6a2L6KiravY1LF5dYHukF9GRfnsU4q8jZLGowfrLhw6Zez48nhLx9aTBssykdE6XWyyjUmTGUbaeB5aedFD4+6gCdgLiQpJahJh2G51W/L0k1W2yCI9W0t+kFJuYJ+orbZvdObNp/Xm0EV5dd8kSJt2zTuvyzzRfnVXV5f9tcU5yo7pZ9RsPuHt41Us4FgVCzxdw4LO1vYG52svLzqv0a8trq/cWd0q2qVoi6J7HvdQcJOANuCs8OxhhMkEP30XFxgh1pYGnVWylCKNK1lazxLlJtf+eouE1l8hwN6junh3dIyvstimTDlafinolIxFnJWzNeeVLE5M+4rr2ZbyZrajpo19pDSx7cqLbJ3ie7aqSPOth8LdQYfAURHnyJ4INRAJ4ezCmJ3y9l1BOWohqVjLYot0zk2qi7VKmx2J7B0PEBlTCdMJ44JyNdUTjlc4Qs8o2KpzarZapHTF5iuc6wpJo+I6YVtZI+WZdpaqu9x92GBJpD1Pemi4icBJh/A8G0L5AW+JxIXwm1lqvhq7NFdpWC1rZ/FK6+Xduo4UmofTzSAguyISkGlBxudYi23h2IxSw6RjlTdD89pvhReZ+BiZsS++osX5flUL215pZJ+oLrKNyu+7Dmva5tOeZzw03EReJC8eukYke0NkMPnYycPJy+c4nWxZl10Yna5q/SIwWyns1FiOWe08tIHQ9CcgwmAiEPMjgEyQzsrCMvTOFXEFl7YtzNPtiZM2Z6VUtBhjJQ19McV1/Skqq/B+y80rB5QGvMgAIs/EieSjx35bdunNjNL+aRmya/OyFeqwQu3hlOKWpOg8TeK2Ut375i7uPZINIcwmIDcga4II8gK0AjORJrmpZn5JWt7lQfUa27YtvMCm0EtFURJcIjJdnZNSYghLqDLGGG388yT/c9TQw9NrRMo5M3PUXSGiWraE0nroecqSJc13omXGW+tlzfbkUoOdEpomy2RbQPJI8/AJXwLIwJzQlA8nT5tq/+vM2v7NE27f+PQVx5X8YKm9S7Oc1pDcgsn0E0jjoyinoCojfJ/z0HATeTRarIyflavmFmZVstDTZa6V2XJX9DklSyzUshSJnn1Q2YLMKnyoNkkY707VIAI/+ZMH0JLfjcPLRXfW+TpY/GTmWjeROaJ9LjJxBvwK2gomgDACAn0KMvnP4YsRnC0vmH1Gw1YW1LHV+VoWU6hjcdI6hpDdUKR1bqKitknaQBW2sXdfY8c/6QD4CDSBkEcShJon9u5ZYXPEjWWutZMcrnh/Z3/U8P6fpDuy7IxHAIAM+hfUHcgjgf7cGtDDg28fKfv7Y5/ntA07KL00/mTNjamZKjaLiL0norAlMlsVbewvGgvbXnuFfaC5dMHIcS9rbD3LDZ29W04arR+kKqyfGTk2wpka8oUrcnS3K9Hf5UqY5Ojb+AbrTX1L5SzNgDbc+YaApIlIHdgW0MPzGluXb5bBMnW/zhT8lcaU8IW2PT1d0fbtDmljVmxRo2hRQaM4tLjRsF5t6d6nt55Iq2iYGyapb08qa+2MlzT9EC1r6klrcBUWyPJSrn0VeMm1fhxzJUx2OJInO13LXukRjn68x9OtgQiS5kBtYNAEelEsoDwPtlNx4nhhfJedn0bl/B391evzqjquh8o6rq/X2LgkkbEjMji3tn4JmTCSTJhYXMd2alpZROUtlsX1LbmxJ+KoK2r0Hdda0kqcv1NIHsu6976j4I0lIAFzItJQTh7xULg7aAINC2wGprA38gJyBEo3whTOiAQ3i7w9IElaL1lUUM+SpHXO9cVaIVXe7IytNLEP5WaxVWDj+ZPbt/Rsfv1Hx/oJjPzEJWyawG4nv/qDcCg1mc5AlcfZcFQ0YwNM4+1XoRXYzksK3j2BgAw4t4fn53xU05zlf1rBYvO1roQijbBBWu/YoO1gkSVNKk3HVfQh823WkoU/7pvb4EiEefydIMNifRmXsbyAY0aELEoJ7hhIBIMm0Lqh+Hk/KaA6hBgS1WhqC4bvVhg/fS2jgq3Ir2Wx51RCYoHKSd0YCytvu1xssrpLAiGGY/xwxz/WHXKt8uljif7MSbi9YxRrPzO0u8GettxzH9zhF03RAFb0DDJQIdQ3hPqGF/ZqjLF+h0quBedUs6g8pSOqgAohNcwrpMbuQ3rzBpJD6zCbEhZayZE9pX/7c/eO101Cki/rj/e5xX32Zr1ZFZ5h6cuZS+vebvCXGvnPQQIg464DMot18lvHyprGnVBSR1bFVmdXC1FFdSysrK1/nUT3JcnBr5DgQOZNwhgLszzdpA7bbzsY0Ch8Gf45L/tqoudcVHmEr/vLEXP3HSQEIvCZxylnDNtY1rxrRrbqSlB2NQsXUXmXtbmWFzWWeeoFHBpFDzUHlyAqXrII4vEmPnek5zxoGH4H/wCZgWb5tUGC8BmoELH/POYydMYFs7PlliBx05155/WtVSYr6gwuR+3BJbA7+lCoHfuwH0GANe+6mwTh/h9b3kGC3o8ubPRW2EFVJptf4FFxZnpVw2J6xiXIC7gUqR6yuBxATkIEghT+wvkhD838NhLegQ0EHIoLUOiG6S2cu+um37Ax3tJbMyD37x9PAPZjDvD+e+P+zvm/HQ888C+nNVAAg8TbvgAAAABJRU5ErkJggg==');
            background-position: 0 0;
            background-repeat: no-repeat;
            z-index: 7;
        }

        .art-headline {
            display: inline-block;
            position: absolute;
            min-width: 50px;
            top: 32px !important;
            left: 50%;
            line-height: 100%;
            transform: translateX(-50%);
            z-index: 102;
        }

        .art-headline,
        .art-headline a,
        .art-headline a:link,
        .art-headline a:visited,
        .art-headline a:hover {
            font-size: 28px;
            font-family: 'Lucida Sans Unicode', 'Lucida Grande', Arial, Helvetica, Sans-Serif;
            font-weight: bold;
            font-style: normal;
            text-decoration: none;
            letter-spacing: 3px;
            text-align: center;
            line-height: 150%;
            padding: 0;
            margin: 0;
            color: #8C3103 !important;
            white-space: nowrap;
        }

        .art-slogan {
            display: inline-block;
            position: absolute;
            min-width: 50px;
            top: 12px !important;
            left: 50%;
            transform: translateX(-50%);
            /*margin-left: -99px !important;*/
            white-space: nowrap;
        }

        .art-slogan,
        .art-slogan a,
        .art-slogan a:link,
        .art-slogan a:visited,
        .art-slogan a:hover {
            font-size: 20px;
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
            font-weight: normal;
            font-style: italic;
            text-decoration: none;
            letter-spacing: 2px;
            text-align: left;
            text-shadow: 0px -2px 0px rgb(255, 242, 235);
            padding: 0;
            margin: 0;
            color: #7C94AB !important;
        }

        .art-header {
            border-radius: 10px 10px 0 0;
            border: 1px dotted #D0E9F1;
            border-top: 1px solid #D0E9F1;
            border-bottom: 1px solid #D0E9F1;
            background-repeat: no-repeat;
            height: 95px;
            background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAB4AAAABfCAYAAAAJbdqyAAAACXBIWXMAAA7DAAAOwwHHb6hkAAA78ElEQVR4Xu3d6WIky3Ed4Hn/F7EtW7I02kVtFEnt4iZqs62N66XIS5F6AndiOvqeDpzMiMzKWvv8+ORBxanIrEI1gEYQ1x/+5qc//djrr+/8x73+6o7VjGWuil1zhPURERERERERkX6t99q978Ux3zqvVSvw/FbOtLIjPZhWpqdPgXl2jq97UQ57sZyvs4xZO2d6MyiTQa1stlct44/bx/54tm58DkXZqG6iusFe2XMis/rIe3veW1tbn1sREdnD0ADYY43Xht9Az85fk/3bjnu+Zuexc7AmIiIiIiIicgUz3+9m3j9bJrtuK9+qeZht5XszPTmTrXs92dn8eq31W7XC6lEuK+rn66iW9cdrorzVs5klOV/Dj+2YF2V83US52vGaVq9Xtdc9OfLn4ch7ExGRa5syAK5hC85k30Cvil3zUmwdERERERERkTMZeY9by2d79axpWZZv1TzMtvK9mZ6cyda9nuxsbO2ilWW1As9v5bIy/XzGsGyPqE9mLczMyJlMPsr4usnmsmb2uoq97ok+DyIiIu99+Obt/6BvuI/XUtaZ4evAf3x2tWucCXtf2atet4iIiIiIyFm13sP1vrdr9crUC8y0chE8N9tzNIdZVkPYh+V93UQ5X2cZb2a+VUM+Z5bm/HEPe9TOYZnC50xUz8B1ULbuc9ExX/OibO04g9lMPmN2vz3M2j/ei6hfJpMxq4+3Rk8REZGt0AGw5zNrYOuOsG/4V8auexa23lW82vWKiIiIiIic2cz3bplembUyfXpgv1bP0RzLskwxKxfVWcabmW/VkM+ZpTl/3MMetXNYpvA5E9WzcK1az6juRVmsoyhbO85gNpPPmN1vD3vcj5lrruHo+xMREWn58M3PP//4cDsQDWQx0+T6jWDrZ9k36OibNOaugF3jEmwNERERERERkTPJvtftzfRmWabI5kwr36ohn8tivYpMLpNB2bzPmVodz/Wy2WzORLlML8xkcqzGtHpirZbJ5jIZk81l+F4mm3sVs+/DGvc103P2urOvQUREZCsfvvX55x8NGwazkwoc1DZBr1F+Xfx4Fvvh4KrYNc/A1roidu0Fy4qIiIiIiMjxZN/PZeuI5YrZOdPKt2rI57JYryKTy2RQNu9zplbHc71sNpszUS7TCzOZHKsxrZ5Yq2WyuUzGZHMZvpfJ5uQ4Mp8rfU5FREQ+eRoAtwbBbBhb489ZE1t/BP5w4H9IYLWrwHuwNrb+mbBr8th5IiIiIiIiMm72+66oD641ki2iXG/da+VbNcbnTSvnawhztWwmU4zmUJTDei1T+JyZlWvVPMxm8pGon6+zjIlyvs4yTCafrffmemT7YC7Kyhey96v33vb0ZcdFRESOiA6AvbMMgwu2h9nsh4IrYdc5G1v3rNj1RWrnYl8RERERERF5Nvs9VLYPrlnL+4xh2aJWx3NZ3WvlWzXG502U83WTyWUyxWgORTms1zKFz5lZuVbNw2wmH8n0y2SKKOfrns+bngyrFdijJ9cj2wdzUVa+0Hu/svnZORERkSN4/v8D+IYNgNEjezt5dPjKzl0T28MSvT8UnBG7nq2w/ZwFu55erK+IiIiIiMhVzX4/lO3Vm8tkM2b0wT2hTJZlUCuHfXpyUd34nMnWazL5KOPrZmYuk0G9+Rrfx2SzIxmTyUZ1E9UN9mJ5X1/T7LVn9VnLzGvNmr3elnsXERFZ4sM3f/qfH598/sm3Pv95yLIP0OcbHfC8rbB9rO3rJ8SuYytsP2fHrjPC+oiIiIiIiFzF7Pc/2V7ZdbO5rBl9cE8ok2UZ1Mphn55cVDc+Z7L1mkw+yvi6mZnLZFBvvsb3MUuyUd34HMtGdRPVDfZieV9f0+y1Z/VZS/Zas9cxOyciInIlH/CDpyHpfajLBr81SwfBBs8PkTV7sT1swX748D+E+ONHgvvfGtvPGbFr68X6ioiIiIiInNle73kya87cW08PXLd1Xm+mlfN8Hnv4mseyBcvOlFnPZ7xa3h/3opzVPZbN6u0V5TK9MIOiDNZ7c0VPNhL18fWzyV5L9nqzuRbrEfXJrpXtV2RzIiIiV/E0AEZPQ9LOQbBZOhDG85oWDoBr2J5mwh9S8AcQf/xIcP9HxfZ9dOw6RrH+IiIiIiIiR7fX+5rMupnMGnDd1tq9mVbO83nsgfAc1JOdJbuez6Fa1h/3opzVPZbN6u0V5bBXLeczZlbO143PtbKRqI+vn032Wnpz/vwe2T31yPZbY20REZEj+/Dt2/+J/K35HP3843eSSvZxHvRja2Vhn7Sn/d+xXAXbx1a+dVc7nq3P5tc7GrZP3P+R1PaHex/le4qIiIiIyGva833C3u9PovUz+7NMLYf1WgZlc5GRNT2WLVjOHzP+XJPNZfX0i3JYR1HO183MXCZT+Jxh2SJbR7NzJsr6OstkcyxTRDlfPwN/Da3rmJ07ujPvXUREpEdqAFy8G4g+hqifBrxs8MvMHgZjj5THvu9adV+rYPuabfSHKztva2wvR8eu40jYnmdh64mIiIiIyPXs+T5g7/cf0fpWj/Y4K1NkcxHrk+2F+ei8KBfVs5kePf2iHNZRlPN1MzOXyaBsPltHLFewbJHJFJhrZaMM1ms5liminK+fRfY6ZudERETkGD58+z9v/+j0tzU///nH73Qo+TeuD1sz4nvsje1xT98KsHMKls1i/c6CXc9RsP3OxNYUERERERGZYc/3H0dc0+oeyxbZeitjMJvJR6J+eLyW8XpztXyr5mGW5aO615trZTFTy7UyrZrnsyabzWQKn+vJtur4sc8ZzLOcrxufW0PPWq29YY3VTVRHmX49RtZmNWOZTM+erIiIyNl8YMNLw07w2HlvYNCbxYbBBVt3BOu9Bb8+7mkr/ocZ/AEHj3s+18P3wL5ng9d1dGz/o1h/ERERERGRUXu+5zjimlb3WLbI1lsZg9lMPtLTL5MpenO1vK+zjIlyUd3rzbWymKnlWplWrcafw85jGRNlfZ1lTJTzx2s5Y/VazteNz62hZ81MLpPJmtmr6OmVyVom07MnKyIicjbNAbDHGhiWf+OGvFlrDoNZ3z2wvZ0B++EIj9XUsnb8zPw1HQHb51JsHREREREREVkm+94rqjPZnlFupuxauK+efO048hlvVi7bB9k5tfOinnh+T85kslHda+WwTytnstlsLjKrz5oy+8NMLRfVRURE5Dy6BsAt1pDVDBv0ZtSGwQVezAjWcy9sf2eCPyQytRz2uAp/jUfA9jkLW09ERERERERysu+xojqT7RnlZsquhfvqydeOI5/xZuWyfZCdUzsv2zPKWd3LZKO6l8mN9GtlMdPKRWb1WVtmj5hhuVZNREREzmXaABhZc1Z7cEPerLdhcEF64oWNYD2PgO31TDI/VGb488+KXdse2N5m8WvguiIiIiIicn57/Kxva0brYi7KzpRZM7sn7MXyUR1lsz5nanU8d22z1sT9Rz1n50wmH9W9Whb7sLrxOZPJRnUvymK9ljE92YxsH1wzk2+Z2avI9MtksrJ9MBdle8zsJSIicjWrDIC73Ae6bNibRfvesAvuxfruje3zyKIf7qyexXqgTOZI/PXtje1xFraeiIiIiIiczx4/42ffW2Auys6UWTO7J+zF8lEdZbM+Z2p1PHdts9bE/Uc9Z+dMJh/VvVoW+7C68TmTyUZ1L8pivZYxPdmMbB9cM5NvmdnLZPplMlnZPjPXNDN7iYiIXM3+A2BEhru9WF+7WP8xwnwrV7DsEbC9ngn+IJjR6sFqZ4HXuDe2v5nYmiIir4p9nSxYVkREZG97fL/ya9bWzWTWMmtN3H+rXyaXyRQ+Z2p1PHepNXsvld3TmnuPemfrXiYXZXzd+JyZmctkUDaX0bPuTNl1MVfLtmook7NMpt9Me6wpIiJyFscaABsy2DV/14A5+8ti2v+G3YxMBvn8Io29jmJ7voLWD3dX/MGvdk12fA9+L1thexERuQr2da9gWRERkb3t8f3Kr1lbN5NZi19zdH3r4/t5mVwmU/icqdXx3KXW7L1Udk9r7j3qna17mVwmU+yRy2RQNndk2evFXC2Px1nd1M73sjk0cg5aer6IiMiVfVhj8DjVbX842C3Y8Bf5vIkGwuxYD3/+0bA9y/nZD7t7YvtaC1tfROSM2Ne4Wdh6R8euo2DZXrP7iYjIF/T1lct+79ny/uGeetbszTMj625t9v5m9rNeUc8og3WUyWUyxcycz0Q5VmN681uK9mb1KFdks1F9TXuuLSIicnWfBsDBcPQQbvtjQ102AGbYuXQdwG4Yy5ne/BGwPcs52Q/NXlSfCfezNra+iMiZsK9ts7D1jo5dR8GyvWb3ExGRL+DX11f6OmvXXbvmqG4ymVlwTz1r9uaZkXW3Nnt/M/tZr6hnlME6ymQzGZZjGRPlsJ7JsRrTm99StDerI5YrenOstrbM/tay59oiIiJbeB4Ae7fAk6i+lfv6NshlA9+MxyC4YOvc+BvGMp4/x7DsMNt3sP8ebM9yLfjDrf8B19eWwL57YfsSEdkC+5q0N7ZPhp2L2DlLsXVm6V0H8yIi0qf3a2qUw16tXI81ei3t19ujJ2/ZWj7bK5vbU2uPVouuAXOtbG+O1Xrheq2eozmTyY1karkiyvk6y/Sa1WcNeJ2ZPfbmj2ivvZ/9vomIiETaA+BZbgtRrVrWrcesQbCZsi9gNzt7PM32usKeje1RrqP1Ay7WZvB9ca0t4F5ERJbo+dqC2aNg+2TYuYidU5PN+zVm6l0H8yIi0od9XS1YtsjWo1yRyRTZXIb1Wtqvt0dP3rK1c2rHvWxuT609Wi26Bsy1sr05VuuF67V6+lwty3JFJpfJFKM506rj+SNm9VkDXmdmjz6fOadm6fmj9l53j7VFRES2sGgA/DQwBThMZfU3t8UfonrG7Rxckw16s7DP0F62Fu3zXp91PexBkmvBH4LXxNbeCtuPiEhN7esHHpf2vcEaqx+J36uIiORlv6ZG9R7ZXjPX3MvINRzxurN76t17b56xHlGvqG6yuQzrFfUczfksq5tMpsB+rXw2J9yMe4c9lvQRERGRYxgaAOOA9DFUBL5ufA6FudtmU25Z7GX8kPfvHV83vk/XXo4E9z/5OtiDJefmf+hfG9vD1ti+ROS82OscsXNaauficWnfG6yx+pH4vS61Vl8RkazM1yHLZHOsVmAfVMv54y1Rr1rdZHMRdu6MvkXUY2SNkXOYbJ9MzjJRNqp72X4oyvi6yWQKzKFWltUKPH+NHIrqRaafz5go5+toJNfKZjJHN/MasNeMflei+yEiImfyaQB8+8fTYDBAh6LA10f4Xtjfjj8+Lvv3IIdwuOuHwAhzCHtV1z6q+54fe2f7xxqrJ7AHTV6Df5OwFFtjC2wvInIu7LXNsHMZdq5wrft2lfuJ15gxep6IyCyZr0OWWSPHsrXjLdE52Z7ZHGPn+vNrx3vN6LGWzN4s05NrZbM5E+WwVy2byaAo6+vG51iWZYpMzmdquYJli0wuqhvMtbKZXFQ3mGtlM5lXpHvy3hr3RPdYRETW8uFpeOeHfwEchu7pbT94HZ7LF36wy4bADJ7je0b7KDecHd+c7XUJ1jfgHz65LvuB2IvqEVxjT2xvInIs7LXLsHMZdq7UXf2+4bOR0XOez9rH/rjIFc18xq/wmlnjfkQ9ezJRNpvrEfXC9VAt54+3YD92fu14rxk91lLbmx33fC1zjqnlsAcT5X09mzE+W8tncqOZwudMlME6E+V93UT1AvugKDtSN5hrZTMZLzon2683l8lnMhmz+lzN7HuieywiImt5HgB7bPBH+EHontj+PH9OdQj8i1+8PwZwGFz4vvSeHknZY+BxLQzrmcQeRnk99mYCRXWDub2x/YnIsbHXssja2LPo+Syej8dFrmjmc36F14xdw8zrmNkz22fWej1szZlrz+53JXhv/P2pHfcwx7K+bnzOZHNFlMN6NlPLFZlcJlP4nJmV83Xjcz2yvaL63qJriOomkymwXyufzYmIiMj1ffGfgGb8oM8+dnDoyYaFvr4Vvw8G8zjMfRr2lkFwDeTw/AJ7v62H9/YM3DU8riOD9QuwB1Suzb8x8W9OWN1g7ij83nC/IjKXf81hLcNepyJbYs9ihPVB7ByRs5r5XF/hNWLXMPM6sOfSvtkeM9bqZWvOXHtmr6tp3evsfcMetbzPoGzW51i2t25m5jKZwudMNmcyuZ5+GdhvVs8tZfbem+nJmSjn6yIiIvI6ngfAfnhX+3iQHybuKdobDnJTg+ACszfYo7Dej/t5BmW/CXjv3q7RsJ5J7IEVOTP/RkxExkWvL18XOSr/7HrsHMTOEZHrWOO1rq8hcmb4/CKWLTK5KBPVjc+ZKOfrKMpgH5TNmUyup18G9pvV84iy1zk7JyIiItIeAHtWR7XjCXRgCMfWhmu21rUB7tOA9z70/QfnaSBs7uf4QbB5W9/f66Oy/QJ/PYjlH1j/BvYAi9Sc5c0QvnETkU8yrxWWEbmC3mfd5/BcEbkOfN0vfa3P6jNqz7VlP6PPXe0c7MfqXivvawYzKJOL6giztbzPGJYtfB3PacEetfNaNTQ7d1Z2fdF1zs6NyvRca20RERGZ66UHwBG/Fxvgmtog2H9sx9gwuMA13u6Bv+9Hdt8zXkONZatY/wb2QIuYM70hsb3KeelzORe+Pmr3lmVErmT0WcfXiYhcx8zX+qw+o/ZcW/Yz+tzVzsF+rO618r5mMIMyuaiOMFvL+4xh2cLX8ZwW7FE7r1VDs3NnN/N+WCbTr1em51prb+0q1yEiIlLzaQDM3Irv1HLs+E168Ofg0PCocIBrQ102DGYew+Difi72w3Xe7on/PByV7Re8uxZ3DOF5b9gaCexBl9dzpR/k7Vqylpxbgz1n9r2KI9wX/PygKMvqW8P9iMgnS18f+Brb2hH2IHJGrdeNva4Qy5koG9VbRs5BS8+Xc7LPe+/nftbzgutH/aJsVDcjOVYvMINYtvB1PKcFe7TOy/TM9noV2fsR1ZmRc2oyfWaut6erXIeIiEjNBxy8VQdxg0b62TlHxfaIw9veYXCRHQi/3aPbJ+1U/P7h44jlH1j/BPbgi1yNvXHxb17w+BLYc2Zfmcd/jkyUZfWt4X5EZA7/+sLX3Nr2WFPkCjKvHctE2SgX1VtGzhEZNet5sz6ZflE2qpuRHKsjzLJ8re6Pt7T69cA+S3tdSe/9yOR7e7bM6iMiIiL7exsAR2wAV/u3h+f6c/Bjf5zJZI4EB7c4DGaD3xYcCGNPv97b/bl9Ig8P93nft7+WLDv/AdcJsBeByJX5N4L2cQ2eOyrq2VsftUbPWXBvS/fney3B+rdE52FvETk29hoetWZvkSvD10z0GsJ6LVNE9RFr9JRzyTwDlqnlsF7LoCiHvVq5YiTXykY5X+/hexmWXcOSNfFchp1TsGzBsmfUe12ZbLaXiIiIvJbUADjLBnLRcfvYH2cymSPCoW0xOgg20V8HF2/36vZJPY37nvEaRlifB7ZWgL04RK4I3zwy7JxeUc/e+qg1es6Ce1u6P99rCda/JToPe4vIsbHX8Kg1e4u8iug1hPVapojqI9boKeeSeQYsU8thvZZBUQ57tXLFSK6VjXK+3sP3Miy7hiVr4rkMO6dg2YJlzyx7fZmMiIiICDN1AFxjgzn/sT+OfMZj5xwZDm1tGDw6ELZzh4fBrdqeYP9L2DPyhK3XwF4sIld2xDeU+EYXjeRYHfX06smZnuyRsH2jWg57iMh5rfXa9n2v4urX1+sVPude5nqz9wVzrWxUHxGti/VaRq4t8wzY8UyOqeX8cYPnIpYtMrlMpvA5k81FfB/E8jONrOXz2APhOSibOzt/ndG1RnWT6ZU1s5eIiIhsb5MBMIPDuajOsHPOAoe2S/8yuLBhMA6E/Zrv7uHtk39YsE9/HUtgX7ougS8W/7GIrMvebHojOVZHPb16cqYneyRs36iWwx4icl5rvbZ936u4+vX1OtPnfNZeMz38Wq1zfJaJ6iOidbFey8i19TwDUQ7rqJbzxw2ei1i2yOQymcLnTDYX8X0Qy880spbPYw+E56Bs7ipmX+tRe4mIiMj2dhsA93ga3N2x3Bk9DYKL+0DXD3n/0fF1Y+fjMLg6EL49ACm9+Zls7Rt/DUth7we2hwb2ohIRERGRZewXjiJb2Pq5w2cdtbKstpbsnlqZEbP7yfrwOYg+d1EO67VMkc2ZWm6kR5SPcr6ORnKM7yv7y35u9DkUERGR2T6w4eCR2aCO1XrM6DETDmuL2jA4GgB7mWFw8W4AiseQ1fd03wu7jllGr5e9yERERESkn/0iVGRtWz9z+JyjVpbV1pLdUyszYnY/WR8+B9HnLsphvZYpsjlTy430iPJRztfRSI5h58i+sp8ffQ5FRERktrcBcG0oeAVsWPgY7sGxI8FhbWHDYD/cxb8IZny+sIEwDoVxbbw3iNZvD9Cu7vvA/a9h9JrxhcaOiYi8Cv0yQ0SWsK8hTCYjMgs+l4hli1Ydz0c+gx9HfC92flRH2VyvWs811sqytTN7yOZeAd6L6H6M5FjW143PmVbO1zzMsnxUN6M5k8kYzGLeH7+qI19vdm8zr2Gk16y1RUREZH+PATAbBnqZzNGwISE7ljV63ij83NjgdmQYbPCcaBjcYvew6faAbYasz/Y9w9M6bC8B9kIUEbmymb/EEJHXY19DmExGZBZ8Lpla3h/HmufreE4E+9TOj+oom+tV67nGWlm2dmYP2dwrwHsR3Y+RXCs7I+drHmZb+Wy9N2cyGYNZzPvjV9W6F6Nm9cO9tfpF9R6Z9dDMtUVERGR/7wbAtSFgNndmONhj9aJVW5u//2wYzIa+NXheYQNhXIPtg2H3Do89uT14m7mth/tcU++1sRekiIiIiCyHv/AU2Vv0TM58drFXq1+tzo7bMZZfIuqJ67ZyPWb1GukTnWP1GTmfyeZYpsjmWnp7ZPIjGZTNbp3zdRRlfX1E1AvXa+XWkFmzJxNls7kim43qvTJrioiIyGvwM6hFA2DTyvrakeGQktWPxu6xDYKXDINNaxhcsH1k4f1tIg9q9XjWvTfb1xqa15LAXrwiIiIikoO/DBXZW/R8tmq9sFern6/jOXjc1/D4UlFPXLeV6zGr10if6Byrz8j5TDbHMkU21+J7GJYtRnJR3ctmt875Ooqyvl7TymM/lonqa8qum6lnemVzRTYb1XvhmjP7ioiIyHmwGVPxAYdW0bDP171WPlNjOWnD+1i8DYMLGOZOGQYXsI5he5rhaXhqH/vjo2492JpbWHIN7IX9SrL3ozfHaiIiInIN/peiJpuT1+KfC8OyRVTPwrWWyvaMclZHmYwst8f9za45c2/WK9uzlsMerM5EeazXMkVUN9iLYefUROf6+hllriWbYcd7ZdZClp+1fsYea5o91xYREXkVOO/o8TQALvyAjw35WMZkc4jlsY/k4T3sHQb/Ezlm8HwbBvuBMNvPUn5Y+xigGnyg/ccR6INrbK173zfsi8DVZe+Bz5lazh8XERGR68BfSqJsTl6Lfy4MyxZRPQvXWirbM8pZHWUystwe9ze75sy9Wa9szyiHvXqyUb2W6eH7eeycmuhcXz+j7LVEuT3vR/YaZrH19rjmPdcWERG5EpxljPJ93g2ATWaoh8M/FNVnwH0I5++ZHwazQW8WGwjjWmw/a8FBLj7cb2rHEZzP+m8l3GcH/8XD68keDe59JrYW6s2LiIjIeeAvL1/ZHvfjSJ8D24vHsgXLFizbi/VFtVz2/Bo7vyV7js8Zli2yuZYZPZhW31aNiXLYC7Fskcm1agj7ZPKRTC/MoCjn6yxTy5neHKuhKGd1L5M5upnXgL2iflEO67VMr2y/bE5ERERel5859GD9vNMOgD3cl3B2r+yvd2cPg98GwcW9P35+CranmXCQa97Vbw/9O1arwB5ZS67Zr/m2D7bvgL3I/cd4jNWOzu99Jrae6cm+It0TERE5M/wl7Svb434c6XNge/FYtmDZgmV7sb6olsueX2Pnt2TP8TnDskU21zKjB9Pq26oxUQ57IZYtMrlWDWGfTD6S6YUZxLJFlIvqqDfHaijKWd3LZI5u5jVgr6hfJpfJ9Mj2y+ZERETk2vxcoRfrmVUdAPfAIZ8fevma15OdAdd7VXg/2DDYD4TLfxraw7qHfdhfBxdsX1vAwe4I1nNLj73cXryz4RcGdgzheYhl18LW31p2L7jvq3qlaxURkevCX9a+opH7EWWxXytX48832dyIbO8o5+soymEdZXNZvp/pzbCcrxufM1E9A9fp6RXlsafP+ZrBDJqdK6Ic1muZEdm+vZlWrsjkMFPL+Uw2xzJFlPN1L5PN9uoRrYv1nhzLFD5nopyvG59j2ajeo7dPb15ERETOBecAo1jfGaYMgCN++Odlc1vC/V+Zv24/DPbD3dYQmB1nw+AjDYQNDnkj7PwRM+7B255uL+RZ7AtD7eMMO2dtbO2jY9dxJbVrfLX7ICIi54W/pH1FI/cjymK/Vq7Gn2+yuRHZ3lHO11GUwzrK5rJ8P9ObYTlfNz5nonoGrtPTK8pjT5/zNYMZNDtXRDms1zIjsn17M61ckclhZmkukyminK97mWy2V49oXay3ckVvBkU5Xzc+x7JRfU296+6xRxEREenjf989ivWeaZMBsPEDL5TNbQ339Qrw2qNhcIEDYT/8ZdhAGNfEPfg92cd72GIA7LFzMh57vb3A92JfYGrHM6LzsH427HqujN0DxM4RERE5Evzl7ZWNXncmj32jbOHzW2D7KFi2YNliNDci6oXrsZyvm97MCNZzL2x/RZSN6rVMEeWiupfJYgaxLKplfB+TyUZ143MsyzImk43qBnMs6+smqiPsx86pHY9E51m9lctkipm5TKaYndvTkfcmIiLyStjvsXuwnlvbdACMWsMuXzsTvI6zs2uyv9rFYXCBg10/CK7Bcwo/DPYDYcT2eDQjA2J2rRmsF7P3MDgDvyhF9VbubNh1XRm7B4idIyIicgT4y+IrG73ubL6nN2a3wvZRsKzJ5Fmm8LkRUS9cj+V83fRmRrCee2H7K6JsVK9liigX1b1MFjOIZVEt4/uYTDaqG59jWZYxmWxUN5hjWV83UR1hP3ZO7XgkOs/qvTmWjepmJMfqJpsrenLs+JqyexMREZHl2O+os1i/o9ltAFy0Blk46Do7vCa8xjPB62EDYRzqssFvDZ5nvXAgjOv2YtdxZOwaMlivlsdfCBe3LwJyHPaF2X98NXjNPVgvERGRLdkvZVtGzrmC7DVj7iiiveH+WS6qrylaE/eFsjnGn4N9EJ7Tk1tTbU3cD1PL4nFWR2vnTG8GRTlfNyM5Vi8ww0R5X/eZqI5YtojqBnv1mtWnV2ZdzNRyPlPLFVG9wD6tfKs2Ilrvil7pWkVE5HWw3zVnsX5nsesAuAcbfF0Ju1Y8tjbcS7Q25vwguMChLhv61uB52C/z18Gj2PXtje2zB/bBvhENhI/Lf+GuHT8bvMYRrKeIiMia8BfBNSPnXEH2mjF3FNHecP8sF9XXFK2J+0LZHOPPwT4Iz+nJram2Ju6HqWXxOKujtXOmN4OinK+bkRyrF5hhoryv+0xURyxbRHWDvXrN6tMrsy5mZubs375uLIf5tW29HpNZf+Ye97xWERGRWdjvknuwnmd0mgGw8UMuf/yK/LWuoXdNny9sSDvjL4MLPHeLYXDBrvUI2F5Hsf6M/kr4fNgX+TNh19SL9RURETkS+yXtqNn9ilm9cF+1fj5zBNH+sF7LzOLXirAezGi+dQ5mRszqU+C+Wj2jnK+jWg6PM5ht5WfmMpnC50yU83VUy+FxBrOt/EiO1QvM9OTMaK5HtkdvrpWP6j16e0VZ7JfNslqPzFpryqw9c397XaeIiEgv9rvhHqzn1VxmAGxw0HVltWv192NLfi/pYfB//dfzxxV+GLzFQLiGXf9W2H5GsN6Rx1DY3L6IyLGwL/Rnwa5nBraWiIjIXuyXtKN6+mUyRTaXhf3s3+zjo2B7r9VrmV61fng8A89tqeWj46zmYXYv2T1lcj5jahk83hLlsWcrZ1o57INGcz7L6gZzXiuHNTQr5+ssY1i2iHK+bnyuR7ZPdi3sV8tnMijKYa9WrujJtPphvZbpMbMXyvRbY92WLdcSERFpYb/nHcF6v4rTDYAz2LBL3mP3zmQyEethA1o2DGaD3nfKgNjcj9n5tYGwrb0mdq2stia/7mxsTUbD4GOxL/Ds2BnhdSzF+ouIiOzBfqHrZeteJhvVl1ir71Ud5X7N2ANeCxrNGZatYecXLFu06ng+ymZZrohyvm58zrRyvmYwg1i2qGXwuIc5tHeuVfNYtojqBnshls3InI/rtPKZTOFzPVmWKXpzrGaiPlivZXrN7lfM7GXW6CkiIrIl9jvdHqznq7rkALhgwyypy9w3vL8+z2oGe6SHwTj0ZVy+Ogwu7mviPrbC7sdsbN01sLU9/XXwebFvEEfD9j0TW1NERGRt+Atl/KWtP27wXC/K9h7vYT2W9nkVeL8Qy65p6bq4dy+b9TnTytaO19Ty/jjWvGyW5Yoo5+vG50wr52sGM4hli1oGj3uYQ7Vc7WN/3OvNtWoeyxZR3WAvxLIZmfNxnSifyfmMyWRZphjJsbqJcqzWyhdR/QzOvn8REbk+9nvZHqyncJcdAHt+iIWDLenD7iureT5bTBkGo/s51YHwfU22l72wezWK9d8C20uhYfD5sG8UR8H2uxa2voiIyJqyv5SOciib7ekp89n9P9vnAPfNROew+tqya+M+fb52/Mhq+8VrQT5nsnUvk2WZIqrvAffd2ls2V2RzEeyDMjmfQVEO661cEWWw3soVmQzK9mM1ERER6cN+55rBesmYlx0AezjMkmXY/UXsnGLqMBjOsT44ED7CXwcz7H6NYL23wvaD9FfC58a+kWyN7WstbH0REZE92C+le345nc329JT57P6f7XOA+2bYOUWrtrZob8ZyLF87fmS1/eK1IJ8z2bqXybJMEdX3gPtu7W00h1i+hfUoRnMmyvm68TmfZfUCMz1ZVkfZXqwmIiIibfa7VP/71SzfT5Z7mQFwBhtmybpq93/qMBjd+7SGwRoKv7fknrC9oHfDYP+xnAL7BrM2to+tsP2IiIiszX4xrV9Oizni86Bn9Dzsc+WxbJGtR7kikymyuTVEa2f3Zjmf98cZ7MPO8XUzmjMj2VYNsVwR5Xwd9WRFRERkHPs9aRbrJ+vSALgTG3DJMq17/BjIwsD23UCYDXozoA/2Lo7418Gev29ZrFfk8XlwWDaD7ct7DILN7QuWHFv5poL/3gvuaQtsDyIiImu56i+39Uv7MUe8b/pcnpc+d89670eUtzpTy+FxX2N14+vROVhv5Yoo11s3ozmTzYmIiEgb+/1nlu+BfWV7GgAPYAMt2YYNHm1Q+zQILtigF/wfcuzN/Xw2EB4dBlt+5NwR7Fll2LkZdh29WC+P7ZPRMPhc2DedPbC9rQHX8nsQERGRNv3CXmR/eh0+670fUd7qXitbO16r10TnYL2VKzK5qF5gppUrZudERETkGf6OcwTrKfvTAHgQG2LJth5DRhjWPg2E78PdMvStwXptGIwD4bdhcIHr37D9GZ+N8lsYfY7ZtWSwXjX4Omt5+utgdPvCJufAvimtha2/B7Y3ERER0S/tRY7g1V6D+HUnc91RPqqbqI5qOd8j6mn1Vs5nZudYpshkikwuk9nb0fcnIiLXxn5fmcX6ybFpADxBbXCFQ62rGBnobeExbLwPap8GwQUOeB0cCL8bBhvoiQPhx1D4vj7bm3ns0WHZIqrvBfc+C1vH86+vGg2Cz4t9k1oDW3trbF8iIiKvTr8UF9nXq70G7Xqz1x3lo/qIbL8og31qOZ8xmZzPsBzLoEw2kzm6s+9fRESOjf0eMov1k3PTAHgDbJh1VGw4V7TqeP4RvO3LDWmfBsL3oS4b/LbgMBgHwrhOZhhsddRTPwq2zyd2P+CeZLC1PPY6q3k3DLZjrCaHYt+oaseX8D2PgO1TRETkleiX4uekz5uclT272Wc4m8/mMrJ9ogzWa1k77o3mzEiO1U02d1Rn3ruIiBwX+11jFusn16AB8AbYAOuo2ECu6M2wnK9v5W39XzwPg22I+xjq3rHBL/N03r2XHwjXBp9P+1oBXvva2PpP7D54rQzWAFsfsdee9274i25fEOWc2De3COtzVGz/IiIiIkehYYaclT272Wc4m8/mMrJ9eteK8lbPZlq5ojfHaijbT0RE5ErY7w2zWD+5Pg2AN9YaXLHa1tjgrchklsI11vBY6xfPw+DHQPg+0GUD3wwcCPthsB8Iv9vTBvBerO1pbbv2Eb6Xw9ZG+NqL0IFw7bicWvnmx46fhf9GXjsu4uk5ERGRNWgII1fln2372B+v6clnc1lRL1yvlovqKJPN9sqa3U9ERGRv9ju+JVhfeV0aAO+gNpzCwdXeWoM2X9tKa33cX8bj3F+sMwwu/DAYB8KZ4eaRsHsYedfHrrvh6f5k3Huz9T3/emvxQ993x29fPN/4j0U2ZN/Ia8dFPD0jIiIiInl+wGgf++M1PflsLivqhevVclEd9WSzZvcTERE5Kvy9Xi/WT8RoAHwAbFh1ZE9DvQPCfeK+mcd594GiDSBXGQYX0NvWegw8cT837/Z4APTewTEGc2/segm8J4/7kuXWYXtB7LWY9Rj8otsXVHq8KDWRg2I/HMg16XO+HL522L2M6mvYcq0tXOlaRETk3GYNIDN9smutlatlo3qBmVaupnbeaD8REZEjsvfuI1g/kYyXGQDPGPqsBYdSZ+WHb0fG9m8euV98MYT0w2A24O3VGgjXBpqbW7gHel9r8Lpv7F708D3oOje4L4+9PrPKoBf//e7j2xfcBzvmYUZkI+yHg6guchazn2N8bbC+UX0NW661ha3vn4iISM2sAeTMQWa2V2+ulm/VDGZauZraeaP9RERE9lJ7P4vHW3wee4iM+LB06DEiM2yxTC2H9VrG+Kxh2SKT2QPui+3N17fGhm2bCAZ+LanruPW2IeMaw+AiHAiTfdG9FnZO49w3PodZf8x/nFTdI4Nr3Nj1j/C9nsCa+Hln/GtsCTrwZW5fmEW2ZD8QsJrBHxww64/LuMw9nXXP1/rc2TWs1X8Jvyfca89+/XkZrI+J6kWmD+rJRnrXniWzrmUyWREREWmzYeuMgesVBrd7XkNmbcvstUcREXnm35/2YP1EZhkaAEfDkWw9mxmV6YVr1rI+M2pGv9a+sLYHHKzhcM0fn44M9t6pDAAznq7hdr4NGdcaBhe1YfDTcBP31Qv7LFXr59fsQfrhPVjqqTdZ355dz7/mZqBD4OL2BXoXtfX33tfVHfz++h8eWIbx5x3BUfeW2desfdtaPf0y+WzfbG4mXJNh5zDs3AzWq+jJsBrTk62xNXvXniWzLu4vyoqIiEjbzIHiFQaTe15DZm3L7LVHEZFX5N+DZrFeIluZ+hfAflhifKaVx2wtszW/p1GZnphp5QqWxf/3SNiQbXV+0Mew8zLu59tA0Q+DawPh/3vXU7NhcHMgzPZoWN3O6/Ru3R5+DxHWA+Belnrq7fbBnmeDr8eZVh8GsjXwGMrWRRZgP6CYTCYL16z1bNWWWqvvEna9XpTBOoqyteNLYM9WX5/rzW6B7cWwfNHKseN4DNXOw5o/XpPNZvtaLoOdLyIissSrDdqia7X78Wr3RURErq323tIfz8LeInvZZADstfLYr5bZmt9TTXReq2Yw08oVraw/hrmj8IO2Kfwwbya2XnGr4TAxMwz2g14b/uIQGP+NWgPhcK8MnhfA63y3JvD1Wq4psTdcZ7anteB+sWcZ4etwKTpwZW5fzLuwc/EYqtTf7dFyqFUTIdgPKplMVqYXq7fyPWb1mcn25EUZrKMoWzueUTsPe7K68bne7JZ69pTNGZ83rUx0LhrJRuf4XIs/B/uIiIiMeLVBZzTcxXots7cj701ERI4H31P2YL1EjuQxAPZDDDvOsGyPTJ9Za61lxv6sR6ZXlMN6TSuPtS3hYG0xP7TDwR2rjcI1SX8/SGwNg3HoG8HzkA2E/TC4aO4ZYfYGezz1gWNLYL+myv4M651h98rfr5bHunjf7tiz7V9jSzwGqXet2uOHEHaMwVwCro1Y9klmTcxI7OL3D39YYfUs7NPqFdUN9vKibFQ3Uc7XUZT1dZOtz7C099Lzz2KNa8SeUW+WNZnMTLivHqyXiIi8Fj8E1GBQRETk9bD3i1msn8hZHHYAfHRb79/uWW1drNe08ljbAxuqDcFBnQ3r8P8d9G4IaFpr3tm5NnC0YTAOcdnAt4cfBg8PhDFz8+48OLY2XHONde3++HuU4ffm7yN7xo1/7S1lwz//MR5/Onb7wv/E1wO2Tg3L4rEamvN7jSw9/2yy1xvVDwp/WGH1LOyDajl/nME+KMpFdeNzPsvqBnMs6+smqhvsNWpWz6XnvyK879H9Y1mTycyE++rBeomIyGvxA18NgEVERK6LvS/M8Of6viJn9TYAluuxwQvK5FhmK60hmq+lsSEdHkuqDv6Yxjo4RCyDx9nDYOxjagPhx75svwjrAPd/dnY/erFe5ul+3e8lPsc1/nW4lA382LGlsGdNLWvHrN7Kdbl9M3vDashyV1O7Prx25HNShT8wsfor0X24luyzjTmUzRXZnGF5PDYK12B68yIiIiIiIrI//16uB+snciUaAF+UH6gUmRzLHMnTkDKDDOSejg3wAz//cZadY4NFGwbjQJgNeXs9DYMLWHNk73jOmdk9WIL1RY/7Bs8ke66Nfz1uwQ8Ha8cNnjsq2zPK+Xro9k3vpbB7wLBz5Q3+wMTqImeWebbxNYBYFtWyeLyG5fHYKFyD6c2LiIiIiIjINvz7tSzWS+SVaAB8cTYoYbWzw6FakxvAPY4NYAM+f4zxfRDmcLjo/zqYDXdHZAfCJtrzmeH9ns2v9XQP/fN4g882Djm3YsNAVjuLp6Fmze0b35Ns3R8/A7wuZ9E9E5GXhW8iWD0L+7R6ZXNr8GubTEZERK5B/7loERGRfeH7rx6sl8gr0gBYLocN10J+QNfAhnp4LIK9PJ+1YSIOggs22B1VGwYbv6doz2flr3sGto7x99GeRfZMGxx2ynt+cNnM3L4BvoFjT/Uact7j2NHgHp1396PiXYatIyIvA99EsHoW9mn1yubW4Nc2mYyIiFyDBsAiIiLrwvdXI1hPEfmCBsByWU8D3gw2lCNqAzwc7q2hDBT9XwWzYfD/u/PHM7Bv0foL4TWv3dbza3qYy+RbWK+Wt8+FO5bB1jZv9xOeSfZcIxviSZ+ngeZdrZ4553Hs9k0V649jR3Lfl+29dm0FZt5lWW8REaHYmzAREREREZGrY++PRrH+ItL24duff/5R5Mz+9mc/e6gd7/Edp1Xb2t/d/f3txWv+4e4ff/7zN/80kfUsbB1b1/ZSsL0yeM5UZT+G1Udgzzu876PeerH17vw9w+fP4HPufcv9W47vmxtje1gDW/vhpz8VEXkZ3xAREREREXkx7L1RD9ZTRPp8oL+YnewbDsvIcjPuMfZgMjnsF2WLWt4ftxo7XvWzn3X51kq+PSDT520g+J+f/ldQ37l7G1T+/NNfkP7DRO/+MhXWfNuD25tp7b8FB554ne+OY41hGTy3BvOEXXsv1uvJbW28D3j/vMeze3/e7bWzxNc3VFsT94N8Tj75mwqWPQq234fbDyciIq/gr0VERERERE6Mvc/JYv1EZK4Pf/GTn3xc2587LCNzLL3H+HliMlnMsJw/jtnWcaux4z3+0mnVsv4qcnuxUa6GL048/tDIG/smWoYsZWhmw0EbGuJw0cPhIqsXmMHB42Owc1/f78vv39dnsLXZ+lhntR64DmP3wt+THmxdu3f2XPnnEJ9lY6+nh//4jy5/dlc7vkStF67Ty/fawl7ren/qsMzR+Ws4jB//mMtkZsuuiTnEsj1avXytlotgn6OI9nbkvRvbY7RPzJ0BuwbEzjmIr4lIt686Ud3L5lqi9bBey+ytd4/ZXI+lPe38bJ9sDo2ccyV2/UvvAfZZ2mttM/aI14pYdrZoPayjtXMmk2WZAuuZHOZHZfvMWEvkyNh7mQf2Hqxg2Tu2Rkv2tSjv4b2L7l82l5HphZkZOdPKYg1FOV/3erIZmV7Z9T786WeffbySP7tjNRTlrM4y0bmyn8znxjIz/fntxTTDX0z0lzd/dfuGW/z13d/85Ccfv37zjeKnn/4zAK3/tIY/Zh+zbOlZehdlnaKsaXso+2H7zCjnvil97r3eHbt7rFdhdcRylFuT9ZqNre/vDz5D7Plkr4OvOexYtx/9qA/rsYbamuw4HmsdR5ZhWXf8q5L2FRGRF/UnP/yhiAS+DKJ6LYN8PivTK6p7Ub52vAX7MeycgmULljVRDuuIZYtsvTfXyhp2DsPO7TW73578deC14fEjyuyRXUvmvBrsN9oD+X6sJ8tk9PTx2Z48qxeZTJHNiUgde5/Si/WV7WW/JmZzWdlemZxlevq1sphp5XrM7FXM6vfhy5999lFEuD/p9eMfU18ZYP8rjsJ/7PlzjdXL/9rD/hdY5a/s7K9By1+Ivv316E/hr0vv3v3VaQX7C9XSD/8qtayFf933teK+L7wOg3s3b3k7t8HWeKzjYB21aibTZwtve7B7cr8/BX7uH8+fe0bx+f7jlf3R7Ye9wn+MML+m7HqtnNU8lq3BvO+T8Yfu32vCdbfC9pH1B3e14yKvqOe1oNfLgd3etI36fREREREREeft/QJ779GD9bkfY2uKyPY+/P4PfvDxS0451lLLVc+3BVmtgA2dRmbveI3eYMbucfVed8r2y+ZafA8vymGvWs5nQnaPUe14w+MbXsIfLsAGJkwr/8d3Xy4++zQU/MrNY8h6U/5zHPifZY1E/wlc/M97fPW+3mMYed9L2RPbL7K9R6znENjT4+MWzGawHnc4qPUeOdLTrtvfL3x2CnwOH8/v/bXAXk/m90CrJuv63e9/f7rfIViu14w9s73J9z/+NmD1rR1lH3Js+NyOaPXC2gjfr8fsflv4re99T0RERERELo69F2CWvKdh64rIsXz4re/94PaP9fz29374TiZzRbVrxuMI661cj2hNNGvNo7N7spXfSfrd7+f83vd/tMiXfvCjj79/8wc/+OzNH/7ws49/dPPHP/zxxy//6JM/efiPh698FvvqnX386dwv+pY1ylpFWdf2UPZTfKnsD/eaZOf3svVrcI9Hg9dRPqflPuDnucDnxp4z9owW+Br5ze/O9xsd2PkjMv1aGavVsHMyWK9x3+/y64NYr7axfbK13/n3Tz7e2cf++NX8GjkmclTleUWjx7GGx2bA9VArgzXkc8f0vYqozv3qv4mIiIjIVs7wc1h2f1HO6rUM1msZk8kUPlc7z44jnzFRrlVDmPsEf3a3n+Wftd6n5N7H4HpfYPsrorrpzbWymImyWVGv3nV683Itsz/3mX4l8+E3vve9j+bXAR5n9Vbuyn7zjtVMJrNU7+eglcNehuWKKOfrmMkct2Ostga/3lL2uc9i/6uMJ+V/TeX+11WzsP+F1++Wv9i7/+Xel+5/KWr/6Uj7C9zaX6s+/kL1ztcZyz7+YvXG/jq1rP/4K8Kb2l7ZscLOxR7suHn8Vet9/YySfQI9nj6+q64Jx/zx4mk9UjfYo7BrxntU4HPw9ozdnjX2fNpzzV47T7773W4fF2B98FhLT7aw/lm1c/E4g1kvmzO/dseOjfpVwOp7euzt3/99iv8NMsdb8Bw5l97Ppc+b0Vwrm5Xtg2uyvK9nLTl3hqX7v4pf2cgv/9u/iYiIiMjGZv4sNvtnO+zX6hnVi0wf1JNtwXVr/TIZ08phLYv9/D+DXyfaI14Hy/k6ymZn5qJ6gZkomzWzl3CvdH+zz9OHtxf27R9UqbXqxufs4wuyXya1fqFUzbB7Vqt72VwE+0S9srnZ9lxzIfzcR34t4aP57vMwx7Ph0G8APGb/xmO1bFGGf799U4aFv3dThovlPxNchrN/hO6DW/vY/lPDtXr5t/3nip/+s8W32tvg97aGDTJtUPkYTpZ92f5qLHvzNEh3yrU9ua2DA9LC/2do8T9Ni8czcOD6BtbNHGc9C597c+9h12r3w+6bfY6L8jl/e4bgmWPP6tvz7V4zvzIB+6aUwXr4Y/jxEtYf1/HHUO1cPL4E9vS97eP/9a//+gYzv3w/9nYc/p3xPwmWm621T9yD31vkl+78x7/0L//yXq2Ox1vwnMn+B2B1ryfr9Z5r+dH1Tol9/guWNSM51KoH52ee/Ufm9nH281nL2fEtPNatXF8t74+Pmt1vbf/dieot/+2f//mB1WfD9Y7m6Hs8wz0UERE5gp7vmZat5aM6wizL+7rxOZOto0ymyOZMlMNetVxUP5Jon6PXYT+v19572PuSN+S9XYtfa3SPyHogljNRDuu1jMnmsqJeuF4ry3LFSI7VC8zMyBWZXCYzG645a91Mr+yamOvNYp7VCjz/i+y/fPz/OazNJr9DFFgAAAAASUVORK5CYII=');
            background-position: center top;
            position: relative;
        }


        .art-sheet {
            margin: 0 auto;
            position: relative;
            cursor: auto;
            width: 100%;
            min-width: 700px;
            max-width: 1920px;
            z-index: auto !important;
        }

        .art-content-layout {
            /*display: table;*/
            width: 100%;
            /*table-layout: fixed;*/
        }

        .art-content-layout-row {
            /*display: table-row;*/
        }

        .art-layout-cell {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            /*display: table-cell;*/
            vertical-align: top;
        }

        /* need only for content layout in post content */
        .art-postcontent .art-content-layout {
            border-collapse: collapse;
        }

        div.art-block img {
            border: none;
            margin: 0;
        }

        .art-blockheader .t,
        .art-blockheader .t a,
        .art-blockheader .t a:link,
        .art-blockheader .t a:visited,
        .art-blockheader .t a:hover {
            color: #72879D;
            font-size: 26px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
            font-weight: normal;
            font-style: normal;
            margin: 0 5px;
        }

        .art-blockcontent table,
        .art-blockcontent li,
        .art-blockcontent a,
        .art-blockcontent a:link,
        .art-blockcontent a:visited,
        .art-blockcontent a:hover {
            color: #2D3F53;
            font-size: 13px;
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
            line-height: 150%;
        }

        .art-blockcontent p {
            margin: 0 5px;
        }

        .art-blockcontent a, .art-blockcontent a:link {
            color: #FF7A38;
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
        }

        .art-blockcontent a:visited, .art-blockcontent a.visited {
            color: #A5B9CF;
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
            text-decoration: none;
        }

        .art-blockcontent a:hover, .art-blockcontent a.hover {
            color: #FF5500;
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
            text-decoration: none;
        }

        .art-block ul > li:before {
            content: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKCAYAAACXDi8zAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAOElEQVQYV2PIqJsCwv+hNBzDBGEYRQKrJFwFVBAuiSyBIkmUBFwQxMcqCMJYBUEYxkARzKibwgAA8tByfwbWBRAAAAAASUVORK5CYII=');
            margin-right: 6px;
            bottom: 2px;
            position: relative;
            display: inline-block;
            vertical-align: middle;
            font-size: 0;
            line-height: 0;
            margin-left: -12px;
        }


        .art-block li {
            font-size: 13px;
            font-family: Arial, 'Arial Unicode MS', Helvetica, Sans-Serif;
            line-height: 175%;
            color: #496888;
            margin: 5px 0 0 10px;
        }

        .art-block ul > li, .art-block ol {
            padding: 0;
        }

        .art-block ul > li {
            padding-left: 12px;
        }

        a.art-button,
        a.art-button:link,
        a:link.art-button:link,
        body a.art-button:link,
        a.art-button:visited,
        body a.art-button:visited,
        input.art-button,
        button.art-button {
            text-decoration: none;
            font-size: 13px;
            font-family: 'Times New Roman', Georgia, Times, Serif;
            font-weight: normal;
            font-style: normal;
            letter-spacing: 1px;
            font-variant: small-caps;
            position: relative;
            display: inline-block;
            vertical-align: middle;
            white-space: nowrap;
            text-align: center;
            color: #111418;
            margin: 0 5px 0 0 !important;
            overflow: visible;
            cursor: pointer;
            text-indent: 0;
            line-height: 30px;
            -webkit-box-sizing: content-box;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
        }

        .art-button img {
            margin: 0;
            vertical-align: middle;
        }

        a img {
            border: 0;
        }

        .art-article img, img.art-article, .art-block img, .art-footer img {
            margin: 7px 7px 7px 7px;
        }

        .art-article table, table.art-article {
            border-collapse: collapse;
            margin: 1px;
        }

        .art-post .art-content-layout-br {
            height: 0;
        }

        .art-article th, .art-article td {
            padding: 2px;
            border: solid 1px #B7C2CD;
            vertical-align: top;
            text-align: left;
        }

        .art-article th {
            text-align: center;
            vertical-align: middle;
            padding: 7px;
        }

        .art-postheader a,
        .art-postheader a:link,
        .art-postheader a:visited,
        .art-postheader a.visited,
        .art-postheader a:hover,
        .art-postheader a.hovered {
            font-size: 20px;
            font-family: Verdana, Geneva, Arial, Helvetica, Sans-Serif;
            font-weight: normal;
            font-style: normal;
            letter-spacing: 2px;
            font-variant: small-caps;
            text-align: left;
            text-shadow: 0 1px 1px rgb(152, 175, 200);
        }

        .art-postheader a, .art-postheader a:link {
            font-family: 'Times New Roman', Georgia, Times, Serif;
            text-decoration: none;
            text-align: left;
            color: #FF7E3D;
        }

        .art-postheader a:visited, .art-postheader a.visited {
            font-family: 'Times New Roman', Georgia, Times, Serif;
            text-decoration: none;
            text-align: left;
            color: #6A8BAF;
        }

        .art-postheader a:hover, .art-postheader a.hovered {
            font-family: 'Times New Roman', Georgia, Times, Serif;
            text-decoration: none;
            text-align: left;
            color: #FF7A38;
        }

        /* Override native 'p' margins*/
        blockquote p,
        .art-postcontent blockquote p,
        .art-blockcontent blockquote p,
        .art-footer blockquote p {
            margin: 0;
            margin: 5px 0;
        }

        .art-content-layout .art-content {
            margin: 0 auto;
        }



        {{-- AAA --}}


        .art-content .art-postcontent-0 .layout-item-0 {
            margin-top: 5px;
            margin-right: 1px;
            margin-bottom: 3px;
            margin-left: 1px;
        }

        .art-content .art-postcontent-0 .layout-item-1 {
            border-top-style: solid;
            border-right-style: solid;
            border-bottom-style: solid;
            border-left-style: solid;
            border-width: 0px;
            border-top-color: #9FB4CB;
            border-right-color: #9FB4CB;
            border-bottom-color: #9FB4CB;
            border-left-color: #9FB4CB;
            border-collapse: separate;
            border-radius: 0px;
        }

        .art-content .art-postcontent-0 .layout-item-2 {
            /*border-top-style: solid;
            border-right-style: solid;
            border-bottom-style: solid;
            border-left-style: solid;
            border-width: 1px;
            border-color: #ECEDF9;*/
            color: #111418;
            padding: 3px;
            vertical-align: top;
            border-radius: 0px;
        }

        .art-content .art-postcontent-0 .layout-item-3 {
            margin: 1px;
        }

        .demographics-table {
            white-space: nowrap;
        }

        .demographics-table tr td {
            padding: 5px;
            vertical-align: middle;
            border: 0;
        }

        .demographics-table tr td:first-child {
            width: 17%;
            padding: 5px;
            text-align: right;
            font-weight: bold;
        }

        .demographics-table tr td:last-child {
            width: 83%;
            padding: 5px;
        }

        .art-article tr, .art-article th, .art-article td {
            text-align: left;
        }

        .page-break {
            page-break-after: always;
        }

        .diagnosis-table {
            width: 100%;
        }
        
        .diagnosis-table th {
            font-size: 15px;
            background: #f4f4f4;
        }
    </style>
</head>
<body>
<main>
    <div id="art-main">
        <header class="art-header">

            <div class="art-shapes">
                <div class="art-object1133370623"></div>

            </div>

            <h1 class="art-headline">
                <a href="/">Diagnostic Report</a>
            </h1>
            <h2 class="art-slogan">Virtual Psychology</h2>


        </header>
        <div class="main-content">
            <div class="art-sheet clearfix">
                <div class="art-layout-wrapper">
                    <div class="art-content-layout">
                        <div class="art-content-layout-row">
                            <div class="art-layout-cell art-content">
                                <article class="art-post art-article">


                                    <div class="art-postcontent art-postcontent-0 clearfix">
                                        <div class="art-content-layout-wrapper layout-item-0">
                                            <div class="art-content-layout layout-item-1">
                                                <div class="art-content-layout-row">
                                                    <div class="art-layout-cell layout-item-2" style="width: 100%; text-align: right;">
                                                        <p>
                                                            <span style="font-weight: bold; ">
                                                                Report Date: {{ date('Y/m/d')  }}
                                                            </span>
                                                            &nbsp;
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="art-content-layout-wrapper layout-item-3">
                                            <div class="art-content-layout layout-item-1">
                                                <div class="art-content-layout-row">
                                                    <div class="art-layout-cell layout-item-2 demographics-table-wrapper" style="width: 100%">
                                                        <table class="art-article demographics-table"
                                                               style="margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; width: 100%; ">
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    Patient Id:<br></td>
                                                                <td>
                                                                    {{ $patient->user_own_id }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Age:<br></td>
                                                                <td>
                                                                    {{$patient->age().'.'.$patient->month_age()}}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <span style="text-align: left; ">Gender:</span><br></td>
                                                                <td>
                                                                    {{ ($patient->getGenderLabel()) }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Education:<br></td>
                                                                <td>
                                                                    {{ $patient->getEducationLabel() }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <span style="text-align: left; ">Socioeconomic Status:</span><br>
                                                                </td>
                                                                <td>
                                                                    {{ $patient->getSocioeconomicLabel() }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Marital Status:<br></td>
                                                                <td>
                                                                    {{ $patient->getMaritalStatusLabel() }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Setting:<br></td>
                                                                <td>
                                                                    {{ $patient->getSettingLabel() }}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    Group:<br></td>
                                                                <td>
                                                                    {{ $patient->getGroupLabel() }}
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="art-content-layout-wrapper layout-item-3">
                                            <div class="art-content-layout layout-item-1">
                                                <div class="art-content-layout-row responsive-layout-row-1">
                                                    <div class="art-layout-cell layout-item-2" style="width: 100%">
                                                        <p style="text-align: center;">
                                                            <span style="font-weight: bold;"><br></span>
                                                        </p>
                                                        <p style="text-align: center;">
                                                            <span style="font-weight: bold;"><br></span>
                                                        </p>
                                                        <p style="text-align: center;">
                                                            <span style="font-weight: bold;"><br></span>
                                                        </p>
                                                        <p style="text-align: center;">
                                                            <img width="254" height="71" alt="" class="art-lightbox" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAP4AAABHCAYAAAAnU/rsAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAACqoSURBVHhe7d3Vm+NGswbw7+/MTW5ytWFmZmZm3jAzZ0MbZmZmZtJ3fjrz7un0kW15xrtrZ1TPo0ceqbu6uqrequqWR/5PM9Dc0RdffNE8++yzzaOPPto899xzzZdffrl0Z3b0/fffN2+99VbzxhtvNF999dXS1YEmEdu89tpr7eHzotIA/Dmiv//+u7ntttua8847r7n88suba6+9trn00kvbv4EUffzxx82DDz7Y3HXXXc2TTz7Z/PHHH+31aejDDz9sLr744uaSSy5pj3PPPbf55JNPlu4uLglgjzzySHPnnXe2559//nnpzmzo+eefby666KJWd46zzz67+fzzz5fuLhb9K4D/119/tRH4pptuah35ggsuaA102WWXNffcc0/zwQcfLLWcbzrnnHNaeWWSn376qXXc33//vXn99debCy+8sPn000+bU089tXnqqaeaF154obn99tub6667bql3f6IbgePXX39tjwceeKB57LHHlu4uJn3zzTfNSSed1Kxfv77Vzf3339/qjG/Mio499tjmmWeeaX755ZdWb2z10EMPLd1dLFp44K9bt64566yzmhtvvLF58cUX24z42Weftcf777/fPP744xuCwA8//LDUa/4I+GR5YOfE3377bfP111+3Z/OQ9a+44oq2nWDw448/tvMRLKbN+ieccELrtIKLgw4tK+aVohOyjqJbbrmlrZDoxjIGMCUA+psFqcYOOuigdglG92QRXO69996lFotFCw18gGZsYGcIBgeGOHR5KIvXrl271HP+6Mgjj2wzuzl89913zcsvv9w62m677dYcd9xxreynnXZa62x//vlnOyftBD3OPg2deOKJzd13393y4MSWDvMKfEsddlbJnX/++S3AgbAmIL/66qvbIGhOsrJyfFbrcGMefPDBbcbnY8a477772mMRaWGBzyGs5RhAVJcRgEZ2BPKHH364efXVV9uNMQ7OWMcff/xS7/kicgO5dba5mJNMfuaZZzZXXXVV6/gcmCPvv//+bZYxP8HA+nxaWhTgW9rQiwBHVjamk67y3TJPkMz6/vrrr28rm1mV+gPw54CsR63fACZlsdJeVthxxx2bHXbYoT1vvfXWLVAYRxuOMI8E1BzcRlGCGHCGkuGUmUcffXSz1157tYcKQPtpKaU+551n4L/77rvNAQccsMHGgrgKp4vefPPNdo1PL3vuuWdbJX300UdLd1dOA/A3MynlDj/88Oa9997bAHy7uaecckpbLtvc8YhKqcy5AWibbbZpjjrqqDZbjCJOZT/gjjvuaDfMlJQA8c4774zNGsDKGRzW5CFOK6vKPHbq7T8o0WvinFdeeWUboAJ8geCYY45pN/GAPTvHnM/fylpzUfb/9ttv7b0uwkf2u/nmm9s9EPMjow3CaTI+PZtfdHPrrbe2fAXbUaTUtuH6xBNPtE8kokO8yCFD33DDDe3YXY8rPXmw57Hffvu196MXgFbRqebYudSp8QT/008/vZUVQLuIHvHnK5kTHQExHl1LpwH4m5k4qGxP8QAfA8hi9SYXYykXAU82ePvtt5fu/B/JFFlD2jgDUMZ/6aWXWgdVaiunOVpNHBqIOK++HA4/mRiYOQledpk5lsc/5AlxvpNPPrkt3R0ADpgcHKjt2itdycbB9TUGHXB+wAHgmoDMetea2K7zK6+80spveUC2XXfdtdXJJOADwDXXXNPOn3zmYR+CbvBiB8uNLoDJzAkQNiUFYcHUEsZ4kcl9/M03BOgCefRCH/TiOlvTi8OGbXbVzYMtbFTSDUDb/6mJnehFUNHXnNhIohBoBUiym2NJA/A3MzGuSE3xnMEhCyQrdlE2fGqyZDjjjDPazMGYdo/ziMsBGA5ZmbNwjJI4FoADpcxLNqU4R5LxHPjKcnirHoAfTwS4AgtyTSDL4W/9yW5tz+k9ghMEXCOfx5T41QRcHDwO6owXGVVI+AGhe44u4JPXfADTGtt4+mvv7G+fAUEwypyQ9kputqEbbfwt2Bg/fOglZwEGYBFQm6cANkov+Jo/GdHTTz/dBn+8ya4tuUqibwFYpUJ+gc2hjwNvfe21qKhUlaEB+JuROPyBBx7YltGcyyGLy+bTkk1A2VbAYEi8ZFTBQKaS7d3LOJyYM8g+IdlO5uAw7isZZXzOhJfPHFB/wOVU+Mt+SF8BRRtOrE0cXDXAATkpICt1ZSNlPqfL3OvNPRWAbG8sMmVsGY085ooXp3YfL8Cvn+MLiECPjz6qG48UBTYyyPx4u6+cL7MrmQGdDvAHaGAGLiDHSzD1mQyCARnpQhsABVq6pofoxdnfnuK4J1vrg1QgEgCe5kd35hDiO/vss09rU/YxvmBH7nzfQx+ymK8grRIEeDQAfzMSR7IJxrFiIIbg6NOSHX5gYECOAhSCik3BPfbYo90XsJcgQGQs5Z81e0gJC7x4cEpODAiyrQ0mm4yHHXZYW1EACT6cVsZFnG277bZrdtlll3ZNzymzlvVlEbLYqU5WN/Y44HNoMgMHPtoA2aGHHtqO41DG0p/5kBkvwFdFhXxDTTttAEmgBRr7JJYK5kYu4DCGdoKC+SHzAPw8pTAfwVGwkZW33377Zuedd24DrP5ATQ5ViMzNptrQi0CjPz7kpRd93SOTpRkSpMYB3zhk0k7AttzBw96KDcRtt9223Q8iSwIi3fqMBuBvRgIgyi+NI1Mp36YhIGfkZBxOwUk4hEzAoYFh9913b0tO42jHsa1rlYmoBD6Z8OG8+ilrrc/33nvvthzNWJxSJgEEALEmzeYex3YfLw4ve1uH4otUFOOADzSqksirjbGN5zv/+gMN8CS7aVsDn25sIsbBzdNmHFJ+GxfoBCLzCA8BDwX4ArX5kEMAAjBBzJwFCmBL5YCHyoHO8DRvAd3mXvRGLwIkvQBcQI8mAR9/+wYCivlrLwgbmx7MX2DkY3hozy78AQ3A34yktJO9GIozUL5yDdimIQYVLDgYw8toHKoka0h8ZTcOHJAoDTk+KoGPj+sAzOkQHgAhuwADHtoJJniGVBKqjQDfWWatnyZMAr6SlcyZlwAmS6dctTZWEgOcTbMStAE+p+bg5oAHPeNh6QMUjjxWBCT9AcVmne9WoBr47gN6uRxwT/BQ9ZBDG3zLNngKFgE+vQgctV7QJODTAX0I+iWZowrI2IKCfZwAm77t16AB+JuRlKAAovQDIgaQ8TnxNCTiW3fGaWVDYKxJQFAOliWtjJSd5BL4+ABFvdkmYyiLZRcOiY+xyv8fsOTIvFYCfMsX+iCngxOTryTBCJDLx3nlV3bxtDwhBx0DsepFoMyhRJft7cSnsgDSVF7mUWd8a/EExJDAIVBFfwK74BXqAr79juUAvyR6sZloLDYUbJwFLk8a9CeTPRVARwPwNyMBlizBqJySkZV7cbg+xGlUDYJIAMJpunjgD7TKVJ8ZWzlrdx3VwJdNstkU4qyqhhr41uGhURkfSEuaBPxDDjmkDWJxTIC2qVaTzdAS+AJZgpnlgmyOh9LaYcON3soDEGyY4WFvQckOTKgL+PY1yioH5ZFf9Af4ZaASELuAX+sF9QE+n7H0YWu+hG+enJiPg6zGIRPgqwRQDXzHAPxNRMAjAzMaAzMkA3c90hpFnJgzBSAOzh6nLYlTcOAS+AADUKgP8AGnL/DrjD8N8DmveZUBLV+UqckavgY+4CCZ/4gjjmj7A4YyWGa0y27/oOugf1VEqoZRwK8z/nKAb6zlAF+wMm92twFrTHMzd0s/etSeLcjr/jjguz8AfxMRhVtDKzEpHqhc8yyck/Qh2UlmtNaNwynvyhIzxAFsLinLOZP2xsq6ry/wgWBWGT9fXqqBr61KBgAij2qIU9cEAKqWEvj0gZTBnpwIqjl8I9IeiH6WE3bmnXNYOpWVRdfm3qyAv9yMb35KeaB3n/7py/6Ls/mtWbOmXQIG2ILmkPHngBic4zMSZ4pDWEcDSrluLkmUB5A4lXIPIDh+shqHYtySGNmzfmMBgLH05RRo1hl/JcBHZNXGvMpqSLYLycYcXdWEB14l8JXsljfkcx8vQZGMyN85VE9dtDGAH/2rMJYDfBVLgr3Dks01vPVxbd99922rvz7Ad38A/iYkhvCMnYE4H5BwLBtxQOARkBLXfWs5AAJyjiwzIdcZkTMxPEMqfbVzTVVgY8kzcVkTf21UBjakQvNU6iPLFdmZLJmXHXtyCo7r169vwWFOljHa4KVNvsBDBsAHJPo1Dl0IjL7TUJJxVAhKYu1DswK+ykrFhR+7aMdGNnPNB5DpCk0Cvn0LbfBwAH75NEiQERB914C82gyl/hwRo4rUno/L8AwAXIzFOTippQDnYDQA00Z24hiIQ3l0Y02q9AMAfDkGYHn0pCzkDHEk/IHRGjpUAt/9TZHxxwFfZWNe5C71AvDmI+BxbLoAMvoz//Jxnu8oALmdfXrR36FCIDcAmzN+vh8AuMbTJ5VFgA98mfNygE9HSnFPVdxnX8Gk/EeqLNEmAd+GJnvF3uxBJnxc15Zu9BX8yDQAf84IuK31HUAsMzEGozE8o6QcdTA25wnw3QcYX2aR/dxPP+dkwvTncByy3kTkMA7ZU3tjCBolAR8QAEMchrOWyxLAz7paG2fBrQv4wJbvyXNUgAvpZxkEtHHe8DRH/YDeOlagUSWpbpT6qqSQAOJbh8BNp/g48BGUgI+O6MqmovnTp88owCdD5ixoAFtJgA/E+tOzAFLutRibjX2Fl+z+dmhLNwIDPSHAt/cQW5KhBL4As9NOO7XBUhvBiG4sbWze4m9e5kxufwssJfDZSODXn8yqhmkfJc8LLSTwGZ3zMoRvWwEDAFurMybn4JiML0h4TssRy++jcwAg0V+JzPjJcBwHH46qj/JZ0KjXtMpOGdg4AMEpujK+rwBzMG1kIm3K7Af4HBwQtQFoS4oa+AAALOapnTmXwEf04smHXXZAStanE/PBI18FJpPrvkQDOCFObX3riz758g5Q0En0o1/065m+CiXkuipHlZA5C4jkLonsbANo2hlHIA2Zv6zrvQoCBL5kiI3zrUdEfnYyR7xUXwkKiE5t6nocTEfamIsggpeloWWFSpK87gm0giBKxs/ywkEm4F9EWkjgI2WlbC9DcDJO6quysh1AW68BHCd3zVq0ppTmMoEAAHzWgnaqOQkA4ckBRPma8l17Y8iQ5KjH4SB4uZ+j3qAih3HCx7j5r72SlORK+fAxvzKYIcEJIGyK2QuR2QUtoAB2Z87rvvHwS9ApCbjNjx7pVnVEj/ZJHOaEHx54loEMQI0bOR0CVh3IgBrvzNvmWp6YhPBVTWhHL/ZdPG4EUnMQNJCgS0a2xCv/ehziL8Ctr0AC4GxNN4IBXQpO5oSHw32BAamMVHyxN73RTcZfNFpY4IcYBnA4u409xmMgJZtHVjbklJqjiINbW3ocZc2nPARMAQVAZLpRJINpY2MQSGTYmrKeVhJqp70xSyKfLISHQ+arqwvEycrxstNeE4CpYGQkFYnMZ274CmCc2Fo1fFQ/XYSPJYlsqiqSoWV2+rH0sb7NUqsksgMi3pmzkr8m2ZndtKMfduwi8yYD+5LBnJTuZbCRoe1TZEzzqwONrG2D0h4Q/6AXvkJPeLGLQMpvtCkrFH3p1HX8neulyyLRwgO/JIbmdI7yEVYfYlj9AMP6btr+80jmZC7mVINgWspaHsg3l37IYOzsJayEYmvn1Uj/KuAPNNBA/WgA/kADrUIagD/QQKuQBuAPNNAqpAH4Aw20CmkA/kADrUIagD/QQKuQBuAPNNAqpAH4Aw20CmkA/kADrUIagD/QQKuQBuAPNNAqpAH4Aw20CmkA/kADrUIagD9Q+++7A60Oiq0H4Bfkf829G87bZbzhxauWvP3Fyzi6XurgrTVef+UFDYvw//teduENNl5Z5q07PnuhhP9JH8D/7yIvOPF2IX7M1t4q5UdJvYSFrf8BfBe8fyzvNfMOOi9fWCl54UH45iWN80jeKOOVV+Qko7P32AsG/g44AN0rnLxj3z1vcM2rmWfxkoiNQcAN6N40I8B564937nnlVF66OS3pU/pLfbg3C/8ZaDqCN69J8/YkbyZyeGlo3nzMbhuALxt4saL3jHmHWnn4BRWvsJL1pskMXn8k6nhHW8nPOM4AtByH21jkdU70IHvndV1eJGn+FJas7kWYec1WgO7llAIAZ5/H7M8ZRH1nwAdI78bzjjnvCXR9GqIrdowt60NgdOZPnNArzbzNN4FgqDA2HklSbC3Ys7Wz14R5Z6HXtbFBC3wOwECycRwjh2gBwF4G6T1qfV9VBNQM7x1nQFTy9Pok77SXJb2McqWvhZoVeTmj960JRpRHLu+a89rm/E46EsxkNPOiHzqhUC9tnNesz67kNi/vv2MHzuCllEpAf/clPAA+Gb20bek/9KONd9fxIe/LU3p6M+20SWSg/sS+ki39+8wm/NILRL3/0d//4eReMMjBZSoN6wN5w6vI7XXNfTKa7KeUZOA4Ww7OIoPKNjKnQDAPBPh+BptOABqAS+DHUb3Flx4SUZGs6c2tXlw5z8CPDci9XOAL5jK4eeIV+xqDbWvwO9OV9pYV9lAsqbSdx+po0YktAJ/eYxfA9xbiDcCnfBFYSZtM5xCp85nxVAOA6j3j+owjb0XlZNrFKUp+BLL+4HReeUyQeaBRwFc2AX6c1BtrvQ8ecByqG9WLN7aa17xUMCXRMWeIPdhA8F0O8DmRN9QK2PjRFZ50Yf7OqkjX6dLYPmsrAKjy7C2Upf9As6MS+HQ+EviyOeAzSowDAH58wecY0A63H6/w9zjyHnobZfoIGkADFMrjRKB5Bz491Bk/wOfwNv3sluad7MpYSyEBbh5LWDqugb/cjB/gx1/YGJhtHnEulQ/dOHufPlvzM37jEBgt9bwX3zJgHgPlIlOAX5f6I4FfO7wMn6jB2XViXCDQZhRZEngEpi/nEASsL4BiJcAHPG1n4Sjkr5cYfYEfAvA4vnk5ppGNbpZLZKffvqUyvdVr/JVm/BL4qjyO5fGg9+OzrzP9CQCCTHxJe0slY9tt7pv1zWGc301LK9E/Ios5zTpw4VdiYlIiqfHDvl3A/8caP8CXpUuH9wOLW2yxRfszSBrGsRnRjxvUg4Ws2WX38OKg1sScQrSP0/UFvk0z2VUpbTJ42Yj0jF2W9Ry9L1mb+jGGkhc++JPNj2r0zfh2tWUzj/Sc/ZCDeYwDoh+f8PhPYDQ2GfJZdQUMqMvQ5ulHLDyS0z56sMlmzZyKYxSRbVabe6Myvl1jmZwu6SG6MDdLo+g1zsgOfszC313ER/xEljnHXg4288MefqhjGtDlhz/pHL/onx75f34VZxTQ6MiPafg58toOvhth3yPfi0Cj+JBb9WwOfgAmv8/ntxf5Gjvh68mIHwmht5IXvUpSNs+1Mw/y6GsTlT5dIy990zXfH5vxGY3D+8HALbfcsjWYvwMEhiCwsr1rYkBvcJM3sKziZ4fs5hIoTpdfQh0FfCCwtjaWX5lREmafwNj4CkCUTlGMNg50nI+SbGSGl7mqTBjTzzcxnnnXwK/X+H76yeYUIAt0DorHpyt7mTOjcBi/ZkN2eyZkcLZHgKdlg8AjsES3Hi9yAD/1xQnIoQ/ZyegzfsDrsSN9AHQNiI0BfHLil4wf4ONfUjYD095BT+Zl3vRQEtkBgr34pjmzuTk7GwtYyMBmNpHZZhTQyGez2UGH5l3q34asREf/AEXO2pf4BRuT13j8M/6IB58iEztZ5vAzPGo+dMcOHo876J7v4SuA8yk82cN99mFfPolUUZHDRmn0QgaBH2/3Hb0yfg182c9vwYmuhMBAWwMzsOgUYULuGxAP7d3nuB4FMlAEwW9cxucoskEeH+LDGBzCQT5jue7sERzlmVwtEwJSG3LGdT9yOBsbTwGAwTiANtFDV8ZXISS7kgu5JorLfKUD2uSiQw6XuZDD2M4Oc4gMllL5oUabrqoJPPVzGK/USXjkPn1zYM5C/tDGBj4Z8eoCPqAABPm0Tx/65mcl8MkpIFge+mwOxnEubR8dAJs9Fn7kWk3kAihJokv/ztE/8NIdXq6F2ID/ALt20Tv5HOETOQVyc4Mp10rwSxL2yZC25i4xGldAIx/94Ut2WZqv8SmYgy+AJ4c22sae5mc8PzzK51yna+3YrDPj16W+zCN7EAjYNQ4TTN032ZLwART9tSUEIwIE5WsfIUcB32dBgvJKPioEZRRH0cd1/JwpVulNLsqilJBfcxXpzVN7gNaHkqKonBkWUKMw43dlfE7g8Z0+2tFbfrOP8wT45DB/yxWftXXol77ma3xn46kKBCo8AFUQdD3ykpE9gEmmIqu5JdiSkSyWNPhHFvw3BfDxKkGD6GXdunWt/OQki8Dkxy1VQHiEVC1Aah6Zmzm7JhsLLADiun7aAYZkVH8D0T1LIc4e/Ru/1r8+0b95rV27dkMw8lm1mDb66Gt8viJYm7+5uefQlh8ISOZd6kMWZxs89KMHSY6/55ozWczTD3QmAVqWJOFmLmRyzxiu6xvbRh5tRmb8EvjOgK3UyqOr8l7WFwwSMCAA4YwYOzitaJZylSEd7o0Cvswp2pu48UyEsfGxC2xN7YwfxVNUePqFVuW2ayFziFIDDooxZ0Ags3UmQ2vjfhRGhq6M3xf4ykMHpzO2tvTNGQQp2V1m8AOOAhsSpPxgJ57Wacb12XLHNwZVT/TGqRycJoE5Ti0ouMfY5oDc35jA56R+QZbfACDbkItO/MimMaNbegQmAV5piw/yeI/9Y3u6si43Z7oyfxnPZ3PMnLWnf1WFv6N/djWO8fALT0FEIor+2ZOvIktLGZk+JA2BOH4R/QIPEEYuPFKhGCMy8W12oOfYIcBPoNCW7gJ4AZSt6YXf8nv6hjm+GF+KLNrQrwBnHgID3uTPnMmFx8g1foQhJANSsk0lzhXDYcgZMTEJfRCHB6JMHg9ZmFJELNVDyaML+O7VAcJE1qxZ0xpQUAFQk7fXADBAEUXbYGMQikQmC0iZl4Oy/OoqhSqJOAdAGLfMJJlDX+CragJ85BpZGDJGIIeKxc9XGxsPpapNO8sqOrcXYrmhraCBpyDLyTi2tSNgkZUuLHPIbgw640DWfbVunTcW8PGTBOiaLMZxCNI2ssjk0I4Olbt+9tp8ZbwAlfPyCzbQ1jwAi24BwRhsYtnEn/A0vvYCgbnQjTZ8hP4FoPgkndJX9A9g9O8zgPIFScX6WVvVRwJTgMaX/cy2TUL6BTwVnUCeftGJcQUYfKNfmGHHtEtbf+MnK5uzStvylQ7pyFzoHU+ymDPZBFttjAEPnsSxJ/8Lf33GAj+NKY5RCE35lGyQgIKQshFQ+BspT/PsPoOJ6JQKvNq7bpKU0AV8a1rKM762eOnPifBwvST8OT8HIJs2eHIC5PvzeOJDWcZJILFsIANjIvMFMECNQxlvHPBjPPxr4DOwa5lLgMbhgZ8hXOOgiHG1t57kSO7hqy3Z6B9vsqVPCGDw1od+HclCxkbmPkvgy4yZP91mjuVB/rQxvoOvKfH5i4qA3RD9yuhp7wzgACnz1vLxp1Q65kMGwVNgpB/36LLUP/0BioQkQLgWXbqnapA96Zo8qsX4vMNnSVBVAvDGTX9+ROcqN3zJY2z+LIDwK9SV8X0WDPmlpKTKEdjxIhd9s10pi/EEWZum/B0fxKYCBlylPR31Ar5zCXzlswxrIgZwX1kmOhIQIFQHhDRpPCleVhVVRV3VAN6UZeAu4HN8gplUJgfcnIxB8cyhv9JeiaY/QMgIMiNHRiKhtlEABVvS4MkArpckEqsazC+GmwT86KMs9ZFx6DUG1iYbbwF2F7mufcZCeNREdnOmT8DPEiugTqaJQ7hXB99ZZXxjOBvDtdjDNQdZreWBWDYTzMirTbI93QBkgApQ9MzG6/5naQSMwBbbqy74BN2YjzH4oGCPpzOe9I6fsyQgOKhijd1FZNfWckBwijz4k8Ga21zgoiZJqkx+dKGiMGcyoxr45IAhuhFQgN419o9uYAjv8CWjwCWJwKH+JdEtn9AueBsJ/Kzx4/ABvs0LWVWp4V6cxmA2VCiDYEAbIOhPaaIjIQgL+PpGkK7HedoneEQpymARU2QTjXNwPGeA1B5f7RmWMhKMyOl6DEEOa7gSyCFAcUTBAb4+XcCP42sf4AtyiHx0Q790wpEZXBBjhGSKviRYKOXy7JZhzc/hb44VPTgrPQGM7RAdzzLjm1+c12GOgrCKis8Yj35cx1tWEnDJz2lj8xD9eZoRH6I3Pkjm2Lo8rL0BGJ/YQFsAoVvZXhbFx33XlOf69dG/fS17S5kj/hKgQCT4uF4Tn5Ag4z9kow8VDkCjGvh05PE3LDnHXiUJiGUQ01fQ850a1ZA5lkTfgjy/D95GAn9UqZ9NJ85PkaWzU24yIqO5ZyD3AZDyANkYfTI+w5WlNgcwSUYiU32QwSTjyNoaV4bwd82PHKoU2SClV0k2UMwbX2MbY1zGnwR8Rjc3bfCSpQQdwEg0n0QMC9i+oMPY7JE5OfA17+g9uugDfMuLWQCfHGRSbXFgZwABzICf7rUjgwBmvFIHqq1UYXimT5ft6dthTuaCL9tIEDZGjSlRqBBK/Qu6xumjfwknPp3+/DlBtitwSHISINn0Mba2dAJwqAR+/BvgVQWCo79rgk17JfFLZ3sVEmX2NEqiky7g93qOXwMfmBjMPZMiIOc3KWByzSAUxeCiHFAb3LXS6aKQAJ8MCMCU6/po6+C4yirncYc+gGZySjF/G7PkZxzrH1m9q1Qrga+9uS4X+AId3XFwbejLNY6pCukDfGWi/RUOZQwHmTgdfuaT+dOrcZz7An8WGR8v+gV8GUU5zQ7W5zaaODo9BchAx2fMzfWQwCYrakNGB9n72F5bds4XYcxVEMgy0H2yJvCy0ST9s2cSXTChKqZDequrRcRHjME++hibD9CLIGJM+ig39/AP8M2fjWtSPWdZo482fNUaH/86CLFJ74zf9TgvwCewxv6m6Bjbt4VsSABCAoK+IhoFcVgKMrlxwPc3ImyZoZ2BTjuKUdo48rm8Zu0s0zCYUsy4yuES+BRHWZyjC/iAUgPfZstygM8BUtKGlyxkHdfH8cgMvNpGHraSFejXPfNW4XB4+gRe+nVmqy7gu582syr1ycpueCmPOT3iZEr7yKUtJ/XUgq35T3QqU6puolO8le3W1LGxg827fIEeBFZj4ml/h/+V+lehslMf/QvQ+Qaq/mxg/8d8+EJXxgdcoNY2gY4PSIKCCBoFfHMYBXz+KvGGrzPMyvhdwGcPtnYO3noBn5JK4Iex9ZHHGTEOBiK8kopxDQRQIrp1STa6+gLfRJSfeGlLOfYWRGn7CNa54w6KYxR9a35RGAdjfE5XE6A4omB6WEmpX67xteHw9EAv4TWKrCctSejeGMaiA4+i2IF9AFaQs3aWZY0VUHcBH9hiA21mDXx2r7+5JwCycXRlHDrSli+RGQFDCTS6V26reFRwqsgum+egaz6VgA7kZSChf/rAT9UxSf/wYK8o8jjTvwSjkiBfTQKDR7DGSh9+IijlSVNZ6mtDvmwAjgI+W8vw4auPOauo2L3u474A1Qv449b4Ab7yTcZKOw5tkAAryjEAoxoYadMH+IxPCVGKcYDM2oxjkYss9VFGb3+H8k88xscPX3NgPM5Xtg1YOBB5+gC/lFNAAfwEO/0SpbWhZ4aVwWQB12uiO4BHnKOU3VkFk4om+kYegyW7jQN+aQNtNlbGL4HPMYE/83DQKx0CZ+SzGy7QuacN3oDnew18UP/S5jlKAPscX+DPeIYf/UtQ9J+lU0180doZD6W1b+2lP/1IIvrHLjVZXmSzLfPkB/YF2Bx1ZfxJwFctlLijX3oDfD6T4BmyiWk/InKw/YqAT0glpmzJ2NpywJz1B1JlVrnpoF8NfHxr4Hu2LtLqh5/rlOHZqwg3ishHeSalb5yB44vArhmbzDIC4OexDyKr6oASGcTYDv36Aj8ZP8BnrHKjx0HXdCNjJygiRtQWEFQ4gqY2skSM50xGdrJ8CXFINqFPnwPqUWt899Nm1pt7XRkfCVgCABno1VyAjyMCIzuQX4CoeVq+AeM4oidVAb6xkWuWVrX+XWNPf4foCBg9LaB/oKcbWZPM5qM92exzAXI9RxjCW3tt3TcHgYsPqTLQctb4+JQZXD/t8GXnVDmuSRj4l3L7zGa9v7JbAx8BNrASPADRh1CyqPWMkqd8ttgF/K6Mj4cxww9vfS0nOBXAcVLCy9w26SibsUzYTnKWIog81vT44xmnkzG1dejrkZgobt7uk1HbScA3jnb61RmfzszF38bXDrg4Mx0KPhxd+W18QQ9/kd2+AmBFz2R3tlTwvFdVghdnFww4KrmjX/c2RcY3/9hqHPDNrXwmnvmYu8ykP2IrQTh6NScBX6ksYKoA2EmA5mNsbv4CJmCYb3yJLrPUi/3NUdLiM/QvQ9sX4AP46QPYbEk+943PvuQhJ5CRxb5KvsdPr2zJ540R36FXwQ2wjY+6Sv1JwEeWKO6XdnYmK/24nydWZMA3spj/xIwfoUcBnzPKbtoaWHtnE1HackzKKh2JEH0yPqJMmxmMoL1Df+sq65xENO38bSLJgLIkB4uzI8ZjAHMiaxSBryzE0TKGw+6r6EzO5QI/lQRHtD9Bf/hpSy90LvDRk4zHmfAwHt3qY25KNn2jBzxUMKI822hLfnKZU2xhjC7g12v8lWZ888cPIPK9ji7ga5fMpK0+/IUObWoJQIgcQOWedmTXV1ARPOid7YGTgwsAyaT0wk6Cfexk2aR9qX98u/RvTO2MYW5kFGAFI33oJ/3pEgglJM/X4+9pQ2Zy0K0NSvoKhpZT6iNzlTDcz1yczYVvmgcb4Ie3MS07yETeTuCbgMjF8QjjTGldwDcwI4qcmGpPANeVQAAApGUfwoislKatgQnbBXxji9SCiM/kM4axjGFSDp/d9xl/YDM+RZelMKcSnY2X+ZVKM7aze0rKrbbaasM63xxkJLKXwAdMUdq40Rdj1sBHZOIceGUujozroBe8tJG5BDVzoGfOkCBIZu3Sjw6QNmSKY9JVF/DNAw+HNkBjnbgc4JdltDH5A3t2AR/Rj8okNtBGP1mK/8UH6NCXYMyPbdMuNu+yPVKxmh97x05IwmHXLv3r61zqX3AUcFOxksUSQB/jksfn6J8MPkenePAVJb5EJCiU+gD8cr2uv2QzCfjIXFKZkDlzMa55OOgF/lTegpi/XXftH8A3YSWU8pLQGnJcZTTHq4GPbJr5QgEBtHfmDL6ii1cJPGRyHJoTpn0EASCClGQyIq1xlOUmFmPn8LfDPZmLvNYwnKjM+IhjczCTjsFKPgDA4YAAD+vPEIeSSUqHAjI8ER70lYwv0pfAN541rj50GcfNob85WJOK6Mo2yxj9VCS+/y3aRwfp42B8dqB3+yBsSUZtzLcEPn5swAnZUzt6Y4Pw70uypdI2usQLUOgOuEtHD6lMgEqf6B0JcoJI1qmI3bW1bOMLmbcjfR140bcx2S5fGCv1T9+Cr8BDbjqq+fA/oJPp7SngV/qQpwuSkQqllKfkgS8deJpFp4KFZYmAWMoD+PyEzvRHMMDufB2vUcRGArqKmHwZ24EXf1AZAb2lDN2GzD0bky3wXSQc5zQ5h8dDFCDacY44fAhQrIu0zVl7ay0KpISaPBcltENAIRjjyq5d7TknMFuHa6+sBULRm3JNClAAQ6Yhh3vA0sWPXPmKK0PircpRWipRZQtKMYbxtMu8aocSxc0jc9ee4+Ep6JWGRuaCh8rDWowurNcZkEMCKRnoTwVE53EKAZJt2EQEN0aWEPhZWrlGfsGS3GSjX4AuQSjTuJe5yUr6cMYunY0ijgas5o1fyYtuALKLZNJyfPqLfoGm1Jv9C+3oBiAtuczTYTknONAJvRub3fgS56+J/7pPX3QZ/Vsu0D8+9G9O0X8NQEFI4ORv2pGZv1mS8UXBhSzktUxWeqvAauwAL1uZv4M+gF4wYOvYfRSRCxaMI9lY4pCBP9CDpRO5BBO86dhBbjIKLoLhhl/SMSDFiXbKNQYUJQhaOzIigJ1rbWV56xRZ32S72iMZT6b0DxAULLMKOrVyShKdOGYmB6gcgRMzpiWB+zIyXiY1anzjyPqciPE4DwNSkrnKOtowPNCYlzKMQeqMaJ76ybgOulDijXJ6JEpzaNUVoxlfELIUoHsBVUapKyzzUREIVDKJgAdEHlcJaLIe+XxmP/q1vsOvdmCBkQzmxhYcVLAaZ4NRFF7mLxv6G69a/pJk/oyvH0B16RfxSTzpB1jZnf2BFfhdZ09+ZR+Ar4yyPWKzUfrnQ+zepf+Q6+SxDKZ/8sQXBREY4Ed0MC6Q0rXxgh1+RC/jsFMTfxcEBQEy0I+9OpjiD+xOp/TDH8wb6IO3//fbeSank8PnSYIwjrYA5/Ok9uHft31IO+31Y2CTcgY010YZq4vCK3xSfpbUR04KjK6mkSG8jUsGh/6TwBe59SO3o+7nfuQZxa9s4zxp3HGUuThG6ammsk+ffu7pQ95SZ9Fb33FDsdu0+g/FDrU8PveVJTzIsZw5hPQTZEp/KPn4nDGc/3eOTfNf9wGVjWyQL/gAAAAASUVORK5CYII="><br>
                                                        </p>
                                                        <p style="text-align: center;">
                                                            <img width="74" height="65" alt="" class="art-lightbox" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABBCAYAAAB2FzKyAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABbWSURBVHhe7VoHdFXF1sa3nk8UQYoooKAgNUgz1FBMKEFAmuCzgIgKPyAQJdRQpFlBgUePlJhCQupNF0JCKAJSFUMPHULK7fXckpvv3zNzbnIDCYa1EtS1+JJz78ycKXu+2XvPnnNuNTxChfCIqAriEVEVxF9EVBH/Y//lgd3it+9T52HioRIlJs/+CvkFp6uQPtjFMuyLPpz8j+Wp3t8AD5UoNnHOA5s7JTgR9FlyiRu83ElXIUvZqOyvx0M3PaZLTq49d8O9jAgrIuLYl1zyV+MvIMplb8AfagmBp81YtN+A+ZlGrDplwYE8ie6IOly3/iZMPVwfxeZPMBQWYtkBPTqHKdE8RIuWOyQ0p6tFqAltwlWYmqlCgVGuTD6qiLFF2RKKKcU6owzLPwwyH7pGSXYnJqQq8eI2M3rGWdEv2Yx+ieLqnyShR4IdTYJMGBmZiwKT8E/MXQmaShx7IRHFiedkOUSiCvGQiHJNoghfHVbi+SAJPik29Esyol+CBX0TzG6XCf3pXoNwPSanq4iREnI0VgvyzMw0Xf05UOSgtGCySvFQiOKmQ7istMAzOh+eCQ4MVqjhk2SBT7y1FFH9Eg3wTjKjT5IDrUI1OFkgIU+SsGhfPobGazEwXofxaSqkXzGwnulycO2qalQ9UZwj9lGETVlmvBRhhC9pkU+iJMwt3gQfrlUm0jAD5c0YEOvEgAQDesQaMS7TjGFpejQlX+YZUYjuUVa0Djfhla1qbDilY51Tz/90okiTuDLxoNGJr48Y0DJcmJt3IvknpkGMpEQTfOP08I6X0Hcv8OavwKDDQP/dNnSJ1KNbDBGbYsWQWAkD48x4g7SuS5wdzYOUOJxj4UNVNaqUqCIyCXf3sfqQjnY2nWxmTJOImAQbfOIM8CGC3iJyfEMK0HVlCnqvy8KIPUTYUZB56qmeBt7JBgxQOEgbqX2KGS2CKbTI1FDPboNUEaqMKCa6s5ilIiSe1WFEvAHdY92dN5EVp8OAfcBwIqr1qM9Ro1o1VJOvek17wzfwHIacAHqTL+ufSCST1g2IK0Q/MtEW0UZM28vM7x/vo5wwOQrhn67EK9vz0JEm60sOvNhxK8gvkXmN3g+81G0cJ6fNiCnw+e4AvP3X4vEadfCvav+G75aLGHyItC3ORO0k9FfYyWRNaB+lxSdpzPT+SRpFsjJ/dLfIszK1aBCkRe9kK95IYCbk0iYiKt6CoeSPOs8O5yT1mBmGMefJ3I4DI88BbyssRNZzqN+qN4YfAPr87KANgDYB7tfM8CayukSaMClFjdsmWav4DlgSUlQWKp0ofqCV6dqdbUSLbUp4sd2NaUKCsRRRPsk2DP0FaNTJB40aNsOII8Dr5Jf6EBHdSftGZQFeUwNJq6qh+/brGHAQeENhgG+8kfwb9ZlITp7qNgrW4a24PBiscuBZBU8cKtX0xCMU18oW4f/2aNCCdrn+SXrSAua43WImMrveu5z4+CzQsFV3vNj9bYy7AQz9HRh1jC7StPdzgAHrTnFt891xAcNPE4lkfv3cCGe+ynu3GY1DVFhzRC+PLb4qE5VMFFtJtqqA0mJDXyKoY6wdA0gDmG/xoR3ONUGfJCsGphWh98JU1K/fDPVbdsVrUzah/YRv4Dl+JTp+tBKeUzeh6eApqPnUE/AY7odBsSr0zSjkJsdCC94PXQMpwvdU2PBGjAoGGpdBfqBTaahEophoIl5iuK6V4E2C91SQySXaaHJsUiUa1S+DfJHCiJrPtkbt6tXRqH5D1PhX9eIdj13V6apbsw5efqEBHqN0p1kKvHWSwgXmo+R+eF+0E/aJk9AuRo2T/OkDoZK1qlI1yh0mMx1T4tXwpJCgLx12ffi5jpmfPLl08jexejK7rnim1jN44cXmePI/T8DbqxdCt4UgwH8+ajxVE88+1xQvNngRdR9/HJ4LEjCM4qq+FM0XE8VML8kEb9KoTlFqHM4zyRL8bTXqXkzfq0arMAuFBBRU0mT60bGkFFFxRjRo0xN1n66Fms/UQ+069XDnKh2EZYx5X4QMjRs2Re3/1EDnRckYRr6rLzdl0Q8zPWbaXWId6EOmmWcwU8vKP9RUKVG/XDOgWXAuutKWzs533uSz7iHKoyfq1HwG1WvURKNGjaGng7MLflM/E0Q1erlcotjFTPvFcD38DhZQKxEeVLY/r1KiGJYfyMXz29XoFU8xEMVSxURRGMCIer5NL9QhjWr8QnNOymz/2bzd2VNn8WR10rQ6DfFCwyZE1FPoQkQN50QJzeRnxWQJbSKt6Bx6B5c0TJtcjvwfZHpcZFrgpQd1aB2kwis7LPCMMqIrHWy77AZ6RenxQqtuqFerFmnTK5yoNavX8ZYWjQ1169VF9Vp10ex55qNqoM1COgNShN59pxldo81oH21BszANeoXcxsErsm+i8ThF/DReeahiothWLcKFDDLDzzK0GEnaMCRaiSGZVgxRKNGkRVfUrlkbzzdqiZq1G+Lt0eMx138R/P0WoXXLtqhRtx6aNGpEjv4pdJ4XhbHkzH1jdXSc0eDtRDW+OqjEZZ3bE4TKVyaOqteoUm61CFrJiVydDUprIW6qLahVrxHXpGrVHqPrcTntfj1ZnA4MjICReL9ltCHHaIfJVjoCrwJ+ilG1RDGOSHr5Sy4oje2bw+A3aQ7mzVqMgFlLETB7OebO/pJ/B8xahgUzl2H29PmYP3MJtLfuyK3cwWyNXZSUmaoKwsomikYS47qGlAtEikSTBZPzrm92znPlOSXFH6QG7AZdQsfYp6vmg6HISe2pS/YER7xLljPF/YsjMR+agRUUf7MPuiOeJroKK4QyiWLjOlgnLgEK2bd4bcSKCklSLjDdEm9A7LwaS7FJsARvwtJUr4i1pT9uKNQHe6DHSviz7mKny75Lm1IxWD8EXpPVlycq+qTPMpqxl6ysGhvDwWpRRhBLlXkfQvqKokyi2EtKRgTri8nAXw6QNA72R4WMLLs8uBCWTVyMz4mRyXHXJCdlHQ5GNOtL1HVQoaBQLuDClwYrtlNbVzuhQ4J0XkQfNnbjbrAiuucsIklJFlafyc2G4OOJHuiqGMoxPeqK+rEyApx2ubAE7MklmyDdprT75MTAfO1Klcuw3Vu3LPAFYLMicFKKmK7eDbrDZk3/jLB7IbS2RAz3SmyxWc8VR7k+ig3A9YSW84rGiqxcM87mWaCkXYsJyFaFD0VpvcWBP/KNOFVgwQWKrO1O9goJyNPbqcyK3/JsuGWQeAuj5EBWvglnVTaqa8P5PDuylFZcVFuhltgqu+CaBpU5SWdIG8+ozDipNOM8jcE0hQq5XjBJ7wUbjS8nzLRAv5N8J2m8m3oWsgiNLKtVeShXo9x7+ThVhZYhWrTeqkT4BfY+TcBVZf81Izr8dAeNI80YGJsPgzzhVUdVeCFYg5dC9Ji/X5zhjudY0DYkH54xEnpFG+AVY0KPGD36RKkwNNaMWZkGnKWF4eByCMJSbpjR5qfb6BinQ6ugPKRdFwEm8zts4ndDaJmQY+VRDV4OUcEj0oD+FMPlsRhDbllRlEMUdSELyIT4aLcJL0VKaBpsQeR5Y3G5S8DMmxI6hOjQNNaGAckGIkqYyqrjOjQONaNJuITZB8VDteP5DnhQhN4t1opucSY026FDsxAjmv1kgMdOAxpHGNAlMhcnSDs5ZDGmZarRJIyITTIT8SZM2+N6qcC3nXshs6CV7PCNzoNHlB194ixo/JMKW8+IOZTZrhxUiKjJe4zwoCPDq2EmxF50f4wh6hy4aUH3MC08FFYMTdUXE7XuJK1+uAmtIy1YeEgQdTLfjs47TZyoQYkGbPlNh7BzZqw+acCb8Sp4xdMBN8yMiXu1VFv0f14nodsOJT9c94gz0ISt6LRTj2wyVwZhYHdBdlxRF/RoHapBdzpretHCdIwy4L8pSnKXQtsqigciyiPUhNTLLqJKcCKXNOQBiWpPB9nhSWxl2dILoTOuWuhwq0XHGDsGxeugtYmHcN//qqF+jHQANmGkQgOfRDuakJmvOeGu3feCeanxu9RoEWXDkCQT3kzUoY/ChvY0xr4b987jfqgQUZPSjWgTY0a7HSaEnbfAQrd1dHzQEyGSrYjOcVb0JLPxUEgPRNSwJObvSlb2yB0LmbASHRQWDFSoaRzqh8YaEmtA26hCvPOzARm3jHRGNKEdHa4HUR2dVd6VGd+csBLSTuTQwTmM/GuEhGVHjUi4akRHMu0W4VbMz2AvTmWUblYm/pwoUuGJ6WxiZvgqdHgz2YL39hgwKiUf7yZq8W6KEcOT2dNLA5/g0NQSH3U/orrEMNMzYtc1icokKC4bySRU6E0OvVmoFZ+kqXn9JDpMt9mhRcswK5YcEsRO3qNGe9YvbRTRl5mJEkhOpkEuk2PMLTxgxMtEjAfV23vDQveL8DoR3D7WgR60eVzWCDnZdAVR5bNVIY2akm5GlzBawWQz2hJhzwWb0GCHEU2CrKgfakQrEsY3QcJr8eYKE+VN9b2pftcoPTlvLdqSH+lEG0ZLutpty8MfpF0MY3flo024He136HE4R+y4EZc1tCFo0CHcgY8yBKEcbmLn0cH59UgVmkc74BuvgcMu/Nnig2o0p/Fb0Saz/pS8g3OSyyeJoQJEOTEpjZxguAW94iWMTdNi7j4d/PYZ4J9hwsyDWozfS36DHOWDaBQjyoeI6hBh4b9OeZWcLHs+NS61APtyhe85TaFEx1A9Xo1x4P2faZdjpkjyqE12DExQoVOchM7kG7MovmNg/pkdWNjirv/dhFd2GNAqyoxltPuKRz5OZN4mOXdq0G6nhCHUh+QQpi9iQp4sExXzURlG8hFGtKMQIOkSmwS7x/SVDeLE0TtmdAsvgEc8OfOUsolaVIoo0lC26yUYkXTRivTbZqTfMeKcVo6fOJwIOKSGR4gBfanu27toUQ4YMD1Dh1npOoxI1sObfFwrChWW/8o0wxVpO2Eh7RlJrqBzjA3eFHeNJVfhn6HFZ5kFmHjABB+yDO9EC5qT/0q64toQ2HzYVTbKJorz5CLDST6KTCOaAjYyv/hs12RcHRfh+A0JXcO1eJW9Ik/VwUiROsO6U1q0iqQdk5zpvF/Fb5lO5VvhGUV1oySMIB8l+rgbTuQbaJJkkl4JDgyIJZOM0KMuxVu1gwx4NthAPkpCnyTabcmx9yVnn88XR1CVcpUWJ5TcQSqFBAo9GodZ0GibFXWDjGgQrEMHMm9fGrt9hA5T0phcbMFZtC7kLgtlEsWasXOei4xJ5KPaRbA4yojIbPGWg07HslgUmZM6e5HDbUcB57Cf9USU2InWHzeSNpnQdqcRc2mLZzhNvqcnBZkdIx14k7ZsPQWEpSB3Gnhaj6ZkOr2SHOQbDZh1UIPP0zWYTmY+J8OAmWnUD022W6oFLYPMCDrnWkAKZzL1aBNhoo1BwihyBXMPUMS/Rws/2ulmHsjHlJ/V6K0wo3uKk3yvGqcLRKhwv1/ulUkUd22uRmzXy7DgVdrOPWhVo11xFDvM8RVwYi/5Ek8ipLWiEEPJHIo16iQ5+jAJbSkSn1NMlA1dd1jRLrqQzEd3L1EEW6ETYxLJWUc76bijxWYiTTDo0hqhAcsyVbTV28jfFOF9IoThts4Gz3DaGCjW6hChxVEya7HgQlYGu82O0RS4eiQ40ZI078uj4qXE/VAuUa5O2QAjElV4MsiEOptVCDknBOJbsXw0T6GzXoMf81Frux7dQvPo2CCI+vJQAZ6gNk9uMsCPtIHhcI6E+tsLuBkwzdLJpLoj7IwKT29WomGwFS2DVbimd9WR1U2W7debZjTenIeG5PPqb1IjnYLIr05o8eRmDZ7dzMKWktdXd+MHcgXP/U+D50IlvEJnzyvuz93LQDlE0cVlokGKbFDQsWX1HzqsOanHbxTzsJv8yYKohBsaCVtOqLHxdx1+OmOgnUSs3lESfMMJDTbQMSXtGq0aaekto512JA2vG3KW6trFpEtQiIxLGnx3woz/ZRkRc8m1MHSxIWmT4cpOGSvtWGHnNdh0Ro/1J03IyDYi6pIea383Y+0xPQ7SBsFaFj9uoU6ExEW4rrdiPcn2Izs+HdfivNL918b3omxnXgwunkg+EMpuV/iA5yt38B6Ld2KWvpvg8sHbyovK4J4ujfLK/5SoEugKNAjbGIKQjUFQ5SvlUgFnYSGigsKxbfVWZJ85L5cK6NV66JUlWuFC3q284gKdWofMlL3ITN3L+3LBoDLCanYPGQDJZIVBbSo9WSLQpHNt83I5falyS8tp1FhgNQkzPrb/GI2ZjuysC9iXmo5j+44JTS0HFSJq9+5kDPP1wYqAFVgyYxHGjx6F65ev83tns07jrUH9sdBvLlZ/sRajBw/CT1u28XsMges2oVnDxrBZSghYv2oDPJp4yDmKlmcEYMzwEVjgPwtTP/wUN67d5OULZ3yB1NhdPO1CavQufD3vGzknkBQXj9eavQq7u78jojq0bI0VX34lFwBrv1mFyOAQng5ctREzJ0+Gr1cXfD5xMjZ9vx6FpdekFP6UqNzbd+D7uhcOHdovlwCXz2dDna+GZJHw3+FDELQpUL5DmpKbi/69emJ/xl6eDw3ajqcf+zdSonbzvMPuwNjBY9HvNV+YLELTPv/QD9eyBfHTPpmGL2Yv5ekZk2cgLjyep12IC1Vg7qez5JzAd4u+wmAvHwRv3iKXAFqlFpPGjEePjh1w4tgxXrZ0/hxsXPMtTzMUOmyY9MG7sFDMxlCuRRL+lKgNq7/Hwll+cq40MnftxfTxU+RcCbZtDMRsv6ki/eM6TJv0AZbNWczzu1NSMX3cp1i55Afk5N7mZQHTZ2P+pwEIWhOCD0e9h0P7D/PyBf6LkByTytMuJEemYOmsJXIOyEjNwNpvf4Cd1GH6JxNhlX9IlnMjBxt+WAVFdByGvTGAlwWuW0OyreVpBq1ajakfvQ9lTskvaMrDnxK14svFWLJwhpwrjdS4FMyZJH5U4Y6d5K/mTJ/O0yu/XYqYncH4Ydn3OHX0BDZ+vw7hW8OxPGA5cvNyeJ2A6XMwecwEbFoRiHO/lfi4gJkLkBLzs5wTSI5MJqKExjFM+3gKyTAXOZdyMdCrP+JjYnl57u08zJgymaeXLpiPzWs3YldCErYFrudlDBpVAWnwe8i/XdqXlYU/JerMmdPo17M7tJqSU7rVbCOHroJaqcIQn344duSofEfgrUGDEBUWztNfL12AmKhgXM++Ce/X+mDFkpXkeC2k8hOQmy/e/M6ZOAd/nMziaXfMmzGfnO4pOSegINNb8FkAT9+4fhUDevbEfMqvWrIKH5A2fkpaxVBAjnzahAmw2ax8h/zk/XEY/Lo3UuMV/D6DqiAfEz8YSUSxeOv+qJAz/3H9BgzpQz5gyxYEBf6IsUNH49QRMYFdyano360Xtq7fjKgdERj/9jtYNM+f32NYvmAx1qxcwdM923kheOt2nh775ljk5AjT8/vgc6QlpPG0Oz6b4o/3hryHwO83Yvsaakc+JDkyFfOmzOP35073x7dfLONpF955YzQO7z8Eh82BKWMnFO/QF/64wH+/EBW2k+cZVPkF+HDkW7hzPVcuKR8VIorhUOZBLJ+9mK4l+PXgL3KpQPbZS/hm/ldYMnMxEsknuMAi/FvX7uDmVaE5qjw1LLTdswP3xdOXIUlim7ly9jq0KvF0gUN2qhcvXsE2ctBBGzcjJiSSl5v0Zty8JAjOOn4WRl3p48eVc1dx6Uy2SJ+5Bjv/SbXA6RNnindU1peDAtbsrKvk1+6z3cmoMFGVifvtLn9X/CVE/RPxiKgK4hFRFcQjoiqIR0RVEI+IqhCA/weoUu/UHz9R4QAAAABJRU5ErkJggg=="><br>
                                                        </p>
                                                        <p style="text-align: center;"><br></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="page-break"></div>
                                        <div class="art-content-layout-wrapper layout-item-3" style="margin-left: 20px; margin-right: 20px;">
                                            <div class="art-content-layout layout-item-1">
                                                <div class="art-content-layout-row">
                                                    <div class="art-layout-cell layout-item-2" style="width: 100%">
                                                        <p><span style="font-weight: bold; font-size: 18px;">Diagnoses:</span></p>
                                                        <table class="art-article diagnosis-table">
                                                            <thead>
                                                                <tr>
                                                                    <th>ICD-Code</th>
                                                                    <th>Name</th>
                                                                    <th>Updated</th>
                                                                </tr>
                                                            </thead>

                                                            <tbody>
                                                                @foreach($diagnosis as $diagnoses)
                                                                    <tr>
                                                                        <td>{!! preg_replace('/"/', '', $diagnoses['icd']) !!}</td>
                                                                        <td>{!! preg_replace('/"/', '', preg_replace('/\\\t/', ' ', $diagnoses['name'])) !!}</td>
                                                                        <td>{{ $diagnoses['updated_at'] ? \Carbon\Carbon::parse($diagnoses['updated_at'])->format('Y-m-d h:i') : '' }}</td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</main>


<script type="text/php">
    if (isset($pdf)) {
        $text = "Page {PAGE_NUM} of {PAGE_COUNT} pages";
        $size = 10;
        $font = $fontMetrics->getFont("Arial");
        $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
        // $x = ($pdf->get_width() - $width) / 2;
        $y = $pdf->get_height() - 40;

        $x = ($pdf->get_width() - $width) + 15;
        $pdf->page_text($x, $y, $text, $font, $size);


        $text = date("Y") . " © Virtual Psychology";
        $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
        $x = ($pdf->get_width() - $width) / 2 - 10;
        $y = $pdf->get_height() - 25;


        $pdf->page_text($x, $y, $text, $font, $size);
    }
</script>

</body>
</html>