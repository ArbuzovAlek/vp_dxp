<div class="popup closable" id="export_popup">
    <div class="info-note-popup popup-content" style="padding:40px 30px 20px 30px; font-size:inherit;">
        <div data-action="popup_close" data-target="export_popup" class ="close-btn x-close"></div>
        <div class = "content">
            <h3>Export</h3>
            <div><a href="/center/settings/export_demographic_diagnoses">Patient demographics and diagnoses</a></div>
            <div><a {{ $severity_scale_available ? 'href=/center/settings/export_severity_scale' : 'class="inactive"' }}>Clinician-rated psychotic symptoms severity scale</a></div>
        </div>
        <div class="close-bottom x-close"><p><a class="art-button" data-action="popup_close" data-target="export_popup">Close</a></p></div>
    </div>
</div>