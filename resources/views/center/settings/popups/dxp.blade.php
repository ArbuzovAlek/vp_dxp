@php
    $diagnosis_view = request()->cookie('diagnose-view');
@endphp
<div class="popup closable" id="dxp_popup">
    <div class="info-note-popup popup-content" style="padding:40px 30px 20px 30px; font-size:inherit;">
        <div data-action="popup_close" data-target="dxp_popup" class ="close-btn x-close"></div>
        <div class = "content">
            <h3>DxP settings</h3>
            <div class="setting-items">
                <div>
                    <div>Present all diagnosis criteria on one page:</div>
                    <div data-field="present_favorite" data-checked_val="all" data-unchecked_val="one" class="toggle-button {{ $ownUser->present_favorite === 'all' ? '' : 'disabled' }}"></div>
                </div>
                <div>
                    <div>Required diagnosis sub-criterion entries:</div>
                    <div data-field="settings" data-key="sub" class="toggle-button {{ ($ownUser->settings['sub'] ?? false) ? '' : 'disabled' }}"></div>
                </div>
                <div>
                    <div>Required diagnosis sub-sub-criterion entries:</div>
                    <div data-field="settings" data-key="sub_sub" class="toggle-button {{ ($ownUser->settings['sub_sub'] ?? false) ? '' : 'disabled' }}"></div>
                </div>
                {{--<div>
                    <div>Specifiers options for un-meet criteria:</div>
                    <div data-field="settings" data-key="use_specifiers_for_unmeet_criteria" class="toggle-button {{ ($ownUser->settings['use_specifiers_for_unmeet_criteria'] ?? false) ? '' : 'disabled' }}"></div>
                </div>--}}
                <div>
                    <div>Set classification clusters page view:</div>
                    <div class="setting-item-actions">
                        <div class="setting-item-actions-text" style="{{ $ownUser->settings['diagnose_view'] !== 'matrix' ? 'color: #808080;' : ''  }}">Matrix</div>
                        <div
                            data-field="settings"
                            data-key="diagnose_view"
                            class="toggle-button always-filled {{ isset($ownUser->settings['diagnose_view']) && $ownUser->settings['diagnose_view'] === 'matrix' ? 'disabled' : '' }}"
                            data-checked_val="list"
                            data-unchecked_val="matrix">
                        </div>
                        <div class="setting-item-actions-text" style="{{ $ownUser->settings['diagnose_view'] === 'matrix' ? 'color: #808080;' : ''  }}">List</div>
                    </div>
                </div>
                <div>
                    <a href="/center/settings/restore_defaults">Restore defaults</a>
                </div>
            </div>
        </div>
        <div class="close-bottom x-close"><p><a class="art-button" data-action="popup_close" data-target="dxp_popup">Close</a></p></div>
    </div>
</div>