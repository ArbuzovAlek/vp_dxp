@extends( 'center.app')
@section('content')

    <link rel="stylesheet" href={{ URL::asset("css/center/diagnosis.css")}} media="all">

    <div id="diagnosis-help-modal" style="display:none;">
        <div class="info-note-popup">
            <div class="close-btn x-close"></div>
            <div class="content">
                The Diagnosis table presents all the diagnoses that have been met and assigned to the selected patient. <br>
                You can sort the diagnosis priority position. <br>
                To edit or delete diagnosis, first select (click/tap) a diagnosis line.
            </div>
            <div class="close-bottom x-close"><p><a class="art-button">Close</a></p></div>
        </div>
    </div>


    <div class="art-sheet clearfix">
        <div class="art-layout-wrapper">
            <div class="art-content-layout">
                <div class="art-content-layout-row">
                    <div class="art-layout-cell art-content">
                        <article class="art-post art-article">
                            <div class="art-postmetadataheader">
                                <h2 class="art-postheader">
                                    <span class="art-postheadericon">Diagnosis</span>
                                    <button type="button" class="help diagnosis-help">?</button>
                                </h2>
                            </div>

                            <div class="art-postcontent art-postcontent-0 clearfix">
                                <div class="art-content-layout-wrapper layout-item-0">
                                    <div class="art-content-layout layout-item-1 dc-art-content-layout">
                                        <div class="art-content-layout-row">
                                            <div class="art-layout-cell layout-item-2 table-container"
                                                 style="width: 95%">
                                                @if (!empty($formals))
                                                    @include('center.diagnosis.table')
                                            </div>
                                            <div class="art-layout-cell layout-item-3"
                                                 style="width: 5%;background-color:#e7ebee; vertical-align: middle;">
                                                <p onclick="diagnosUp('up')" style="text-align: center;"
                                                   title="Move diagnosis priority up">
                                                    <img width="15" height="7" alt="Down"
                                                         src="/css/images/arrow-b-2.png"
                                                         style="transform: scaleY(-1);margin-top: 5px; margin-right: 5px; margin-bottom: 5px; margin-left: 5px;">&nbsp;<br>
                                                </p>
                                                <p onclick="diagnosUp('down')" style="text-align: center;"
                                                   title="Move diagnosis priority down">
                                                    <img width="15" height="7" alt="Down"
                                                         src="/css/images/arrow-b-2.png"
                                                         style="margin-top: 5px; margin-right: 5px; margin-bottom: 5px; margin-left: 5px;">&nbsp;<br>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="art-article-controls">
                                    <div class="art-article-controls-actions">
                                        <span class="art-article-controls-actions-title">Action:</span>
                                        <button title="Add diagnosis" class="art-article-controls-actions-button action-button action-button-add diagnosis-diagnosis-add" type="button">
                                        </button>
                                        <button title="Edit selected diagnosis" class="art-article-controls-actions-button action-button action-button-edit disabled art-article-controls-actions-button-edit diagnosis-diagnosis-edit" type="button">
                                        </button>
                                        <button title="Delete selected diagnosis" class="art-article-controls-actions-button action-button action-button-delete disabled art-article-controls-actions-button-delete diagnosis-diagnosis-delete" type="button">
                                        </button>
                                        <button title="Analytics for selected diagnosis" class="art-article-controls-actions-button action-button disabled action-button-analytics art-article-controls-actions-button-analytics" type="button">
                                        </button>
                                    </div>
                                </div>

                                @else
                                    No diagnosis has been assigned to this patient.
                                @endif
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('center.add_edit_and_delete_diagnos_popup')

    @if(request()->input('error_diagnosis_option'))
        @include('layouts.error_diagnosis_option')
    @endif

    <script src={{ URL::asset("/js/center/diagnosis.js")}} ></script>
@endsection