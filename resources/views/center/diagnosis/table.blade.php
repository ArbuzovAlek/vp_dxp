<div class="diagnosis-table-wrapper" data-url="/center/diagnosis/reload_table/" data-page="{{ $formals->currentPage() }}" data-per-page="{{ $formals->perPage() }}">
    <table class="MsoNormalTable art-article-table-aligned-blocks" border="0" cellspacing="0" cellpadding="0" width="657" style="width: 100%; ">
        @if (isset($selected_diagnos))
            <tbody id="table-diagnosis" data-selected_diagnos="{{$selected_diagnos}}" data-free_text_id_step_selected="{{$free_text_id_step}}">
        @else
            <tbody id="table-diagnosis" data-selected_diagnos="">
        @endif
        @foreach ($formals as $key => $formal)
            @php
                if (isset($formal['free_text_id_step'])) {
                    $free_text_id_step=$formal['free_text_id_step'];
                } else {
                    $free_text_id_step = null;
                }

                $elements = array_filter($formal, 'is_array');
                unset($elements['option']);
            @endphp
        <tr
            id="{{$formal['diagnose_id']}}"
            data-diagnos_id="{{$formal['diagnose_id']}}"
            data-patient_id="{{$patient_id}}"
            data-free_text_id_step="{{$free_text_id_step}}"
            data-is_created="{{ $formal['free_text_is_created'] ?? '' }}"
            data-option_id="{{ $formal['option_id'] ?? ''  }}"
            style="height: 51.8pt;"
            class="{{ app('request')->input('highlight') == $formal['diagnose_id'] ? 'active' : '' }}"
            data-formal="{{ json_encode($formal)  }}"
            onclick=""
        >
            <td width="106" valign="top" style="padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt; height: 51.8pt; width: 15%; ">
                <p class="MsoNormal" style="margin-top: 2.25pt; margin-right: 0cm; margin-bottom: 2.25pt; margin-left: 0cm; line-height: 15.75pt; ">
                    <span style="font-family: Tahoma, sans-serif; letter-spacing: 0.75pt; ">
                        @if(isset($formal['multi_icd']))
                            <div class="result-table-item-code">
                                {{ $formal['multi_icd'][0]['code'] }}
                            </div>
                        @else
                            @foreach ($elements as $diagnosis)
                                <div class="result-table-item-code">
                                    @if(!$loop->first) <br> @endif
                                    {{ $diagnosis['code'] }}
                                </div>
                            @endforeach
                        @endif
                    </span>
                </p>
            </td>
            <td width="551" valign="top" style="padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt; height: 51.8pt; width: 10px; ">
                <p class="MsoNormal" style="margin-top: 2.25pt; margin-right: 0cm; margin-bottom: 2.25pt; margin-left: 0cm; line-height: 15.75pt; "><span style="font-family: Tahoma, sans-serif; letter-spacing: 0.75pt; ">
                    @if(isset($formal['multi_icd']))
                        <div class="result-table-item-text">
                            @foreach($formal['multi_icd'] as $icd)
                                @if(!$loop->first){{$icd['code']}}@endif
                                {{ $icd['text'] }}@if(!$loop->last && substr($icd['text'], -1));@endif{{''}}@if($loop->last && $formal['text']), {{$formal['text']}} @endif
                            @endforeach
                        </div>
                    @else
                        @foreach ($elements as $diagnos)
                            <div class="result-table-item-text">
                                @if(!$loop->first)<br> @endif
                                {!! $diagnos['text'] !!}{{ !empty($diagnos['options_text']) ? ' ' . $diagnos['options_text'] : '' }}@if(!$loop->last && (substr($diagnos['text'], -1) !== ';')); @endif
                            </div>
                        @endforeach
                    @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
    </table>
    <div class="pagination-block" data-wrapper=".diagnosis-table-wrapper">
        {{ $formals->appends(['per_page' => $formals->perPage()])->links(null, ['per_page_list' => [5, 10, 20, 50, 'All']]) }}
    </div>
</div>