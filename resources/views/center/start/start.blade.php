@extends( 'center.app')

@section('content')
<script src={{ URL::asset("js/center/start.js")}}></script>
<div class="art-sheet clearfix">
  <div class="art-layout-wrapper">
  @yield('error') 
    <div class="art-content-layout">
      <div class="art-content-layout-row">
        <div class="art-layout-cell art-content">
          <article class="art-post art-article">
            <div class="art-postcontent art-postcontent-0 clearfix">
              <div class="art-content-layout-wrapper layout-item-0">
                <div class="art-content-layout layout-item-1">
                  <div class="art-content-layout-row">
                    <div onclick="PopUpShow()" title="Select patient Id." class="art-layout-cell layout-item-2" style="width: 50%; cursor:pointer;">
                        <p style="text-align: center;">
                            <span style="font-family: Verdana; font-size: 16px; font-weight: bold;">
                                Patient Id<br>[
                                    <span id='patient-id'>{{request()->selected_patient_user_id}}</span>
                                    ]<br>
                            </span>
                        </p>
                      <p style="text-align: center;"><img width="301" height="203" alt="" src="/css/images/tmp12A5.png" class=""><br></p>
                    </div>
                    <div class="art-layout-cell layout-item-3" style="width: 50%">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <a href="{{ $ownUser->selected_patient ? '/' : '/center/error_patients' }}" title="Diagnostic process for selected patient." class="start-diagnosis-process-link">
                            </a>
                            <br>
                            <img width="307" height="132" alt="" src="/css/images/tmpAB4A.png" style="margin-top: 1px;" class="">
                        </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div id="inf"></div>
          </article>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection