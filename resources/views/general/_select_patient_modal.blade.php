<div id="select-patient-popup">
    <div class="modal-content">
        <div class="modal-header info-popup select-patient-header-footer">
            <span class="close height-for-close">&times;</span>
            <span id="header-selected_patient">Selected patient: {{request()->selected_patient_user_id}}</span>
        </div>
        <div class="modal-body modal-no-padding patients-panel-wrapper">

        </div>
        <div class="modal-footer info-popup select-patient-header-footer">
            <span class="close">Close</span>
        </div>
    </div>
</div>