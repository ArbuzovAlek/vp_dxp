<div id="other-popup">
    <div class="other-popup-content art-sheet clearfix">
        <div class="art-layout-wrapper">
            <div class="art-content-layout">
                <div class="art-content-layout-row">
                    <div class="art-layout-cell art-content">
                        <article class="art-post art-article">
                            <div class="art-postcontent art-postcontent-0 clearfix">
                                <div class="other-header art-content-layout layout-item-0">
                                    <div class="art-content-layout-row">
                                        <div class="art-layout-cell layout-item-2" style="width: 100%" >
                                            <p style="text-align: center;">
                                                <span style="color: rgb(250, 83, 0); font-family: 'Comic Sans MS'; font-weight: bold; letter-spacing: 2px; text-shadow: rgba(23, 23, 23, 0.808594) 0px -1px 0px; font-size: 20px;">{{$diagnosis_name}}</span><br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="square-div art-content-layout-br layout-item-1"></div>
                                <div class="other-textarea art-content-layout-wrapper layout-item-3">
                                    <div class="art-content-layout layout-item-4">
                                        <div class="art-content-layout-row">
                                            <div class="art-layout-cell layout-item-5" style="width: 100%" >
                                                <textarea maxlength="255" class="other-text-area other-input">{{$free_text}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
<!--                                 <div class="other-links art-content-layout-wrapper layout-item-6">
                                    <div class="art-content-layout layout-item-7">
                                        <div class="art-content-layout-row">
                                            <div class="art-layout-cell layout-item-8" style="width: 100%" >
                                                <p><img width="19" height="19" alt="Note" src="/css/images/edit-button%20(2)-3-2-2-2-2-2-2.png" style="margin-top: 6px; margin-left: 10px;">&nbsp; &nbsp;<img width="19" height="19" alt="More Information" src="/css/images/Note32-3-2-2-2-2-2-2.png" style="margin-top: 3px; margin-right: 5px; margin-left: 10px;"><br></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
 -->                                <div class="other-buttons art-content-layout-wrapper layout-item-9">
                                    <div class="other-divider art-content-layout layout-item-10">
                                        <div class="art-content-layout-row">
                                            <div class="art-layout-cell layout-item-11" style="width: 100%" >
                                                <p style="text-align: center;">
                                                    <a id="other-enter-btn" class="art-button continue">Enter</a>&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;            <a class="art-button close">Cancel</a>
                                                </p>

<!--                                                     <div class="popup">
                                                        <span class="popuptext">If you choose not to specify the reasons why the criteria are not met for the disorder click Cancel, and consider whether the "Unspecified" category for the disorder could be diagnosed.</span>
                                                    </div>
 -->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="art-content-layout-wrapper">
                                    <div class="art-content-layout">
                                        <div class="art-content-layout-row">
                                            <div class="art-layout-cell">
                                                <div class = "popup">
                                                    <div class="popuptext">
                                                        <span class="close-popup">&times;</span>
                                                        <span class="content">If you choose not to specify the reasons why the criteria are not met for the disorder click Cancel, and consider whether the "Unspecified" category for the disorder could be diagnosed.</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <footer class="art-footer">
            <div class="art-content-layout layout-item-0">
                <div class="art-content-layout-row">
                    <div class="art-layout-cell layout-item-1" style="width: 100%"></div>
                </div>
            </div>
        </footer>
    </div>
</div>
