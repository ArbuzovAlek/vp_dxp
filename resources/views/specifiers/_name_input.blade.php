<style>
.art-content .art-postcontent-0 .layout-item-3 { border-top-width:2px;border-top-style:Groove;border-top-color:#7493B4;margin-top: 0px;margin-right: 20px;margin-bottom: 20px;margin-left: 20px;  }
.art-content .art-postcontent-0 .layout-item-5 { margin-top: 10px;margin-right: 20px;margin-bottom: 20px;margin-left: 20px;  }
.art-content .art-postcontent-0 .layout-item-6 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-color:#E1E8EF; color: #111418; background: ; border-spacing: 27px 0px; border-collapse: separate; border-radius: 0px;  }
.art-content .art-postcontent-0 .layout-item-7 { color: #323B43; background: ; padding: 5px; vertical-align: top; border-radius: 0px;  }
.art-content .art-postcontent-0 .layout-item-9 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #303841; background: #F7F7F8 url('/css/images/d033a.png') scroll; padding: 5px; vertical-align: top; border-radius: 0px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }

</style>
@php($s_var=$specify->variable)
<div class="art-content-layout-wrapper layout-item-5">
<div class="art-content-layout layout-item-6">
    <div class="art-content-layout-row">
    <div class="art-layout-cell layout-item-7" style="width: 100%" >
        <p><span class="f_ic1" style="font-size: 14px;">{{$specify->text}}</span><span style="font-size: 14px;"><span style="color: rgb(17, 20, 24);"><span class="f_ic1"></span></span><span style="color: rgb(17, 20, 24);"><span class="f_ic1"></span><span class="f_ic1"></span></span></span></p>
    </div>
    </div>
</div>
</div>
<div class="art-content-layout-wrapper layout-item-5">
<div class="art-content-layout layout-item-6">
    <div class="art-content-layout-row">
    <div class="art-layout-cell layout-item-9" style="width: 100%" >
        <p><span style="color: rgb(43, 61, 80); font-size: 14px;">{{empty($specify->additional_text) ? "Enter particular substance name:" : $specify->additional_text}}</span></p><p><input type="text" class="substance-input" value= "{{($variables->$s_var ?? NULL)}}"><br></p>
    </div>
    </div>
</div>
</div>
 <script type="text/javascript">
    variables = JSON.parse($("#variables").val());
    variable = $("#variable").val();
    $(document).on('keyup', "input.substance-input", function(){
        variables[variable]= $(this).val();
        $("#variables").val(JSON.stringify(variables));
    })   
</script>