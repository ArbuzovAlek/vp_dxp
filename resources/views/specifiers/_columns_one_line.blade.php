
<style>
.art-content .art-postcontent-0 .layout-item-5 { margin-top: 15px;margin-right: 20px;margin-bottom: 15px;margin-left: 20px;  }
.art-content .art-postcontent-0 .layout-item-6 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; color: #111418; border-spacing: 5px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-8 { color: #0B0D0F; background: #F5F5FA url('/css/images/93909.png') scroll; padding-right: 10px;padding-left: 10px; vertical-align: top;  }
.art-content .art-postcontent-0 .layout-item-9 { color: #111418; padding-right: 10px;padding-left: 10px; vertical-align: top;  }
.art-content .art-postcontent-0 .layout-item-10 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #0B0D0F; background: #F7F7F8 url('/css/images/92cc6.png') scroll; padding: 0px; vertical-align: top; border-radius: 5px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }

</style>
@php $selected = $specify->variable; @endphp
<div class="art-content-layout-wrapper layout-item-5">
    <div class="art-content-layout layout-item-6">
        <div class="art-content-layout-row">
            <div class="art-layout-cell layout-item-8" style="width: 25%" >
                <p>
                    <span style="color: rgb(45, 63, 83); font-style: italic; font-weight: bold;">{{$specify->text}}</span>
                </p>
            </div>
            <div class="art-layout-cell layout-item-9" style="width: 60%" >
                <p>
                    <span style="font-size: 14px; color: #0E141B;">{{$specify->additional_text}}</span>
                </p>
            </div>
            <div class="art-layout-cell layout-item-10" style="width: 15%" >
                <p style="text-align: center;">
                    <span class="specifiers-answers">
                        @foreach($specifiers_items as $key => $item)
                            <span style="color: rgb(11, 13, 15);">
                            @php 
                                $class_a = "art-button";
                                $checked = '';
                                if($item->var_data == ($variables->$selected ?? null)){
                                    $class_a = 'art-button chosen';
                                    $checked = "checked";
                                }
                            @endphp
                            <a id="{{$item->id}}" class="{{$class_a}}" var_data="{{$item->var_data}}">{{$item->btn_text}}</a>
                            <input type="radio" name="choose-one" value="{{$item->id}}" class="hidden-input" {{$checked}} >
                        </span>
                        @endforeach
                    </span><br>
                </p>
            </div>
        </div>
    </div>
</div>