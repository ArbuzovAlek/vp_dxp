<style>
.art-content .art-postcontent-0 .layout-item-2 { border-spacing: 20px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-5 { margin: 20px;  }
.art-content .art-postcontent-0 .layout-item-6 { border-style:Double;border-width:3px;border-color:#D8DEE4; color: #0B0D0F; background: #F7F7F8 url('/css/images/e357d.png') scroll; padding: 5px; border-radius: 10px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }

</style>
@php $selected = $specify->variable;
$ncd_types =false; 
if($specify->behavior && $specify->behavior->name == 'ncd_types'){
    $ncd_types = true;
}
$href = '#';
@endphp
<div class="specifiers-answers">
    <div class="art-content-layout-wrapper layout-item-5">
        <div class="art-content-layout layout-item-2">
            <div class="art-content-layout-row">
                <div class="art-layout-cell layout-item-6" style="width: 100%" >
                    @foreach($specifiers_items as $key=>$item)
                    @php 
                        $class_a = "art-button";
                        $checked = '';
                        if($item->var_data == ($variables->$selected ?? null)){
                            $class_a = 'art-button chosen';
                            $checked = "checked";
                        }
                        if($ncd_types){
                            $path = $specify->ncd_type_url($breadcrumbs_str,$item->var_data,$diagnosis);
                            $href = $path."?return_type=ncd_specify&return_to=".urlencode(Request::fullurl());
                            $href = $path;
                        }
                    @endphp
                        <p style="padding-left: 20px; line-height: 32px;">
                            <span style="color: rgb(48, 56, 65);">
                                <span style="text-align: center; -webkit-border-horizontal-spacing: 27px;">
                                <a id="{{$item->id}}" class="{{$class_a}}" var_data="{{$item->var_data}}" href="{{$href}}">{{$item->btn_text}}</a>
                                <input type="radio" name="choose-one" value="{{$item->id}}" class="hidden-input" {{$checked}} >
                                </span><br>
                            </span>
                        </p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>