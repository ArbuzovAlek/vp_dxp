@extends( 'layouts.app')

@section('content')

    <link rel="stylesheet" href={{ URL::asset("css/checker.css")}} />

    <div class="error-modal-content" style="display:none">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close height-for-close">&times;</span>
                <span class="modal-header-text"></span>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
        </div>
    </div>

    <div class="art-sheet clearfix">
        <div class="art-layout-wrapper">
            <div class="art-content-layout">
                <div class="art-content-layout-row">
                    <div class="art-layout-cell art-content">
                        <div class="art-post art-article">
                            <div class="checker-wrapper step-1">
                                <table class="table checker-table">
                                    <tr>
                                        <th>Add symptom:</th>
                                        <th>Selected symptoms</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="symptom-add-block">
                                                <input type="text" name="query" id="search-symptom-input">
                                                <input type="button" id="add_symptom" value="Add">
                                            </div>
                                        </td>
                                        <td>
                                            <ul id="checked-symptom" class="checker-list"></ul>
                                        </td>
                                    </tr>
                                </table>

                                <div class="checker-buttons">
                                    <button type="button" id="checker-finish" class="art-button">Continue</button>
                                </div>
                                
                                <div class="checker-beta">
                                    <img src="/css/images/beta.jpg" alt="">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src={{ URL::asset("js/autocomplete.js")}}></script>
    <script src={{ URL::asset("js/checker.js")}}></script>
@endsection