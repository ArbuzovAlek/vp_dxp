@extends( 'layouts.app')

@section('content')

    <link rel="stylesheet" href={{ URL::asset("css/checker.css")}} />

    <div class="error-modal-content" style="display:none">
        <div class="modal-content">
            <div class="modal-header">
                <span></span>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
        </div>
    </div>

    <div class="art-sheet clearfix">
        <div class="art-layout-wrapper">
            <div class="art-content-layout">
                <div class="art-content-layout-row">
                    <div class="art-layout-cell art-content">
                        <div class="art-post art-article">
                            <div class="checker-wrapper checker-symptom-add">

                                <p style="text-align: right">
                                    <a href="/checker/diagnosis">Edit diagnosis weight</a>
                                    <br>
                                    <br>
                                </p>

                                <table class="table checker-table">
                                    <tr>
                                        <th></th>
                                        <th>Selected diagnosis</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <form id="checker-symptom-add-form">
                                                <p>Select diagnosis</p>
                                                <input id="new-symptom-diagnosis" type="text" name="diagnosis" placeholder="start typing">

                                                <br>
                                                <br>

                                                <p>Symptom name</p>
                                                <input id="new-symptom-name" type="text" name="name">

                                                <br>
                                                <br>
                                            </form>
                                        </td>
                                        <td>
                                            <ul id="checker-new-diagnosis" class="checker-list"></ul>
                                        </td>
                                    </tr>
                                </table>

                                <div class="checker-buttons">
                                    <button type="button" id="checker-add-new-diagnosis" class="art-button">Save</button>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src={{ URL::asset("js/autocomplete.js")}}></script>
    <script src={{ URL::asset("js/checker.js")}}></script>
@endsection