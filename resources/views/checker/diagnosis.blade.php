@extends( 'layouts.app')

@section('content')

    <link rel="stylesheet" href={{ URL::asset("css/ion.rangeSlider.min.css")}} />
    <link rel="stylesheet" href={{ URL::asset("css/checker.css")}} />

    <div class="error-modal-content" style="display:none">
        <div class="modal-content">
            <div class="modal-header">
                <span></span>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
        </div>
    </div>

    <div class="art-sheet clearfix">
        <div class="art-layout-wrapper">
            <div class="art-content-layout">
                <div class="art-content-layout-row">
                    <div class="art-layout-cell art-content">
                        <div class="art-post art-article">

                            <p style="text-align: right">
                                <a href="/checker/symptom">Add new symptom</a>
                                <br>
                                <br>
                            </p>

                            <div class="checker-wrapper checker-symptom-add">
                                <table class="table checker-table">
                                    <tr>
                                        <th>Edit symptom weight of diagnosis</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <form id="checker-update-diagnosis-form">
                                                <p>Select diagnosis</p>
                                                <input id="edit-diagnosis" type="text" name="diagnosis" placeholder="start typing">

                                                <div id="checker-diagnosis-weight-list"></div>
                                            </form>
                                        </td>
                                    </tr>
                                </table>

                                <div class="checker-buttons">
                                    <button type="button" id="checker-update-diagnosis" class="art-button">Save</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src={{ URL::asset("js/ion.rangeSlider.min.js")}}></script>
    <script src={{ URL::asset("js/autocomplete.js")}}></script>
    <script src={{ URL::asset("js/checker.js")}}></script>
@endsection