@extends( 'layouts.app')

@section('content')

    <link rel="stylesheet" href={{ URL::asset("css/checker.css")}} />

    <div class="error-modal-content" style="display:none">
        <div class="modal-content">
            <div class="modal-header">
                <span></span>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
        </div>
    </div>

    <div class="art-sheet clearfix">
        <div class="art-layout-wrapper">
            <div class="art-content-layout">
                <div class="art-content-layout-row">
                    <div class="art-layout-cell art-content">
                        <div class="art-post art-article">
                            <div class="checker-wrapper checker-results">
                                <h2>Diagnoses with matched symptoms</h2>
                                @if (count($diagnoses))
                                    <ul>
                                        @foreach($diagnoses as $diagnosis)
                                        @if($diagnosis->probability)
                                            <li>
                                                <a href="/diagnosis/{{ $diagnosis->id }}/criteria">
                                                    {{ $diagnosis->name }}
                                                    <i style="color: #bfbfbf;">(p. {{ $diagnosis->probability }})</i>
                                                </a>
                                            </li>
                                        @endif
                                        @endforeach
                                    </ul>
                                @else
                                    Diagnoses not found
                                @endif

                                <p>
                                    <a href="/checker">Back</a>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <script src={{ URL::asset("js/autocomplete.js")}}></script>
    <script src={{ URL::asset("js/checker.js")}}></script>
@endsection