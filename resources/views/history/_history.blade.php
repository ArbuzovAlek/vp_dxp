<div class="art-content-layout-wrapper layout-item-4 history-wrapper" data-url="/history/history-paginate">
    <div class="art-content-layout layout-item-5">
        <div class="art-content-layout-row responsive-layout-row-1">
            <div class="art-layout-cell layout-item-6" style="width: 100%">
                <div class="art-article-wrapper">
                    <table id="history_table" class="art-article art-article-dark" data-wrapper=".history-wrapper" data-patient_diagnosis_id="{{ @$patient_diagnosis_id }}" data-diagnosis_id="{{ @$diagnosis_id }}">
                        <tbody>
                        <tr>
                            <th class="sigma-art-article-cell-number"></th>
                            <th
                                    class="
                                sigma-art-article-cell-patient
                                col-sort
                                {{ app('request')->input('order-by') === 'patient_diagnosis_id' ? 'col-sort-' . app('request')->input('order-direction') : ''  }}
                                            "
                                    data-col="patient_diagnosis_id"
                            >
                                Patient
                            </th>
                            <th
                                    class="
                                sigma-art-article-cell-link
                                col-sort
                                {{ app('request')->input('order-by') === 'title' ? 'col-sort-' . app('request')->input('order-direction') : ''  }}
                                            "
                                    data-col="title"
                            >
                                Link
                            </th>
                            <th
                                    class="
                                sigma-art-article-cell-updated
                                col-sort
                                {{ app('request')->input('order-by') === 'updated_at' ? 'col-sort-' . app('request')->input('order-direction') : ''  }}
                                    {{ app('request')->input('order-by') === null ? 'col-sort-desc' : '' }}
                                            "
                                    data-col="updated_at"
                            >
                                Updated
                            </th>
                        </tr>
                        @foreach($history as $key => $val)
                            <tr>
                                <td>{{($history->currentPage() - 1) * $history->perPage() + $loop->iteration}}</td>
                                <td>{{$val->patient_diagnosis_id}}</td>
                                <td><a href="{{$val->url}}"> {!! str_replace('Specifiers', 'Specifier', $val->title) !!}</a></td>
                                <td class="text-center">{{$val->updated_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="pagination-block" data-wrapper=".history-wrapper">
                    {{ $history->appends(['per_page' => $history->perPage()])->links(null, ['per_page_list' => [10, 25, 50, 100]]) }}
                </div>

            </div>
        </div>
    </div>
</div>