<div class="art-content-layout-wrapper layout-item-0 formals-wrapper" data-url="/history/formals-paginate">
    <div class="art-content-layout layout-item-1">
        <div class="art-content-layout-row responsive-layout-row-1">
            <div class="art-layout-cell layout-item-3">
                <div class="art-article-wrapper">
                    <table class="art-article art-article-table-aligned-blocks" id="results_table" data-wrapper=".formals-wrapper">
                        <tbody>
                        <tr>
                            <th class="sigma-art-article-cell-number"></th>
                            <th
                                class="
                                    sigma-art-article-cell-patient
                                    col-sort
                                    {{ app('request')->input('order-by') === 'patient_id' ? 'col-sort-' . app('request')->input('order-direction') : ''  }}
                                "
                                data-col="patient_id"
                            >
                                Patient
                            </th>
                            <th
                                class="
                                    sigma-art-article-cell-icd-code
                                    col-sort
                                    {{ app('request')->input('order-by') === 'code' ? 'col-sort-' . app('request')->input('order-direction') : ''  }}
                                "
                                data-col="code"
                            >
                                ICD-Code
                            </th>
                            <th
                                class="
                                    sigma-art-article-cell-dx
                                    col-sort
                                    {{ app('request')->input('order-by') === 'text' ? 'col-sort-' . app('request')->input('order-direction') : ''  }}
                                "
                                data-col="text"
                            >
                                Dx
                            </th>
                            <th
                                class="
                                    sigma-art-article-cell-assigned
                                    col-sort
                                    {{ app('request')->input('order-by') === 'assigned' ? 'col-sort-' . app('request')->input('order-direction') : ''  }}
                                "
                                data-col="assigned"
                            >
                                Assigned
                            </th>
                            <th
                                class="
                                    sigma-art-article-cell-updated
                                    col-sort
                                    {{ app('request')->input('order-by') === 'updated_at' ? 'col-sort-' . app('request')->input('order-direction') : '' }}
                                    {{ app('request')->input('order-by') === null ? 'col-sort-desc' : '' }}
                                "
                                data-col="updated_at"
                            >
                                Updated
                            </th>
                        </tr>
                        @foreach($formals as $key => $val)
                            @if(!empty($val['multi_icd']))
                                <tr
                                    data-formal="{{ json_encode($val['multi_icd'][0]) }}"
                                    data-patient_id="{{$val['multi_icd'][0]['patient_id']}}"
                                    data-diagnosis_id="{{$val['multi_icd'][0]['diagnosis_id']}}"
                                    class="{{ app('request')->input('highlight') == $val['multi_icd'][0]['diagnosis_id'] ? 'selected' : '' }}"
                                    onclick=""
                                >
                                    <td>{{($formals->currentPage() - 1) * $formals->perPage() + $loop->iteration}}</td>
                                    <td>{{$val['multi_icd'][0]['patient_id']}}</td>
                                    <td>
                                        @foreach($val['multi_icd'] as $item)
                                            @if (is_array($item['code']))
                                                @foreach($item['code'] as $code)
                                                    <div class="result-table-item-code">
                                                        {{ $code['code'] }}
                                                    </div>
                                                    <br><br>
                                                @endforeach
                                            @else
                                                <div class="result-table-item-code">
                                                    {{ $item['code'] }}
                                                </div>
                                                <br><br>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($val['multi_icd'] as $item)
                                            <div class="result-table-item-text">
                                                {!! $item['text'].($loop->last ? '' : ';') !!}
                                            </div>
                                            @if($loop->last)
                                                , {{$formal['text']}}
                                            @endif
                                            <br><br>

                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        @if ($val['multi_icd'][0] && $val['multi_icd'][0]['assigned'] === true)
                                            Yes
                                        @elseif($val['multi_icd'][0] && $val['multi_icd'][0]['assigned'] === false)
                                            No
                                        @else
                                            NA
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        {{ (new DateTime($val['multi_icd'][0]['updated_at']))->format('Y-m-d H:i')  }}
                                    </td>
                                </tr>
                            @else
                                <tr
                                    data-formal="{{ json_encode($val[0]) }}"
                                    data-patient_id="{{$val[0]['patient_id']}}"
                                    data-diagnosis_id="{{$val[0]['diagnosis_id']}}"
                                    data-free_text_id_step="{{ isset($val[0]['free_text_id_step']) ? $val[0]['free_text_id_step'] : '' }}"
                                    data-is_created="{{ $val[0]['free_text_is_created'] ?? '' }}"
                                    data-option_id="{{ $val[0]['option_id'] ?? '' }}"
                                    class="{{ app('request')->input('highlight') == $val[0]['diagnosis_id'] ? 'selected' : '' }}"
                                    onclick=""
                                >
                                    <td>{{($formals->currentPage() - 1) * $formals->perPage() + $loop->iteration}}</td>
                                    <td>{{$val[0]['patient_user_own_id']}}</td>
                                    <td>
                                        @foreach($val as $item)
                                            <div class="result-table-item-code">
                                                @if (is_array($item['code']))
                                                    @foreach($item['code'] as $code)
                                                    @endforeach
                                                @else
                                                    {{ $item['code'] }}
                                                @endif
                                                @if(!$loop->last)
                                                    <br> <br>
                                                @endif
                                            </div>
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($val as $item)
                                            <div class="result-table-item-text">
                                                {!! $item['text'] !!}{{ !empty($item['options_text']) ? ' ' . $item['options_text'] : '' }}@if($loop->last && !empty($item['multi_icd_text'])), {{$item['multi_icd_text']}}@endif
                                                @if(!$loop->last)
                                                    <br> <br>
                                                @endif
                                            </div>
                                        @endforeach
                                    </td>
                                    <td class="text-center">
                                        @if ($val[0] && $val[0]['assigned'] === true)
                                            Yes
                                        @elseif($val[0] && $val[0]['assigned'] === false)
                                            No
                                        @else
                                            NA
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        {{ (new DateTime($val[0]['updated_at']))->format('Y-m-d H:i')  }}
                                    </td>
                                </tr>
                            @endif

                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="pagination-block" data-wrapper=".formals-wrapper">
                    {{ $formals->appends(['per_page' => $formals->perPage()])->links(null, ['per_page_list' => [5, 10, 20, 50]]) }}
                </div>

                <div class="art-article-controls">
                    <div class="art-article-controls-actions">
                        <span class="art-article-controls-actions-title">Action:</span>
                        <button title="Add diagnosis" class="art-article-controls-actions-button action-button action-button-add results-diagnosis-add" type="button">
                        </button>
                        <button title="Edit selected diagnosis" class="art-article-controls-actions-button action-button action-button-edit disabled art-article-controls-actions-button-edit results-diagnosis-edit" type="button">
                        </button>
                        <button title="Delete selected diagnosis" class="art-article-controls-actions-button action-button action-button-delete disabled art-article-controls-actions-button-delete results-diagnosis-delete" type="button">
                        </button>
                        <button title="Analytics for selected diagnosis" class="art-article-controls-actions-button action-button disabled action-button-analytics art-article-controls-actions-button-analytics ">
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('center.add_edit_and_delete_diagnos_popup')