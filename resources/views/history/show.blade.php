@extends(( $layout ? 'center.app' : 'layouts.ajax' ))
@section('content')
    <link rel="stylesheet" href={{ URL::asset("css/center/diagnosis.css")}} media="all">
    <link rel="stylesheet" href={{ URL::asset("css/history.css")}} media="all">

    <div id="history-help-modal" style="display:none;">
        <div class="info-note-popup">
            <div class="close-btn x-close"></div>
            <div class="content">
                The History table presents a list of all pages that have been visited during the diagnostic process of the selected patient.
            </div>
            <div class="close-bottom x-close"><p><a class="art-button">Close</a></p></div>
        </div>
    </div>

    <div class="art-sheet clearfix">
        <div class="art-layout-wrapper">
            <div class="art-content-layout">
                <div class="art-content-layout-row">
                    <div class="art-layout-cell art-content">
                        <article class="art-post art-article">
                            <div class="art-postmetadataheader">
                                <h2 class="art-postheader">
                                    <span class="art-postheadericon">Diagnostic Process History</span>
                                    <button type="button" class="help history-help">?</button>
                                </h2>
                            </div>
                            <div class="art-postcontent art-postcontent-0 clearfix">
                                @include("history._history", ['history' => $history])
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src={{ URL::asset("js/center/diagnosis.js")}} ></script>
    <script src={{ URL::asset("js/history.js")}}></script>
@stop
<!-- Trigger/Open The Modal -->
