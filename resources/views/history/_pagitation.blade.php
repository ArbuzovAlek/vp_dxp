<div class="art-article-controls-pagination" data-wrapper="{{ $wrapper  }}">
    <div class="art-article-controls-pagination-pages">
        <span class="art-article-controls-pagination-page-start">
            {{ (($paginator->currentPage() - 1) * $paginator->perPage()) + 1 }}
        </span>
        -
        <span class="art-article-controls-pagination-page-end">
            @if ($paginator->currentPage() * $paginator->perPage() > $paginator->total())
                {{ $paginator->total() }}
            @else
                {{ $paginator->currentPage() * $paginator->perPage() }}
            @endif
        </span>
        /
        <span class="art-article-controls-pagination-page-total-count">
            {{ $paginator->total() }}
        </span>

        <div class="art-article-controls-pagination-pages-buttons">
            <button
                    type="button"
                    class="art-article-controls-pagination-page-prev"
                    {{ $paginator->onFirstPage() ?  'disabled' : '' }}
                    data-page="{{ $paginator->currentPage() - 1 }}"
            >
            </button>
            <button
                    type="button"
                    class="art-article-controls-pagination-page-next"
                    {{ $paginator->hasMorePages() ?  '' : 'disabled' }}
                    data-page="{{ $paginator->currentPage() + 1 }}"
            >
            </button>
        </div>
    </div>
    <form class="art-article-controls-pagination-pages-go-to">
        Go to:
        <input type="text" name="go-to-page" class="art-article-controls-pagination-pages-go-to-input" value="{{ app('request')->input('row-number')  }}">
        <button type="submit" class="art-article-controls-pagination-pages-go-to-button"></button>
    </form>
</div>