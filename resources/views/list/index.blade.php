<link rel="stylesheet" href={{ URL::asset("css/list/bootstrap.min.css")}} media="all">
<link rel="stylesheet" href={{ URL::asset("css/list/bootstrap-table.min.css")}} media="all">
<link rel="stylesheet" href={{ URL::asset("css/list/custom.css")}} media="all">
@extends('layouts.app')
@section('content')
    <body>
    <div class="art-sheet clearfix">
        <div class="art-layout-wrapper">
            <div class="art-content-layout">
                <div class="art-content-layout-row">
                    <div class="art-layout-cell art-content">
                        <table id="table-list"
                               data-toggle="table"
                               data-search="true"
                               data-url="json/diagnosis_list.json"
                               data-sort-name="name"
                               data-sort-order="asc"
                               data-pagination="true"
                               data-page-size="10"
                               data-page-list="[10, 20, 50, 100]"
                        >
                            <thead>
                            <tr>
                                <th data-field="name" data-sortable="true"><b>Name</b></th>
                                <th data-field="ICD10" data-sortable="true"><b>ICD 10</b></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src={{ URL::asset("js/list/jquery-3.3.1.min.js")}} ></script>
    <script src={{ URL::asset("js/list/popper.min.js")}}></script>
    <script src={{ URL::asset("js/list/bootstrap.min.js")}}></script>
    <script src={{ URL::asset("js/list/bootstrap-table.min.js")}}></script>
    <script>
        $('#table-list').bootstrapTable({
            rowAttributes: {
                title: 'Click to view'
            },
            onClickRow: function (row, element, field) {
                window.location.href = "diagnosis/" + row.id + "/criteria";
            }
        })
    </script>
    </body>
@stop
