<div class="popup closable visible" id="diagnosisOption" class="modal">
    <div class="popup-wrapper" id="diagnosis-option-error" style="background-color: unset;">
        <div class="modal-content patient-error-form">
            <div class="modal-header">
                <span class="close height-for-close">&times;</span>
                <span>There are no selected diagnosis.</span>
            </div>
            <div class="modal-body">
                <p>First select a diagnosis at Results or Diagnosis table.</p>
            </div>
            <div class="modal-footer">
                <span class="close">Close</span>
            </div>
        </div>
    </div>
    </div>
<script src={{ URL::asset("/js/error.js")}}></script>