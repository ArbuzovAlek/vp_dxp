<div class="patients-panel" data-url="/get_patients" data-page="{{ $patients->currentPage() }}" data-per-page="{{ $patients->perPage() }}" data-order-by="{{ app('request')->input('order-by') }}" data-order-direction="{{ app('request')->input('order-direction') }}">
    <div class="table-wrapper">
        <div class="divider"></div>
        <table id="patients_table" data-table="patients">
            <thead>
                <tr>
                    <th
                        data-col="id"
                        class="
                            {{ app('request')->input('order-by') === 'id' ? 'col-sort-' . app('request')->input('order-direction') : '' }}
                            {{ app('request')->input('order-by') === null ? 'col-sort-asc' : '' }}
                        "
                    >
                        <span>Id</span>
                    </th>
                    <th
                        data-col="group"
                        class="{{ app('request')->input('order-by') === 'group' ? 'col-sort-' . app('request')->input('order-direction') : '' }}"
                    >
                        <span>Group</span>
                    </th>
                    <th
                        data-col="created_at"
                        class="{{ app('request')->input('order-by') === 'created_at' ? 'col-sort-' . app('request')->input('order-direction') : '' }}"
                    >
                        <span>Created</span>
                    </th>
                    <th
                        data-col="criteria"
                        class="{{ app('request')->input('order-by') === 'criteria' ? 'col-sort-' . app('request')->input('order-direction') : '' }}"
                    >
                        <span>Criteria</span>
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($patients as $patient)
                    <tr data-id="{{$patient->id}}" class="{{ $ownUser->selected_patient == $patient->id ? 'active' : '' }}">
                        <td>{{$patient->user_own_id}}</td>
                        <td>{{$patient->getGroupLabel()}}</td>
                        <td>{{$patient->created_at}}</td>
                        <td>{{$patient->getCriteriaCount()}}/{{$patient->allowed_criteria_count}}</td>
                    </tr>
                @endforeach

                @if(!count($patients))
                    <tr>
                        <td colspan="100%">
                            You do not have patients in your database.
                            <br>
                            You can create a new patient from the <a href="http://35.224.165.111:8080/records">Records Cabinet.</a>
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>

        <div class="pagination-block" data-wrapper=".patients-panel">
            {{ $patients->appends(['per_page' => $patients->perPage(), 'order-by' => app('request')->input('order-by'), 'order-direction' => app('request')->input('order-direction') ])->links(null, ['per_page_list' => [5, 10, 20, 50, 'All']]) }}
        </div>

    </div>
</div>