@if ($ownUser->selected_patient == $patient->id)
<tr data-id="{{$patient->id}}" class="active">
@else 
<tr data-id="{{$patient->id}}">
@endif
    <td>{{$patient->user_own_id}}</td>
    <td>{{$patient->getGroupLabel()}}</td>
    <td>{{$patient->created_at}}</td>
    <td>{{$patient->getCriteriaCount()}}/{{$patient->allowed_criteria_count}}</td>
</tr>
