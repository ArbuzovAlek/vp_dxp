@extends(request()->ajax() ? 'layouts.ajax' : 'home.index')
@section('error')
    <div id="myModal" class="modal"></div>
    <div id="select-patient-error" style="display:none">
        <div class="modal-content patient-error-form">
            <div class="modal-header">
                <span class="close height-for-close">&times;</span>
                <span>You've reached free criteria limit.</span>
            </div>
            <div class="modal-body">
                <p>Please add criteria.</p>
            </div>
            <div class="modal-footer">
                <span class="close">Close</span>
            </div>
        </div>
    </div>
    <script src={{ URL::asset("/js/error.js")}}></script>
@stop
