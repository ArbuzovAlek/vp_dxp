@extends( $layout ? 'layouts.app' : 'layouts.ajax' )
@section('title', $diagnosis['name'])
@section('content')
<style>.art-content .art-postcontent-0 .layout-item-0 { margin-top: 0px;margin-bottom: 10px;  }
.art-content .art-postcontent-0 .layout-item-1 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:0px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; color: #111418; background: ;  border-collapse: separate;  }
/*.art-content .art-postcontent-0 .layout-item-2 { border-top-width:1px;border-top-style:solid;border-top-color:#9FB4CB;margin-top: 10px;margin-bottom: 10px;  }*/
.art-content .art-postcontent-0 .layout-item-3 { color: #111418; background: ; padding-top: 0px;padding-right: 10px;padding-bottom: 0px;padding-left: 10px; vertical-align: middle;  }
.art-content .art-postcontent-0 .layout-item-4 { margin-top: 10px;margin-bottom: 10px;  }
.art-content .art-postcontent-0 .layout-item-5 { color: #111418; border-spacing: 25px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-6 { border-top-style:Dotted;border-bottom-style:Dotted;border-top-width:1px;border-bottom-width:1px;border-top-color:#9FB4CB;border-bottom-color:#9FB4CB; color: #0B0D0F; background: #EEF2F6;background: rgba(238, 242, 246, 0.6); padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-7 { margin-top: 0px;  }
.art-content .art-postcontent-0 .layout-item-8 { color: #111418;  border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-9 { color: #111418; background: ; padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-10 { border-top-style:solid;border-right-style:solid;border-bottom-style:solid;border-left-style:solid;border-width:1px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; color: #111418;  border-collapse: separate;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
</style>
<input type="hidden" name="level" id = "level_page" value="{{$level}}">
<div class="art-sheet clearfix">
    <div class="art-layout-wrapper">
        <div class="art-content-layout">
            <div class="art-content-layout-row">
                <div class="art-layout-cell art-content">
                	<article class="art-post art-article">
                        <div class="art-postcontent art-postcontent-0 clearfix">
                        	<div class="art-content-layout-wrapper layout-item-0">
								<div class="art-content-layout layout-item-1">
								    <div class="art-content-layout-row">
								    	<div class="art-layout-cell layout-item-3" style="width: 100%" >
        									<p class="diagnosis-name breadcrumbs MsoNormal" style="text-align: right;">
        										<span>{!! Helper::get_breadcrumbs($breadcrumbs) !!}</span>
        									</p>
	        								<h1 style="text-align: center;">
	        									<span style="line-height: 31px; text-align: left; font-variant: small-caps; letter-spacing: 2px; font-weight: bold; color: rgb(73, 104, 136); text-shadow: rgba(255, 143, 87, 0.808594) 1px 1px 2px; font-family: Tahoma; font-size: 26px;">
	        										{{$diagnosis_obj['name']}}
	        									</span>
	        									<br>
        									</h1>
    									</div>
    								</div>
    							</div>
    						</div>

                            <div class="art-content-layout-br diagnosis-divider"></div>
                            @php
                            $has_children = Helper::has_children($diagnosis_obj);
                            $ncd = (($diagnosis_obj['behavior'] == "ncd" || $diagnosis_obj['behavior'] == "major_mild_ncd") ? true : false);
                            $other_child = $diagnosis->is_other_child();
                            $other_cluster = $diagnosis->behavior == 'other_cluster' ? true : false;
                            @endphp
                            @if(count($breadcrumbs)==1 && !$other_cluster && ($has_children || $ncd))
                                @foreach ($diagnosis_obj['children'] as $key=>$val)
                                    @if($val['behavior'] == "major_mild_ncd" || $other_cluster)
                                        @include('diagnosis._columns_nested_diagnosis',['key' => $key, 'name' => $val['name'],'children' => ($val['children'] ?? null) ,'level' => $level.'.'.$key,'breadcrumbs' => $breadcrumbs, 'border_class' => '','ncd' => $ncd])
                                    @else
                                        @include('diagnosis._nested_diagnosis', ['key' => $key, 'name' => $val['name'],'children' => ($val['children'] ?? null) ,'level' => $level,'breadcrumbs' => $breadcrumbs, 'has_children' => $has_children, 'type' =>  $val['behavior']])
                                    @endif
                                @endforeach
                            @else
                                @if($ncd || $other_cluster)                             
                                    @include('diagnosis._columns_nested_diagnosis', ['key' => $level, 'name' => '','children' => $diagnosis_obj['children'],'level' => $level,'breadcrumbs' => $breadcrumbs, 'border_class' => "columns-border",'other_cluster'=>$other_cluster])
                                @elseif($diagnosis->behavior == 'other_present')
                                    @include('diagnosis._other_conditions', ['key' => $level, 'name' => '','children' => $diagnosis_obj['children'],'level' => $level, 'has_children' => $has_children])
                                @elseif($other_child && Helper::other_columns_present($diagnosis_obj['children']))
                                    @include('diagnosis._other_item', ['key' => $level, 'name' => '','children' => $diagnosis_obj['children'],'level' => $level, 'has_children' => $has_children])
                                @else
                                    @include('diagnosis._nested_diagnosis', ['key' => $level, 'name' => '','children' => $diagnosis_obj['children'],'level' => $level, 'has_children' => $has_children])
                                @endif

                            @endif
                            <div class="art-content-layout-br diagnosis-divider"></div>
                            @include('general._menu', ['page' => 'diagnosis','note' =>$diagnosis->note, 'info' => $diagnosis->info])
    					</div>
	    			</article>
	    		</div>
	    	</div>
	    </div>
	</div>
</div>
@stop
