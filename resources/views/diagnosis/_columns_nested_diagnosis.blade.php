<style>.art-content .art-postcontent-0 .layout-item-0 { margin-top: 0px;margin-bottom: 10px;  }
art-content .art-postcontent-0 .layout-item-5 { color: #111418; border-spacing: 25px 0px; border-collapse: separate;  }
.art-content .art-postcontent-0 .layout-item-7 { margin-top: 10px;margin-bottom: 0px;  }
.art-content .art-postcontent-0 .layout-item-8 { border-top-style:Dotted;border-right-style:Dotted;border-left-style:Dotted;border-top-width:1px;border-right-width:1px;border-left-width:1px;border-top-color:#9FB4CB;border-right-color:#9FB4CB;border-left-color:#9FB4CB; color: #0B0D0F; background: #EEF2F6;background: rgba(238, 242, 246, 0.6); padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px; border-radius: 5px;  }
.art-content .art-postcontent-0 .layout-item-9 { border-right-style:Dotted;border-bottom-style:Dotted;border-left-style:Dotted;border-right-width:1px;border-bottom-width:1px;border-left-width:1px;border-right-color:#9FB4CB;border-bottom-color:#9FB4CB;border-left-color:#9FB4CB; color: #0B0D0F; background: #EEF2F6;background: rgba(238, 242, 246, 0.6); padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px; border-radius: 5px;  }
.ie7 .art-post .art-layout-cell {border:none !important; padding:0 !important; }
.ie6 .art-post .art-layout-cell {border:none !important; padding:0 !important; }

</style>
@if(count($breadcrumbs)==1 && empty($other_cluster))
<div class="art-content-layout-wrapper layout-item-7">
    <div class="art-content-layout layout-item-5">
        <div class="art-content-layout-row">
            <div class="diagnosis-name art-layout-cell layout-item-8" style="width: 100%" >
                <p style="text-align: center;">                     
                    <span style="font-family: Tahoma;"><b>
                        <a href= "/diagnosis/{{$level}}">
                            <span   id = "{{$key}}" class ="disorder_list" level ="{{$level}}" style="font-size: 14px; color: rgb(0, 0, 0);">{{$name}}
                                </span>
                        </a></b>
                    </span>
                </p>
            </div>
        </div>
    </div>
</div>
@endif
<div class="art-content-layout-wrapper layout-item-0">
    <div class="art-content-layout layout-item-5">
        <div class="art-content-layout-row">
            @foreach($children as $k1=>$v1)
                <div class="diagnosis-name art-layout-cell layout-item-9 {{$border_class}}" style="width: 50%" >
                    <p class="MsoNormal" style="padding-left: 20px; text-align: center;">
                        <a href="/diagnosis/{{$level.'.'.$k1.($ncd ? '/criteria' : '')}}">
                            <span style="font-size: 14px;color: rgb(0, 0, 0); font-weight: bold;">{{$v1['name']}}</span>
                        </a>
                        <span style="font-weight: bold;"></span>
                    </p>
                    <ul style="padding-left: 20px;">

                        @foreach($v1['children'] as $k2=>$v2)
                            <li>
                                <a href="/diagnosis/{{$level.'.'.$k1.'.'.$k2}}">
                                    <span style="font-size: 14px; color: rgb(0, 0, 0);">{{$v2['name']}}</span>
                                </a><br>
                                @if($v1['behavior'] != 'other_present' && !empty($v2['children']))
                                    <ul style="padding-left: 20px;">
                                        @foreach($v2['children'] as $k3=>$v3)
                                        <li>
                                            <a href="/diagnosis/{{$level.'.'.$k1.'.'.$k2.'.'.$k3}}">
                                            <span style="font-size: 14px; color: rgb(0, 0, 0);">{{$v3['name']}}</span>
                                            </a><br>
                                        </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
    </div>
</div>
