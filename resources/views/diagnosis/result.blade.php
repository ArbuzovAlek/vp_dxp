@extends(( $layout ? 'layouts.app' : 'layouts.ajax' ))
@section('title', $diagnosis->name)
@section('content')

<div style="display: none">
    <div id="note-result-popup">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close show-result-modal height-for-close">×</span>
                <span>Validation Error</span>
            </div>
            <div class="modal-body">
                <p>Please select if you want to assign diagnosis to patient.</p>
            </div>
            <div class="modal-footer">
                <span class="close show-result-modal">Close</span>
            </div>
        </div>

    </div>

    <div id="note-result-popup-2">
        <div class="info-note-popup">
            <div class="close-btn x-close"></div>
            <div class="content">
                The diagnosis criteria have been met. Are you sure you do not want to assign the diagnosis to this patient?
            </div>
            <div class="close-bottom">
                <p>
                    <button type="button" class="art-button" id="note-popup-2-continue-btn">Continue</button>
                    <a class="art-button x-close">Close</a>
                </p>
            </div>
        </div>
    </div>

    <div id="note-result-popup-3">
        <div class="info-note-popup">
            <div class="close-btn x-close"></div>
            <div class="content">
                The diagnosis criteria have not been met.In diagnostic center you can see/inspect the diagnostic process and analytics data.
            </div>
            <div class="close-bottom">
                <p>
                    <button type="button" class="art-button" id="note-popup-3-continue-btn">Continue</button>
                    <a class="art-button x-close">Close</a>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="info-note-popup">
    <div class="close-btn result-cancel-btn"></div>
    <div class="content">
        <form id="result-step-form"
              data-criteria="{{$criteria_hav_been_met ? 'true' : 'false' }}"
              data-specifier-missing="{{$diagnosis_specifiers_are_missing ? 'true' : 'false' }}"
              data-diagnosis_id="{{ $diagnosis->id  }}"
              data-diagnosis_code="{{ $formal_code  }}"
              class="art-postcontent art-postcontent-0 clearfix"
        >

            <div class="art-content-layout-wrapper">
                <div class="art-content-layout criteria-item ">
                    <div class="art-content-layout-row responsive-layout-row-3 diagnosis-status">
                        <div class="art-layout-cell criteria-num criteria-num-0">
                            <p>
                                <span>@if ($criteria_hav_been_met === true) <img src="{{asset('images/diagnosis_met_dot.jpg')}}" width="30" height="30"/> @else <img src="{{asset('images/diagnosis_not_met_dot.jpg')}}" width="30" height="30"/> @endif</span>
                            </p>
                        </div>
                        <div class="art-layout-cell criterion-text">
                                @if ($criteria_hav_been_met === false)
                                    Full criteria for <strong>{{$diagnosis->name}}</strong> have not been met.
                                @else
                                    Full criteria for <strong>{{$diagnosis->name}}</strong> have been met.
                                @endif
                        </div>
                        <div class="art-layout-cell" style="width: 25%;"></div>
                    </div>
                </div>
            </div>

            <div class="art-content-layout-wrapper">
                <div class="art-content-layout criteria-item ">
                    <div class="art-content-layout-row responsive-layout-row-3 diagnosis-status"><div class="art-layout-cell criteria-num criteria-num-0">
                            <p><span></span></p>
                        </div>
                        <div class="art-layout-cell criterion-text">
                            <p>
                                Do you want to assign diagnosis to patient Id {{ request()->selected_patient_user_id }}?
                            </p>
                        </div>
                        <div id="4" class="art-layout-cell criterion-answers" style="width: 25%;">
                            <p>
                                <a class="art-button {{ !$criteria_hav_been_met ? 'disabled' : ''  }}">
                                    Yes
                                </a>
                                <input
                                        type="radio"
                                        {{ !$criteria_hav_been_met ? 'disabled' : ''  }}
                                        name="assign_to"
                                        value="Yes"
                                        class="hidden-input"
                                >
                                <a class="art-button {{ !$criteria_hav_been_met ? 'disabled' : ''  }}">
                                    No
                                </a>
                                <input
                                        type="radio"
                                        {{ !$criteria_hav_been_met ? 'disabled' : ''  }}
                                        name="assign_to"
                                        value="No"
                                        class="hidden-input"
                                >
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="art-content-layout-br criteria-master-divider"></div>

            <p style="margin-bottom: 20px">
                Later you can edit the diagnosis (and its assignment to the patient) from the diagnostic center Results table.
                <br>
            </p>

            <!--- Options was here --->

            <div class="art-content-layout layout-item-16">
                <div class="art-content-layout-row responsive-layout-row-1">
                    <div class="art-layout-cell layout-item-18 close-bottom" style="width: 100%">
                        <p style="text-align: center; margin: 0">
                            <button type="button" class="art-button" id="result-continue-btn">Continue</button>
                            <button type="button" class="art-button result-cancel-btn">Cancel</button>
                        </p>
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>

@stop