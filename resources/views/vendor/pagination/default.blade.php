<div class="pagination-wrapper">
    <div class="float-left pagination-detail">
        <span class="pagination-info">
            Showing
            {{ (($paginator->currentPage() - 1) * $paginator->perPage()) + 1 }}
            to
            @if ($paginator->currentPage() * $paginator->perPage() > $paginator->total())
                {{ $paginator->total() }}
            @else
                {{ $paginator->currentPage() * $paginator->perPage() }}
            @endif
            of
            {{ $paginator->total() }}
            rows
        </span>
        <span class="page-list">
            <span class="btn-group dropdown dropup">
                <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
                    <span class="page-size">
                        {{ $paginator->perPage() === $paginator->total() ? 'All' : $paginator->perPage() }}
                    </span>
                    <span class="caret"></span>
                </button>
                <div class="dropdown-menu">
                    @foreach($per_page_list as $per_page)
                        @if ($per_page === 'All')
                            <a class="dropdown-item {{ $paginator->perPage() === $paginator->total() ? 'active' : '' }}" ref="#">All</a>
                        @else
                            <a class="dropdown-item {{ $per_page == $paginator->perPage() ? 'active' : '' }}" ref="#">{{ $per_page }}</a>
                        @endif
                    @endforeach

                </div>
            </span>
            rows per page
        </span>
    </div>

    @if ($paginator->hasPages())
    <div class="float-right">
        <ol class="pagination">
            @php
                $totalPages = $paginator->lastPage();
                $pageNumber = $paginator->currentPage();
                $paginationPagesBySide = 1;
                $paginationSuccessivelySize = 5;
                $paginationUseIntermediate = false;

            @endphp

                @if ($paginator->onFirstPage())
                    <li class="page-item disabled">
                        <a class="page-link">‹</a>
                    </li>
                @else
                    <li class="page-item">
                        <a class="page-link page-link-prev" href="{{ $paginator->previousPageUrl() }}" rel="prev" data-page="{{ $paginator->currentPage() - 1 }}">‹</a>
                    </li>
                @endif

                @if ($totalPages < $paginationSuccessivelySize)
                    @php $from = 1 @endphp
                    @php $to = $totalPages @endphp
                @else
                    @php $from = $pageNumber - $paginationPagesBySide @endphp
                    @php $to = $from + ($paginationPagesBySide * 2) @endphp
                @endif

                @if ($pageNumber < ($paginationSuccessivelySize - 1))
                    @php $to = $paginationSuccessivelySize @endphp
                @endif

                @if ($paginationSuccessivelySize > $totalPages - $from)
                    @php $from = $from - ($paginationSuccessivelySize - ($totalPages - $from)) + 1 @endphp
                @endif

                @if ($from < 1)
                    @php $from = 1 @endphp
                @endif

                @if ($to > $totalPages)
                    @php $to = $totalPages @endphp
                @endif

                @php
                    $middleSize = round($paginationPagesBySide / 2);
                @endphp

                @if ($from > 1)
                    @php $max = $paginationPagesBySide @endphp
                    @if ($max >= $from)
                            @php $max = $from - 1 @endphp
                    @endif
                    @for ($i = 1; $i <= $max; $i++)
                        <li class="page-item{{ $i === $pageNumber ? ' active' : '' }}">
                            <a class="page-link page-link-number" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                        </li>
                    @endfor
                    @if (($from - 1) === $max + 1)
                        @php $i = $from - 1 @endphp
                        <li class="page-item{{ $i === $pageNumber ? ' active' : '' }}">
                            <a class="page-link page-link-number" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                        </li>
                    @else
                        @if (($from - 1) > $max)
                            @if (($from - $paginationPagesBySide * 2) > $paginationPagesBySide && $paginationUseIntermediate )
                                @php $i = round((($from - $middleSize) / 2) + $middleSize) @endphp
                                <li class="page-item{{ $i === $pageNumber ? ' active' : '' }}">
                                    <a class="page-link page-link-number" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                                </li>
                            @else
                                <li class="page-item disabled">
                                    <a class="page-link">...</a>
                                </li>
                            @endif
                        @endif
                    @endif
                @endif

                @for ($i = $from; $i <= $to; $i++)
                    <li class="page-item{{ $i === $pageNumber ? ' active' : '' }}">
                        <a class="page-link page-link-number" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                    </li>
                @endfor

                @if ($totalPages > $to)
                    @php $min = $totalPages - ($paginationPagesBySide - 1) @endphp
                    @if ($to >= $min)
                        @php $min = $to + 1 @endphp
                    @endif
                    @if (($to + 1) === $min - 1)
                        @php $i = $to + 1 @endphp
                        <li class="page-item{{ $i === $pageNumber ? ' active' : '' }}">
                            <a class="page-link page-link-number" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                        </li>
                    @else
                        @if ($min > ($to + 1))
                            @if (($totalPages - $to) > $paginationPagesBySide * 2 && $paginationUseIntermediate)
                                @php $i = round((($totalPages - $middleSize - $to) / 2) + $to) @endphp
                                <li class="page-item{{ $i === $pageNumber ? ' active' : '' }}">
                                    <a class="page-link page-link-number" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                                </li>
                            @else
                                <li class="page-item disabled">
                                    <a class="page-link">...</a>
                                </li>
                            @endif
                        @endif
                    @endif

                    @for ($i = $min; $i <= $totalPages; $i++)
                        <li class="page-item hidden-xs">
                            <a class="page-link page-link-number" href="{{ $paginator->url($paginator->lastPage()) }}">
                                {{ $paginator->lastPage() }}
                            </a>
                        </li>
                    @endfor
                @endif

            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a class="page-link page-link-next" href="{{ $paginator->nextPageUrl() }}" rel="next" data-page="{{ $paginator->currentPage() + 1 }}">›</a>
                </li>
            @else
                <li class="page-item disabled">
                    <a class="page-link">›</a>
                </li>
            @endif
        </ol>
    </div>
    @endif
</div>

